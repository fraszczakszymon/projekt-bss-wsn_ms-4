#include "compiler_defs.h"
#include "C8051F020_defs.h"

#include "dev.h"
#include "uart.h"
#include "wsn.h"

void main (void) 
{
    int deviceId = -1;
    Init_Device();
    P5 = 0x0F;

    do {
        Wait_for_button_release();
        Wait_for_button_pressed();
        if (B1_ON()) {
            deviceId = P4; // Adres slave, odczytany z mikroprzelacznika P4
            LED1_BLINK();
        }
        if (B2_ON()) {
            deviceId = 0;
            LED2_BLINK();
        }
    } while (deviceId == -1);
    
    if (deviceId > 0) {
        printf("Wybrany wezel w trybie slave, adres: %x\n", deviceId);
        runSlaveMode(deviceId);
    } else if (deviceId == 0) {
        printf("Wybrany wezel w trybie master\n");
        runMasterMode();
    }
}