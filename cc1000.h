// Funkcje komunikacji z CC1000 dla UNI DC (C8051F020)
//***  27.07.2011 Z.Kubiak
// Obs�uga CC1000 bez SPI

#ifndef __CC1000_H
#define __CC1000_H

#define CC1000_DEBUG
//#define PASMO_868
#define PASMO_433

// Konfiguracja dla modu�u UNI_RF
// sygna�y modu�u CC1000PP - po�. z UNI DC F020
//*********************************************
//            uC              CC1000PP
// we//OD   P0.2 <---------- DCLK   
// OD//PP   P0.3 <---------> DIO
// wy//PP   P0.5 ----------> PCLK	
// OD/PP    P0.4 <---------> PDATA  
// wy//PP   P0.7 ----------> PALE
// we//OD   P1.2 <---------- RSSI       // na razie nie do��czony
//*********************************************

volatile SBIT (DCLK, 0x80, 2);  // we (OD)
volatile SBIT (DIO, 0x80, 3);	// we/wy (OD/PP)
volatile SBIT (PCLK, 0x80, 5);  // wy (PP)
volatile SBIT (PDATA, 0x80, 4); // we/wy (OD/PP)
volatile SBIT (PALE, 0x80, 7);  // wy (PP)		
//volatile SBIT (RSSI, 0x90, 2);  // we (OD) - ADC //nie do��czony	

// Variables
extern volatile U8 DATA;
extern volatile U32 PREAMBULE;

// Current and pll values
#define CC1000_TX_CURRENT 0xF3  // 0xCA (pr�d VCO 2,35 mA)
#define CC1000_RX_CURRENT 0x8C  //  "
#define CC1000_TX_PLL 0x58      // REFDIV=11
#define CC1000_RX_PLL 0x58      // REFDIV=11
#define CC1000_PA_VALUE 0xF0    // max moc wyj. 5dBm 


// Preambule and sync configuration
#define CC1000_PREAMBULE 		0x55
#define CC1000_PREAMBULE_LENGTH 0x20    // min 0x10
#define CC1000_SYNC_BYTE		0x71
#define SYNC_SEQ                0x71555555 
// Lock
#define CC1000_LOCK_NOK         0x00
#define CC1000_LOCK_OK          0x01
#define CC1000_LOCK_RECAL_OK    0x02

#define CC1000_CAL_TIMEOUT      0x7FFE
#define CC1000_LOCK_TIMEOUT     0x7FFE  //0xFFFF

// Register addresses 
#define CC1000_MAIN             0x00
#define CC1000_FREQ_2A          0x01
#define CC1000_FREQ_1A          0x02
#define CC1000_FREQ_0A          0x03
#define CC1000_FREQ_2B          0x04
#define CC1000_FREQ_1B          0x05
#define CC1000_FREQ_0B          0x06
#define CC1000_FSEP1            0x07
#define CC1000_FSEP0            0x08
#define CC1000_CURRENT          0x09
#define CC1000_FRONT_END        0x0A
#define CC1000_PA_POW           0x0B
#define CC1000_PLL              0x0C
#define CC1000_LOCK             0x0D
#define CC1000_CAL              0x0E
#define CC1000_MODEM2           0x0F
#define CC1000_MODEM1           0x10
#define CC1000_MODEM0           0x11
#define CC1000_MATCH            0x12
#define CC1000_FSCTRL           0x13
#define CC1000_FSHAPE7          0x14
#define CC1000_FSHAPE6          0x15
#define CC1000_FSHAPE5          0x16
#define CC1000_FSHAPE4          0x17
#define CC1000_FSHAPE3          0x18
#define CC1000_FSHAPE2          0x19
#define CC1000_FSHAPE1          0x1A
#define CC1000_FSDELAY          0x1B
#define CC1000_PRESCALER        0x1C
#define CC1000_TEST6            0x40
#define CC1000_TEST5            0x41
#define CC1000_TEST4            0x42
#define CC1000_TEST3            0x43
#define CC1000_TEST2            0x44
#define CC1000_TEST1            0x45
#define CC1000_TEST0            0x46

// Functions declarations
U8 BitReceive(void);
void BitSend(U8);

void  CC1000_WriteToRegister(U8, U8);
U8  CC1000_ReadFromRegister(U8);
void  CC1000_Reset(void);
void  CC1000_ConfigureRegisters(void);
void  CC1000_ConfigureRegistersRx(void);
void  CC1000_ConfigureRegistersTx(void);
void  CC1000_DisplayAllRegisters(void);
void  CC1000_SetupPD(void);
void  CC1000_ResetFreqSynth(void);
void  CC1000_WakeUpToRX(U8);
void  CC1000_WakeUpToTX(U8);
U8  CC1000_SetupRX(U8);
U8  CC1000_SetupTX(U8);

U8  CC1000_Calibrate(void);
void  CC1000_Initialize(void);
void  CC1000_SendByte(U8);
U8  CC1000_ReceiveByte(void);
U8  CC1000_Sync(void);
//U8 CC1000_ReceiveFrame(struct TransceiverFrame*);
//U8 CC1000_SendTestFrame(void);
//U8 CC1000_GetRssi(void);
//U8 RFSendPacket(U8*, U8); 

//#########################################################################



//========================================================================/
//  Cz�� funkcji do obs�ugi CC1000 przygotowano na podstawie            */
//  AN009 (SWRA82), AN015 (SWRA76) oraz CC1000_Data_Sheet_2_3 (SWRS048)  */
//  (http://focus.ti.com/lit/an/swra082/swra082.pdf)                     */
//  (http://focus.ti.com/lit/an/swra076/swra076.pdf)                     */                                                                        */
//  Dane wysy�ane s� od najm�odszej pozycji                              */
//  autor Zygmunt Kubiak   24.07.2011                                    */                                                                      */
//========================================================================/


static U8 Transceiver_State = 0;
static U8 Transceiver_Timeout = 10;
static U8 NrSekw = 0;

volatile U32 PREAMBULE;

//=======================================================================
//           Funkcje obs�ugi interfejsu konfiguracji CC1000 
//                      Sygna�y PDATA, PCLK i PALE
//=======================================================================

//  Zapis danych do rejestru 
//  address - adres rejestru; value - warto�� do zapisu 
void CC1000_WriteToRegister(U8 address, U8 value)   
{
    U8 BitCnt, i;  

    P0MDOUT |= 0x10;                // Wyj�cie P0.4 (PDATA) jako push-pull
	PCLK = 1;
	PALE = 1;
	address = (address<<1)|0x01;    // W lewo bo adr. 7-bit; (LSB = 1 to zapis)
	PALE = 0;                               // Przygotowanie zapisu adresu
	for (BitCnt = 0; BitCnt < 8; BitCnt++)	// Wysy�anie adresu po jednym bicie (szeregowo)
	{
		PCLK = 1;
		if (address & 0x80)         // Test MSB 
			PDATA = 1;
		else 
			PDATA = 0;
		address = address<<1;		// Nast�pny bit  
		PCLK = 0;                   // Zapis bitu
        for (i=0; i<0x0F; i++);     // Op�nienie
	}
    PCLK = 1;
    for (i=0; i<0x0F; i++);         // Op�nienie
	PALE = 1;                               // Przygotowanie zapisu danej
	for (BitCnt = 0; BitCnt < 8; BitCnt++)	// wysy�anie danych po jednym bicie
	{
		PCLK = 1;
		if (value & 0x80)           // Test MSB 
            PDATA = 1;
		else 
			PDATA = 0;
		value = value<<1;		    // Nast�pny bit
		PCLK = 0;
        for (i=0; i<0x0F; i++);     // Op�nienie
	}
	PCLK  = 1;
	PDATA = 1;  
}

// Odczyt danych z rejestru 
// Parametry:  adres - adres rejestru; Value - zwracana zawarto�� rejestru
// Uwaga: przy odczycie PDATA jest wej�ciem  
U8 CC1000_ReadFromRegister(U8 address) 
{
    U8 Value = 0;
    U8 BitCnt, i; 

    P0MDOUT |= 0x10;            // Wyj�cie P0.4 (PDATA) jako push-pull
	PCLK = 1;
	PALE = 1;
	address = (address<<1)&0xFE;	        // Adres 7-bit; (LSB = 0 - odczyt);
	PALE = 0;
	for (BitCnt = 0; BitCnt < 8; BitCnt++)	// Wysy�anie adresu po jednym bicie (szeregowo)
	{
		PCLK = 1;
		if (address & 0x80)
			PDATA = 1; 
		else 
			PDATA = 0; 
		address = address<<1;   // Nast�pny bit
		PCLK = 0;
        for (i=0; i<0x0F; i++); // Op�nienie
	}
	PCLK = 1;
    for (i=0; i<0x0F; i++);     // Op�nienie
	PALE  = 1;
    P0MDOUT &= 0xEF;            // Port P0.4 (PDATA) jako open-drain (wej�cie)
	PDATA = 1;                  // Wa�ne aby PDATA by�o wej�ciem
	for (BitCnt = 0; BitCnt < 8; BitCnt++)	
    {
		PCLK = 0;
		Value = Value<<1;
		if (PDATA) 
			Value |= 0x01;  
		else 
			Value &= 0xFE;  
		PCLK = 1;
        for (i=0; i<0x0F; i++); // Op�nienie
	}
	PDATA = 1;
	return Value;
}

//=======================================================================
//              Funkcje obs�ugi interfejsu danych CC1000
//                       Sygna�y DIO i DCLK 
// Uwaga w C8051F020 nie ma mo�liwo�ci obs�ugi DCLK przy pomocy przerwa�
//          INT0 i INT1 mog� reagowa� tylko na zbocze opadaj�ce
//=======================================================================

// Odczyt jednego bitu danych z DIO
U8 BitReceive(void) // Odczyt przy zboczu narastaj�cym 
{
    while(DCLK);    // Czekaj a� DCLK zmieni stan z "1" -> "0"
    while(!DCLK);   // Czekaj a� DCLK zmieni stan z "0" -> "1" 
//putchar(DIO+0x30);
    return DIO;     // Odczyt przy zboczu narastaj�cym
}

// Odczyt bajta danych z CC1000
U8 CC1000_ReceiveByte(void) 
{
    U8 i, DATA = 0;
    for(i=0; i<8; i++) {
        DATA >>= 1;
        if(!BitReceive()) DATA |= 0x80;     // Odbi�r bit�w z negacj�
    }
//	printf("0x%x ", DATA);
    return DATA;
}

// Wys�anie jednego bitu danych na DIO //ok
void BitSend(U8 bit0)   // Zapis przy zboczu opadaj�cym 
{
    while(!DCLK);   // Czekaj a� DCLK zmieni stan z "0" -> "1"
    while(DCLK);    // Czekaj a� DCLK zmieni stan z "1" -> "0"
    if(bit0) DIO = 1; else DIO = 0; // Zapis przy zboczu opadaj�cym
}

// Wys�anie bajta danych na CC1000 //ok
void CC1000_SendByte(U8 byte) 
{
    U8 i;
    for(i=0; i<8; i++) {
        BitSend(byte & 0x01);
        byte >>= 1;
    }
}


//=======================================================================
//=======================================================================
//                  Funkcje do konfiguracji CC1000 
//=======================================================================

//  Zerowanie CC1000, wg AN009
void CC1000_Reset(void)
{
	U8 MainValue;

	MainValue = CC1000_ReadFromRegister(CC1000_MAIN);
	CC1000_WriteToRegister(CC1000_MAIN,MainValue & 0xFE);   
	CC1000_WriteToRegister(CC1000_MAIN,MainValue | 0x01);  
}

//  Kalibracja CC1000;  wg AN009 
//  Funkcja zwraca 0 gdy niepowodzenie; r�na od 0 - sukces
//  Modyfikacja wg �r�d�a
U8 CC1000_Calibrate(void)   
{  
	U16 TimeOutCounter;
 printf("\nKalibracja...");
    // Wy��czenie PA w celu unikni�cia wzbudzenia podczas kalibracji do trybu TX
  	CC1000_WriteToRegister(CC1000_PA_POW,0x00);
	CC1000_WriteToRegister(CC1000_CAL,0xA6);    // Start kalibracji                                    
	Delay_x100us(200);
    // Czekanie na pe�n� kalibracj�
  	for (TimeOutCounter=CC1000_CAL_TIMEOUT; ((CC1000_ReadFromRegister(CC1000_CAL)&0x08)==0)&&(TimeOutCounter>0); TimeOutCounter--);        
  	// Czekanie  zamkni�cie p�tli fazowej
  	for (TimeOutCounter=CC1000_LOCK_TIMEOUT; ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0)&&(TimeOutCounter>0); TimeOutCounter--);
  	CC1000_WriteToRegister(CC1000_CAL, 0x26);     // Zako�czenie kalibracji
  	CC1000_WriteToRegister(CC1000_PA_POW, CC1000_PA_VALUE);    // W��czenie PA
  	return ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01) == 1);
}

// Prze��cza CC1000 do trybu RX (z TX);  wg AN009
// Przy przej�ciu CC1000 do RX z trybu PD, u�y� najpierw C1000_WakeupToRX
// Parametry: WE: RXCurrent; char lock_status
U8 CC1000_SetupRX(U8 RXCurrent) 
{
    U16 i;
    U8 lock_status; 

    CC1000_WriteToRegister(CC1000_MAIN,0x11);           // Prze��cz. do RX, cz�stotl. rej. A
    CC1000_WriteToRegister(CC1000_PLL, CC1000_RX_PLL); 
    CC1000_WriteToRegister(CC1000_CURRENT, RXCurrent); 
	Delay_x100us(3);                                    // Oczekiwanie 300us 
    // Oczekiwanie na stabilizacj� PLL
    for (i=CC1000_LOCK_TIMEOUT; ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0)&&(i>0); i--);
    if ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0x01) 
        lock_status = CC1000_LOCK_OK;  // PLL zamkni�ta
    else    // Je�li PLL nie zamkni�ta
 	{
        if (CC1000_Calibrate()) // Je�li kalibracja z sukcesem
            lock_status = CC1000_LOCK_RECAL_OK;  
        else   // Je�li kalibracja niepoprawna
		{ // Reset syntezatora cz�stotliwo�ci (ref.: Errata Note 01)
            CC1000_ResetFreqSynth();
            lock_status = CC1000_LOCK_NOK; 
        }
    }
    return (lock_status);  // zwrot statusu syntezatora
}

// Prze��cza CC1000 do trybu TX (z RX); wg AN009
// Przy przej�ciu CC1000 do TX z trybu PD, u�y� najpierw C1000_WakeupToTX
// Parametry WE: char    TXCurrent; WY: char    lock_status 
U8 CC1000_SetupTX(U8 TXCurrent) 
{
    U16 i;      
    U8 lock_status; 

    CC1000_WriteToRegister(CC1000_PA_POW,0x00); // Wy��czenie PA 
    CC1000_WriteToRegister(CC1000_MAIN,0xE1);   // Prze�. do TX, do cz�stotl. rej. B
    CC1000_WriteToRegister(CC1000_PLL, CC1000_TX_PLL);
    CC1000_WriteToRegister(CC1000_CURRENT, TXCurrent);
    Delay_x100us(3);                            // Czekaj 250us
    // Oczekiwanie na stabilizacj� PLL
    for (i=CC1000_LOCK_TIMEOUT; ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0)&&(i>0); i--);
    if ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0x01)
        lock_status = CC1000_LOCK_OK; // PLL stabilna
    else  // Je�li brak stabilno�ci PLL
	{
        if (CC1000_Calibrate()) // Je�li kalibracja poprawna
            lock_status = CC1000_LOCK_RECAL_OK; 
        else   // Je�li kalibracja niepoprawna
		{  // Reset syntezatora cz�stotliwo�ci (ref.: Errata Note 01)
            CC1000_ResetFreqSynth();
            lock_status = CC1000_LOCK_NOK;  
        }
    }
    // Zwi�kszenie mocy wyj�ciowej
    CC1000_WriteToRegister(CC1000_PA_POW, CC1000_PA_VALUE); // W��czenie PA
    return (lock_status);   // zwrot statusu syntezatora
}

//  Prze��cz CC1000 do trybu u�pienia (power down);  wg AN009
//  Do wybudzenia nale�y u�y� CC1000_WakeUpToRX(TX) przed CC1000_SetupRX(TX)
void CC1000_SetupPD(void)
{
    CC1000_WriteToRegister(CC1000_MAIN,0x3F);    // CC1000 do power-down
    CC1000_WriteToRegister(CC1000_PA_POW,0x00);  // Wy��czenie PA dla ograniczenia pr�du
}


//  Wybudzenie CC1000 do trybu RX; wg AN009
//  Zako�czenie procedury wymaga nast�pnie wywo�ania CC1000_SetupRX
//  Parametr: RXCurrent - pr�d odbiornika
void CC1000_WakeUpToRX(U8 RXCurrent)  
{
    CC1000_WriteToRegister(CC1000_MAIN,0x3B);    // W��czenie oscylatora xtal
    CC1000_WriteToRegister(CC1000_CURRENT,RXCurrent);   
    CC1000_WriteToRegister(CC1000_PLL, CC1000_RX_PLL); 
    // Oczekiwanie na stabilizacj� drga� rezonatora kwarcowego
    Delay_x100us(50);   // typowo 2-5ms
    CC1000_WriteToRegister(CC1000_MAIN,0x39);    
    Delay_x100us(3);     // Oczekiwanie 250us 
    CC1000_WriteToRegister(CC1000_MAIN,0x31);    // W��czenie syntezatora cz�stotliwo�ci
}

//  Wybudzenie CC1000 trybu PD do trybu TX; wg AN009 
//  Wywo�anie CC1000_SetupTX po tej procedurze ko�czy operacj�
//  Parametr: TXCurrent - pr�d nadajnika
void CC1000_WakeUpToTX(U8 TXCurrent)  
{
    CC1000_WriteToRegister(CC1000_MAIN,0xFB);  // W��czenie oscylatora xtal
    CC1000_WriteToRegister(CC1000_CURRENT,TXCurrent);
    CC1000_WriteToRegister(CC1000_PLL, CC1000_TX_PLL);
    // Oczekiwanie na stabilizacj� drga� rezonatora kwarcowego
    Delay_x100us(50);    // typowo 2-5ms 
    CC1000_WriteToRegister(CC1000_MAIN,0xF9); 
    Delay_x100us(3);     // Oczekiwanie 250us
    CC1000_WriteToRegister(CC1000_PA_POW, CC1000_PA_VALUE);    // W��czenie PA
    CC1000_WriteToRegister(CC1000_MAIN,0xF1);    // W��czenie syntezatora cz�stotliwo�ci  
}

// 	Konfiguracja trybu RX
void CC1000_ConfigureRegistersRx(void)
{
	CC1000_ConfigureRegisters();
	CC1000_WriteToRegister(CC1000_CURRENT, CC1000_RX_CURRENT);
	CC1000_WriteToRegister(CC1000_PLL, CC1000_RX_PLL);
}

// 	Konfiguracja trybu TX
void CC1000_ConfigureRegistersTx(void)
{
	CC1000_ConfigureRegisters();
	CC1000_WriteToRegister(CC1000_CURRENT, CC1000_TX_CURRENT);
	CC1000_WriteToRegister(CC1000_PLL, CC1000_TX_PLL);
}

//  Reset syntezatora cz�stotliwo�ci CC1000; wg AN009
void CC1000_ResetFreqSynth(void)
{
  	S8 modem1_value;
  	modem1_value = CC1000_ReadFromRegister(CC1000_MODEM1)&~0x01;
  	CC1000_WriteToRegister(CC1000_MODEM1,modem1_value);
  	CC1000_WriteToRegister(CC1000_MODEM1,modem1_value|0x01);
}

//=======================================================================
//	Inicjalizacja CC1000
//=======================================================================
void CC1000_Initialize(void)
{
	U8 cal; 

    P0MDOUT |= 0x10;    // PDATA (P0.4 jako push-pull)
    PCLK = 1;           // Przy braku transmisji parametr�w: PCLK = 1
    CC1000_SetupPD();
    CC1000_Reset();	
    Delay_x100us(20);	// czekaj 2 ms
	CC1000_WriteToRegister(CC1000_MAIN,0x11);	// Kalibracja trybu RX
	CC1000_ConfigureRegistersRx();
	cal = CC1000_Calibrate();
	if (cal == 0)
	{
		#ifdef CC1000_DEBUG 
            printf("B��d kalibracji trybu RX\n");
		#endif
	}
	else
	{
		#ifdef CC1000_DEBUG 
            printf("Poprawna kalibracja trybu RX\n"); 
		#endif
	}
	
	CC1000_WriteToRegister(CC1000_MAIN,0xA1);	// Kalibracja trybu TX
	CC1000_ConfigureRegistersTx();
	cal = CC1000_Calibrate();
	if (cal == 0)
	{
		#ifdef CC1000_DEBUG 
		            printf("B��d kalibracji trybu TX\n");            
		#endif
	}
	else
	{
		#ifdef CC1000_DEBUG 
            printf("Poprawna kalibracja trybu TX\n"); 
		#endif
	}
	CC1000_SetupPD();	
}

// 	Konfiguracja rejestr�w
void CC1000_ConfigureRegisters(void)
{
//  MODEM0 (Manchester): 0x07 - 0,3kBaud; 0x17 - 0,6kBaud; 0x27 - 1,2kBaud;	
//  MODEM0 (Manchester): 0x37 - 2,4kBaud; 0x47 - 4,8kBaud; 0x57 - 9,6kBaud;	
//  MODEM0 (Manchester): 0x55 - 19,2kBaud; 0x54 - 38,4kBaud; 	
//  MODEM0 (NRZ): 0x03 - 0,6kBaud; 0x13 - 1,2kBaud; 0x23 - 2,4kBaud;	
//  MODEM0 (NRZ): 0x33 - 4,8kBaud; 0x43 - 9,6kBaud; 0x53 - 19,2kBaud;	
//  MODEM0 (NRZ): 0x51 - 38,4kBaud; 0x50 - 76,8kBaud; 	
//=======================================================================
#ifdef PASMO_868
//  wg AN015 dla 869,850 MHz, +5 dBm    // 586000 585B43 01AB REFDIF=6
//  wg AN011 dla 868,500 MHz,           // A1E000 A1D74F 030E REFDIV=11
//  wg AN011 dla 869,021 MHz,           // 848000 8478E3 0280 REFDIV=9
//  wg AN011 dla 869.170 MHz,           // A20000 A1F74F 030E REFDIV=11
//  wg AN011 dla 869.314 MHz,           // CE4000 CE34F0 03E3 REFDIV=14
//  wg AN011 dla 869.614 MHz,           // CE4000 CE472B 03E3 REFDIV=14 
	CC1000_WriteToRegister(CC1000_FREQ_2A,	0xA1);
	CC1000_WriteToRegister(CC1000_FREQ_1A,	0xE0);
	CC1000_WriteToRegister(CC1000_FREQ_0A,	0x00);
	CC1000_WriteToRegister(CC1000_FREQ_2B,	0xA1);
	CC1000_WriteToRegister(CC1000_FREQ_1B,	0xD7);
	CC1000_WriteToRegister(CC1000_FREQ_0B,	0x4F);
	CC1000_WriteToRegister(CC1000_FSEP1,  	0x03);
	CC1000_WriteToRegister(CC1000_FSEP0,  	0x0E);
	CC1000_WriteToRegister(CC1000_FRONT_END,0x32);
	CC1000_WriteToRegister(CC1000_PA_POW, 	0xFF);       
	CC1000_WriteToRegister(CC1000_PLL,   	0x58);  // REFDIV=11
	CC1000_WriteToRegister(CC1000_LOCK,   	0x10);
	CC1000_WriteToRegister(CC1000_CAL,    	0x26);
	CC1000_WriteToRegister(CC1000_MODEM2, 	0x90);
	CC1000_WriteToRegister(CC1000_MODEM1, 	0x69);
	CC1000_WriteToRegister(CC1000_MODEM0, 	0x37);  // 2,4 (Manchester)
	CC1000_WriteToRegister(CC1000_MATCH,  	0x10);
	CC1000_WriteToRegister(CC1000_FSCTRL, 	0x01);
#endif

#ifdef PASMO_433
// 422000 42049C 0280
//  wg AN011 dla 433.371 MHz,           // 496000 495819 02C7 REFDIV=10
//  wg AN011 dla 433.505 MHz,           // 50C000 50B74F 030E REFDIV=11
//  wg AN011 dla 433.711 MHz,           // 5F8000 5F75BB 039C REFDIV=13
//  wg AN011 dla 434.026 MHz,           // 422000 4218E4 0280 REFDIV=9
//  wg AN011 dla 434.278 MHz,           // 5FA000 5F95BB 039C REFDIV=13 
	CC1000_WriteToRegister(CC1000_MAIN,	0x11);      //0xE1
	CC1000_WriteToRegister(CC1000_FREQ_2A,	0x50);
	CC1000_WriteToRegister(CC1000_FREQ_1A,	0xC0);
	CC1000_WriteToRegister(CC1000_FREQ_0A,	0x00);
	CC1000_WriteToRegister(CC1000_FREQ_2B,	0x50);
	CC1000_WriteToRegister(CC1000_FREQ_1B,	0xB7);
	CC1000_WriteToRegister(CC1000_FREQ_0B,	0x4F);
	CC1000_WriteToRegister(CC1000_FSEP1,  	0x03);
	CC1000_WriteToRegister(CC1000_FSEP0,  	0x0E);
	CC1000_WriteToRegister(CC1000_FRONT_END,0x12);
	CC1000_WriteToRegister(CC1000_PA_POW, 	0x70);       
	CC1000_WriteToRegister(CC1000_PLL,   	0x58);  // REFDIV=11
	CC1000_WriteToRegister(CC1000_LOCK,   	0x10);
	CC1000_WriteToRegister(CC1000_CAL,    	0x26);
	CC1000_WriteToRegister(CC1000_MODEM2, 	0xB7);  // 0x90
	CC1000_WriteToRegister(CC1000_MODEM1, 	0x6F);  // 0x69 
	CC1000_WriteToRegister(CC1000_MODEM0, 	0x37);  // 2,4 (Manchester)
	CC1000_WriteToRegister(CC1000_MATCH,  	0x70);
	CC1000_WriteToRegister(CC1000_FSCTRL, 	0x01);

#endif
}


// Wy�wietlenie zawarto�ci wszystkich rejestr�w
// W komentarzu warto�ci domy�lne po Reset
void CC1000_DisplayAllRegisters(void)
{
	printf("MAIN: 0x%x \n", CC1000_ReadFromRegister(CC1000_MAIN));
	printf("FREQ_2A: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_2A));    // 0x75
	printf("FREQ_1A: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_1A));    // 0xA0    
	printf("FREQ_0A: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_0A));    // 0xCB
	printf("FREQ_2B: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_2B));    // 0x75
	printf("FREQ_1B: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_1B));    // 0xA5
	printf("FREQ_0B: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_0B));    // 0x4E
	printf("FSEP1: 0x%x \n", CC1000_ReadFromRegister(CC1000_FSEP1));        // 0x00
	printf("FSEP0: 0x%x \n", CC1000_ReadFromRegister(CC1000_FSEP0));        // 0x59 
	printf("CURRENT: 0x%x \n", CC1000_ReadFromRegister(CC1000_CURRENT));    // 0xCA
	printf("FRONT_END: 0x%x \n", CC1000_ReadFromRegister(CC1000_FRONT_END));// 0x08 
	printf("PA_POW: 0x%x \n", CC1000_ReadFromRegister(CC1000_PA_POW));      // 0x0F
	printf("PLL: 0x%x \n", CC1000_ReadFromRegister(CC1000_PLL));            // 0x10 
	printf("LOCK: 0x%x \n", CC1000_ReadFromRegister(CC1000_LOCK));          // 0x00 
	printf("CAL: 0x%x \n", CC1000_ReadFromRegister(CC1000_CAL));            // 0x05
	printf("MODEM2: 0x%x \n", CC1000_ReadFromRegister(CC1000_MODEM2));      // 0x96
	printf("MODEM1: 0x%x \n", CC1000_ReadFromRegister(CC1000_MODEM1));      // 0x67
	printf("MODEM0: 0x%x \n", CC1000_ReadFromRegister(CC1000_MODEM0));      // 0x24
	printf("MATCH: 0x%x \n", CC1000_ReadFromRegister(CC1000_MATCH));        // 0x00
	printf("FSCTRL: 0x%x \n", CC1000_ReadFromRegister(CC1000_FSCTRL));      // 0x01
	printf("PRESCALER: 0x%x \n", CC1000_ReadFromRegister(CC1000_PRESCALER));// 0x00 
	printf("TEST6: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST6));        // 0x10
	printf("TEST5: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST5));        // 0x08
	printf("TEST4: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST4));        // 0x25
	printf("TEST3: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST3));        // 0x04
	printf("TEST2: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST2));        // 0x00
	printf("TEST1: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST1));        // 0x00 
	printf("TEST0: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST0));        // 0x00
}


//=======================================================================
//              Funkcje przygotowania transmisji radiowej
//=======================================================================

// Synchronizacja bajtowa CC1000
U8 CC1000_Sync(void)
{
    PREAMBULE >>= 1;
    if (!BitReceive())          // Odbi�r bit�w z negacj�
        PREAMBULE |= 0x80000000;
    else
        PREAMBULE &= 0x7FFFFFFF; 
    if (PREAMBULE == SYNC_SEQ)
        return 1;               // Jest synchronizacja 
    else return 0;              // Brak synchronizacji
}        

//=======================================================================================
// !!! dla ReceiveFrame lini� DIO ustawi� jako wej�cie, tzn. P0MDOUT &= 0xF7; i DI0 = 1;

/*
// testowa procedura
U8 CC1000_SendTestFrame(void)
{
    U8 i;

    CC1000_WakeUpToTX(CC1000_TX_CURRENT);   // wybudzenie do TX
    CC1000_SetupTX(CC1000_TX_CURRENT);
	P0MDOUT |= 0x08;	                    // wyj�cie P0.3 (DIO) push-pull
printf("\nwybudzenie do TX  ok");
    for (i = 0; i < CC1000_PREAMBULE_LENGTH; ++i) //wysy�anie preambu�y 
        CC1000_SendByte(CC1000_PREAMBULE);
    CC1000_SendByte(CC1000_SYNC_BYTE);      // bajt synchronizacyjny (SFD)

    CC1000_SendByte(0x12); 
    CC1000_SendByte(0x34); 
    CC1000_SendByte(0x56);  
    CC1000_SendByte(0x78);  
    CC1000_SendByte(0x90);  
    CC1000_SendByte(0x11);  
    CC1000_SendByte(0x22);  
    CC1000_SendByte(0x33);  
    CC1000_SendByte(0x44);  
    CC1000_SendByte(0x55);  
    CC1000_SendByte(0x66);  
    CC1000_SendByte(0x77);  
    CC1000_SendByte(0x88);  
    CC1000_SendByte(0x99);  
    CC1000_SendByte(0xFF); 

	CC1000_SetupPD();	

return 0;
}
*/


#endif



