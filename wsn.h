#ifndef __wsn
#define __wsn

#include "compiler_defs.h"
#include "C8051F020_defs.h"
#include "dev.h"
#include "uart.h"
#include "crc8.h"
#include "cc1000.h"

#define NI 0xB4

U8 frame[19];
U8 crc = 0x0000;
int slaveAddress = 0x00;
int slaves[] = {0, 0, 0};
int i, mode = 0;
int acknowledge = 0;

void clearFrame();
void sendFrame(U8);
void sendData(U8*);
U8 waitForFrame(int);

void runSlaveMode(int);
void sendDataWithoutSecurityToMaster();
void sendDataWithCRC8ToMaster();

void runMasterMode();
int getMode();
void sendTestFrame();
void sendDataWithoutSecurity();
void sendDataWithCRC8();

void clearFrame()
{
    for (i=0; i<19; i++)
        frame[i] = 0x00;
}

void sendFrame(U8 frameType)
{
    CC1000_WakeUpToTX(CC1000_TX_CURRENT);
    CC1000_SetupTX(CC1000_TX_CURRENT);
    P0MDOUT |= 0x08;
    for (i = 0; i < CC1000_PREAMBULE_LENGTH; ++i)
        CC1000_SendByte(CC1000_PREAMBULE);

    CC1000_SendByte(CC1000_SYNC_BYTE);
    CC1000_SendByte(NI);
    CC1000_SendByte(slaveAddress);
    CC1000_SendByte(frameType);

    printf("\nN=%x F=%x SI=%x ", NI, frameType, slaveAddress);
}

void sendData(U8* frame)
{
    CC1000_WakeUpToTX(CC1000_TX_CURRENT);
    CC1000_SetupTX(CC1000_TX_CURRENT);
    P0MDOUT |= 0x08;
    for (i=0; i<CC1000_PREAMBULE_LENGTH; ++i)
        CC1000_SendByte(CC1000_PREAMBULE);

    CC1000_SendByte(CC1000_SYNC_BYTE);
    for (i=0; i<19; i++)
        CC1000_SendByte(frame[i]);

    printf("\nN=%x F=%x SI=%x ", frame[0], frame[1], frame[2]);
    
    for (i=3; i<19; i++)
        printf("%x", frame[i]);
}

U8 waitForFrame(int limitAttempts)
{
    int attempt = 0;
    CC1000_WakeUpToRX(CC1000_RX_CURRENT);
    CC1000_SetupRX(CC1000_RX_CURRENT);
    P0MDOUT &= ~(0x08);
    DIO = 1;
    PREAMBULE = 0;
    while(attempt <= limitAttempts) {
        while(!CC1000_Sync());
        for (i=0; i<19; i++) {
            frame[i] = CC1000_ReceiveByte();
        }

        if (frame[0] == NI && frame[1] == slaveAddress) {
            printf("\nS=%x F=%x SI=%x SN=xx ", CC1000_SYNC_BYTE, frame[2], slaveAddress);
            for (i=3; i<19; i++)
                printf("%x", frame[i]);

            return frame[2];
        } else {
            if (limitAttempts != 0) {
                attempt++;
                Delay_x100us(1000);
            }
            continue;
        }
    }
    
    return 0x00;
}

//
//
// Slave
//
//
void runSlaveMode(int slaveId)
{
    clearFrame();
    slaveAddress = slaveId;
    CC1000_Initialize();

    switch (waitForFrame(0)) {
        case 0x01:
            sendFrame(0x02);
            break;
        case 0x83:
        case 0x86:
            sendFrame(0x0C);
            break;
        case 0x04:
            sendDataWithoutSecurityToMaster();
            break;
        case 0x07:
            sendDataWithCRC8ToMaster();
            break;
        case 0x03:
        case 0x06:
        case 0x0D:
        default:
            break;
    }

    CC1000_SetupPD();
    runSlaveMode(slaveId);
}

void sendDataWithoutSecurityToMaster()
{
    frame[0] = NI;
    frame[1] = slaveAddress;
    frame[2] = 0x05;
    frame[3] = 0x00;
    frame[4] = 'L';
    frame[5] = 'u';
    frame[6] = 'b';
    frame[7] = 'i';
    frame[8] = 'e';
    frame[9] = 'R';
    frame[10] = 'a';
    frame[11] = 'd';
    frame[12] = 'i';
    frame[13] = 'o';
    frame[14] = '!';
    frame[15] = '!';
    frame[16] = '!';
    frame[17] = '!';
    frame[18] = 0x00;

    sendData(frame);
}

void sendDataWithCRC8ToMaster()
{
    frame[0] = NI;
    frame[1] = slaveAddress;
    frame[2] = 0x08;
    frame[3] = 0x00;
    frame[4] = 'L';
    frame[5] = 'u';
    frame[6] = 'b';
    frame[7] = 'i';
    frame[8] = 'e';
    frame[9] = 'R';
    frame[10] = 'a';
    frame[11] = 'd';
    frame[12] = 'i';
    frame[13] = 'o';
    frame[14] = '!';
    frame[15] = '!';
    frame[16] = '!';
    frame[17] = '!';
    frame[18] = crc8(frame[4], crc, 14);

    sendData(frame);
}

//
//
// Master
//
//
void runMasterMode()
{
    clearFrame();
    mode = getMode();

    if (mode >= 1 && mode <= 3) {
        printf("Wybrany tryb %d", mode);
        CC1000_Initialize();

        sendTestFrame();

        switch (mode) {
            case 1:
                sendDataWithoutSecurity();
                break;
            case 2:
                sendDataWithCRC8();
                break;
            default:
                break;
        }
        CC1000_SetupPD();
    } else if (mode == 4) {
        if (acknowledge == 0) {
            acknowledge = 1;
            printf("Wybrano tryb z potwierdzeniem");
        } else {
            acknowledge = 0;
            printf("Wybrano tryb bez potwierdzenia");
        }
    }
    runMasterMode();
}

int getMode()
{
    do {
        Wait_for_button_release();
        Wait_for_button_pressed();
        if (B1_ON()) {
            mode = 1;
            LED1_BLINK();
        } else if (B2_ON()) {
            mode = 2;
            LED2_BLINK();
        } else if (B3_ON()) {
            mode = 3;
            LED3_BLINK();
        } else if (B4_ON()) {
            mode = 4;
            LED4_BLINK();
        }
    } while (mode == 0);

    return mode;
}

void sendTestFrame()
{
    for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
        sendFrame(0x01);
        if (waitForFrame(4) == 0x02) {
            slaves[slaveAddress] = 1;
        }
    }
}

void sendDataWithoutSecurity()
{
    for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
        if (slaves[slaveAddress] != 1) {
            continue;
        }
        frame[0] = NI;
        frame[1] = slaveAddress;
        frame[2] = (acknowledge == 1) ? 0x83 : 0x03;
        frame[3] = 0x00;
        frame[4] = 'M';
        frame[5] = 'a';
        frame[6] = 'r';
        frame[7] = 'c';
        frame[8] = 'i';
        frame[9] = 'n';
        frame[10] = 'S';
        frame[11] = 'z';
        frame[12] = 'y';
        frame[13] = 'm';
        frame[14] = 'o';
        frame[15] = 'n';
        frame[16] = '!';
        frame[17] = '!';
        frame[18] = 0x00;

        sendData(frame);
        if (acknowledge == 1) {
            waitForFrame(4);
        }
        sendFrame(0x04);
        if (waitForFrame(4) == 0x05) {
            sendFrame(0x0D);
        }
    }
}

void sendDataWithCRC8()
{
    for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
        if (slaves[slaveAddress] != 1) {
            continue;
        }
        frame[0] = NI;
        frame[1] = slaveAddress;
        frame[2] = (acknowledge == 1) ? 0x86 : 0x06;
        frame[3] = 0x00;
        frame[4] = 'M';
        frame[5] = 'a';
        frame[6] = 'r';
        frame[7] = 'c';
        frame[8] = 'i';
        frame[9] = 'n';
        frame[10] = 'S';
        frame[11] = 'z';
        frame[12] = 'y';
        frame[13] = 'm';
        frame[14] = 'o';
        frame[15] = 'n';
        frame[16] = '!';
        frame[17] = '!';
        frame[18] = crc8(frame[4], crc, 14);

        sendData(frame);
        if (acknowledge == 1) {
            waitForFrame(4);
        }
        sendFrame(0x07);
        if (waitForFrame(4) == 0x08) {
            sendFrame(0x0D);
        }
    }
}

#endif