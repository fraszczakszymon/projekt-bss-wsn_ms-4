#ifndef __crc8
#define __crc8

#include "compiler_defs.h"
#include "C8051F020_defs.h"
#include "dev.h"
#include "uart.h"

#define crc8polynom 0x07

U8 crc8(U8* data, U8 crc, U16 len) {

    U16 i = 0;
    U16 j = 0;
    for (i = 0; i < len; i++) {
        crc ^= data[i];
        for (j = 0; j < 8; j++) {
            if ((crc & 0x80) != 0)
                crc = (crc << 1) ^ crc8polynom; 
            else 
                crc <<=1;
        }
    }

    return crc;
}

#endif