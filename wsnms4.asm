;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.3.0 #8604 (May 11 2013) (MINGW32)
; This file was generated Tue Jan 28 20:56:30 2014
;--------------------------------------------------------
	.module wsnms4
	.optsdcc -mmcs51 --model-small
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _CC1000_WriteToRegister_PARM_2
	.globl _crc8_PARM_3
	.globl _crc8_PARM_2
	.globl _main
	.globl _crc8
	.globl _printf
	.globl _printBCD
	.globl _gethex
	.globl _printc
	.globl _getchar
	.globl _putchar
	.globl _UART_SetBaudRate
	.globl _B4_PRESSED
	.globl _B3_PRESSED
	.globl _B2_PRESSED
	.globl _B1_PRESSED
	.globl _Wait_for_button_pressed
	.globl _Wait_for_button_release
	.globl _Delay_x100us
	.globl _Tus100_ISR
	.globl _Init_Device
	.globl _Interrupts_Init
	.globl _Oscillator_Init
	.globl _Port_IO_Init
	.globl _UART_Init
	.globl _Timer_Init
	.globl _Reset_Sources_Init
	.globl _PALE
	.globl _PDATA
	.globl _PCLK
	.globl _DIO
	.globl _DCLK
	.globl _SPIEN
	.globl _MSTEN
	.globl _SLVSEL
	.globl _TXBSY
	.globl _RXOVRN
	.globl _MODF
	.globl _WCOL
	.globl _SPIF
	.globl _AD0LJST
	.globl _AD0WINT
	.globl _AD0CM0
	.globl _AD0CM1
	.globl _AD0BUSY
	.globl _AD0INT
	.globl _AD0TM
	.globl _AD0EN
	.globl _CCF0
	.globl _CCF1
	.globl _CCF2
	.globl _CCF3
	.globl _CCF4
	.globl _CR
	.globl _CF
	.globl _P
	.globl _F1
	.globl _OV
	.globl _RS0
	.globl _RS1
	.globl _F0
	.globl _AC
	.globl _CY
	.globl _CPRL2
	.globl _CT2
	.globl _TR2
	.globl _EXEN2
	.globl _TCLK0
	.globl _RCLK0
	.globl _EXF2
	.globl _TF2
	.globl _SMBTOE
	.globl _SMBFTE
	.globl _AA
	.globl _SI
	.globl _STO
	.globl _STA
	.globl _ENSMB
	.globl _BUSY
	.globl _PX0
	.globl _PT0
	.globl _PX1
	.globl _PT1
	.globl _PS
	.globl _PT2
	.globl _EX0
	.globl _ET0
	.globl _EX1
	.globl _ET1
	.globl _ES0
	.globl _ET2
	.globl _IEGF0
	.globl _EA
	.globl _RI0
	.globl _TI0
	.globl _RB80
	.globl _TB80
	.globl _REN0
	.globl _SM20
	.globl _SM10
	.globl _SM00
	.globl _IT0
	.globl _IE0
	.globl _IT1
	.globl _IE1
	.globl _TR0
	.globl _TF0
	.globl _TR1
	.globl _TF1
	.globl __XPAGE
	.globl _DAC1
	.globl _DAC0
	.globl _TMR4
	.globl _TMR4RL
	.globl _T4
	.globl _RCAP4
	.globl _TMR2
	.globl _TMR2RL
	.globl _T2
	.globl _RCAP2
	.globl _ADC0LT
	.globl _ADC0GT
	.globl _ADC0
	.globl _TMR3
	.globl _TMR3RL
	.globl _DP
	.globl _WDTCN
	.globl _PCA0CPH4
	.globl _PCA0CPH3
	.globl _PCA0CPH2
	.globl _PCA0CPH1
	.globl _PCA0CPH0
	.globl _PCA0H
	.globl _SPI0CN
	.globl _EIP2
	.globl _EIP1
	.globl _TH4
	.globl _TL4
	.globl _SADDR1
	.globl _SBUF1
	.globl _SCON1
	.globl _B
	.globl _RSTSRC
	.globl _PCA0CPL4
	.globl _PCA0CPL3
	.globl _PCA0CPL2
	.globl _PCA0CPL1
	.globl _PCA0CPL0
	.globl _PCA0L
	.globl _ADC0CN
	.globl _EIE2
	.globl _EIE1
	.globl _RCAP4H
	.globl _RCAP4L
	.globl _XBR2
	.globl _XBR1
	.globl _XBR0
	.globl _ACC
	.globl _PCA0CPM4
	.globl _PCA0CPM3
	.globl _PCA0CPM2
	.globl _PCA0CPM1
	.globl _PCA0CPM0
	.globl _PCA0MD
	.globl _PCA0CN
	.globl _DAC1CN
	.globl _DAC1H
	.globl _DAC1L
	.globl _DAC0CN
	.globl _DAC0H
	.globl _DAC0L
	.globl _REF0CN
	.globl _PSW
	.globl _SMB0CR
	.globl _TH2
	.globl _TL2
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _T4CON
	.globl _T2CON
	.globl _ADC0LTH
	.globl _ADC0LTL
	.globl _ADC0GTH
	.globl _ADC0GTL
	.globl _SMB0ADR
	.globl _SMB0DAT
	.globl _SMB0STA
	.globl _SMB0CN
	.globl _ADC0H
	.globl _ADC0L
	.globl _P1MDIN
	.globl _ADC0CF
	.globl _AMX0SL
	.globl _AMX0CF
	.globl _SADEN0
	.globl _IP
	.globl _FLACL
	.globl _FLSCL
	.globl _P74OUT
	.globl _OSCICN
	.globl _OSCXCN
	.globl _P3
	.globl _EMI0CN
	.globl _SADEN1
	.globl _P3IF
	.globl _AMX1SL
	.globl _ADC1CF
	.globl _ADC1CN
	.globl _SADDR0
	.globl _IE
	.globl _P3MDOUT
	.globl _P2MDOUT
	.globl _P1MDOUT
	.globl _P0MDOUT
	.globl _EMI0CF
	.globl _EMI0TC
	.globl _P2
	.globl _CPT1CN
	.globl _CPT0CN
	.globl _SPI0CKR
	.globl _ADC1
	.globl _SPI0DAT
	.globl _SPI0CFG
	.globl _SBUF0
	.globl _SCON0
	.globl _P7
	.globl _TMR3H
	.globl _TMR3L
	.globl _TMR3RLH
	.globl _TMR3RLL
	.globl _TMR3CN
	.globl _P1
	.globl _PSCTL
	.globl _CKCON
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _PCON
	.globl _P6
	.globl _P5
	.globl _P4
	.globl _DPH
	.globl _DPL
	.globl _SP
	.globl _P0
	.globl _acknowledge
	.globl _mode
	.globl _i
	.globl _slaves
	.globl _slaveAddress
	.globl _crc
	.globl _frame
	.globl _PREAMBULE
	.globl _printBCD_PARM_2
	.globl _T_1s
	.globl _T_1ms
	.globl _hus
	.globl _us100
	.globl _CC1000_WriteToRegister
	.globl _CC1000_ReadFromRegister
	.globl _BitReceive
	.globl _CC1000_ReceiveByte
	.globl _BitSend
	.globl _CC1000_SendByte
	.globl _CC1000_Reset
	.globl _CC1000_Calibrate
	.globl _CC1000_SetupRX
	.globl _CC1000_SetupTX
	.globl _CC1000_SetupPD
	.globl _CC1000_WakeUpToRX
	.globl _CC1000_WakeUpToTX
	.globl _CC1000_ConfigureRegistersRx
	.globl _CC1000_ConfigureRegistersTx
	.globl _CC1000_ResetFreqSynth
	.globl _CC1000_Initialize
	.globl _CC1000_ConfigureRegisters
	.globl _CC1000_DisplayAllRegisters
	.globl _CC1000_Sync
	.globl _clearFrame
	.globl _sendFrame
	.globl _sendData
	.globl _waitForFrame
	.globl _runSlaveMode
	.globl _sendDataWithoutSecurityToMaster
	.globl _sendDataWithCRC8ToMaster
	.globl _runMasterMode
	.globl _getMode
	.globl _sendTestFrame
	.globl _sendDataWithoutSecurity
	.globl _sendDataWithCRC8
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
G$P0$0$0 == 0x0080
_P0	=	0x0080
G$SP$0$0 == 0x0081
_SP	=	0x0081
G$DPL$0$0 == 0x0082
_DPL	=	0x0082
G$DPH$0$0 == 0x0083
_DPH	=	0x0083
G$P4$0$0 == 0x0084
_P4	=	0x0084
G$P5$0$0 == 0x0085
_P5	=	0x0085
G$P6$0$0 == 0x0086
_P6	=	0x0086
G$PCON$0$0 == 0x0087
_PCON	=	0x0087
G$TCON$0$0 == 0x0088
_TCON	=	0x0088
G$TMOD$0$0 == 0x0089
_TMOD	=	0x0089
G$TL0$0$0 == 0x008a
_TL0	=	0x008a
G$TL1$0$0 == 0x008b
_TL1	=	0x008b
G$TH0$0$0 == 0x008c
_TH0	=	0x008c
G$TH1$0$0 == 0x008d
_TH1	=	0x008d
G$CKCON$0$0 == 0x008e
_CKCON	=	0x008e
G$PSCTL$0$0 == 0x008f
_PSCTL	=	0x008f
G$P1$0$0 == 0x0090
_P1	=	0x0090
G$TMR3CN$0$0 == 0x0091
_TMR3CN	=	0x0091
G$TMR3RLL$0$0 == 0x0092
_TMR3RLL	=	0x0092
G$TMR3RLH$0$0 == 0x0093
_TMR3RLH	=	0x0093
G$TMR3L$0$0 == 0x0094
_TMR3L	=	0x0094
G$TMR3H$0$0 == 0x0095
_TMR3H	=	0x0095
G$P7$0$0 == 0x0096
_P7	=	0x0096
G$SCON0$0$0 == 0x0098
_SCON0	=	0x0098
G$SBUF0$0$0 == 0x0099
_SBUF0	=	0x0099
G$SPI0CFG$0$0 == 0x009a
_SPI0CFG	=	0x009a
G$SPI0DAT$0$0 == 0x009b
_SPI0DAT	=	0x009b
G$ADC1$0$0 == 0x009c
_ADC1	=	0x009c
G$SPI0CKR$0$0 == 0x009d
_SPI0CKR	=	0x009d
G$CPT0CN$0$0 == 0x009e
_CPT0CN	=	0x009e
G$CPT1CN$0$0 == 0x009f
_CPT1CN	=	0x009f
G$P2$0$0 == 0x00a0
_P2	=	0x00a0
G$EMI0TC$0$0 == 0x00a1
_EMI0TC	=	0x00a1
G$EMI0CF$0$0 == 0x00a3
_EMI0CF	=	0x00a3
G$P0MDOUT$0$0 == 0x00a4
_P0MDOUT	=	0x00a4
G$P1MDOUT$0$0 == 0x00a5
_P1MDOUT	=	0x00a5
G$P2MDOUT$0$0 == 0x00a6
_P2MDOUT	=	0x00a6
G$P3MDOUT$0$0 == 0x00a7
_P3MDOUT	=	0x00a7
G$IE$0$0 == 0x00a8
_IE	=	0x00a8
G$SADDR0$0$0 == 0x00a9
_SADDR0	=	0x00a9
G$ADC1CN$0$0 == 0x00aa
_ADC1CN	=	0x00aa
G$ADC1CF$0$0 == 0x00ab
_ADC1CF	=	0x00ab
G$AMX1SL$0$0 == 0x00ac
_AMX1SL	=	0x00ac
G$P3IF$0$0 == 0x00ad
_P3IF	=	0x00ad
G$SADEN1$0$0 == 0x00ae
_SADEN1	=	0x00ae
G$EMI0CN$0$0 == 0x00af
_EMI0CN	=	0x00af
G$P3$0$0 == 0x00b0
_P3	=	0x00b0
G$OSCXCN$0$0 == 0x00b1
_OSCXCN	=	0x00b1
G$OSCICN$0$0 == 0x00b2
_OSCICN	=	0x00b2
G$P74OUT$0$0 == 0x00b5
_P74OUT	=	0x00b5
G$FLSCL$0$0 == 0x00b6
_FLSCL	=	0x00b6
G$FLACL$0$0 == 0x00b7
_FLACL	=	0x00b7
G$IP$0$0 == 0x00b8
_IP	=	0x00b8
G$SADEN0$0$0 == 0x00b9
_SADEN0	=	0x00b9
G$AMX0CF$0$0 == 0x00ba
_AMX0CF	=	0x00ba
G$AMX0SL$0$0 == 0x00bb
_AMX0SL	=	0x00bb
G$ADC0CF$0$0 == 0x00bc
_ADC0CF	=	0x00bc
G$P1MDIN$0$0 == 0x00bd
_P1MDIN	=	0x00bd
G$ADC0L$0$0 == 0x00be
_ADC0L	=	0x00be
G$ADC0H$0$0 == 0x00bf
_ADC0H	=	0x00bf
G$SMB0CN$0$0 == 0x00c0
_SMB0CN	=	0x00c0
G$SMB0STA$0$0 == 0x00c1
_SMB0STA	=	0x00c1
G$SMB0DAT$0$0 == 0x00c2
_SMB0DAT	=	0x00c2
G$SMB0ADR$0$0 == 0x00c3
_SMB0ADR	=	0x00c3
G$ADC0GTL$0$0 == 0x00c4
_ADC0GTL	=	0x00c4
G$ADC0GTH$0$0 == 0x00c5
_ADC0GTH	=	0x00c5
G$ADC0LTL$0$0 == 0x00c6
_ADC0LTL	=	0x00c6
G$ADC0LTH$0$0 == 0x00c7
_ADC0LTH	=	0x00c7
G$T2CON$0$0 == 0x00c8
_T2CON	=	0x00c8
G$T4CON$0$0 == 0x00c9
_T4CON	=	0x00c9
G$RCAP2L$0$0 == 0x00ca
_RCAP2L	=	0x00ca
G$RCAP2H$0$0 == 0x00cb
_RCAP2H	=	0x00cb
G$TL2$0$0 == 0x00cc
_TL2	=	0x00cc
G$TH2$0$0 == 0x00cd
_TH2	=	0x00cd
G$SMB0CR$0$0 == 0x00cf
_SMB0CR	=	0x00cf
G$PSW$0$0 == 0x00d0
_PSW	=	0x00d0
G$REF0CN$0$0 == 0x00d1
_REF0CN	=	0x00d1
G$DAC0L$0$0 == 0x00d2
_DAC0L	=	0x00d2
G$DAC0H$0$0 == 0x00d3
_DAC0H	=	0x00d3
G$DAC0CN$0$0 == 0x00d4
_DAC0CN	=	0x00d4
G$DAC1L$0$0 == 0x00d5
_DAC1L	=	0x00d5
G$DAC1H$0$0 == 0x00d6
_DAC1H	=	0x00d6
G$DAC1CN$0$0 == 0x00d7
_DAC1CN	=	0x00d7
G$PCA0CN$0$0 == 0x00d8
_PCA0CN	=	0x00d8
G$PCA0MD$0$0 == 0x00d9
_PCA0MD	=	0x00d9
G$PCA0CPM0$0$0 == 0x00da
_PCA0CPM0	=	0x00da
G$PCA0CPM1$0$0 == 0x00db
_PCA0CPM1	=	0x00db
G$PCA0CPM2$0$0 == 0x00dc
_PCA0CPM2	=	0x00dc
G$PCA0CPM3$0$0 == 0x00dd
_PCA0CPM3	=	0x00dd
G$PCA0CPM4$0$0 == 0x00de
_PCA0CPM4	=	0x00de
G$ACC$0$0 == 0x00e0
_ACC	=	0x00e0
G$XBR0$0$0 == 0x00e1
_XBR0	=	0x00e1
G$XBR1$0$0 == 0x00e2
_XBR1	=	0x00e2
G$XBR2$0$0 == 0x00e3
_XBR2	=	0x00e3
G$RCAP4L$0$0 == 0x00e4
_RCAP4L	=	0x00e4
G$RCAP4H$0$0 == 0x00e5
_RCAP4H	=	0x00e5
G$EIE1$0$0 == 0x00e6
_EIE1	=	0x00e6
G$EIE2$0$0 == 0x00e7
_EIE2	=	0x00e7
G$ADC0CN$0$0 == 0x00e8
_ADC0CN	=	0x00e8
G$PCA0L$0$0 == 0x00e9
_PCA0L	=	0x00e9
G$PCA0CPL0$0$0 == 0x00ea
_PCA0CPL0	=	0x00ea
G$PCA0CPL1$0$0 == 0x00eb
_PCA0CPL1	=	0x00eb
G$PCA0CPL2$0$0 == 0x00ec
_PCA0CPL2	=	0x00ec
G$PCA0CPL3$0$0 == 0x00ed
_PCA0CPL3	=	0x00ed
G$PCA0CPL4$0$0 == 0x00ee
_PCA0CPL4	=	0x00ee
G$RSTSRC$0$0 == 0x00ef
_RSTSRC	=	0x00ef
G$B$0$0 == 0x00f0
_B	=	0x00f0
G$SCON1$0$0 == 0x00f1
_SCON1	=	0x00f1
G$SBUF1$0$0 == 0x00f2
_SBUF1	=	0x00f2
G$SADDR1$0$0 == 0x00f3
_SADDR1	=	0x00f3
G$TL4$0$0 == 0x00f4
_TL4	=	0x00f4
G$TH4$0$0 == 0x00f5
_TH4	=	0x00f5
G$EIP1$0$0 == 0x00f6
_EIP1	=	0x00f6
G$EIP2$0$0 == 0x00f7
_EIP2	=	0x00f7
G$SPI0CN$0$0 == 0x00f8
_SPI0CN	=	0x00f8
G$PCA0H$0$0 == 0x00f9
_PCA0H	=	0x00f9
G$PCA0CPH0$0$0 == 0x00fa
_PCA0CPH0	=	0x00fa
G$PCA0CPH1$0$0 == 0x00fb
_PCA0CPH1	=	0x00fb
G$PCA0CPH2$0$0 == 0x00fc
_PCA0CPH2	=	0x00fc
G$PCA0CPH3$0$0 == 0x00fd
_PCA0CPH3	=	0x00fd
G$PCA0CPH4$0$0 == 0x00fe
_PCA0CPH4	=	0x00fe
G$WDTCN$0$0 == 0x00ff
_WDTCN	=	0x00ff
G$DP$0$0 == 0x8382
_DP	=	0x8382
G$TMR3RL$0$0 == 0x9392
_TMR3RL	=	0x9392
G$TMR3$0$0 == 0x9594
_TMR3	=	0x9594
G$ADC0$0$0 == 0xbfbe
_ADC0	=	0xbfbe
G$ADC0GT$0$0 == 0xc5c4
_ADC0GT	=	0xc5c4
G$ADC0LT$0$0 == 0xc7c6
_ADC0LT	=	0xc7c6
G$RCAP2$0$0 == 0xcbca
_RCAP2	=	0xcbca
G$T2$0$0 == 0xcdcc
_T2	=	0xcdcc
G$TMR2RL$0$0 == 0xcbca
_TMR2RL	=	0xcbca
G$TMR2$0$0 == 0xcdcc
_TMR2	=	0xcdcc
G$RCAP4$0$0 == 0xe5e4
_RCAP4	=	0xe5e4
G$T4$0$0 == 0xf5f4
_T4	=	0xf5f4
G$TMR4RL$0$0 == 0xe5e4
_TMR4RL	=	0xe5e4
G$TMR4$0$0 == 0xf5f4
_TMR4	=	0xf5f4
G$DAC0$0$0 == 0xd3d2
_DAC0	=	0xd3d2
G$DAC1$0$0 == 0xd6d5
_DAC1	=	0xd6d5
G$_XPAGE$0$0 == 0x00af
__XPAGE	=	0x00af
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
G$TF1$0$0 == 0x008f
_TF1	=	0x008f
G$TR1$0$0 == 0x008e
_TR1	=	0x008e
G$TF0$0$0 == 0x008d
_TF0	=	0x008d
G$TR0$0$0 == 0x008c
_TR0	=	0x008c
G$IE1$0$0 == 0x008b
_IE1	=	0x008b
G$IT1$0$0 == 0x008a
_IT1	=	0x008a
G$IE0$0$0 == 0x0089
_IE0	=	0x0089
G$IT0$0$0 == 0x0088
_IT0	=	0x0088
G$SM00$0$0 == 0x009f
_SM00	=	0x009f
G$SM10$0$0 == 0x009e
_SM10	=	0x009e
G$SM20$0$0 == 0x009d
_SM20	=	0x009d
G$REN0$0$0 == 0x009c
_REN0	=	0x009c
G$TB80$0$0 == 0x009b
_TB80	=	0x009b
G$RB80$0$0 == 0x009a
_RB80	=	0x009a
G$TI0$0$0 == 0x0099
_TI0	=	0x0099
G$RI0$0$0 == 0x0098
_RI0	=	0x0098
G$EA$0$0 == 0x00af
_EA	=	0x00af
G$IEGF0$0$0 == 0x00ae
_IEGF0	=	0x00ae
G$ET2$0$0 == 0x00ad
_ET2	=	0x00ad
G$ES0$0$0 == 0x00ac
_ES0	=	0x00ac
G$ET1$0$0 == 0x00ab
_ET1	=	0x00ab
G$EX1$0$0 == 0x00aa
_EX1	=	0x00aa
G$ET0$0$0 == 0x00a9
_ET0	=	0x00a9
G$EX0$0$0 == 0x00a8
_EX0	=	0x00a8
G$PT2$0$0 == 0x00bd
_PT2	=	0x00bd
G$PS$0$0 == 0x00bc
_PS	=	0x00bc
G$PT1$0$0 == 0x00bb
_PT1	=	0x00bb
G$PX1$0$0 == 0x00ba
_PX1	=	0x00ba
G$PT0$0$0 == 0x00b9
_PT0	=	0x00b9
G$PX0$0$0 == 0x00b8
_PX0	=	0x00b8
G$BUSY$0$0 == 0x00c7
_BUSY	=	0x00c7
G$ENSMB$0$0 == 0x00c6
_ENSMB	=	0x00c6
G$STA$0$0 == 0x00c5
_STA	=	0x00c5
G$STO$0$0 == 0x00c4
_STO	=	0x00c4
G$SI$0$0 == 0x00c3
_SI	=	0x00c3
G$AA$0$0 == 0x00c2
_AA	=	0x00c2
G$SMBFTE$0$0 == 0x00c1
_SMBFTE	=	0x00c1
G$SMBTOE$0$0 == 0x00c0
_SMBTOE	=	0x00c0
G$TF2$0$0 == 0x00cf
_TF2	=	0x00cf
G$EXF2$0$0 == 0x00ce
_EXF2	=	0x00ce
G$RCLK0$0$0 == 0x00cd
_RCLK0	=	0x00cd
G$TCLK0$0$0 == 0x00cc
_TCLK0	=	0x00cc
G$EXEN2$0$0 == 0x00cb
_EXEN2	=	0x00cb
G$TR2$0$0 == 0x00ca
_TR2	=	0x00ca
G$CT2$0$0 == 0x00c9
_CT2	=	0x00c9
G$CPRL2$0$0 == 0x00c8
_CPRL2	=	0x00c8
G$CY$0$0 == 0x00d7
_CY	=	0x00d7
G$AC$0$0 == 0x00d6
_AC	=	0x00d6
G$F0$0$0 == 0x00d5
_F0	=	0x00d5
G$RS1$0$0 == 0x00d4
_RS1	=	0x00d4
G$RS0$0$0 == 0x00d3
_RS0	=	0x00d3
G$OV$0$0 == 0x00d2
_OV	=	0x00d2
G$F1$0$0 == 0x00d1
_F1	=	0x00d1
G$P$0$0 == 0x00d0
_P	=	0x00d0
G$CF$0$0 == 0x00df
_CF	=	0x00df
G$CR$0$0 == 0x00de
_CR	=	0x00de
G$CCF4$0$0 == 0x00dc
_CCF4	=	0x00dc
G$CCF3$0$0 == 0x00db
_CCF3	=	0x00db
G$CCF2$0$0 == 0x00da
_CCF2	=	0x00da
G$CCF1$0$0 == 0x00d9
_CCF1	=	0x00d9
G$CCF0$0$0 == 0x00d8
_CCF0	=	0x00d8
G$AD0EN$0$0 == 0x00ef
_AD0EN	=	0x00ef
G$AD0TM$0$0 == 0x00ee
_AD0TM	=	0x00ee
G$AD0INT$0$0 == 0x00ed
_AD0INT	=	0x00ed
G$AD0BUSY$0$0 == 0x00ec
_AD0BUSY	=	0x00ec
G$AD0CM1$0$0 == 0x00eb
_AD0CM1	=	0x00eb
G$AD0CM0$0$0 == 0x00ea
_AD0CM0	=	0x00ea
G$AD0WINT$0$0 == 0x00e9
_AD0WINT	=	0x00e9
G$AD0LJST$0$0 == 0x00e8
_AD0LJST	=	0x00e8
G$SPIF$0$0 == 0x00ff
_SPIF	=	0x00ff
G$WCOL$0$0 == 0x00fe
_WCOL	=	0x00fe
G$MODF$0$0 == 0x00fd
_MODF	=	0x00fd
G$RXOVRN$0$0 == 0x00fc
_RXOVRN	=	0x00fc
G$TXBSY$0$0 == 0x00fb
_TXBSY	=	0x00fb
G$SLVSEL$0$0 == 0x00fa
_SLVSEL	=	0x00fa
G$MSTEN$0$0 == 0x00f9
_MSTEN	=	0x00f9
G$SPIEN$0$0 == 0x00f8
_SPIEN	=	0x00f8
G$DCLK$0$0 == 0x0082
_DCLK	=	0x0082
G$DIO$0$0 == 0x0083
_DIO	=	0x0083
G$PCLK$0$0 == 0x0085
_PCLK	=	0x0085
G$PDATA$0$0 == 0x0084
_PDATA	=	0x0084
G$PALE$0$0 == 0x0087
_PALE	=	0x0087
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
G$us100$0$0==.
_us100::
	.ds 1
G$hus$0$0==.
_hus::
	.ds 2
G$T_1ms$0$0==.
_T_1ms::
	.ds 2
G$T_1s$0$0==.
_T_1s::
	.ds 2
Lwsnms4.printBCD$precission$1$44==.
_printBCD_PARM_2:
	.ds 1
Lwsnms4.printBCD$tmp$1$45==.
_printBCD_tmp_1_45:
	.ds 5
Fwsnms4$Transceiver_State$0$0==.
_Transceiver_State:
	.ds 1
Fwsnms4$Transceiver_Timeout$0$0==.
_Transceiver_Timeout:
	.ds 1
Fwsnms4$NrSekw$0$0==.
_NrSekw:
	.ds 1
G$PREAMBULE$0$0==.
_PREAMBULE::
	.ds 4
G$frame$0$0==.
_frame::
	.ds 19
G$crc$0$0==.
_crc::
	.ds 1
G$slaveAddress$0$0==.
_slaveAddress::
	.ds 2
G$slaves$0$0==.
_slaves::
	.ds 6
G$i$0$0==.
_i::
	.ds 2
G$mode$0$0==.
_mode::
	.ds 2
G$acknowledge$0$0==.
_acknowledge::
	.ds 2
Lwsnms4.sendData$frame$1$141==.
_sendData_frame_1_141:
	.ds 3
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
Lwsnms4.crc8$crc$1$56==.
_crc8_PARM_2:
	.ds 1
Lwsnms4.crc8$len$1$56==.
_crc8_PARM_3:
	.ds 2
	.area	OSEG    (OVR,DATA)
Lwsnms4.CC1000_WriteToRegister$value$1$80==.
_CC1000_WriteToRegister_PARM_2:
	.ds 1
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
Lwsnms4.printBCD$s$1$45==.
_printBCD_s_1_45:
	.ds 1
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	ljmp	_Tus100_ISR
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
	.globl __sdcc_gsinit_startup
	.globl __sdcc_program_startup
	.globl __start__stack
	.globl __mcs51_genXINIT
	.globl __mcs51_genXRAMCLEAR
	.globl __mcs51_genRAMCLEAR
	C$dev.h$108$1$185 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:108: volatile U8   us100  = 0;		
	mov	_us100,#0x00
	C$dev.h$109$1$185 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:109: volatile U16  hus    = 0;	// us x 100
	clr	a
	mov	_hus,a
	mov	(_hus + 1),a
	C$dev.h$110$1$185 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:110: volatile U16  T_1ms  = 0; 	// TIME_MS   = 0;
	clr	a
	mov	_T_1ms,a
	mov	(_T_1ms + 1),a
	C$dev.h$111$1$185 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:111: volatile U16  T_1s   = 0;	// TIME      = 0;
	clr	a
	mov	_T_1s,a
	mov	(_T_1s + 1),a
	C$cc1000.h$136$1$185 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:136: static U8 Transceiver_State = 0;
	mov	_Transceiver_State,#0x00
	C$cc1000.h$137$1$185 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:137: static U8 Transceiver_Timeout = 10;
	mov	_Transceiver_Timeout,#0x0A
	C$cc1000.h$138$1$185 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:138: static U8 NrSekw = 0;
	mov	_NrSekw,#0x00
	C$wsn.h$14$1$185 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:14: U8 crc = 0x0000;
	mov	_crc,#0x00
	C$wsn.h$15$1$185 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:15: int slaveAddress = 0x00;
	clr	a
	mov	_slaveAddress,a
	mov	(_slaveAddress + 1),a
	C$wsn.h$16$1$185 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:16: int slaves[] = {0, 0, 0};
	clr	a
	mov	(_slaves + 0),a
	mov	(_slaves + 1),a
	mov	((_slaves + 0x0002) + 0),a
	mov	((_slaves + 0x0002) + 1),a
	mov	((_slaves + 0x0004) + 0),a
	mov	((_slaves + 0x0004) + 1),a
	C$wsn.h$17$1$185 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:17: int i, mode = 0;
	clr	a
	mov	_mode,a
	mov	(_mode + 1),a
	C$wsn.h$18$1$185 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:18: int acknowledge = 0;
	clr	a
	mov	_acknowledge,a
	mov	(_acknowledge + 1),a
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
__sdcc_program_startup:
	ljmp	_main
;	return from main will return to caller
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'Reset_Sources_Init'
;------------------------------------------------------------
	G$Reset_Sources_Init$0$0 ==.
	C$dev.h$41$0$0 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:41: void Reset_Sources_Init()
;	-----------------------------------------
;	 function Reset_Sources_Init
;	-----------------------------------------
_Reset_Sources_Init:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
	C$dev.h$43$1$1 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:43: WDTCN     = 0xDE;
	mov	_WDTCN,#0xDE
	C$dev.h$44$1$1 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:44: WDTCN     = 0xAD;
	mov	_WDTCN,#0xAD
	C$dev.h$45$1$1 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:45: RSTSRC    = 0x04;
	mov	_RSTSRC,#0x04
	C$dev.h$46$1$1 ==.
	XG$Reset_Sources_Init$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Timer_Init'
;------------------------------------------------------------
	G$Timer_Init$0$0 ==.
	C$dev.h$48$1$1 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:48: void Timer_Init()
;	-----------------------------------------
;	 function Timer_Init
;	-----------------------------------------
_Timer_Init:
	C$dev.h$50$1$2 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:50: CKCON     = 0x38;   // Timer0, Timer1 i Timer2 u�ywaj� SYSCLK
	mov	_CKCON,#0x38
	C$dev.h$51$1$2 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:51: TCON      = 0x50;   // Zezwolenie (start) dla Timer0 (TR0) i Timer1 (TR1)
	mov	_TCON,#0x50
	C$dev.h$52$1$2 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:52: TMOD      = 0x21;   // Timer0 - tryb 16-bitowy, Timer1 - tryb 8-bitowy z autoprze�adowaniem
	mov	_TMOD,#0x21
	C$dev.h$53$1$2 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:53: TH1       = 0xFA;   // Warto�� prze�adowania Timera1 dla pr�dkosci 115200 bit/s
	mov	_TH1,#0xFA
	C$dev.h$54$1$2 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:54: T2CON     = 0x04;   // Start Timera2
	mov	_T2CON,#0x04
	C$dev.h$55$1$2 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:55: RCAP2L = (U8) (65536-(SYSCLK/10000)); 		// Dla przerwa� 100us z T2L
	mov	_RCAP2L,#0x5D
	C$dev.h$56$1$2 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:56: RCAP2H = (U8) ((65536-(SYSCLK/10000)) >>8);
	mov	_RCAP2H,#0xF7
	C$dev.h$57$1$2 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:57: TL2 = RCAP2L;   // Warto�� prze�adowania Timera2
	mov	_TL2,_RCAP2L
	C$dev.h$58$1$2 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:58: TH2 = RCAP2H;   //   "           "          " 
	mov	_TH2,_RCAP2H
	C$dev.h$59$1$2 ==.
	XG$Timer_Init$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'UART_Init'
;------------------------------------------------------------
	G$UART_Init$0$0 ==.
	C$dev.h$61$1$2 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:61: void UART_Init()
;	-----------------------------------------
;	 function UART_Init
;	-----------------------------------------
_UART_Init:
	C$dev.h$63$1$3 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:63: SCON0     = 0x50;	// Tryb 8-bitowy; zezwolenie na odbi�r
	mov	_SCON0,#0x50
	C$dev.h$64$1$3 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:64: TI0		  = 1;		// Wa�ny; Transmit Interrupt Flag;  	
	setb	_TI0
	C$dev.h$65$1$3 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:65: SCON1     = 0x50;	// Tryb 8-bitowy; zezwolenie na odbi�r
	mov	_SCON1,#0x50
	C$dev.h$67$1$3 ==.
	XG$UART_Init$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Port_IO_Init'
;------------------------------------------------------------
	G$Port_IO_Init$0$0 ==.
	C$dev.h$69$1$3 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:69: void Port_IO_Init()
;	-----------------------------------------
;	 function Port_IO_Init
;	-----------------------------------------
_Port_IO_Init:
	C$dev.h$71$1$4 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:71: P0MDOUT   = 0xA1;	// Port P0.0(TX0), P0.5(PCLK), P0.7(PALE) - wyj. push-pull
	mov	_P0MDOUT,#0xA1
	C$dev.h$72$1$4 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:72: P74OUT    = 0x08;   // Port P5.4-P5.7 (LED1-4) - wyj. push-pull  
	mov	_P74OUT,#0x08
	C$dev.h$73$1$4 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:73: XBR0      = 0x04;   // Pod��czenie do port�w UART0
	mov	_XBR0,#0x04
	C$dev.h$74$1$4 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:74: XBR2      = 0x40;   // Zezwolenie Crossbar 
	mov	_XBR2,#0x40
	C$dev.h$75$1$4 ==.
	XG$Port_IO_Init$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Oscillator_Init'
;------------------------------------------------------------
;i                         Allocated to registers r6 r7 
;------------------------------------------------------------
	G$Oscillator_Init$0$0 ==.
	C$dev.h$77$1$4 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:77: void Oscillator_Init()
;	-----------------------------------------
;	 function Oscillator_Init
;	-----------------------------------------
_Oscillator_Init:
	C$dev.h$80$1$5 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:80: OSCXCN    = 0x67;
	mov	_OSCXCN,#0x67
	C$dev.h$81$1$5 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:81: for (i = 0; i < 3000; i++);  // Wait 1ms for initialization
	mov	r6,#0xB8
	mov	r7,#0x0B
00107$:
	dec	r6
	cjne	r6,#0xFF,00121$
	dec	r7
00121$:
	mov	a,r6
	orl	a,r7
	jnz	00107$
	C$dev.h$82$1$5 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:82: while ((OSCXCN & 0x80) == 0);
00102$:
	mov	a,_OSCXCN
	jnb	acc.7,00102$
	C$dev.h$83$1$5 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:83: OSCICN    = 0x08;
	mov	_OSCICN,#0x08
	C$dev.h$84$1$5 ==.
	XG$Oscillator_Init$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Interrupts_Init'
;------------------------------------------------------------
	G$Interrupts_Init$0$0 ==.
	C$dev.h$86$1$5 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:86: void Interrupts_Init()
;	-----------------------------------------
;	 function Interrupts_Init
;	-----------------------------------------
_Interrupts_Init:
	C$dev.h$88$1$6 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:88: IE        = 0xA0;   // Zezwolenia na przerwania globalne i od Timera2 
	mov	_IE,#0xA0
	C$dev.h$89$1$6 ==.
	XG$Interrupts_Init$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Init_Device'
;------------------------------------------------------------
	G$Init_Device$0$0 ==.
	C$dev.h$93$1$6 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:93: void Init_Device(void)
;	-----------------------------------------
;	 function Init_Device
;	-----------------------------------------
_Init_Device:
	C$dev.h$95$1$8 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:95: Reset_Sources_Init();
	lcall	_Reset_Sources_Init
	C$dev.h$96$1$8 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:96: Timer_Init();
	lcall	_Timer_Init
	C$dev.h$97$1$8 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:97: UART_Init();
	lcall	_UART_Init
	C$dev.h$98$1$8 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:98: Port_IO_Init();
	lcall	_Port_IO_Init
	C$dev.h$99$1$8 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:99: Oscillator_Init();
	lcall	_Oscillator_Init
	C$dev.h$100$1$8 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:100: Interrupts_Init();
	lcall	_Interrupts_Init
	C$dev.h$101$1$8 ==.
	XG$Init_Device$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Tus100_ISR'
;------------------------------------------------------------
	G$Tus100_ISR$0$0 ==.
	C$dev.h$115$1$8 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:115: void Tus100_ISR (void) __interrupt 5    // Przerwania 100us
;	-----------------------------------------
;	 function Tus100_ISR
;	-----------------------------------------
_Tus100_ISR:
	push	acc
	push	psw
	C$dev.h$117$1$10 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:117: TF2 = 0;
	clr	_TF2
	C$dev.h$118$1$10 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:118: ++us100;
	inc	_us100
	C$dev.h$119$1$10 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:119: ++hus;
	mov	a,#0x01
	add	a,_hus
	mov	_hus,a
	clr	a
	addc	a,(_hus + 1)
	mov	(_hus + 1),a
	C$dev.h$120$1$10 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:120: if (us100 >= 10) 	
	mov	a,#0x100 - 0x0A
	add	a,_us100
	jnc	00102$
	C$dev.h$121$2$11 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:121: {	us100 = 0;
	mov	_us100,#0x00
	C$dev.h$122$2$11 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:122: ++T_1ms; 
	mov	a,#0x01
	add	a,_T_1ms
	mov	_T_1ms,a
	clr	a
	addc	a,(_T_1ms + 1)
	mov	(_T_1ms + 1),a
00102$:
	C$dev.h$124$1$10 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:124: if (T_1ms >= 1000)          //  Modulo 1000
	clr	c
	mov	a,_T_1ms
	subb	a,#0xE8
	mov	a,(_T_1ms + 1)
	subb	a,#0x03
	jc	00105$
	C$dev.h$125$2$12 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:125: {	T_1ms = 0;
	clr	a
	mov	_T_1ms,a
	mov	(_T_1ms + 1),a
	C$dev.h$126$2$12 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:126: ++T_1s; 
	mov	a,#0x01
	add	a,_T_1s
	mov	_T_1s,a
	clr	a
	addc	a,(_T_1s + 1)
	mov	(_T_1s + 1),a
00105$:
	pop	psw
	pop	acc
	C$dev.h$128$1$10 ==.
	XG$Tus100_ISR$0$0 ==.
	reti
;	eliminated unneeded mov psw,# (no regs used in bank)
;	eliminated unneeded push/pop dpl
;	eliminated unneeded push/pop dph
;	eliminated unneeded push/pop b
;------------------------------------------------------------
;Allocation info for local variables in function 'Delay_x100us'
;------------------------------------------------------------
;n100us                    Allocated to registers r6 r7 
;------------------------------------------------------------
	G$Delay_x100us$0$0 ==.
	C$dev.h$130$1$10 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:130: void Delay_x100us(U16 n100us)	// W 100 x us
;	-----------------------------------------
;	 function Delay_x100us
;	-----------------------------------------
_Delay_x100us:
	mov	r6,dpl
	mov	r7,dph
	C$dev.h$132$1$14 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:132: hus = 0;
	clr	a
	mov	_hus,a
	mov	(_hus + 1),a
	C$dev.h$133$1$14 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:133: while (hus <= n100us);
00101$:
	clr	c
	mov	a,r6
	subb	a,_hus
	mov	a,r7
	subb	a,(_hus + 1)
	jnc	00101$
	C$dev.h$134$1$14 ==.
	XG$Delay_x100us$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Wait_for_button_release'
;------------------------------------------------------------
	G$Wait_for_button_release$0$0 ==.
	C$dev.h$143$1$14 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:143: void Wait_for_button_release (void) 
;	-----------------------------------------
;	 function Wait_for_button_release
;	-----------------------------------------
_Wait_for_button_release:
	C$dev.h$145$1$16 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:145: while (B1_ON() || B2_ON() || B3_ON() || B4_ON());  
00104$:
	mov	a,_P5
	jnb	acc.0,00104$
	mov	a,_P5
	jnb	acc.1,00104$
	mov	a,_P5
	jnb	acc.2,00104$
	mov	a,_P5
	jnb	acc.3,00104$
	C$dev.h$146$1$16 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:146: Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w                                         
	mov	dptr,#0x0064
	lcall	_Delay_x100us
	C$dev.h$147$1$16 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:147: while (B1_ON() || B2_ON() || B3_ON() || B4_ON());  
00110$:
	mov	a,_P5
	jnb	acc.0,00110$
	mov	a,_P5
	jnb	acc.1,00110$
	mov	a,_P5
	jnb	acc.2,00110$
	mov	a,_P5
	jnb	acc.3,00110$
	C$dev.h$148$1$16 ==.
	XG$Wait_for_button_release$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Wait_for_button_pressed'
;------------------------------------------------------------
	G$Wait_for_button_pressed$0$0 ==.
	C$dev.h$151$1$16 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:151: void Wait_for_button_pressed (void) 
;	-----------------------------------------
;	 function Wait_for_button_pressed
;	-----------------------------------------
_Wait_for_button_pressed:
	C$dev.h$153$1$18 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:153: while (!(B1_ON() || B2_ON() || B3_ON() || B4_ON()));  
00104$:
	mov	a,_P5
	jnb	acc.0,00106$
	mov	a,_P5
	jnb	acc.1,00106$
	mov	a,_P5
	jnb	acc.2,00106$
	mov	a,_P5
	jb	acc.3,00104$
00106$:
	C$dev.h$154$1$18 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:154: Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w                                         
	mov	dptr,#0x0064
	lcall	_Delay_x100us
	C$dev.h$155$1$18 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:155: while (!(B1_ON() || B2_ON() || B3_ON() || B4_ON()));  
00110$:
	mov	a,_P5
	jnb	acc.0,00113$
	mov	a,_P5
	jnb	acc.1,00113$
	mov	a,_P5
	jnb	acc.2,00113$
	mov	a,_P5
	jb	acc.3,00110$
00113$:
	C$dev.h$156$1$18 ==.
	XG$Wait_for_button_pressed$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'B1_PRESSED'
;------------------------------------------------------------
	G$B1_PRESSED$0$0 ==.
	C$dev.h$159$1$18 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:159: U8 B1_PRESSED(void)
;	-----------------------------------------
;	 function B1_PRESSED
;	-----------------------------------------
_B1_PRESSED:
	C$dev.h$161$1$20 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:161: if(B1_ON())
	mov	a,_P5
	jb	acc.0,00104$
	C$dev.h$163$2$21 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:163: Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w 
	mov	dptr,#0x0064
	lcall	_Delay_x100us
	C$dev.h$164$2$21 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:164: if(B1_ON())     return 1;   // Naci�ni�ty
	mov	a,_P5
	jb	acc.0,00104$
	mov	dpl,#0x01
	sjmp	00105$
00104$:
	C$dev.h$166$1$20 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:166: return 0;
	mov	dpl,#0x00
00105$:
	C$dev.h$167$1$20 ==.
	XG$B1_PRESSED$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'B2_PRESSED'
;------------------------------------------------------------
	G$B2_PRESSED$0$0 ==.
	C$dev.h$169$1$20 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:169: U8 B2_PRESSED(void)
;	-----------------------------------------
;	 function B2_PRESSED
;	-----------------------------------------
_B2_PRESSED:
	C$dev.h$171$1$23 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:171: if(B2_ON())
	mov	a,_P5
	jb	acc.1,00104$
	C$dev.h$173$2$24 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:173: Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w 
	mov	dptr,#0x0064
	lcall	_Delay_x100us
	C$dev.h$174$2$24 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:174: if(B2_ON())     return 1;   // Naci�ni�ty
	mov	a,_P5
	jb	acc.1,00104$
	mov	dpl,#0x01
	sjmp	00105$
00104$:
	C$dev.h$176$1$23 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:176: return 0;
	mov	dpl,#0x00
00105$:
	C$dev.h$177$1$23 ==.
	XG$B2_PRESSED$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'B3_PRESSED'
;------------------------------------------------------------
	G$B3_PRESSED$0$0 ==.
	C$dev.h$179$1$23 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:179: U8 B3_PRESSED(void)
;	-----------------------------------------
;	 function B3_PRESSED
;	-----------------------------------------
_B3_PRESSED:
	C$dev.h$181$1$26 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:181: if(B3_ON())
	mov	a,_P5
	jb	acc.2,00104$
	C$dev.h$183$2$27 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:183: Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w 
	mov	dptr,#0x0064
	lcall	_Delay_x100us
	C$dev.h$184$2$27 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:184: if(B3_ON())     return 1;   // Naci�ni�ty
	mov	a,_P5
	jb	acc.2,00104$
	mov	dpl,#0x01
	sjmp	00105$
00104$:
	C$dev.h$186$1$26 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:186: return 0;
	mov	dpl,#0x00
00105$:
	C$dev.h$187$1$26 ==.
	XG$B3_PRESSED$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'B4_PRESSED'
;------------------------------------------------------------
	G$B4_PRESSED$0$0 ==.
	C$dev.h$189$1$26 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:189: U8 B4_PRESSED(void)
;	-----------------------------------------
;	 function B4_PRESSED
;	-----------------------------------------
_B4_PRESSED:
	C$dev.h$191$1$29 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:191: if(B4_ON())
	mov	a,_P5
	jb	acc.3,00104$
	C$dev.h$193$2$30 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:193: Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w 
	mov	dptr,#0x0064
	lcall	_Delay_x100us
	C$dev.h$194$2$30 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:194: if(B4_ON())     return 1;   // Naci�ni�ty
	mov	a,_P5
	jb	acc.3,00104$
	mov	dpl,#0x01
	sjmp	00105$
00104$:
	C$dev.h$196$1$29 ==.
;	C:\Workspace\WSN_MS-4\/dev.h:196: return 0;
	mov	dpl,#0x00
00105$:
	C$dev.h$197$1$29 ==.
	XG$B4_PRESSED$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'UART_SetBaudRate'
;------------------------------------------------------------
;baudRate                  Allocated to registers r7 
;------------------------------------------------------------
	G$UART_SetBaudRate$0$0 ==.
	C$uart.h$16$1$29 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:16: void UART_SetBaudRate(U8 baudRate)
;	-----------------------------------------
;	 function UART_SetBaudRate
;	-----------------------------------------
_UART_SetBaudRate:
	mov	r7,dpl
	C$uart.h$18$1$32 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:18: switch(baudRate)
	cjne	r7,#0x01,00120$
	sjmp	00101$
00120$:
	cjne	r7,#0x02,00121$
	sjmp	00102$
00121$:
	cjne	r7,#0x03,00122$
	sjmp	00103$
00122$:
	C$uart.h$20$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:20: case UART_BAUDRATE_2400:	//?
	cjne	r7,#0x04,00106$
	sjmp	00104$
00101$:
	C$uart.h$21$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:21: CKCON &= 0xF4;
	anl	_CKCON,#0xF4
	C$uart.h$22$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:22: TH1    = 0x2B;
	mov	_TH1,#0x2B
	C$uart.h$23$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:23: break;
	C$uart.h$25$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:25: case UART_BAUDRATE_9600:
	sjmp	00106$
00102$:
	C$uart.h$26$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:26: CKCON &= 0xF4;
	anl	_CKCON,#0xF4
	C$uart.h$27$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:27: CKCON |= 0x10;
	orl	_CKCON,#0x10
	C$uart.h$28$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:28: TH1    = 0x96;
	mov	_TH1,#0x96
	C$uart.h$29$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:29: break;
	C$uart.h$31$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:31: case UART_BAUDRATE_56000:
	sjmp	00106$
00103$:
	C$uart.h$32$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:32: CKCON |= 0x08;
	orl	_CKCON,#0x08
	C$uart.h$33$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:33: TH1    = 0x25;
	mov	_TH1,#0x25
	C$uart.h$34$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:34: break;
	C$uart.h$36$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:36: case UART_BAUDRATE_115200:
	sjmp	00106$
00104$:
	C$uart.h$37$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:37: CKCON |= 0x08;
	orl	_CKCON,#0x08
	C$uart.h$38$2$33 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:38: TH1    = 0x96;
	mov	_TH1,#0x96
	C$uart.h$40$1$32 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:40: }
00106$:
	C$uart.h$41$1$32 ==.
	XG$UART_SetBaudRate$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putchar'
;------------------------------------------------------------
;c                         Allocated to registers r7 
;------------------------------------------------------------
	G$putchar$0$0 ==.
	C$uart.h$51$1$32 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:51: void putchar(S8 c)
;	-----------------------------------------
;	 function putchar
;	-----------------------------------------
_putchar:
	mov	r7,dpl
	C$uart.h$53$1$35 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:53: while (!TI0);
00101$:
	C$uart.h$54$1$35 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:54: TI0   = 0;
	jbc	_TI0,00112$
	sjmp	00101$
00112$:
	C$uart.h$55$1$35 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:55: SBUF0 = c;
	mov	_SBUF0,r7
	C$uart.h$56$1$35 ==.
	XG$putchar$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'getchar'
;------------------------------------------------------------
	G$getchar$0$0 ==.
	C$uart.h$63$1$35 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:63: S8 getchar(void)
;	-----------------------------------------
;	 function getchar
;	-----------------------------------------
_getchar:
	C$uart.h$65$1$37 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:65: while (!RI0);
00101$:
	C$uart.h$66$1$37 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:66: RI0 = 0;
	jbc	_RI0,00112$
	sjmp	00101$
00112$:
	C$uart.h$67$1$37 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:67: return SBUF0;
	mov	dpl,_SBUF0
	C$uart.h$68$1$37 ==.
	XG$getchar$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'printc'
;------------------------------------------------------------
;val                       Allocated to registers r7 
;------------------------------------------------------------
	G$printc$0$0 ==.
	C$uart.h$75$1$37 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:75: void printc(U8 val)
;	-----------------------------------------
;	 function printc
;	-----------------------------------------
_printc:
	C$uart.h$78$1$39 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:78: val += (val>0x09) ? 0x37 : 0x30;
	mov	a,dpl
	mov	r7,a
	add	a,#0xff - 0x09
	jnc	00103$
	mov	r6,#0x37
	sjmp	00104$
00103$:
	mov	r6,#0x30
00104$:
	mov	a,r6
	add	a,r7
	C$uart.h$79$1$39 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:79: putchar(val);    
	mov	dpl,a
	lcall	_putchar
	C$uart.h$80$1$39 ==.
	XG$printc$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'gethex'
;------------------------------------------------------------
;result                    Allocated to registers r7 
;tmp                       Allocated to registers r6 
;------------------------------------------------------------
	G$gethex$0$0 ==.
	C$uart.h$88$1$39 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:88: U8 gethex(void)
;	-----------------------------------------
;	 function gethex
;	-----------------------------------------
_gethex:
	C$uart.h$90$1$39 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:90: S8 result = 0;
	mov	r7,#0x00
	C$uart.h$93$1$41 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:93: while (!RI0);
00101$:
	C$uart.h$94$1$41 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:94: RI0 = 0;
	jbc	_RI0,00169$
	sjmp	00101$
00169$:
	C$uart.h$95$1$41 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:95: tmp = SBUF0;
	mov	r6,_SBUF0
	C$uart.h$97$1$41 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:97: if (tmp != 0x5C) {
	cjne	r6,#0x5C,00170$
	sjmp	00113$
00170$:
	C$uart.h$99$2$42 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:99: if (tmp > 0x60 && tmp < 0x67)
	clr	c
	mov	a,#(0x60 ^ 0x80)
	mov	b,r6
	xrl	b,#0x80
	subb	a,b
	jnc	00105$
	clr	c
	mov	a,r6
	xrl	a,#0x80
	subb	a,#0xe7
	jnc	00105$
	C$uart.h$100$2$42 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:100: tmp -= 0x20;
	mov	a,r6
	add	a,#0xE0
	mov	r6,a
00105$:
	C$uart.h$102$2$42 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:102: result = (tmp<0x40) ? (tmp-0x30)<<4 : (tmp-55)<<4;
	clr	c
	mov	a,r6
	xrl	a,#0x80
	subb	a,#0xc0
	jnc	00123$
	mov	a,r6
	add	a,#0xD0
	swap	a
	anl	a,#0xF0
	mov	r5,a
	sjmp	00124$
00123$:
	mov	a,r6
	add	a,#0xC9
	mov	r4,a
	swap	a
	anl	a,#0xF0
	mov	r5,a
00124$:
	mov	ar7,r5
	C$uart.h$104$2$42 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:104: while (!RI0);
00107$:
	C$uart.h$105$2$42 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:105: RI0 = 0;
	jbc	_RI0,00174$
	sjmp	00107$
00174$:
	C$uart.h$106$2$42 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:106: tmp = SBUF0;
	mov	r6,_SBUF0
	C$uart.h$108$2$42 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:108: if (tmp > 0x60 && tmp < 0x67)
	clr	c
	mov	a,#(0x60 ^ 0x80)
	mov	b,r6
	xrl	b,#0x80
	subb	a,b
	jnc	00111$
	clr	c
	mov	a,r6
	xrl	a,#0x80
	subb	a,#0xe7
	jnc	00111$
	C$uart.h$109$2$42 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:109: tmp -= 0x20;
	mov	a,r6
	add	a,#0xE0
	mov	r6,a
00111$:
	C$uart.h$111$2$42 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:111: result |= (tmp<0x40) ? (tmp-0x30) : (tmp-55);
	clr	c
	mov	a,r6
	xrl	a,#0x80
	subb	a,#0xc0
	jnc	00125$
	mov	a,r6
	add	a,#0xD0
	mov	r5,a
	sjmp	00126$
00125$:
	mov	a,r6
	add	a,#0xC9
	mov	r5,a
00126$:
	mov	a,r5
	orl	ar7,a
	C$uart.h$114$2$43 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:114: while (!RI0);
	sjmp	00120$
00113$:
	C$uart.h$115$2$43 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:115: RI0 = 0;
	jbc	_RI0,00178$
	sjmp	00113$
00178$:
	C$uart.h$117$2$43 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:117: if (SBUF0 == 'n')
	mov	a,#0x6E
	cjne	a,_SBUF0,00120$
	C$uart.h$118$2$43 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:118: return '\n';
	mov	dpl,#0x0A
	sjmp	00121$
00120$:
	C$uart.h$120$1$41 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:120: return result;
	mov	dpl,r7
00121$:
	C$uart.h$121$1$41 ==.
	XG$gethex$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'printBCD'
;------------------------------------------------------------
;precission                Allocated with name '_printBCD_PARM_2'
;value                     Allocated to registers r6 r7 
;i                         Allocated to registers r5 
;tmp                       Allocated with name '_printBCD_tmp_1_45'
;------------------------------------------------------------
	G$printBCD$0$0 ==.
	C$uart.h$132$1$41 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:132: void printBCD(S16 value, U8 precission)
;	-----------------------------------------
;	 function printBCD
;	-----------------------------------------
_printBCD:
	mov	r6,dpl
	mov	r7,dph
	C$uart.h$135$1$41 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:135: __bit  s = 0;
	clr	_printBCD_s_1_45
	C$uart.h$137$1$45 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:137: if (value < 0)
	mov	a,r7
	jnb	acc.7,00121$
	C$uart.h$138$1$45 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:138: putchar('-');
	mov	dpl,#0x2D
	push	ar7
	push	ar6
	lcall	_putchar
	pop	ar6
	pop	ar7
	C$uart.h$140$1$45 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:140: for (i = 4; i >= 0; --i) {
00121$:
	mov	r5,#0x04
00114$:
	C$uart.h$141$2$46 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:141: tmp[i] = (value%10);
	mov	a,r5
	add	a,#_printBCD_tmp_1_45
	mov	r1,a
	mov	__modsint_PARM_2,#0x0A
	mov	(__modsint_PARM_2 + 1),#0x00
	mov	dpl,r6
	mov	dph,r7
	push	ar7
	push	ar6
	push	ar5
	push	ar1
	lcall	__modsint
	mov	r3,dpl
	pop	ar1
	pop	ar5
	pop	ar6
	pop	ar7
	mov	@r1,ar3
	C$uart.h$142$1$45 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:142: value /= 10;
	mov	__divsint_PARM_2,#0x0A
	mov	(__divsint_PARM_2 + 1),#0x00
	mov	dpl,r6
	mov	dph,r7
	push	ar5
	lcall	__divsint
	mov	r6,dpl
	mov	r7,dph
	pop	ar5
	C$uart.h$140$1$45 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:140: for (i = 4; i >= 0; --i) {
	dec	r5
	mov	a,r5
	jnb	acc.7,00114$
	C$uart.h$145$1$45 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:145: for (i = 0; i < 5; ++i) {
	mov	r7,#0x00
00116$:
	C$uart.h$147$2$47 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:147: if (tmp[i] != 0)
	mov	a,r7
	add	a,#_printBCD_tmp_1_45
	mov	r1,a
	mov	a,@r1
	jz	00105$
	C$uart.h$148$2$47 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:148: s = 1;
	setb	_printBCD_s_1_45
00105$:
	C$uart.h$150$2$47 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:150: if ( (5-i) == precission) {
	mov	a,r7
	mov	r5,a
	rlc	a
	subb	a,acc
	mov	r6,a
	mov	a,#0x05
	clr	c
	subb	a,r5
	mov	r5,a
	clr	a
	subb	a,r6
	mov	r6,a
	mov	r3,_printBCD_PARM_2
	mov	r4,#0x00
	mov	a,r5
	cjne	a,ar3,00109$
	mov	a,r6
	cjne	a,ar4,00109$
	C$uart.h$151$3$48 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:151: if (!s)
	jb	_printBCD_s_1_45,00107$
	C$uart.h$152$3$48 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:152: putchar('0');
	mov	dpl,#0x30
	push	ar7
	lcall	_putchar
	pop	ar7
00107$:
	C$uart.h$153$3$48 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:153: putchar(',');
	mov	dpl,#0x2C
	push	ar7
	lcall	_putchar
	pop	ar7
00109$:
	C$uart.h$156$2$47 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:156: if (s || i == 4)
	jb	_printBCD_s_1_45,00110$
	cjne	r7,#0x04,00117$
00110$:
	C$uart.h$157$2$47 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:157: printc(tmp[i]);
	mov	a,r7
	add	a,#_printBCD_tmp_1_45
	mov	r1,a
	mov	dpl,@r1
	push	ar7
	lcall	_printc
	pop	ar7
00117$:
	C$uart.h$145$1$45 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:145: for (i = 0; i < 5; ++i) {
	inc	r7
	clr	c
	mov	a,r7
	xrl	a,#0x80
	subb	a,#0x85
	jc	00116$
	C$uart.h$159$1$45 ==.
	XG$printBCD$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'printf'
;------------------------------------------------------------
;string                    Allocated to stack - _bp -5
;ap                        Allocated to stack - _bp +1
;ival                      Allocated to registers r4 r3 
;precission                Allocated to registers r7 
;sloc0                     Allocated to stack - _bp +5
;sloc1                     Allocated to stack - _bp +6
;------------------------------------------------------------
	G$printf$0$0 ==.
	C$uart.h$167$1$45 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:167: void printf(const S8* string, ...)
;	-----------------------------------------
;	 function printf
;	-----------------------------------------
_printf:
	push	_bp
	mov	_bp,sp
	inc	sp
	C$uart.h$171$1$45 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:171: U8 precission = 0;
	mov	r7,#0x00
	C$uart.h$173$2$51 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:173: va_start(ap,string);
	mov	a,_bp
	add	a,#0xFB
	mov	r6,a
	mov	r0,_bp
	inc	r0
	mov	@r0,ar6
00114$:
	C$uart.h$175$1$50 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:175: for (; *string; ++string) {
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	mov	ar3,@r0
	inc	r0
	mov	ar4,@r0
	inc	r0
	mov	ar5,@r0
	mov	dpl,r3
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r2,a
	jnz	00142$
	ljmp	00116$
00142$:
	C$uart.h$176$2$52 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:176: if (*string == '%') {
	cjne	r2,#0x25,00143$
	sjmp	00144$
00143$:
	ljmp	00110$
00144$:
	C$uart.h$177$3$53 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:177: ++string;
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	mov	a,#0x01
	add	a,r3
	mov	@r0,a
	clr	a
	addc	a,r4
	inc	r0
	mov	@r0,a
	inc	r0
	mov	@r0,ar5
	C$uart.h$178$3$53 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:178: switch (*string) {
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	mov	ar3,@r0
	inc	r0
	mov	ar4,@r0
	inc	r0
	mov	ar5,@r0
	mov	dpl,r3
	mov	dph,r4
	mov	b,r5
	lcall	__gptrget
	mov	r6,a
	cjne	r6,#0x2E,00145$
	ljmp	00103$
00145$:
	cjne	r6,#0x58,00146$
	sjmp	00102$
00146$:
	cjne	r6,#0x64,00147$
	ljmp	00104$
00147$:
	cjne	r6,#0x78,00148$
	sjmp	00149$
00148$:
	ljmp	00105$
00149$:
	C$uart.h$181$1$50 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:181: ival = va_arg(ap, U16);
	push	ar7
	mov	r0,_bp
	inc	r0
	mov	a,@r0
	add	a,#0xFE
	mov	r7,a
	mov	r0,_bp
	inc	r0
	mov	@r0,ar7
	mov	ar1,r7
	mov	ar4,@r1
	inc	r1
	mov	ar3,@r1
	dec	r1
	C$uart.h$183$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:183: printc(ival>>4);
	mov	ar6,r4
	mov	a,r3
	swap	a
	xch	a,r6
	swap	a
	anl	a,#0x0F
	xrl	a,r6
	xch	a,r6
	anl	a,#0x0F
	xch	a,r6
	xrl	a,r6
	xch	a,r6
	mov	r7,a
	mov	dpl,r6
	push	ar7
	push	ar4
	push	ar3
	lcall	_printc
	pop	ar3
	pop	ar4
	pop	ar7
	C$uart.h$184$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:184: printc(ival&0x000F);
	mov	a,#0x0F
	anl	a,r4
	mov	r6,a
	mov	r7,#0x00
	mov	dpl,r6
	push	ar7
	lcall	_printc
	pop	ar7
	C$uart.h$186$1$50 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:186: break;
	pop	ar7
	ljmp	00115$
	C$uart.h$188$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:188: case 'X':
00102$:
	C$uart.h$190$1$50 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:190: ival = va_arg(ap, U16);
	push	ar7
	mov	r0,_bp
	inc	r0
	mov	a,@r0
	add	a,#0xFE
	mov	r6,a
	mov	r0,_bp
	inc	r0
	mov	@r0,ar6
	mov	ar1,r6
	mov	ar4,@r1
	inc	r1
	mov	ar3,@r1
	dec	r1
	C$uart.h$192$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:192: printc(ival>>12);
	mov	a,r3
	swap	a
	anl	a,#0x0F
	mov	r6,a
	mov	r7,#0x00
	mov	dpl,r6
	push	ar7
	push	ar4
	push	ar3
	lcall	_printc
	pop	ar3
	pop	ar4
	pop	ar7
	C$uart.h$193$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:193: printc((ival>>8)&0x000F);
	mov	ar7,r3
	mov	a,#0x0F
	anl	a,r7
	mov	dpl,a
	push	ar7
	push	ar4
	push	ar3
	lcall	_printc
	pop	ar3
	pop	ar4
	pop	ar7
	C$uart.h$194$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:194: printc((ival>>4)&0x000F);
	mov	ar6,r4
	mov	a,r3
	swap	a
	xch	a,r6
	swap	a
	anl	a,#0x0F
	xrl	a,r6
	xch	a,r6
	anl	a,#0x0F
	xch	a,r6
	xrl	a,r6
	xch	a,r6
	anl	ar6,#0x0F
	mov	r7,#0x00
	mov	dpl,r6
	push	ar7
	push	ar4
	push	ar3
	lcall	_printc
	pop	ar3
	pop	ar4
	pop	ar7
	C$uart.h$195$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:195: printc(ival&0x000F);
	mov	a,#0x0F
	anl	a,r4
	mov	r6,a
	mov	r7,#0x00
	mov	dpl,r6
	push	ar7
	lcall	_printc
	pop	ar7
	C$uart.h$197$1$50 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:197: break;
	pop	ar7
	ljmp	00115$
	C$uart.h$199$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:199: case '.':
00103$:
	C$uart.h$201$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:201: ++string;
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	mov	a,#0x01
	add	a,r3
	mov	@r0,a
	clr	a
	addc	a,r4
	inc	r0
	mov	@r0,a
	inc	r0
	mov	@r0,ar5
	C$uart.h$202$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:202: precission = (U16) *string-'0';
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	mov	ar5,@r0
	inc	r0
	mov	ar4,@r0
	inc	r0
	mov	ar2,@r0
	mov	dpl,r5
	mov	dph,r4
	mov	b,r2
	lcall	__gptrget
	mov	r3,a
	rlc	a
	subb	a,acc
	mov	r6,a
	mov	a,r3
	add	a,#0xD0
	mov	r7,a
	C$uart.h$203$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:203: ++string;
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	mov	a,#0x01
	add	a,r5
	mov	@r0,a
	clr	a
	addc	a,r4
	inc	r0
	mov	@r0,a
	inc	r0
	mov	@r0,ar2
	C$uart.h$205$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:205: case 'd':
00104$:
	C$uart.h$207$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:207: ival = va_arg(ap, S16);
	mov	r0,_bp
	inc	r0
	mov	a,@r0
	add	a,#0xFE
	mov	r6,a
	mov	r0,_bp
	inc	r0
	mov	@r0,ar6
	mov	ar1,r6
	mov	ar4,@r1
	inc	r1
	mov	ar3,@r1
	dec	r1
	C$uart.h$209$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:209: printBCD(ival, precission);
	mov	_printBCD_PARM_2,r7
	mov	dpl,r4
	mov	dph,r3
	push	ar7
	lcall	_printBCD
	pop	ar7
	C$uart.h$211$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:211: break;
	C$uart.h$213$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:213: default:
	sjmp	00115$
00105$:
	C$uart.h$214$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:214: ival = va_arg(ap, U16);
	mov	r0,_bp
	inc	r0
	mov	a,@r0
	add	a,#0xFE
	mov	r6,a
	mov	r0,_bp
	inc	r0
	mov	@r0,ar6
	mov	ar1,r6
	mov	ar4,@r1
	inc	r1
	mov	ar3,@r1
	dec	r1
	C$uart.h$216$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:216: printc(ival>>4);
	mov	ar5,r4
	mov	a,r3
	swap	a
	xch	a,r5
	swap	a
	anl	a,#0x0F
	xrl	a,r5
	xch	a,r5
	anl	a,#0x0F
	xch	a,r5
	xrl	a,r5
	xch	a,r5
	mov	dpl,r5
	push	ar7
	push	ar4
	push	ar3
	lcall	_printc
	pop	ar3
	pop	ar4
	C$uart.h$217$4$54 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:217: printc(ival&0x000F);
	mov	a,#0x0F
	anl	a,r4
	mov	dpl,a
	lcall	_printc
	pop	ar7
	C$uart.h$218$2$52 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:218: }
	sjmp	00115$
00110$:
	C$uart.h$220$3$55 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:220: if (*string == '\n')
	cjne	r2,#0x0A,00108$
	C$uart.h$221$3$55 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:221: putchar(0x0D);
	mov	dpl,#0x0D
	push	ar7
	lcall	_putchar
	pop	ar7
00108$:
	C$uart.h$223$3$55 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:223: putchar(*string);
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	mov	ar4,@r0
	inc	r0
	mov	ar5,@r0
	inc	r0
	mov	ar6,@r0
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	__gptrget
	mov	dpl,a
	push	ar7
	lcall	_putchar
	pop	ar7
00115$:
	C$uart.h$175$1$50 ==.
;	C:\Workspace\WSN_MS-4\/uart.h:175: for (; *string; ++string) {
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	inc	@r0
	cjne	@r0,#0x00,00152$
	inc	r0
	inc	@r0
00152$:
	ljmp	00114$
00116$:
	dec	sp
	pop	_bp
	C$uart.h$226$1$50 ==.
	XG$printf$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'crc8'
;------------------------------------------------------------
;crc                       Allocated with name '_crc8_PARM_2'
;len                       Allocated with name '_crc8_PARM_3'
;data                      Allocated to registers r5 r6 r7 
;i                         Allocated to registers r3 r4 
;j                         Allocated to registers r1 r2 
;------------------------------------------------------------
	G$crc8$0$0 ==.
	C$crc8.h$11$1$50 ==.
;	C:\Workspace\WSN_MS-4\/crc8.h:11: U8 crc8(U8* data, U8 crc, U16 len) {
;	-----------------------------------------
;	 function crc8
;	-----------------------------------------
_crc8:
	mov	r5,dpl
	mov	r6,dph
	mov	r7,b
	C$crc8.h$15$1$57 ==.
;	C:\Workspace\WSN_MS-4\/crc8.h:15: for (i = 0; i < len; i++) {
	mov	r3,#0x00
	mov	r4,#0x00
00109$:
	clr	c
	mov	a,r3
	subb	a,_crc8_PARM_3
	mov	a,r4
	subb	a,(_crc8_PARM_3 + 1)
	jnc	00105$
	C$crc8.h$16$2$58 ==.
;	C:\Workspace\WSN_MS-4\/crc8.h:16: crc ^= data[i];
	mov	a,r3
	add	a,r5
	mov	r0,a
	mov	a,r4
	addc	a,r6
	mov	r1,a
	mov	ar2,r7
	mov	dpl,r0
	mov	dph,r1
	mov	b,r2
	lcall	__gptrget
	mov	r0,a
	xrl	_crc8_PARM_2,a
	C$crc8.h$17$1$57 ==.
;	C:\Workspace\WSN_MS-4\/crc8.h:17: for (j = 0; j < 8; j++) {
	mov	r1,#0x00
	mov	r2,#0x00
00106$:
	C$crc8.h$18$3$59 ==.
;	C:\Workspace\WSN_MS-4\/crc8.h:18: if ((crc & 0x80) != 0)
	mov	a,_crc8_PARM_2
	jnb	acc.7,00102$
	C$crc8.h$19$3$59 ==.
;	C:\Workspace\WSN_MS-4\/crc8.h:19: crc = (crc << 1) ^ crc8polynom; 
	mov	a,_crc8_PARM_2
	add	a,_crc8_PARM_2
	mov	r0,a
	mov	a,#0x07
	xrl	a,r0
	mov	_crc8_PARM_2,a
	sjmp	00107$
00102$:
	C$crc8.h$21$3$59 ==.
;	C:\Workspace\WSN_MS-4\/crc8.h:21: crc <<=1;
	mov	a,_crc8_PARM_2
	add	a,_crc8_PARM_2
	mov	_crc8_PARM_2,a
00107$:
	C$crc8.h$17$2$58 ==.
;	C:\Workspace\WSN_MS-4\/crc8.h:17: for (j = 0; j < 8; j++) {
	inc	r1
	cjne	r1,#0x00,00132$
	inc	r2
00132$:
	clr	c
	mov	a,r1
	subb	a,#0x08
	mov	a,r2
	subb	a,#0x00
	jc	00106$
	C$crc8.h$15$1$57 ==.
;	C:\Workspace\WSN_MS-4\/crc8.h:15: for (i = 0; i < len; i++) {
	inc	r3
	cjne	r3,#0x00,00109$
	inc	r4
	sjmp	00109$
00105$:
	C$crc8.h$25$1$57 ==.
;	C:\Workspace\WSN_MS-4\/crc8.h:25: return crc;
	mov	dpl,_crc8_PARM_2
	C$crc8.h$26$1$57 ==.
	XG$crc8$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_WriteToRegister'
;------------------------------------------------------------
;value                     Allocated with name '_CC1000_WriteToRegister_PARM_2'
;address                   Allocated to registers r7 
;BitCnt                    Allocated to registers r6 
;i                         Allocated to registers r5 
;------------------------------------------------------------
	G$CC1000_WriteToRegister$0$0 ==.
	C$cc1000.h$149$1$57 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:149: void CC1000_WriteToRegister(U8 address, U8 value)   
;	-----------------------------------------
;	 function CC1000_WriteToRegister
;	-----------------------------------------
_CC1000_WriteToRegister:
	mov	r7,dpl
	C$cc1000.h$153$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:153: P0MDOUT |= 0x10;                // Wyj�cie P0.4 (PDATA) jako push-pull
	orl	_P0MDOUT,#0x10
	C$cc1000.h$154$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:154: PCLK = 1;
	setb	_PCLK
	C$cc1000.h$155$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:155: PALE = 1;
	setb	_PALE
	C$cc1000.h$156$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:156: address = (address<<1)|0x01;    // W lewo bo adr. 7-bit; (LSB = 1 to zapis)
	mov	a,r7
	add	a,r7
	mov	r6,a
	mov	a,#0x01
	orl	a,r6
	mov	r7,a
	C$cc1000.h$157$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:157: PALE = 0;                               // Przygotowanie zapisu adresu
	clr	_PALE
	C$cc1000.h$158$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:158: for (BitCnt = 0; BitCnt < 8; BitCnt++)	// Wysy�anie adresu po jednym bicie (szeregowo)
	mov	r6,#0x00
00115$:
	C$cc1000.h$160$2$82 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:160: PCLK = 1;
	setb	_PCLK
	C$cc1000.h$161$2$82 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:161: if (address & 0x80)         // Test MSB 
	mov	a,r7
	jnb	acc.7,00102$
	C$cc1000.h$162$2$82 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:162: PDATA = 1;
	setb	_PDATA
	sjmp	00103$
00102$:
	C$cc1000.h$164$2$82 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:164: PDATA = 0;
	clr	_PDATA
00103$:
	C$cc1000.h$165$2$82 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:165: address = address<<1;		// Nast�pny bit  
	mov	a,r7
	add	a,r7
	mov	r7,a
	C$cc1000.h$166$2$82 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:166: PCLK = 0;                   // Zapis bitu
	clr	_PCLK
	C$cc1000.h$167$2$82 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:167: for (i=0; i<0x0F; i++);     // Op�nienie
	mov	r5,#0x0F
00114$:
	mov	ar4,r5
	mov	a,r4
	dec	a
	mov	r5,a
	jnz	00114$
	C$cc1000.h$158$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:158: for (BitCnt = 0; BitCnt < 8; BitCnt++)	// Wysy�anie adresu po jednym bicie (szeregowo)
	inc	r6
	cjne	r6,#0x08,00167$
00167$:
	jc	00115$
	C$cc1000.h$169$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:169: PCLK = 1;
	setb	_PCLK
	C$cc1000.h$170$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:170: for (i=0; i<0x0F; i++);         // Op�nienie
	mov	r5,#0x0F
00119$:
	mov	ar7,r5
	mov	a,r7
	dec	a
	mov	r5,a
	jnz	00119$
	C$cc1000.h$171$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:171: PALE = 1;                               // Przygotowanie zapisu danej
	setb	_PALE
	C$cc1000.h$172$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:172: for (BitCnt = 0; BitCnt < 8; BitCnt++)	// wysy�anie danych po jednym bicie
	mov	r7,#0x00
00123$:
	C$cc1000.h$174$2$83 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:174: PCLK = 1;
	setb	_PCLK
	C$cc1000.h$175$2$83 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:175: if (value & 0x80)           // Test MSB 
	mov	a,_CC1000_WriteToRegister_PARM_2
	jnb	acc.7,00108$
	C$cc1000.h$176$2$83 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:176: PDATA = 1;
	setb	_PDATA
	sjmp	00109$
00108$:
	C$cc1000.h$178$2$83 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:178: PDATA = 0;
	clr	_PDATA
00109$:
	C$cc1000.h$179$2$83 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:179: value = value<<1;		    // Nast�pny bit
	mov	a,_CC1000_WriteToRegister_PARM_2
	add	a,_CC1000_WriteToRegister_PARM_2
	mov	_CC1000_WriteToRegister_PARM_2,a
	C$cc1000.h$180$2$83 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:180: PCLK = 0;
	clr	_PCLK
	C$cc1000.h$181$2$83 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:181: for (i=0; i<0x0F; i++);     // Op�nienie
	mov	r5,#0x0F
00122$:
	mov	ar6,r5
	mov	a,r6
	dec	a
	mov	r5,a
	jnz	00122$
	C$cc1000.h$172$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:172: for (BitCnt = 0; BitCnt < 8; BitCnt++)	// wysy�anie danych po jednym bicie
	inc	r7
	cjne	r7,#0x08,00172$
00172$:
	jc	00123$
	C$cc1000.h$183$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:183: PCLK  = 1;
	setb	_PCLK
	C$cc1000.h$184$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:184: PDATA = 1;  
	setb	_PDATA
	C$cc1000.h$185$1$81 ==.
	XG$CC1000_WriteToRegister$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_ReadFromRegister'
;------------------------------------------------------------
;address                   Allocated to registers r7 
;Value                     Allocated to registers r6 
;BitCnt                    Allocated to registers r5 
;i                         Allocated to registers r4 
;------------------------------------------------------------
	G$CC1000_ReadFromRegister$0$0 ==.
	C$cc1000.h$190$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:190: U8 CC1000_ReadFromRegister(U8 address) 
;	-----------------------------------------
;	 function CC1000_ReadFromRegister
;	-----------------------------------------
_CC1000_ReadFromRegister:
	mov	r7,dpl
	C$cc1000.h$192$1$81 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:192: U8 Value = 0;
	mov	r6,#0x00
	C$cc1000.h$195$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:195: P0MDOUT |= 0x10;            // Wyj�cie P0.4 (PDATA) jako push-pull
	orl	_P0MDOUT,#0x10
	C$cc1000.h$196$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:196: PCLK = 1;
	setb	_PCLK
	C$cc1000.h$197$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:197: PALE = 1;
	setb	_PALE
	C$cc1000.h$198$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:198: address = (address<<1)&0xFE;	        // Adres 7-bit; (LSB = 0 - odczyt);
	mov	a,r7
	add	a,r7
	mov	r5,a
	mov	a,#0xFE
	anl	a,r5
	mov	r7,a
	C$cc1000.h$199$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:199: PALE = 0;
	clr	_PALE
	C$cc1000.h$200$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:200: for (BitCnt = 0; BitCnt < 8; BitCnt++)	// Wysy�anie adresu po jednym bicie (szeregowo)
	mov	r5,#0x00
00115$:
	C$cc1000.h$202$2$86 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:202: PCLK = 1;
	setb	_PCLK
	C$cc1000.h$203$2$86 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:203: if (address & 0x80)
	mov	a,r7
	jnb	acc.7,00102$
	C$cc1000.h$204$2$86 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:204: PDATA = 1; 
	setb	_PDATA
	sjmp	00103$
00102$:
	C$cc1000.h$206$2$86 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:206: PDATA = 0; 
	clr	_PDATA
00103$:
	C$cc1000.h$207$2$86 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:207: address = address<<1;   // Nast�pny bit
	mov	a,r7
	add	a,r7
	mov	r7,a
	C$cc1000.h$208$2$86 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:208: PCLK = 0;
	clr	_PCLK
	C$cc1000.h$209$2$86 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:209: for (i=0; i<0x0F; i++); // Op�nienie
	mov	r4,#0x0F
00114$:
	mov	ar3,r4
	mov	a,r3
	dec	a
	mov	r4,a
	jnz	00114$
	C$cc1000.h$200$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:200: for (BitCnt = 0; BitCnt < 8; BitCnt++)	// Wysy�anie adresu po jednym bicie (szeregowo)
	inc	r5
	cjne	r5,#0x08,00167$
00167$:
	jc	00115$
	C$cc1000.h$211$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:211: PCLK = 1;
	setb	_PCLK
	C$cc1000.h$212$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:212: for (i=0; i<0x0F; i++);     // Op�nienie
	mov	r4,#0x0F
00119$:
	mov	ar7,r4
	mov	a,r7
	dec	a
	mov	r4,a
	jnz	00119$
	C$cc1000.h$213$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:213: PALE  = 1;
	setb	_PALE
	C$cc1000.h$214$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:214: P0MDOUT &= 0xEF;            // Port P0.4 (PDATA) jako open-drain (wej�cie)
	anl	_P0MDOUT,#0xEF
	C$cc1000.h$215$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:215: PDATA = 1;                  // Wa�ne aby PDATA by�o wej�ciem
	setb	_PDATA
	C$cc1000.h$216$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:216: for (BitCnt = 0; BitCnt < 8; BitCnt++)	
	mov	r7,#0x00
00123$:
	C$cc1000.h$218$2$87 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:218: PCLK = 0;
	clr	_PCLK
	C$cc1000.h$219$2$87 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:219: Value = Value<<1;
	mov	a,r6
	add	a,r6
	mov	r6,a
	C$cc1000.h$220$2$87 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:220: if (PDATA) 
	jnb	_PDATA,00108$
	C$cc1000.h$221$2$87 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:221: Value |= 0x01;  
	orl	ar6,#0x01
	sjmp	00109$
00108$:
	C$cc1000.h$223$2$87 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:223: Value &= 0xFE;  
	anl	ar6,#0xFE
00109$:
	C$cc1000.h$224$2$87 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:224: PCLK = 1;
	setb	_PCLK
	C$cc1000.h$225$2$87 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:225: for (i=0; i<0x0F; i++); // Op�nienie
	mov	r4,#0x0F
00122$:
	mov	ar5,r4
	mov	a,r5
	dec	a
	mov	r4,a
	jnz	00122$
	C$cc1000.h$216$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:216: for (BitCnt = 0; BitCnt < 8; BitCnt++)	
	inc	r7
	cjne	r7,#0x08,00172$
00172$:
	jc	00123$
	C$cc1000.h$227$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:227: PDATA = 1;
	setb	_PDATA
	C$cc1000.h$228$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:228: return Value;
	mov	dpl,r6
	C$cc1000.h$229$1$85 ==.
	XG$CC1000_ReadFromRegister$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'BitReceive'
;------------------------------------------------------------
	G$BitReceive$0$0 ==.
	C$cc1000.h$239$1$85 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:239: U8 BitReceive(void) // Odczyt przy zboczu narastaj�cym 
;	-----------------------------------------
;	 function BitReceive
;	-----------------------------------------
_BitReceive:
	C$cc1000.h$241$1$89 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:241: while(DCLK);    // Czekaj a� DCLK zmieni stan z "1" -> "0"
00101$:
	jb	_DCLK,00101$
	C$cc1000.h$242$1$89 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:242: while(!DCLK);   // Czekaj a� DCLK zmieni stan z "0" -> "1" 
00104$:
	jnb	_DCLK,00104$
	C$cc1000.h$244$1$89 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:244: return DIO;     // Odczyt przy zboczu narastaj�cym
	mov	c,_DIO
	clr	a
	rlc	a
	mov	dpl,a
	C$cc1000.h$245$1$89 ==.
	XG$BitReceive$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_ReceiveByte'
;------------------------------------------------------------
;i                         Allocated to registers r6 
;DATA                      Allocated to registers r7 
;------------------------------------------------------------
	G$CC1000_ReceiveByte$0$0 ==.
	C$cc1000.h$248$1$89 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:248: U8 CC1000_ReceiveByte(void) 
;	-----------------------------------------
;	 function CC1000_ReceiveByte
;	-----------------------------------------
_CC1000_ReceiveByte:
	C$cc1000.h$250$1$89 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:250: U8 i, DATA = 0;
	mov	r7,#0x00
	C$cc1000.h$251$1$91 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:251: for(i=0; i<8; i++) {
	mov	r6,#0x08
00106$:
	C$cc1000.h$252$2$92 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:252: DATA >>= 1;
	mov	a,r7
	clr	c
	rrc	a
	mov	r7,a
	C$cc1000.h$253$2$92 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:253: if(!BitReceive()) DATA |= 0x80;     // Odbi�r bit�w z negacj�
	push	ar7
	push	ar6
	lcall	_BitReceive
	mov	a,dpl
	pop	ar6
	pop	ar7
	jnz	00102$
	orl	ar7,#0x80
00102$:
	mov	ar5,r6
	mov	a,r5
	dec	a
	C$cc1000.h$251$2$92 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:251: for(i=0; i<8; i++) {
	mov	r6,a
	jnz	00106$
	C$cc1000.h$256$1$91 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:256: return DATA;
	mov	dpl,r7
	C$cc1000.h$257$1$91 ==.
	XG$CC1000_ReceiveByte$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'BitSend'
;------------------------------------------------------------
;bit0                      Allocated to registers r7 
;------------------------------------------------------------
	G$BitSend$0$0 ==.
	C$cc1000.h$260$1$91 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:260: void BitSend(U8 bit0)   // Zapis przy zboczu opadaj�cym 
;	-----------------------------------------
;	 function BitSend
;	-----------------------------------------
_BitSend:
	mov	r7,dpl
	C$cc1000.h$262$1$94 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:262: while(!DCLK);   // Czekaj a� DCLK zmieni stan z "0" -> "1"
00101$:
	jnb	_DCLK,00101$
	C$cc1000.h$263$1$94 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:263: while(DCLK);    // Czekaj a� DCLK zmieni stan z "1" -> "0"
00104$:
	jb	_DCLK,00104$
	C$cc1000.h$264$1$94 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:264: if(bit0) DIO = 1; else DIO = 0; // Zapis przy zboczu opadaj�cym
	mov	a,r7
	jz	00108$
	setb	_DIO
	sjmp	00110$
00108$:
	clr	_DIO
00110$:
	C$cc1000.h$265$1$94 ==.
	XG$BitSend$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_SendByte'
;------------------------------------------------------------
;byte                      Allocated to registers r7 
;i                         Allocated to registers r6 
;------------------------------------------------------------
	G$CC1000_SendByte$0$0 ==.
	C$cc1000.h$268$1$94 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:268: void CC1000_SendByte(U8 byte) 
;	-----------------------------------------
;	 function CC1000_SendByte
;	-----------------------------------------
_CC1000_SendByte:
	mov	r7,dpl
	C$cc1000.h$271$1$96 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:271: for(i=0; i<8; i++) {
	mov	r6,#0x00
00102$:
	C$cc1000.h$272$2$97 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:272: BitSend(byte & 0x01);
	mov	a,#0x01
	anl	a,r7
	mov	dpl,a
	push	ar7
	push	ar6
	lcall	_BitSend
	pop	ar6
	pop	ar7
	C$cc1000.h$273$2$97 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:273: byte >>= 1;
	mov	a,r7
	clr	c
	rrc	a
	mov	r7,a
	C$cc1000.h$271$1$96 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:271: for(i=0; i<8; i++) {
	inc	r6
	cjne	r6,#0x08,00110$
00110$:
	jc	00102$
	C$cc1000.h$275$1$96 ==.
	XG$CC1000_SendByte$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_Reset'
;------------------------------------------------------------
;MainValue                 Allocated to registers r7 
;------------------------------------------------------------
	G$CC1000_Reset$0$0 ==.
	C$cc1000.h$284$1$96 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:284: void CC1000_Reset(void)
;	-----------------------------------------
;	 function CC1000_Reset
;	-----------------------------------------
_CC1000_Reset:
	C$cc1000.h$288$1$99 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:288: MainValue = CC1000_ReadFromRegister(CC1000_MAIN);
	mov	dpl,#0x00
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	C$cc1000.h$289$1$99 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:289: CC1000_WriteToRegister(CC1000_MAIN,MainValue & 0xFE);   
	mov	a,#0xFE
	anl	a,r7
	mov	_CC1000_WriteToRegister_PARM_2,a
	mov	dpl,#0x00
	push	ar7
	lcall	_CC1000_WriteToRegister
	pop	ar7
	C$cc1000.h$290$1$99 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:290: CC1000_WriteToRegister(CC1000_MAIN,MainValue | 0x01);  
	mov	a,#0x01
	orl	a,r7
	mov	_CC1000_WriteToRegister_PARM_2,a
	mov	dpl,#0x00
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$291$1$99 ==.
	XG$CC1000_Reset$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_Calibrate'
;------------------------------------------------------------
;TimeOutCounter            Allocated to registers r6 r7 
;------------------------------------------------------------
	G$CC1000_Calibrate$0$0 ==.
	C$cc1000.h$296$1$99 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:296: U8 CC1000_Calibrate(void)   
;	-----------------------------------------
;	 function CC1000_Calibrate
;	-----------------------------------------
_CC1000_Calibrate:
	C$cc1000.h$299$1$101 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:299: printf("\nKalibracja...");
	mov	a,#__str_0
	push	acc
	mov	a,#(__str_0 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	dec	sp
	dec	sp
	dec	sp
	C$cc1000.h$301$1$101 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:301: CC1000_WriteToRegister(CC1000_PA_POW,0x00);
	mov	_CC1000_WriteToRegister_PARM_2,#0x00
	mov	dpl,#0x0B
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$302$1$101 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:302: CC1000_WriteToRegister(CC1000_CAL,0xA6);    // Start kalibracji                                    
	mov	_CC1000_WriteToRegister_PARM_2,#0xA6
	mov	dpl,#0x0E
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$303$1$101 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:303: Delay_x100us(200);
	mov	dptr,#0x00C8
	lcall	_Delay_x100us
	C$cc1000.h$305$1$101 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:305: for (TimeOutCounter=CC1000_CAL_TIMEOUT; ((CC1000_ReadFromRegister(CC1000_CAL)&0x08)==0)&&(TimeOutCounter>0); TimeOutCounter--);        
	mov	r6,#0xFE
	mov	r7,#0x7F
00105$:
	mov	dpl,#0x0E
	push	ar7
	push	ar6
	lcall	_CC1000_ReadFromRegister
	mov	a,dpl
	pop	ar6
	pop	ar7
	anl	a,#0x08
	mov	r5,a
	jz	00136$
	sjmp	00122$
00136$:
	mov	a,r6
	orl	a,r7
	jz	00122$
	dec	r6
	cjne	r6,#0xFF,00138$
	dec	r7
00138$:
	C$cc1000.h$307$1$101 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:307: for (TimeOutCounter=CC1000_LOCK_TIMEOUT; ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0)&&(TimeOutCounter>0); TimeOutCounter--);
	sjmp	00105$
00122$:
	mov	r6,#0xFE
	mov	r7,#0x7F
00109$:
	mov	dpl,#0x0D
	push	ar7
	push	ar6
	lcall	_CC1000_ReadFromRegister
	mov	a,dpl
	pop	ar6
	pop	ar7
	anl	a,#0x01
	mov	r5,a
	jz	00140$
	sjmp	00102$
00140$:
	mov	a,r6
	orl	a,r7
	jz	00102$
	dec	r6
	cjne	r6,#0xFF,00142$
	dec	r7
00142$:
	sjmp	00109$
00102$:
	C$cc1000.h$308$1$101 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:308: CC1000_WriteToRegister(CC1000_CAL, 0x26);     // Zako�czenie kalibracji
	mov	_CC1000_WriteToRegister_PARM_2,#0x26
	mov	dpl,#0x0E
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$309$1$101 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:309: CC1000_WriteToRegister(CC1000_PA_POW, CC1000_PA_VALUE);    // W��czenie PA
	mov	_CC1000_WriteToRegister_PARM_2,#0xF0
	mov	dpl,#0x0B
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$310$1$101 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:310: return ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01) == 1);
	mov	dpl,#0x0D
	lcall	_CC1000_ReadFromRegister
	mov	a,dpl
	anl	a,#0x01
	mov	r7,a
	clr	a
	cjne	r7,#0x01,00143$
	inc	a
00143$:
	mov	dpl,a
	C$cc1000.h$311$1$101 ==.
	XG$CC1000_Calibrate$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_SetupRX'
;------------------------------------------------------------
;RXCurrent                 Allocated to registers r7 
;i                         Allocated to registers r6 r7 
;lock_status               Allocated to registers r7 
;------------------------------------------------------------
	G$CC1000_SetupRX$0$0 ==.
	C$cc1000.h$316$1$101 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:316: U8 CC1000_SetupRX(U8 RXCurrent) 
;	-----------------------------------------
;	 function CC1000_SetupRX
;	-----------------------------------------
_CC1000_SetupRX:
	mov	r7,dpl
	C$cc1000.h$321$1$103 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:321: CC1000_WriteToRegister(CC1000_MAIN,0x11);           // Prze��cz. do RX, cz�stotl. rej. A
	mov	_CC1000_WriteToRegister_PARM_2,#0x11
	mov	dpl,#0x00
	push	ar7
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$322$1$103 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:322: CC1000_WriteToRegister(CC1000_PLL, CC1000_RX_PLL); 
	mov	_CC1000_WriteToRegister_PARM_2,#0x58
	mov	dpl,#0x0C
	lcall	_CC1000_WriteToRegister
	pop	ar7
	C$cc1000.h$323$1$103 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:323: CC1000_WriteToRegister(CC1000_CURRENT, RXCurrent); 
	mov	_CC1000_WriteToRegister_PARM_2,r7
	mov	dpl,#0x09
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$324$1$103 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:324: Delay_x100us(3);                                    // Oczekiwanie 300us 
	mov	dptr,#0x0003
	lcall	_Delay_x100us
	C$cc1000.h$326$1$103 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:326: for (i=CC1000_LOCK_TIMEOUT; ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0)&&(i>0); i--);
	mov	r6,#0xFE
	mov	r7,#0x7F
00110$:
	mov	dpl,#0x0D
	push	ar7
	push	ar6
	lcall	_CC1000_ReadFromRegister
	mov	a,dpl
	pop	ar6
	pop	ar7
	anl	a,#0x01
	mov	r5,a
	jnz	00101$
	mov	a,r6
	orl	a,r7
	jz	00101$
	dec	r6
	cjne	r6,#0xFF,00134$
	dec	r7
00134$:
	sjmp	00110$
00101$:
	C$cc1000.h$327$1$103 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:327: if ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0x01) 
	mov	dpl,#0x0D
	lcall	_CC1000_ReadFromRegister
	mov	a,dpl
	anl	a,#0x01
	mov	r7,a
	cjne	r7,#0x01,00106$
	C$cc1000.h$328$1$103 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:328: lock_status = CC1000_LOCK_OK;  // PLL zamkni�ta
	mov	r7,#0x01
	sjmp	00107$
00106$:
	C$cc1000.h$331$2$104 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:331: if (CC1000_Calibrate()) // Je�li kalibracja z sukcesem
	lcall	_CC1000_Calibrate
	mov	a,dpl
	jz	00103$
	C$cc1000.h$332$2$104 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:332: lock_status = CC1000_LOCK_RECAL_OK;  
	mov	r7,#0x02
	sjmp	00107$
00103$:
	C$cc1000.h$335$3$105 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:335: CC1000_ResetFreqSynth();
	lcall	_CC1000_ResetFreqSynth
	C$cc1000.h$336$3$105 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:336: lock_status = CC1000_LOCK_NOK; 
	mov	r7,#0x00
00107$:
	C$cc1000.h$339$1$103 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:339: return (lock_status);  // zwrot statusu syntezatora
	mov	dpl,r7
	C$cc1000.h$340$1$103 ==.
	XG$CC1000_SetupRX$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_SetupTX'
;------------------------------------------------------------
;TXCurrent                 Allocated to registers r7 
;i                         Allocated to registers r6 r7 
;lock_status               Allocated to registers r7 
;------------------------------------------------------------
	G$CC1000_SetupTX$0$0 ==.
	C$cc1000.h$345$1$103 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:345: U8 CC1000_SetupTX(U8 TXCurrent) 
;	-----------------------------------------
;	 function CC1000_SetupTX
;	-----------------------------------------
_CC1000_SetupTX:
	mov	r7,dpl
	C$cc1000.h$350$1$107 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:350: CC1000_WriteToRegister(CC1000_PA_POW,0x00); // Wy��czenie PA 
	mov	_CC1000_WriteToRegister_PARM_2,#0x00
	mov	dpl,#0x0B
	push	ar7
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$351$1$107 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:351: CC1000_WriteToRegister(CC1000_MAIN,0xE1);   // Prze�. do TX, do cz�stotl. rej. B
	mov	_CC1000_WriteToRegister_PARM_2,#0xE1
	mov	dpl,#0x00
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$352$1$107 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:352: CC1000_WriteToRegister(CC1000_PLL, CC1000_TX_PLL);
	mov	_CC1000_WriteToRegister_PARM_2,#0x58
	mov	dpl,#0x0C
	lcall	_CC1000_WriteToRegister
	pop	ar7
	C$cc1000.h$353$1$107 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:353: CC1000_WriteToRegister(CC1000_CURRENT, TXCurrent);
	mov	_CC1000_WriteToRegister_PARM_2,r7
	mov	dpl,#0x09
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$354$1$107 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:354: Delay_x100us(3);                            // Czekaj 250us
	mov	dptr,#0x0003
	lcall	_Delay_x100us
	C$cc1000.h$356$1$107 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:356: for (i=CC1000_LOCK_TIMEOUT; ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0)&&(i>0); i--);
	mov	r6,#0xFE
	mov	r7,#0x7F
00110$:
	mov	dpl,#0x0D
	push	ar7
	push	ar6
	lcall	_CC1000_ReadFromRegister
	mov	a,dpl
	pop	ar6
	pop	ar7
	anl	a,#0x01
	mov	r5,a
	jnz	00101$
	mov	a,r6
	orl	a,r7
	jz	00101$
	dec	r6
	cjne	r6,#0xFF,00134$
	dec	r7
00134$:
	sjmp	00110$
00101$:
	C$cc1000.h$357$1$107 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:357: if ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0x01)
	mov	dpl,#0x0D
	lcall	_CC1000_ReadFromRegister
	mov	a,dpl
	anl	a,#0x01
	mov	r7,a
	cjne	r7,#0x01,00106$
	C$cc1000.h$358$1$107 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:358: lock_status = CC1000_LOCK_OK; // PLL stabilna
	mov	r7,#0x01
	sjmp	00107$
00106$:
	C$cc1000.h$361$2$108 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:361: if (CC1000_Calibrate()) // Je�li kalibracja poprawna
	lcall	_CC1000_Calibrate
	mov	a,dpl
	jz	00103$
	C$cc1000.h$362$2$108 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:362: lock_status = CC1000_LOCK_RECAL_OK; 
	mov	r7,#0x02
	sjmp	00107$
00103$:
	C$cc1000.h$365$3$109 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:365: CC1000_ResetFreqSynth();
	lcall	_CC1000_ResetFreqSynth
	C$cc1000.h$366$3$109 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:366: lock_status = CC1000_LOCK_NOK;  
	mov	r7,#0x00
00107$:
	C$cc1000.h$370$1$107 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:370: CC1000_WriteToRegister(CC1000_PA_POW, CC1000_PA_VALUE); // W��czenie PA
	mov	_CC1000_WriteToRegister_PARM_2,#0xF0
	mov	dpl,#0x0B
	push	ar7
	lcall	_CC1000_WriteToRegister
	pop	ar7
	C$cc1000.h$371$1$107 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:371: return (lock_status);   // zwrot statusu syntezatora
	mov	dpl,r7
	C$cc1000.h$372$1$107 ==.
	XG$CC1000_SetupTX$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_SetupPD'
;------------------------------------------------------------
	G$CC1000_SetupPD$0$0 ==.
	C$cc1000.h$376$1$107 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:376: void CC1000_SetupPD(void)
;	-----------------------------------------
;	 function CC1000_SetupPD
;	-----------------------------------------
_CC1000_SetupPD:
	C$cc1000.h$378$1$111 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:378: CC1000_WriteToRegister(CC1000_MAIN,0x3F);    // CC1000 do power-down
	mov	_CC1000_WriteToRegister_PARM_2,#0x3F
	mov	dpl,#0x00
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$379$1$111 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:379: CC1000_WriteToRegister(CC1000_PA_POW,0x00);  // Wy��czenie PA dla ograniczenia pr�du
	mov	_CC1000_WriteToRegister_PARM_2,#0x00
	mov	dpl,#0x0B
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$380$1$111 ==.
	XG$CC1000_SetupPD$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_WakeUpToRX'
;------------------------------------------------------------
;RXCurrent                 Allocated to registers r7 
;------------------------------------------------------------
	G$CC1000_WakeUpToRX$0$0 ==.
	C$cc1000.h$386$1$111 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:386: void CC1000_WakeUpToRX(U8 RXCurrent)  
;	-----------------------------------------
;	 function CC1000_WakeUpToRX
;	-----------------------------------------
_CC1000_WakeUpToRX:
	mov	r7,dpl
	C$cc1000.h$388$1$113 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:388: CC1000_WriteToRegister(CC1000_MAIN,0x3B);    // W��czenie oscylatora xtal
	mov	_CC1000_WriteToRegister_PARM_2,#0x3B
	mov	dpl,#0x00
	push	ar7
	lcall	_CC1000_WriteToRegister
	pop	ar7
	C$cc1000.h$389$1$113 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:389: CC1000_WriteToRegister(CC1000_CURRENT,RXCurrent);   
	mov	_CC1000_WriteToRegister_PARM_2,r7
	mov	dpl,#0x09
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$390$1$113 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:390: CC1000_WriteToRegister(CC1000_PLL, CC1000_RX_PLL); 
	mov	_CC1000_WriteToRegister_PARM_2,#0x58
	mov	dpl,#0x0C
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$392$1$113 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:392: Delay_x100us(50);   // typowo 2-5ms
	mov	dptr,#0x0032
	lcall	_Delay_x100us
	C$cc1000.h$393$1$113 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:393: CC1000_WriteToRegister(CC1000_MAIN,0x39);    
	mov	_CC1000_WriteToRegister_PARM_2,#0x39
	mov	dpl,#0x00
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$394$1$113 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:394: Delay_x100us(3);     // Oczekiwanie 250us 
	mov	dptr,#0x0003
	lcall	_Delay_x100us
	C$cc1000.h$395$1$113 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:395: CC1000_WriteToRegister(CC1000_MAIN,0x31);    // W��czenie syntezatora cz�stotliwo�ci
	mov	_CC1000_WriteToRegister_PARM_2,#0x31
	mov	dpl,#0x00
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$396$1$113 ==.
	XG$CC1000_WakeUpToRX$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_WakeUpToTX'
;------------------------------------------------------------
;TXCurrent                 Allocated to registers r7 
;------------------------------------------------------------
	G$CC1000_WakeUpToTX$0$0 ==.
	C$cc1000.h$401$1$113 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:401: void CC1000_WakeUpToTX(U8 TXCurrent)  
;	-----------------------------------------
;	 function CC1000_WakeUpToTX
;	-----------------------------------------
_CC1000_WakeUpToTX:
	mov	r7,dpl
	C$cc1000.h$403$1$115 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:403: CC1000_WriteToRegister(CC1000_MAIN,0xFB);  // W��czenie oscylatora xtal
	mov	_CC1000_WriteToRegister_PARM_2,#0xFB
	mov	dpl,#0x00
	push	ar7
	lcall	_CC1000_WriteToRegister
	pop	ar7
	C$cc1000.h$404$1$115 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:404: CC1000_WriteToRegister(CC1000_CURRENT,TXCurrent);
	mov	_CC1000_WriteToRegister_PARM_2,r7
	mov	dpl,#0x09
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$405$1$115 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:405: CC1000_WriteToRegister(CC1000_PLL, CC1000_TX_PLL);
	mov	_CC1000_WriteToRegister_PARM_2,#0x58
	mov	dpl,#0x0C
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$407$1$115 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:407: Delay_x100us(50);    // typowo 2-5ms 
	mov	dptr,#0x0032
	lcall	_Delay_x100us
	C$cc1000.h$408$1$115 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:408: CC1000_WriteToRegister(CC1000_MAIN,0xF9); 
	mov	_CC1000_WriteToRegister_PARM_2,#0xF9
	mov	dpl,#0x00
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$409$1$115 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:409: Delay_x100us(3);     // Oczekiwanie 250us
	mov	dptr,#0x0003
	lcall	_Delay_x100us
	C$cc1000.h$410$1$115 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:410: CC1000_WriteToRegister(CC1000_PA_POW, CC1000_PA_VALUE);    // W��czenie PA
	mov	_CC1000_WriteToRegister_PARM_2,#0xF0
	mov	dpl,#0x0B
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$411$1$115 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:411: CC1000_WriteToRegister(CC1000_MAIN,0xF1);    // W��czenie syntezatora cz�stotliwo�ci  
	mov	_CC1000_WriteToRegister_PARM_2,#0xF1
	mov	dpl,#0x00
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$412$1$115 ==.
	XG$CC1000_WakeUpToTX$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_ConfigureRegistersRx'
;------------------------------------------------------------
	G$CC1000_ConfigureRegistersRx$0$0 ==.
	C$cc1000.h$415$1$115 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:415: void CC1000_ConfigureRegistersRx(void)
;	-----------------------------------------
;	 function CC1000_ConfigureRegistersRx
;	-----------------------------------------
_CC1000_ConfigureRegistersRx:
	C$cc1000.h$417$1$117 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:417: CC1000_ConfigureRegisters();
	lcall	_CC1000_ConfigureRegisters
	C$cc1000.h$418$1$117 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:418: CC1000_WriteToRegister(CC1000_CURRENT, CC1000_RX_CURRENT);
	mov	_CC1000_WriteToRegister_PARM_2,#0x8C
	mov	dpl,#0x09
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$419$1$117 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:419: CC1000_WriteToRegister(CC1000_PLL, CC1000_RX_PLL);
	mov	_CC1000_WriteToRegister_PARM_2,#0x58
	mov	dpl,#0x0C
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$420$1$117 ==.
	XG$CC1000_ConfigureRegistersRx$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_ConfigureRegistersTx'
;------------------------------------------------------------
	G$CC1000_ConfigureRegistersTx$0$0 ==.
	C$cc1000.h$423$1$117 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:423: void CC1000_ConfigureRegistersTx(void)
;	-----------------------------------------
;	 function CC1000_ConfigureRegistersTx
;	-----------------------------------------
_CC1000_ConfigureRegistersTx:
	C$cc1000.h$425$1$119 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:425: CC1000_ConfigureRegisters();
	lcall	_CC1000_ConfigureRegisters
	C$cc1000.h$426$1$119 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:426: CC1000_WriteToRegister(CC1000_CURRENT, CC1000_TX_CURRENT);
	mov	_CC1000_WriteToRegister_PARM_2,#0xF3
	mov	dpl,#0x09
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$427$1$119 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:427: CC1000_WriteToRegister(CC1000_PLL, CC1000_TX_PLL);
	mov	_CC1000_WriteToRegister_PARM_2,#0x58
	mov	dpl,#0x0C
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$428$1$119 ==.
	XG$CC1000_ConfigureRegistersTx$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_ResetFreqSynth'
;------------------------------------------------------------
;modem1_value              Allocated to registers r7 
;------------------------------------------------------------
	G$CC1000_ResetFreqSynth$0$0 ==.
	C$cc1000.h$431$1$119 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:431: void CC1000_ResetFreqSynth(void)
;	-----------------------------------------
;	 function CC1000_ResetFreqSynth
;	-----------------------------------------
_CC1000_ResetFreqSynth:
	C$cc1000.h$434$1$121 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:434: modem1_value = CC1000_ReadFromRegister(CC1000_MODEM1)&~0x01;
	mov	dpl,#0x10
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	anl	ar7,#0xFE
	C$cc1000.h$435$1$121 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:435: CC1000_WriteToRegister(CC1000_MODEM1,modem1_value);
	mov	_CC1000_WriteToRegister_PARM_2,r7
	mov	dpl,#0x10
	push	ar7
	lcall	_CC1000_WriteToRegister
	pop	ar7
	C$cc1000.h$436$1$121 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:436: CC1000_WriteToRegister(CC1000_MODEM1,modem1_value|0x01);
	mov	a,#0x01
	orl	a,r7
	mov	_CC1000_WriteToRegister_PARM_2,a
	mov	dpl,#0x10
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$437$1$121 ==.
	XG$CC1000_ResetFreqSynth$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_Initialize'
;------------------------------------------------------------
;cal                       Allocated to registers r7 
;------------------------------------------------------------
	G$CC1000_Initialize$0$0 ==.
	C$cc1000.h$442$1$121 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:442: void CC1000_Initialize(void)
;	-----------------------------------------
;	 function CC1000_Initialize
;	-----------------------------------------
_CC1000_Initialize:
	C$cc1000.h$446$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:446: P0MDOUT |= 0x10;    // PDATA (P0.4 jako push-pull)
	orl	_P0MDOUT,#0x10
	C$cc1000.h$447$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:447: PCLK = 1;           // Przy braku transmisji parametr�w: PCLK = 1
	setb	_PCLK
	C$cc1000.h$448$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:448: CC1000_SetupPD();
	lcall	_CC1000_SetupPD
	C$cc1000.h$449$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:449: CC1000_Reset();	
	lcall	_CC1000_Reset
	C$cc1000.h$450$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:450: Delay_x100us(20);	// czekaj 2 ms
	mov	dptr,#0x0014
	lcall	_Delay_x100us
	C$cc1000.h$451$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:451: CC1000_WriteToRegister(CC1000_MAIN,0x11);	// Kalibracja trybu RX
	mov	_CC1000_WriteToRegister_PARM_2,#0x11
	mov	dpl,#0x00
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$452$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:452: CC1000_ConfigureRegistersRx();
	lcall	_CC1000_ConfigureRegistersRx
	C$cc1000.h$453$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:453: cal = CC1000_Calibrate();
	lcall	_CC1000_Calibrate
	C$cc1000.h$454$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:454: if (cal == 0)
	mov	a,dpl
	mov	r7,a
	jnz	00102$
	C$cc1000.h$457$2$124 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:457: printf("B��d kalibracji trybu RX\n");
	mov	a,#__str_1
	push	acc
	mov	a,#(__str_1 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	dec	sp
	dec	sp
	dec	sp
	sjmp	00103$
00102$:
	C$cc1000.h$463$2$125 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:463: printf("Poprawna kalibracja trybu RX\n"); 
	mov	a,#__str_2
	push	acc
	mov	a,#(__str_2 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	dec	sp
	dec	sp
	dec	sp
00103$:
	C$cc1000.h$467$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:467: CC1000_WriteToRegister(CC1000_MAIN,0xA1);	// Kalibracja trybu TX
	mov	_CC1000_WriteToRegister_PARM_2,#0xA1
	mov	dpl,#0x00
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$468$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:468: CC1000_ConfigureRegistersTx();
	lcall	_CC1000_ConfigureRegistersTx
	C$cc1000.h$469$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:469: cal = CC1000_Calibrate();
	lcall	_CC1000_Calibrate
	C$cc1000.h$470$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:470: if (cal == 0)
	mov	a,dpl
	mov	r7,a
	jnz	00105$
	C$cc1000.h$473$2$126 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:473: printf("B��d kalibracji trybu TX\n");            
	mov	a,#__str_3
	push	acc
	mov	a,#(__str_3 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	dec	sp
	dec	sp
	dec	sp
	sjmp	00106$
00105$:
	C$cc1000.h$479$2$127 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:479: printf("Poprawna kalibracja trybu TX\n"); 
	mov	a,#__str_4
	push	acc
	mov	a,#(__str_4 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	dec	sp
	dec	sp
	dec	sp
00106$:
	C$cc1000.h$482$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:482: CC1000_SetupPD();	
	lcall	_CC1000_SetupPD
	C$cc1000.h$483$1$123 ==.
	XG$CC1000_Initialize$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_ConfigureRegisters'
;------------------------------------------------------------
	G$CC1000_ConfigureRegisters$0$0 ==.
	C$cc1000.h$486$1$123 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:486: void CC1000_ConfigureRegisters(void)
;	-----------------------------------------
;	 function CC1000_ConfigureRegisters
;	-----------------------------------------
_CC1000_ConfigureRegisters:
	C$cc1000.h$529$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:529: CC1000_WriteToRegister(CC1000_MAIN,	0x11);      //0xE1
	mov	_CC1000_WriteToRegister_PARM_2,#0x11
	mov	dpl,#0x00
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$530$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:530: CC1000_WriteToRegister(CC1000_FREQ_2A,	0x50);
	mov	_CC1000_WriteToRegister_PARM_2,#0x50
	mov	dpl,#0x01
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$531$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:531: CC1000_WriteToRegister(CC1000_FREQ_1A,	0xC0);
	mov	_CC1000_WriteToRegister_PARM_2,#0xC0
	mov	dpl,#0x02
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$532$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:532: CC1000_WriteToRegister(CC1000_FREQ_0A,	0x00);
	mov	_CC1000_WriteToRegister_PARM_2,#0x00
	mov	dpl,#0x03
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$533$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:533: CC1000_WriteToRegister(CC1000_FREQ_2B,	0x50);
	mov	_CC1000_WriteToRegister_PARM_2,#0x50
	mov	dpl,#0x04
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$534$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:534: CC1000_WriteToRegister(CC1000_FREQ_1B,	0xB7);
	mov	_CC1000_WriteToRegister_PARM_2,#0xB7
	mov	dpl,#0x05
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$535$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:535: CC1000_WriteToRegister(CC1000_FREQ_0B,	0x4F);
	mov	_CC1000_WriteToRegister_PARM_2,#0x4F
	mov	dpl,#0x06
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$536$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:536: CC1000_WriteToRegister(CC1000_FSEP1,  	0x03);
	mov	_CC1000_WriteToRegister_PARM_2,#0x03
	mov	dpl,#0x07
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$537$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:537: CC1000_WriteToRegister(CC1000_FSEP0,  	0x0E);
	mov	_CC1000_WriteToRegister_PARM_2,#0x0E
	mov	dpl,#0x08
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$538$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:538: CC1000_WriteToRegister(CC1000_FRONT_END,0x12);
	mov	_CC1000_WriteToRegister_PARM_2,#0x12
	mov	dpl,#0x0A
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$539$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:539: CC1000_WriteToRegister(CC1000_PA_POW, 	0x70);       
	mov	_CC1000_WriteToRegister_PARM_2,#0x70
	mov	dpl,#0x0B
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$540$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:540: CC1000_WriteToRegister(CC1000_PLL,   	0x58);  // REFDIV=11
	mov	_CC1000_WriteToRegister_PARM_2,#0x58
	mov	dpl,#0x0C
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$541$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:541: CC1000_WriteToRegister(CC1000_LOCK,   	0x10);
	mov	_CC1000_WriteToRegister_PARM_2,#0x10
	mov	dpl,#0x0D
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$542$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:542: CC1000_WriteToRegister(CC1000_CAL,    	0x26);
	mov	_CC1000_WriteToRegister_PARM_2,#0x26
	mov	dpl,#0x0E
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$543$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:543: CC1000_WriteToRegister(CC1000_MODEM2, 	0xB7);  // 0x90
	mov	_CC1000_WriteToRegister_PARM_2,#0xB7
	mov	dpl,#0x0F
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$544$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:544: CC1000_WriteToRegister(CC1000_MODEM1, 	0x6F);  // 0x69 
	mov	_CC1000_WriteToRegister_PARM_2,#0x6F
	mov	dpl,#0x10
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$545$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:545: CC1000_WriteToRegister(CC1000_MODEM0, 	0x37);  // 2,4 (Manchester)
	mov	_CC1000_WriteToRegister_PARM_2,#0x37
	mov	dpl,#0x11
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$546$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:546: CC1000_WriteToRegister(CC1000_MATCH,  	0x70);
	mov	_CC1000_WriteToRegister_PARM_2,#0x70
	mov	dpl,#0x12
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$547$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:547: CC1000_WriteToRegister(CC1000_FSCTRL, 	0x01);
	mov	_CC1000_WriteToRegister_PARM_2,#0x01
	mov	dpl,#0x13
	lcall	_CC1000_WriteToRegister
	C$cc1000.h$550$1$129 ==.
	XG$CC1000_ConfigureRegisters$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_DisplayAllRegisters'
;------------------------------------------------------------
	G$CC1000_DisplayAllRegisters$0$0 ==.
	C$cc1000.h$555$1$129 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:555: void CC1000_DisplayAllRegisters(void)
;	-----------------------------------------
;	 function CC1000_DisplayAllRegisters
;	-----------------------------------------
_CC1000_DisplayAllRegisters:
	C$cc1000.h$557$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:557: printf("MAIN: 0x%x \n", CC1000_ReadFromRegister(CC1000_MAIN));
	mov	dpl,#0x00
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_5
	push	acc
	mov	a,#(__str_5 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$558$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:558: printf("FREQ_2A: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_2A));    // 0x75
	mov	dpl,#0x01
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_6
	push	acc
	mov	a,#(__str_6 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$559$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:559: printf("FREQ_1A: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_1A));    // 0xA0    
	mov	dpl,#0x02
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_7
	push	acc
	mov	a,#(__str_7 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$560$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:560: printf("FREQ_0A: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_0A));    // 0xCB
	mov	dpl,#0x03
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_8
	push	acc
	mov	a,#(__str_8 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$561$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:561: printf("FREQ_2B: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_2B));    // 0x75
	mov	dpl,#0x04
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_9
	push	acc
	mov	a,#(__str_9 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$562$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:562: printf("FREQ_1B: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_1B));    // 0xA5
	mov	dpl,#0x05
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_10
	push	acc
	mov	a,#(__str_10 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$563$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:563: printf("FREQ_0B: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_0B));    // 0x4E
	mov	dpl,#0x06
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_11
	push	acc
	mov	a,#(__str_11 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$564$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:564: printf("FSEP1: 0x%x \n", CC1000_ReadFromRegister(CC1000_FSEP1));        // 0x00
	mov	dpl,#0x07
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_12
	push	acc
	mov	a,#(__str_12 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$565$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:565: printf("FSEP0: 0x%x \n", CC1000_ReadFromRegister(CC1000_FSEP0));        // 0x59 
	mov	dpl,#0x08
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_13
	push	acc
	mov	a,#(__str_13 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$566$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:566: printf("CURRENT: 0x%x \n", CC1000_ReadFromRegister(CC1000_CURRENT));    // 0xCA
	mov	dpl,#0x09
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_14
	push	acc
	mov	a,#(__str_14 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$567$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:567: printf("FRONT_END: 0x%x \n", CC1000_ReadFromRegister(CC1000_FRONT_END));// 0x08 
	mov	dpl,#0x0A
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_15
	push	acc
	mov	a,#(__str_15 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$568$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:568: printf("PA_POW: 0x%x \n", CC1000_ReadFromRegister(CC1000_PA_POW));      // 0x0F
	mov	dpl,#0x0B
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_16
	push	acc
	mov	a,#(__str_16 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$569$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:569: printf("PLL: 0x%x \n", CC1000_ReadFromRegister(CC1000_PLL));            // 0x10 
	mov	dpl,#0x0C
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_17
	push	acc
	mov	a,#(__str_17 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$570$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:570: printf("LOCK: 0x%x \n", CC1000_ReadFromRegister(CC1000_LOCK));          // 0x00 
	mov	dpl,#0x0D
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_18
	push	acc
	mov	a,#(__str_18 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$571$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:571: printf("CAL: 0x%x \n", CC1000_ReadFromRegister(CC1000_CAL));            // 0x05
	mov	dpl,#0x0E
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_19
	push	acc
	mov	a,#(__str_19 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$572$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:572: printf("MODEM2: 0x%x \n", CC1000_ReadFromRegister(CC1000_MODEM2));      // 0x96
	mov	dpl,#0x0F
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_20
	push	acc
	mov	a,#(__str_20 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$573$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:573: printf("MODEM1: 0x%x \n", CC1000_ReadFromRegister(CC1000_MODEM1));      // 0x67
	mov	dpl,#0x10
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_21
	push	acc
	mov	a,#(__str_21 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$574$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:574: printf("MODEM0: 0x%x \n", CC1000_ReadFromRegister(CC1000_MODEM0));      // 0x24
	mov	dpl,#0x11
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_22
	push	acc
	mov	a,#(__str_22 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$575$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:575: printf("MATCH: 0x%x \n", CC1000_ReadFromRegister(CC1000_MATCH));        // 0x00
	mov	dpl,#0x12
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_23
	push	acc
	mov	a,#(__str_23 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$576$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:576: printf("FSCTRL: 0x%x \n", CC1000_ReadFromRegister(CC1000_FSCTRL));      // 0x01
	mov	dpl,#0x13
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_24
	push	acc
	mov	a,#(__str_24 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$577$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:577: printf("PRESCALER: 0x%x \n", CC1000_ReadFromRegister(CC1000_PRESCALER));// 0x00 
	mov	dpl,#0x1C
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_25
	push	acc
	mov	a,#(__str_25 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$578$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:578: printf("TEST6: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST6));        // 0x10
	mov	dpl,#0x40
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_26
	push	acc
	mov	a,#(__str_26 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$579$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:579: printf("TEST5: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST5));        // 0x08
	mov	dpl,#0x41
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_27
	push	acc
	mov	a,#(__str_27 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$580$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:580: printf("TEST4: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST4));        // 0x25
	mov	dpl,#0x42
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_28
	push	acc
	mov	a,#(__str_28 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$581$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:581: printf("TEST3: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST3));        // 0x04
	mov	dpl,#0x43
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_29
	push	acc
	mov	a,#(__str_29 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$582$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:582: printf("TEST2: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST2));        // 0x00
	mov	dpl,#0x44
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_30
	push	acc
	mov	a,#(__str_30 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$583$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:583: printf("TEST1: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST1));        // 0x00 
	mov	dpl,#0x45
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_31
	push	acc
	mov	a,#(__str_31 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$584$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:584: printf("TEST0: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST0));        // 0x00
	mov	dpl,#0x46
	lcall	_CC1000_ReadFromRegister
	mov	r7,dpl
	mov	r6,#0x00
	push	ar7
	push	ar6
	mov	a,#__str_32
	push	acc
	mov	a,#(__str_32 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$cc1000.h$585$1$131 ==.
	XG$CC1000_DisplayAllRegisters$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'CC1000_Sync'
;------------------------------------------------------------
	G$CC1000_Sync$0$0 ==.
	C$cc1000.h$593$1$131 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:593: U8 CC1000_Sync(void)
;	-----------------------------------------
;	 function CC1000_Sync
;	-----------------------------------------
_CC1000_Sync:
	C$cc1000.h$595$1$133 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:595: PREAMBULE >>= 1;
	mov	a,(_PREAMBULE + 3)
	clr	c
	rrc	a
	mov	(_PREAMBULE + 3),a
	mov	a,(_PREAMBULE + 2)
	rrc	a
	mov	(_PREAMBULE + 2),a
	mov	a,(_PREAMBULE + 1)
	rrc	a
	mov	(_PREAMBULE + 1),a
	mov	a,_PREAMBULE
	rrc	a
	mov	_PREAMBULE,a
	C$cc1000.h$596$1$133 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:596: if (!BitReceive())          // Odbi�r bit�w z negacj�
	lcall	_BitReceive
	mov	a,dpl
	jnz	00102$
	C$cc1000.h$597$1$133 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:597: PREAMBULE |= 0x80000000;
	mov	a,_PREAMBULE
	mov	a,(_PREAMBULE + 1)
	mov	a,(_PREAMBULE + 2)
	orl	(_PREAMBULE + 3),#0x80
	sjmp	00103$
00102$:
	C$cc1000.h$599$1$133 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:599: PREAMBULE &= 0x7FFFFFFF; 
	mov	a,_PREAMBULE
	mov	a,(_PREAMBULE + 1)
	mov	a,(_PREAMBULE + 2)
	anl	(_PREAMBULE + 3),#0x7F
00103$:
	C$cc1000.h$600$1$133 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:600: if (PREAMBULE == SYNC_SEQ)
	mov	a,#0x55
	cjne	a,_PREAMBULE,00105$
	mov	a,#0x55
	cjne	a,(_PREAMBULE + 1),00105$
	mov	a,#0x55
	cjne	a,(_PREAMBULE + 2),00105$
	mov	a,#0x71
	cjne	a,(_PREAMBULE + 3),00105$
	C$cc1000.h$601$1$133 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:601: return 1;               // Jest synchronizacja 
	mov	dpl,#0x01
	sjmp	00107$
00105$:
	C$cc1000.h$602$1$133 ==.
;	C:\Workspace\WSN_MS-4\/cc1000.h:602: else return 0;              // Brak synchronizacji
	mov	dpl,#0x00
00107$:
	C$cc1000.h$603$1$133 ==.
	XG$CC1000_Sync$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'clearFrame'
;------------------------------------------------------------
	G$clearFrame$0$0 ==.
	C$wsn.h$35$1$133 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:35: void clearFrame()
;	-----------------------------------------
;	 function clearFrame
;	-----------------------------------------
_clearFrame:
	C$wsn.h$37$1$138 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:37: for (i=0; i<19; i++)
	clr	a
	mov	_i,a
	mov	(_i + 1),a
00102$:
	C$wsn.h$38$1$138 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:38: frame[i] = 0x00;
	mov	a,_i
	add	a,#_frame
	mov	r0,a
	mov	@r0,#0x00
	C$wsn.h$37$1$138 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:37: for (i=0; i<19; i++)
	inc	_i
	clr	a
	cjne	a,_i,00109$
	inc	(_i + 1)
00109$:
	clr	c
	mov	a,_i
	subb	a,#0x13
	mov	a,(_i + 1)
	xrl	a,#0x80
	subb	a,#0x80
	jc	00102$
	C$wsn.h$39$1$138 ==.
	XG$clearFrame$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'sendFrame'
;------------------------------------------------------------
;frameType                 Allocated to registers r7 
;------------------------------------------------------------
	G$sendFrame$0$0 ==.
	C$wsn.h$41$1$138 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:41: void sendFrame(U8 frameType)
;	-----------------------------------------
;	 function sendFrame
;	-----------------------------------------
_sendFrame:
	mov	r7,dpl
	C$wsn.h$43$1$140 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:43: CC1000_WakeUpToTX(CC1000_TX_CURRENT);
	mov	dpl,#0xF3
	push	ar7
	lcall	_CC1000_WakeUpToTX
	C$wsn.h$44$1$140 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:44: CC1000_SetupTX(CC1000_TX_CURRENT);
	mov	dpl,#0xF3
	lcall	_CC1000_SetupTX
	pop	ar7
	C$wsn.h$45$1$140 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:45: P0MDOUT |= 0x08;
	orl	_P0MDOUT,#0x08
	C$wsn.h$46$1$140 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:46: for (i = 0; i < CC1000_PREAMBULE_LENGTH; ++i)
	clr	a
	mov	_i,a
	mov	(_i + 1),a
00102$:
	C$wsn.h$47$1$140 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:47: CC1000_SendByte(CC1000_PREAMBULE);
	mov	dpl,#0x55
	push	ar7
	lcall	_CC1000_SendByte
	pop	ar7
	C$wsn.h$46$1$140 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:46: for (i = 0; i < CC1000_PREAMBULE_LENGTH; ++i)
	inc	_i
	clr	a
	cjne	a,_i,00112$
	inc	(_i + 1)
00112$:
	clr	c
	mov	a,_i
	subb	a,#0x20
	mov	a,(_i + 1)
	xrl	a,#0x80
	subb	a,#0x80
	jc	00102$
	C$wsn.h$49$1$140 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:49: CC1000_SendByte(CC1000_SYNC_BYTE);
	mov	dpl,#0x71
	push	ar7
	lcall	_CC1000_SendByte
	C$wsn.h$50$1$140 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:50: CC1000_SendByte(NI);
	mov	dpl,#0xB4
	lcall	_CC1000_SendByte
	C$wsn.h$51$1$140 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:51: CC1000_SendByte(slaveAddress);
	mov	dpl,_slaveAddress
	lcall	_CC1000_SendByte
	pop	ar7
	C$wsn.h$52$1$140 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:52: CC1000_SendByte(frameType);
	mov	dpl,r7
	push	ar7
	lcall	_CC1000_SendByte
	pop	ar7
	C$wsn.h$54$1$140 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:54: printf("\nN=%x F=%x SI=%x ", NI, frameType, slaveAddress);
	mov	r6,#0x00
	push	_slaveAddress
	push	(_slaveAddress + 1)
	push	ar7
	push	ar6
	mov	a,#0xB4
	push	acc
	clr	a
	push	acc
	mov	a,#__str_33
	push	acc
	mov	a,#(__str_33 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xf7
	mov	sp,a
	C$wsn.h$55$1$140 ==.
	XG$sendFrame$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'sendData'
;------------------------------------------------------------
;frame                     Allocated with name '_sendData_frame_1_141'
;------------------------------------------------------------
	G$sendData$0$0 ==.
	C$wsn.h$57$1$140 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:57: void sendData(U8* frame)
;	-----------------------------------------
;	 function sendData
;	-----------------------------------------
_sendData:
	mov	_sendData_frame_1_141,dpl
	mov	(_sendData_frame_1_141 + 1),dph
	mov	(_sendData_frame_1_141 + 2),b
	C$wsn.h$59$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:59: CC1000_WakeUpToTX(CC1000_TX_CURRENT);
	mov	dpl,#0xF3
	lcall	_CC1000_WakeUpToTX
	C$wsn.h$60$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:60: CC1000_SetupTX(CC1000_TX_CURRENT);
	mov	dpl,#0xF3
	lcall	_CC1000_SetupTX
	C$wsn.h$61$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:61: P0MDOUT |= 0x08;
	orl	_P0MDOUT,#0x08
	C$wsn.h$62$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:62: for (i=0; i<CC1000_PREAMBULE_LENGTH; ++i)
	clr	a
	mov	_i,a
	mov	(_i + 1),a
00104$:
	C$wsn.h$63$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:63: CC1000_SendByte(CC1000_PREAMBULE);
	mov	dpl,#0x55
	lcall	_CC1000_SendByte
	C$wsn.h$62$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:62: for (i=0; i<CC1000_PREAMBULE_LENGTH; ++i)
	inc	_i
	clr	a
	cjne	a,_i,00127$
	inc	(_i + 1)
00127$:
	clr	c
	mov	a,_i
	subb	a,#0x20
	mov	a,(_i + 1)
	xrl	a,#0x80
	subb	a,#0x80
	jc	00104$
	C$wsn.h$65$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:65: CC1000_SendByte(CC1000_SYNC_BYTE);
	mov	dpl,#0x71
	lcall	_CC1000_SendByte
	C$wsn.h$66$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:66: for (i=0; i<19; i++)
	clr	a
	mov	_i,a
	mov	(_i + 1),a
00106$:
	C$wsn.h$67$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:67: CC1000_SendByte(frame[i]);
	mov	a,_i
	add	a,_sendData_frame_1_141
	mov	r2,a
	mov	a,(_i + 1)
	addc	a,(_sendData_frame_1_141 + 1)
	mov	r3,a
	mov	r4,(_sendData_frame_1_141 + 2)
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	dpl,a
	lcall	_CC1000_SendByte
	C$wsn.h$66$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:66: for (i=0; i<19; i++)
	inc	_i
	clr	a
	cjne	a,_i,00129$
	inc	(_i + 1)
00129$:
	clr	c
	mov	a,_i
	subb	a,#0x13
	mov	a,(_i + 1)
	xrl	a,#0x80
	subb	a,#0x80
	jc	00106$
	C$wsn.h$69$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:69: printf("\nN=%x F=%x SI=%x ", frame[0], frame[1], frame[2]);
	mov	a,#0x02
	add	a,_sendData_frame_1_141
	mov	r2,a
	clr	a
	addc	a,(_sendData_frame_1_141 + 1)
	mov	r3,a
	mov	r4,(_sendData_frame_1_141 + 2)
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	r4,#0x00
	mov	a,#0x01
	add	a,_sendData_frame_1_141
	mov	r0,a
	clr	a
	addc	a,(_sendData_frame_1_141 + 1)
	mov	r1,a
	mov	r3,(_sendData_frame_1_141 + 2)
	mov	dpl,r0
	mov	dph,r1
	mov	b,r3
	lcall	__gptrget
	mov	r0,a
	mov	r3,#0x00
	mov	dpl,_sendData_frame_1_141
	mov	dph,(_sendData_frame_1_141 + 1)
	mov	b,(_sendData_frame_1_141 + 2)
	lcall	__gptrget
	mov	r1,a
	mov	r7,#0x00
	push	ar2
	push	ar4
	push	ar0
	push	ar3
	push	ar1
	push	ar7
	mov	a,#__str_33
	push	acc
	mov	a,#(__str_33 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xf7
	mov	sp,a
	C$wsn.h$71$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:71: for (i=3; i<19; i++)
	mov	_i,#0x03
	mov	(_i + 1),#0x00
00108$:
	C$wsn.h$72$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:72: printf("%x", frame[i]);
	mov	a,_i
	add	a,_sendData_frame_1_141
	mov	r5,a
	mov	a,(_i + 1)
	addc	a,(_sendData_frame_1_141 + 1)
	mov	r6,a
	mov	r7,(_sendData_frame_1_141 + 2)
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r5,a
	mov	r7,#0x00
	push	ar5
	push	ar7
	mov	a,#__str_34
	push	acc
	mov	a,#(__str_34 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$wsn.h$71$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:71: for (i=3; i<19; i++)
	inc	_i
	clr	a
	cjne	a,_i,00131$
	inc	(_i + 1)
00131$:
	clr	c
	mov	a,_i
	subb	a,#0x13
	mov	a,(_i + 1)
	xrl	a,#0x80
	subb	a,#0x80
	jc	00108$
	C$wsn.h$73$1$142 ==.
	XG$sendData$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'waitForFrame'
;------------------------------------------------------------
;limitAttempts             Allocated to registers r6 r7 
;attempt                   Allocated to registers r4 r5 
;------------------------------------------------------------
	G$waitForFrame$0$0 ==.
	C$wsn.h$75$1$142 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:75: U8 waitForFrame(int limitAttempts)
;	-----------------------------------------
;	 function waitForFrame
;	-----------------------------------------
_waitForFrame:
	mov	r6,dpl
	mov	r7,dph
	C$wsn.h$78$1$144 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:78: CC1000_WakeUpToRX(CC1000_RX_CURRENT);
	mov	dpl,#0x8C
	push	ar7
	push	ar6
	lcall	_CC1000_WakeUpToRX
	C$wsn.h$79$1$144 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:79: CC1000_SetupRX(CC1000_RX_CURRENT);
	mov	dpl,#0x8C
	lcall	_CC1000_SetupRX
	pop	ar6
	pop	ar7
	C$wsn.h$80$1$144 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:80: P0MDOUT &= ~(0x08);
	mov	r5,_P0MDOUT
	mov	a,#0xF7
	anl	a,r5
	mov	_P0MDOUT,a
	C$wsn.h$81$1$144 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:81: DIO = 1;
	setb	_DIO
	C$wsn.h$82$1$144 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:82: PREAMBULE = 0;
	C$wsn.h$83$1$144 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:83: while(attempt <= limitAttempts) {
	clr a
	mov _PREAMBULE,a
	mov (_PREAMBULE + 1),a
	mov (_PREAMBULE + 2),a
	mov (_PREAMBULE + 3),a
	mov r4,a
	mov r5,a
00112$:
	clr	c
	mov	a,r6
	subb	a,r4
	mov	a,r7
	xrl	a,#0x80
	mov	b,r5
	xrl	b,#0x80
	subb	a,b
	jnc	00151$
	ljmp	00114$
00151$:
	C$wsn.h$84$2$145 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:84: while(!CC1000_Sync());
00101$:
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	lcall	_CC1000_Sync
	mov	a,dpl
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
	jz	00101$
	C$wsn.h$85$2$145 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:85: for (i=0; i<19; i++) {
	clr	a
	mov	_i,a
	mov	(_i + 1),a
00115$:
	C$wsn.h$86$3$146 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:86: frame[i] = CC1000_ReceiveByte();
	mov	a,_i
	add	a,#_frame
	mov	r1,a
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	push	ar1
	lcall	_CC1000_ReceiveByte
	mov	a,dpl
	pop	ar1
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
	mov	@r1,a
	C$wsn.h$85$2$145 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:85: for (i=0; i<19; i++) {
	inc	_i
	clr	a
	cjne	a,_i,00153$
	inc	(_i + 1)
00153$:
	clr	c
	mov	a,_i
	subb	a,#0x13
	mov	a,(_i + 1)
	xrl	a,#0x80
	subb	a,#0x80
	jc	00115$
	C$wsn.h$89$2$145 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:89: if (frame[0] == NI && frame[1] == slaveAddress) {
	mov	a,#0xB4
	cjne	a,_frame,00109$
	mov	r2,(_frame + 0x0001)
	mov	r3,#0x00
	mov	a,r2
	cjne	a,_slaveAddress,00109$
	mov	a,r3
	cjne	a,(_slaveAddress + 1),00109$
	C$wsn.h$90$3$147 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:90: printf("\nS=%x F=%x SI=%x SN=xx ", CC1000_SYNC_BYTE, frame[2], slaveAddress);
	mov	r2,(_frame + 0x0002)
	mov	r3,#0x00
	push	_slaveAddress
	push	(_slaveAddress + 1)
	push	ar2
	push	ar3
	mov	a,#0x71
	push	acc
	clr	a
	push	acc
	mov	a,#__str_35
	push	acc
	mov	a,#(__str_35 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xf7
	mov	sp,a
	C$wsn.h$91$3$147 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:91: for (i=3; i<19; i++)
	mov	_i,#0x03
	mov	(_i + 1),#0x00
00117$:
	C$wsn.h$92$3$147 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:92: printf("%x", frame[i]);
	mov	a,_i
	add	a,#_frame
	mov	r1,a
	mov	ar3,@r1
	mov	r2,#0x00
	push	ar3
	push	ar2
	mov	a,#__str_34
	push	acc
	mov	a,#(__str_34 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$wsn.h$91$3$147 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:91: for (i=3; i<19; i++)
	inc	_i
	clr	a
	cjne	a,_i,00159$
	inc	(_i + 1)
00159$:
	clr	c
	mov	a,_i
	subb	a,#0x13
	mov	a,(_i + 1)
	xrl	a,#0x80
	subb	a,#0x80
	jc	00117$
	C$wsn.h$94$3$147 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:94: return frame[2];
	mov	dpl,(_frame + 0x0002)
	sjmp	00119$
00109$:
	C$wsn.h$96$3$148 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:96: if (limitAttempts != 0) {
	mov	a,r6
	orl	a,r7
	jnz	00161$
	ljmp	00112$
00161$:
	C$wsn.h$97$4$149 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:97: attempt++;
	inc	r4
	cjne	r4,#0x00,00162$
	inc	r5
00162$:
	C$wsn.h$98$4$149 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:98: Delay_x100us(1000);
	mov	dptr,#0x03E8
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	lcall	_Delay_x100us
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
	C$wsn.h$100$3$148 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:100: continue;
	ljmp	00112$
00114$:
	C$wsn.h$104$1$144 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:104: return 0x00;
	mov	dpl,#0x00
00119$:
	C$wsn.h$105$1$144 ==.
	XG$waitForFrame$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'runSlaveMode'
;------------------------------------------------------------
;slaveId                   Allocated to registers r6 r7 
;------------------------------------------------------------
	G$runSlaveMode$0$0 ==.
	C$wsn.h$112$1$144 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:112: void runSlaveMode(int slaveId)
;	-----------------------------------------
;	 function runSlaveMode
;	-----------------------------------------
_runSlaveMode:
	mov	r6,dpl
	mov	r7,dph
	C$wsn.h$114$1$151 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:114: clearFrame();
	push	ar7
	push	ar6
	lcall	_clearFrame
	pop	ar6
	pop	ar7
	C$wsn.h$115$1$151 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:115: slaveAddress = slaveId;
	mov	_slaveAddress,r6
	mov	(_slaveAddress + 1),r7
	C$wsn.h$116$1$151 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:116: CC1000_Initialize();
	push	ar7
	push	ar6
	lcall	_CC1000_Initialize
	C$wsn.h$118$1$151 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:118: switch (waitForFrame(0)) {
	mov	dptr,#0x0000
	lcall	_waitForFrame
	mov	r5,dpl
	pop	ar6
	pop	ar7
	cjne	r5,#0x01,00137$
	sjmp	00101$
00137$:
	cjne	r5,#0x03,00138$
	sjmp	00110$
00138$:
	cjne	r5,#0x04,00139$
	sjmp	00104$
00139$:
	cjne	r5,#0x06,00140$
	sjmp	00110$
00140$:
	cjne	r5,#0x07,00141$
	sjmp	00105$
00141$:
	cjne	r5,#0x0D,00142$
	sjmp	00110$
00142$:
	cjne	r5,#0x83,00143$
	sjmp	00103$
00143$:
	C$wsn.h$119$2$152 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:119: case 0x01:
	cjne	r5,#0x86,00110$
	sjmp	00103$
00101$:
	C$wsn.h$120$2$152 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:120: sendFrame(0x02);
	mov	dpl,#0x02
	push	ar7
	push	ar6
	lcall	_sendFrame
	pop	ar6
	pop	ar7
	C$wsn.h$121$2$152 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:121: break;
	C$wsn.h$123$2$152 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:123: case 0x86:
	sjmp	00110$
00103$:
	C$wsn.h$124$2$152 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:124: sendFrame(0x0C);
	mov	dpl,#0x0C
	push	ar7
	push	ar6
	lcall	_sendFrame
	pop	ar6
	pop	ar7
	C$wsn.h$125$2$152 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:125: break;
	C$wsn.h$126$2$152 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:126: case 0x04:
	sjmp	00110$
00104$:
	C$wsn.h$127$2$152 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:127: sendDataWithoutSecurityToMaster();
	push	ar7
	push	ar6
	lcall	_sendDataWithoutSecurityToMaster
	pop	ar6
	pop	ar7
	C$wsn.h$128$2$152 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:128: break;
	C$wsn.h$129$2$152 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:129: case 0x07:
	sjmp	00110$
00105$:
	C$wsn.h$130$2$152 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:130: sendDataWithCRC8ToMaster();
	push	ar7
	push	ar6
	lcall	_sendDataWithCRC8ToMaster
	pop	ar6
	pop	ar7
	C$wsn.h$137$1$151 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:137: }
00110$:
	C$wsn.h$139$1$151 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:139: CC1000_SetupPD();
	push	ar7
	push	ar6
	lcall	_CC1000_SetupPD
	pop	ar6
	pop	ar7
	C$wsn.h$140$1$151 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:140: runSlaveMode(slaveId);
	mov	dpl,r6
	mov	dph,r7
	lcall	_runSlaveMode
	C$wsn.h$141$1$151 ==.
	XG$runSlaveMode$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'sendDataWithoutSecurityToMaster'
;------------------------------------------------------------
	G$sendDataWithoutSecurityToMaster$0$0 ==.
	C$wsn.h$143$1$151 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:143: void sendDataWithoutSecurityToMaster()
;	-----------------------------------------
;	 function sendDataWithoutSecurityToMaster
;	-----------------------------------------
_sendDataWithoutSecurityToMaster:
	C$wsn.h$145$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:145: frame[0] = NI;
	mov	_frame,#0xB4
	C$wsn.h$146$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:146: frame[1] = slaveAddress;
	mov	r7,_slaveAddress
	mov	(_frame + 0x0001),r7
	C$wsn.h$147$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:147: frame[2] = 0x05;
	mov	(_frame + 0x0002),#0x05
	C$wsn.h$148$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:148: frame[3] = 0x00;
	mov	(_frame + 0x0003),#0x00
	C$wsn.h$149$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:149: frame[4] = 'L';
	mov	(_frame + 0x0004),#0x4C
	C$wsn.h$150$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:150: frame[5] = 'u';
	mov	(_frame + 0x0005),#0x75
	C$wsn.h$151$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:151: frame[6] = 'b';
	mov	(_frame + 0x0006),#0x62
	C$wsn.h$152$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:152: frame[7] = 'i';
	mov	(_frame + 0x0007),#0x69
	C$wsn.h$153$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:153: frame[8] = 'e';
	mov	(_frame + 0x0008),#0x65
	C$wsn.h$154$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:154: frame[9] = 'R';
	mov	(_frame + 0x0009),#0x52
	C$wsn.h$155$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:155: frame[10] = 'a';
	mov	(_frame + 0x000a),#0x61
	C$wsn.h$156$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:156: frame[11] = 'd';
	mov	(_frame + 0x000b),#0x64
	C$wsn.h$157$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:157: frame[12] = 'i';
	mov	(_frame + 0x000c),#0x69
	C$wsn.h$158$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:158: frame[13] = 'o';
	mov	(_frame + 0x000d),#0x6F
	C$wsn.h$159$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:159: frame[14] = '!';
	mov	(_frame + 0x000e),#0x21
	C$wsn.h$160$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:160: frame[15] = '!';
	mov	(_frame + 0x000f),#0x21
	C$wsn.h$161$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:161: frame[16] = '!';
	mov	(_frame + 0x0010),#0x21
	C$wsn.h$162$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:162: frame[17] = '!';
	mov	(_frame + 0x0011),#0x21
	C$wsn.h$163$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:163: frame[18] = 0x00;
	mov	(_frame + 0x0012),#0x00
	C$wsn.h$165$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:165: sendData(frame);
	mov	dptr,#_frame
	mov	b,#0x40
	lcall	_sendData
	C$wsn.h$166$1$153 ==.
	XG$sendDataWithoutSecurityToMaster$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'sendDataWithCRC8ToMaster'
;------------------------------------------------------------
	G$sendDataWithCRC8ToMaster$0$0 ==.
	C$wsn.h$168$1$153 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:168: void sendDataWithCRC8ToMaster()
;	-----------------------------------------
;	 function sendDataWithCRC8ToMaster
;	-----------------------------------------
_sendDataWithCRC8ToMaster:
	C$wsn.h$170$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:170: frame[0] = NI;
	mov	_frame,#0xB4
	C$wsn.h$171$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:171: frame[1] = slaveAddress;
	mov	r7,_slaveAddress
	mov	(_frame + 0x0001),r7
	C$wsn.h$172$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:172: frame[2] = 0x08;
	mov	(_frame + 0x0002),#0x08
	C$wsn.h$173$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:173: frame[3] = 0x00;
	mov	(_frame + 0x0003),#0x00
	C$wsn.h$174$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:174: frame[4] = 'L';
	mov	(_frame + 0x0004),#0x4C
	C$wsn.h$175$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:175: frame[5] = 'u';
	mov	(_frame + 0x0005),#0x75
	C$wsn.h$176$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:176: frame[6] = 'b';
	mov	(_frame + 0x0006),#0x62
	C$wsn.h$177$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:177: frame[7] = 'i';
	mov	(_frame + 0x0007),#0x69
	C$wsn.h$178$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:178: frame[8] = 'e';
	mov	(_frame + 0x0008),#0x65
	C$wsn.h$179$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:179: frame[9] = 'R';
	mov	(_frame + 0x0009),#0x52
	C$wsn.h$180$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:180: frame[10] = 'a';
	mov	(_frame + 0x000a),#0x61
	C$wsn.h$181$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:181: frame[11] = 'd';
	mov	(_frame + 0x000b),#0x64
	C$wsn.h$182$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:182: frame[12] = 'i';
	mov	(_frame + 0x000c),#0x69
	C$wsn.h$183$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:183: frame[13] = 'o';
	mov	(_frame + 0x000d),#0x6F
	C$wsn.h$184$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:184: frame[14] = '!';
	mov	(_frame + 0x000e),#0x21
	C$wsn.h$185$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:185: frame[15] = '!';
	mov	(_frame + 0x000f),#0x21
	C$wsn.h$186$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:186: frame[16] = '!';
	mov	(_frame + 0x0010),#0x21
	C$wsn.h$187$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:187: frame[17] = '!';
	mov	(_frame + 0x0011),#0x21
	C$wsn.h$188$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:188: frame[18] = crc8(frame[4], crc, 14);
	mov	r5,(_frame + 0x0004)
	mov	r6,#0x00
	mov	r7,#0x00
	mov	_crc8_PARM_2,_crc
	mov	_crc8_PARM_3,#0x0E
	mov	(_crc8_PARM_3 + 1),#0x00
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	_crc8
	mov	a,dpl
	mov	(_frame + 0x0012),a
	C$wsn.h$190$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:190: sendData(frame);
	mov	dptr,#_frame
	mov	b,#0x40
	lcall	_sendData
	C$wsn.h$191$1$154 ==.
	XG$sendDataWithCRC8ToMaster$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'runMasterMode'
;------------------------------------------------------------
	G$runMasterMode$0$0 ==.
	C$wsn.h$198$1$154 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:198: void runMasterMode()
;	-----------------------------------------
;	 function runMasterMode
;	-----------------------------------------
_runMasterMode:
	C$wsn.h$200$1$155 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:200: clearFrame();
	lcall	_clearFrame
	C$wsn.h$201$1$155 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:201: mode = getMode();
	lcall	_getMode
	mov	_mode,dpl
	mov	(_mode + 1),dph
	C$wsn.h$203$1$155 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:203: if (mode >= 1 && mode <= 3) {
	clr	c
	mov	a,_mode
	subb	a,#0x01
	mov	a,(_mode + 1)
	xrl	a,#0x80
	subb	a,#0x80
	jc	00111$
	mov	a,#0x03
	subb	a,_mode
	clr	a
	xrl	a,#0x80
	mov	b,(_mode + 1)
	xrl	b,#0x80
	subb	a,b
	jc	00111$
	C$wsn.h$204$2$156 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:204: printf("Wybrany tryb %d", mode);
	push	_mode
	push	(_mode + 1)
	mov	a,#__str_36
	push	acc
	mov	a,#(__str_36 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	C$wsn.h$205$2$156 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:205: CC1000_Initialize();
	lcall	_CC1000_Initialize
	C$wsn.h$207$2$156 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:207: sendTestFrame();
	lcall	_sendTestFrame
	C$wsn.h$209$2$156 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:209: switch (mode) {
	mov	a,#0x01
	cjne	a,_mode,00136$
	clr	a
	cjne	a,(_mode + 1),00136$
	sjmp	00101$
00136$:
	mov	a,#0x02
	cjne	a,_mode,00137$
	clr	a
	cjne	a,(_mode + 1),00137$
	sjmp	00102$
00137$:
	C$wsn.h$210$3$157 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:210: case 1:
	sjmp	00104$
00101$:
	C$wsn.h$211$3$157 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:211: sendDataWithoutSecurity();
	lcall	_sendDataWithoutSecurity
	C$wsn.h$212$3$157 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:212: break;
	C$wsn.h$213$3$157 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:213: case 2:
	sjmp	00104$
00102$:
	C$wsn.h$214$3$157 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:214: sendDataWithCRC8();
	lcall	_sendDataWithCRC8
	C$wsn.h$218$2$156 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:218: }
00104$:
	C$wsn.h$219$2$156 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:219: CC1000_SetupPD();
	lcall	_CC1000_SetupPD
	sjmp	00112$
00111$:
	C$wsn.h$220$1$155 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:220: } else if (mode == 4) {
	mov	a,#0x04
	cjne	a,_mode,00138$
	clr	a
	cjne	a,(_mode + 1),00138$
	sjmp	00139$
00138$:
	sjmp	00112$
00139$:
	C$wsn.h$221$2$158 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:221: if (acknowledge == 0) {
	mov	a,_acknowledge
	orl	a,(_acknowledge + 1)
	C$wsn.h$222$3$159 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:222: acknowledge = 1;
	jnz	00106$
	mov	_acknowledge,#0x01
	mov	(_acknowledge + 1),a
	C$wsn.h$223$3$159 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:223: printf("Wybrano tryb z potwierdzeniem");
	mov	a,#__str_37
	push	acc
	mov	a,#(__str_37 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	dec	sp
	dec	sp
	dec	sp
	sjmp	00112$
00106$:
	C$wsn.h$225$3$160 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:225: acknowledge = 0;
	clr	a
	mov	_acknowledge,a
	mov	(_acknowledge + 1),a
	C$wsn.h$226$3$160 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:226: printf("Wybrano tryb bez potwierdzenia");
	mov	a,#__str_38
	push	acc
	mov	a,#(__str_38 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	dec	sp
	dec	sp
	dec	sp
00112$:
	C$wsn.h$229$1$155 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:229: runMasterMode();
	lcall	_runMasterMode
	C$wsn.h$230$1$155 ==.
	XG$runMasterMode$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'getMode'
;------------------------------------------------------------
	G$getMode$0$0 ==.
	C$wsn.h$232$1$155 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:232: int getMode()
;	-----------------------------------------
;	 function getMode
;	-----------------------------------------
_getMode:
	C$wsn.h$234$1$161 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:234: do {
00124$:
	C$wsn.h$235$2$162 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:235: Wait_for_button_release();
	lcall	_Wait_for_button_release
	C$wsn.h$236$2$162 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:236: Wait_for_button_pressed();
	lcall	_Wait_for_button_pressed
	C$wsn.h$237$2$162 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:237: if (B1_ON()) {
	mov	a,_P5
	jb	acc.0,00122$
	C$wsn.h$238$3$163 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:238: mode = 1;
	mov	_mode,#0x01
	mov	(_mode + 1),#0x00
	C$wsn.h$239$4$164 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:239: LED1_BLINK();
	orl	_P5,#0x10
	mov	dptr,#0x0BB8
	lcall	_Delay_x100us
	mov	r7,_P5
	mov	a,#0xEF
	anl	a,r7
	mov	_P5,a
	sjmp	00125$
00122$:
	C$wsn.h$240$2$162 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:240: } else if (B2_ON()) {
	mov	a,_P5
	jb	acc.1,00119$
	C$wsn.h$241$3$165 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:241: mode = 2;
	mov	_mode,#0x02
	mov	(_mode + 1),#0x00
	C$wsn.h$242$4$166 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:242: LED2_BLINK();
	orl	_P5,#0x20
	mov	dptr,#0x0BB8
	lcall	_Delay_x100us
	mov	r7,_P5
	mov	a,#0xDF
	anl	a,r7
	mov	_P5,a
	sjmp	00125$
00119$:
	C$wsn.h$243$2$162 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:243: } else if (B3_ON()) {
	mov	a,_P5
	jb	acc.2,00116$
	C$wsn.h$244$3$167 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:244: mode = 3;
	mov	_mode,#0x03
	mov	(_mode + 1),#0x00
	C$wsn.h$245$4$168 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:245: LED3_BLINK();
	orl	_P5,#0x40
	mov	dptr,#0x0BB8
	lcall	_Delay_x100us
	mov	r7,_P5
	mov	a,#0xBF
	anl	a,r7
	mov	_P5,a
	sjmp	00125$
00116$:
	C$wsn.h$246$2$162 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:246: } else if (B4_ON()) {
	mov	a,_P5
	jb	acc.3,00125$
	C$wsn.h$247$3$169 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:247: mode = 4;
	mov	_mode,#0x04
	mov	(_mode + 1),#0x00
	C$wsn.h$248$4$170 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:248: LED4_BLINK();
	orl	_P5,#0x80
	mov	dptr,#0x0BB8
	lcall	_Delay_x100us
	mov	r7,_P5
	mov	a,#0x7F
	anl	a,r7
	mov	_P5,a
00125$:
	C$wsn.h$250$1$161 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:250: } while (mode == 0);
	mov	a,_mode
	orl	a,(_mode + 1)
	jnz	00151$
	ljmp	00124$
00151$:
	C$wsn.h$252$1$161 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:252: return mode;
	mov	dpl,_mode
	mov	dph,(_mode + 1)
	C$wsn.h$253$1$161 ==.
	XG$getMode$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'sendTestFrame'
;------------------------------------------------------------
	G$sendTestFrame$0$0 ==.
	C$wsn.h$255$1$161 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:255: void sendTestFrame()
;	-----------------------------------------
;	 function sendTestFrame
;	-----------------------------------------
_sendTestFrame:
	C$wsn.h$257$1$171 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:257: for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
	clr	a
	mov	_slaveAddress,a
	mov	(_slaveAddress + 1),a
00104$:
	C$wsn.h$258$2$172 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:258: sendFrame(0x01);
	mov	dpl,#0x01
	lcall	_sendFrame
	C$wsn.h$259$2$172 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:259: if (waitForFrame(4) == 0x02) {
	mov	dptr,#0x0004
	lcall	_waitForFrame
	mov	r7,dpl
	cjne	r7,#0x02,00105$
	C$wsn.h$260$3$173 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:260: slaves[slaveAddress] = 1;
	mov	a,_slaveAddress
	add	a,_slaveAddress
	mov	r6,a
	mov	a,(_slaveAddress + 1)
	rlc	a
	mov	r7,a
	mov	a,r6
	add	a,#_slaves
	mov	r0,a
	mov	@r0,#0x01
	inc	r0
	mov	@r0,#0x00
00105$:
	C$wsn.h$257$1$171 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:257: for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
	inc	_slaveAddress
	clr	a
	cjne	a,_slaveAddress,00116$
	inc	(_slaveAddress + 1)
00116$:
	clr	c
	mov	a,#0x02
	subb	a,_slaveAddress
	clr	a
	xrl	a,#0x80
	mov	b,(_slaveAddress + 1)
	xrl	b,#0x80
	subb	a,b
	jnc	00104$
	C$wsn.h$263$1$171 ==.
	XG$sendTestFrame$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'sendDataWithoutSecurity'
;------------------------------------------------------------
	G$sendDataWithoutSecurity$0$0 ==.
	C$wsn.h$265$1$171 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:265: void sendDataWithoutSecurity()
;	-----------------------------------------
;	 function sendDataWithoutSecurity
;	-----------------------------------------
_sendDataWithoutSecurity:
	C$wsn.h$267$1$174 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:267: for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
	clr	a
	mov	_slaveAddress,a
	mov	(_slaveAddress + 1),a
00109$:
	C$wsn.h$268$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:268: if (slaves[slaveAddress] != 1) {
	mov	a,_slaveAddress
	add	a,_slaveAddress
	mov	r6,a
	mov	a,(_slaveAddress + 1)
	rlc	a
	mov	a,r6
	add	a,#_slaves
	mov	r1,a
	mov	ar6,@r1
	inc	r1
	mov	ar7,@r1
	dec	r1
	cjne	r6,#0x01,00129$
	cjne	r7,#0x00,00129$
	sjmp	00130$
00129$:
	ljmp	00107$
00130$:
	C$wsn.h$271$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:271: frame[0] = NI;
	mov	_frame,#0xB4
	C$wsn.h$272$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:272: frame[1] = slaveAddress;
	mov	r7,_slaveAddress
	mov	(_frame + 0x0001),r7
	C$wsn.h$273$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:273: frame[2] = (acknowledge == 1) ? 0x83 : 0x03;
	mov	a,#0x01
	cjne	a,_acknowledge,00131$
	clr	a
	cjne	a,(_acknowledge + 1),00131$
	sjmp	00132$
00131$:
	sjmp	00112$
00132$:
	mov	r7,#0x83
	sjmp	00113$
00112$:
	mov	r7,#0x03
00113$:
	mov	(_frame + 0x0002),r7
	C$wsn.h$274$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:274: frame[3] = 0x00;
	mov	(_frame + 0x0003),#0x00
	C$wsn.h$275$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:275: frame[4] = 'M';
	mov	(_frame + 0x0004),#0x4D
	C$wsn.h$276$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:276: frame[5] = 'a';
	mov	(_frame + 0x0005),#0x61
	C$wsn.h$277$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:277: frame[6] = 'r';
	mov	(_frame + 0x0006),#0x72
	C$wsn.h$278$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:278: frame[7] = 'c';
	mov	(_frame + 0x0007),#0x63
	C$wsn.h$279$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:279: frame[8] = 'i';
	mov	(_frame + 0x0008),#0x69
	C$wsn.h$280$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:280: frame[9] = 'n';
	mov	(_frame + 0x0009),#0x6E
	C$wsn.h$281$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:281: frame[10] = 'S';
	mov	(_frame + 0x000a),#0x53
	C$wsn.h$282$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:282: frame[11] = 'z';
	mov	(_frame + 0x000b),#0x7A
	C$wsn.h$283$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:283: frame[12] = 'y';
	mov	(_frame + 0x000c),#0x79
	C$wsn.h$284$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:284: frame[13] = 'm';
	mov	(_frame + 0x000d),#0x6D
	C$wsn.h$285$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:285: frame[14] = 'o';
	mov	(_frame + 0x000e),#0x6F
	C$wsn.h$286$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:286: frame[15] = 'n';
	mov	(_frame + 0x000f),#0x6E
	C$wsn.h$287$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:287: frame[16] = '!';
	mov	(_frame + 0x0010),#0x21
	C$wsn.h$288$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:288: frame[17] = '!';
	mov	(_frame + 0x0011),#0x21
	C$wsn.h$289$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:289: frame[18] = 0x00;
	mov	(_frame + 0x0012),#0x00
	C$wsn.h$291$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:291: sendData(frame);
	mov	dptr,#_frame
	mov	b,#0x40
	lcall	_sendData
	C$wsn.h$292$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:292: if (acknowledge == 1) {
	mov	a,#0x01
	cjne	a,_acknowledge,00133$
	clr	a
	cjne	a,(_acknowledge + 1),00133$
	sjmp	00134$
00133$:
	sjmp	00104$
00134$:
	C$wsn.h$293$3$177 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:293: waitForFrame(4);
	mov	dptr,#0x0004
	lcall	_waitForFrame
00104$:
	C$wsn.h$295$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:295: sendFrame(0x04);
	mov	dpl,#0x04
	lcall	_sendFrame
	C$wsn.h$296$2$175 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:296: if (waitForFrame(4) == 0x05) {
	mov	dptr,#0x0004
	lcall	_waitForFrame
	mov	r7,dpl
	cjne	r7,#0x05,00107$
	C$wsn.h$297$3$178 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:297: sendFrame(0x0D);
	mov	dpl,#0x0D
	lcall	_sendFrame
00107$:
	C$wsn.h$267$1$174 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:267: for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
	inc	_slaveAddress
	clr	a
	cjne	a,_slaveAddress,00137$
	inc	(_slaveAddress + 1)
00137$:
	clr	c
	mov	a,#0x02
	subb	a,_slaveAddress
	clr	a
	xrl	a,#0x80
	mov	b,(_slaveAddress + 1)
	xrl	b,#0x80
	subb	a,b
	jc	00138$
	ljmp	00109$
00138$:
	C$wsn.h$300$1$174 ==.
	XG$sendDataWithoutSecurity$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'sendDataWithCRC8'
;------------------------------------------------------------
	G$sendDataWithCRC8$0$0 ==.
	C$wsn.h$302$1$174 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:302: void sendDataWithCRC8()
;	-----------------------------------------
;	 function sendDataWithCRC8
;	-----------------------------------------
_sendDataWithCRC8:
	C$wsn.h$304$1$179 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:304: for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
	clr	a
	mov	_slaveAddress,a
	mov	(_slaveAddress + 1),a
00109$:
	C$wsn.h$305$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:305: if (slaves[slaveAddress] != 1) {
	mov	a,_slaveAddress
	add	a,_slaveAddress
	mov	r6,a
	mov	a,(_slaveAddress + 1)
	rlc	a
	mov	a,r6
	add	a,#_slaves
	mov	r1,a
	mov	ar6,@r1
	inc	r1
	mov	ar7,@r1
	dec	r1
	cjne	r6,#0x01,00129$
	cjne	r7,#0x00,00129$
	sjmp	00130$
00129$:
	ljmp	00107$
00130$:
	C$wsn.h$308$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:308: frame[0] = NI;
	mov	_frame,#0xB4
	C$wsn.h$309$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:309: frame[1] = slaveAddress;
	mov	r7,_slaveAddress
	mov	(_frame + 0x0001),r7
	C$wsn.h$310$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:310: frame[2] = (acknowledge == 1) ? 0x86 : 0x06;
	mov	a,#0x01
	cjne	a,_acknowledge,00131$
	clr	a
	cjne	a,(_acknowledge + 1),00131$
	sjmp	00132$
00131$:
	sjmp	00112$
00132$:
	mov	r7,#0x86
	sjmp	00113$
00112$:
	mov	r7,#0x06
00113$:
	mov	(_frame + 0x0002),r7
	C$wsn.h$311$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:311: frame[3] = 0x00;
	mov	(_frame + 0x0003),#0x00
	C$wsn.h$312$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:312: frame[4] = 'M';
	mov	(_frame + 0x0004),#0x4D
	C$wsn.h$313$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:313: frame[5] = 'a';
	mov	(_frame + 0x0005),#0x61
	C$wsn.h$314$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:314: frame[6] = 'r';
	mov	(_frame + 0x0006),#0x72
	C$wsn.h$315$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:315: frame[7] = 'c';
	mov	(_frame + 0x0007),#0x63
	C$wsn.h$316$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:316: frame[8] = 'i';
	mov	(_frame + 0x0008),#0x69
	C$wsn.h$317$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:317: frame[9] = 'n';
	mov	(_frame + 0x0009),#0x6E
	C$wsn.h$318$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:318: frame[10] = 'S';
	mov	(_frame + 0x000a),#0x53
	C$wsn.h$319$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:319: frame[11] = 'z';
	mov	(_frame + 0x000b),#0x7A
	C$wsn.h$320$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:320: frame[12] = 'y';
	mov	(_frame + 0x000c),#0x79
	C$wsn.h$321$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:321: frame[13] = 'm';
	mov	(_frame + 0x000d),#0x6D
	C$wsn.h$322$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:322: frame[14] = 'o';
	mov	(_frame + 0x000e),#0x6F
	C$wsn.h$323$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:323: frame[15] = 'n';
	mov	(_frame + 0x000f),#0x6E
	C$wsn.h$324$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:324: frame[16] = '!';
	mov	(_frame + 0x0010),#0x21
	C$wsn.h$325$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:325: frame[17] = '!';
	mov	(_frame + 0x0011),#0x21
	C$wsn.h$326$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:326: frame[18] = crc8(frame[4], crc, 14);
	mov	r5,(_frame + 0x0004)
	mov	r6,#0x00
	mov	r7,#0x00
	mov	_crc8_PARM_2,_crc
	mov	_crc8_PARM_3,#0x0E
	mov	(_crc8_PARM_3 + 1),#0x00
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	_crc8
	mov	a,dpl
	mov	(_frame + 0x0012),a
	C$wsn.h$328$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:328: sendData(frame);
	mov	dptr,#_frame
	mov	b,#0x40
	lcall	_sendData
	C$wsn.h$329$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:329: if (acknowledge == 1) {
	mov	a,#0x01
	cjne	a,_acknowledge,00133$
	clr	a
	cjne	a,(_acknowledge + 1),00133$
	sjmp	00134$
00133$:
	sjmp	00104$
00134$:
	C$wsn.h$330$3$182 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:330: waitForFrame(4);
	mov	dptr,#0x0004
	lcall	_waitForFrame
00104$:
	C$wsn.h$332$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:332: sendFrame(0x07);
	mov	dpl,#0x07
	lcall	_sendFrame
	C$wsn.h$333$2$180 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:333: if (waitForFrame(4) == 0x08) {
	mov	dptr,#0x0004
	lcall	_waitForFrame
	mov	r7,dpl
	cjne	r7,#0x08,00107$
	C$wsn.h$334$3$183 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:334: sendFrame(0x0D);
	mov	dpl,#0x0D
	lcall	_sendFrame
00107$:
	C$wsn.h$304$1$179 ==.
;	C:\Workspace\WSN_MS-4\/wsn.h:304: for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
	inc	_slaveAddress
	clr	a
	cjne	a,_slaveAddress,00137$
	inc	(_slaveAddress + 1)
00137$:
	clr	c
	mov	a,#0x02
	subb	a,_slaveAddress
	clr	a
	xrl	a,#0x80
	mov	b,(_slaveAddress + 1)
	xrl	b,#0x80
	subb	a,b
	jc	00138$
	ljmp	00109$
00138$:
	C$wsn.h$337$1$179 ==.
	XG$sendDataWithCRC8$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;deviceId                  Allocated to registers r6 r7 
;------------------------------------------------------------
	G$main$0$0 ==.
	C$wsnms4.c$8$1$179 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:8: void main (void) 
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
	C$wsnms4.c$10$1$179 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:10: int deviceId = -1;
	mov	r6,#0xFF
	mov	r7,#0xFF
	C$wsnms4.c$11$1$185 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:11: Init_Device();
	push	ar7
	push	ar6
	lcall	_Init_Device
	pop	ar6
	pop	ar7
	C$wsnms4.c$12$1$185 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:12: P5 = 0x0F;
	mov	_P5,#0x0F
	C$wsnms4.c$14$1$185 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:14: do {
00111$:
	C$wsnms4.c$15$2$186 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:15: Wait_for_button_release();
	push	ar7
	push	ar6
	lcall	_Wait_for_button_release
	C$wsnms4.c$16$2$186 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:16: Wait_for_button_pressed();
	lcall	_Wait_for_button_pressed
	pop	ar6
	pop	ar7
	C$wsnms4.c$17$2$186 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:17: if (B1_ON()) {
	mov	a,_P5
	jb	acc.0,00105$
	C$wsnms4.c$18$3$187 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:18: deviceId = P4; // Adres slave, odczytany z mikroprzelacznika P4
	mov	r6,_P4
	mov	r7,#0x00
	C$wsnms4.c$19$4$188 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:19: LED1_BLINK();
	orl	_P5,#0x10
	mov	dptr,#0x0BB8
	push	ar7
	push	ar6
	lcall	_Delay_x100us
	pop	ar6
	pop	ar7
	mov	r5,_P5
	mov	a,#0xEF
	anl	a,r5
	mov	_P5,a
00105$:
	C$wsnms4.c$21$2$186 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:21: if (B2_ON()) {
	mov	a,_P5
	jb	acc.1,00112$
	C$wsnms4.c$22$3$189 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:22: deviceId = 0;
	mov	r6,#0x00
	mov	r7,#0x00
	C$wsnms4.c$23$4$190 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:23: LED2_BLINK();
	orl	_P5,#0x20
	mov	dptr,#0x0BB8
	push	ar7
	push	ar6
	lcall	_Delay_x100us
	pop	ar6
	pop	ar7
	mov	r5,_P5
	mov	a,#0xDF
	anl	a,r5
	mov	_P5,a
00112$:
	C$wsnms4.c$25$1$185 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:25: } while (deviceId == -1);
	cjne	r6,#0xFF,00141$
	cjne	r7,#0xFF,00141$
	sjmp	00111$
00141$:
	C$wsnms4.c$27$1$185 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:27: if (deviceId > 0) {
	clr	c
	clr	a
	subb	a,r6
	clr	a
	xrl	a,#0x80
	mov	b,r7
	xrl	b,#0x80
	subb	a,b
	jnc	00117$
	C$wsnms4.c$28$2$191 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:28: printf("Wybrany wezel w trybie slave, adres: %x\n", deviceId);
	push	ar7
	push	ar6
	push	ar6
	push	ar7
	mov	a,#__str_39
	push	acc
	mov	a,#(__str_39 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar6
	pop	ar7
	C$wsnms4.c$29$2$191 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:29: runSlaveMode(deviceId);
	mov	dpl,r6
	mov	dph,r7
	lcall	_runSlaveMode
	sjmp	00119$
00117$:
	C$wsnms4.c$30$1$185 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:30: } else if (deviceId == 0) {
	mov	a,r6
	orl	a,r7
	jnz	00119$
	C$wsnms4.c$31$2$192 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:31: printf("Wybrany wezel w trybie master\n");
	mov	a,#__str_40
	push	acc
	mov	a,#(__str_40 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	lcall	_printf
	dec	sp
	dec	sp
	dec	sp
	C$wsnms4.c$32$2$192 ==.
;	C:\Workspace\WSN_MS-4\wsnms4.c:32: runMasterMode();
	lcall	_runMasterMode
00119$:
	C$wsnms4.c$34$1$185 ==.
	XG$main$0$0 ==.
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
Fwsnms4$_str_0$0$0 == .
__str_0:
	.db 0x0A
	.ascii "Kalibracja..."
	.db 0x00
Fwsnms4$_str_1$0$0 == .
__str_1:
	.ascii "B"
	.db 0xB3
	.db 0xB9
	.ascii "d kalibracji trybu RX"
	.db 0x0A
	.db 0x00
Fwsnms4$_str_2$0$0 == .
__str_2:
	.ascii "Poprawna kalibracja trybu RX"
	.db 0x0A
	.db 0x00
Fwsnms4$_str_3$0$0 == .
__str_3:
	.ascii "B"
	.db 0xB3
	.db 0xB9
	.ascii "d kalibracji trybu TX"
	.db 0x0A
	.db 0x00
Fwsnms4$_str_4$0$0 == .
__str_4:
	.ascii "Poprawna kalibracja trybu TX"
	.db 0x0A
	.db 0x00
Fwsnms4$_str_5$0$0 == .
__str_5:
	.ascii "MAIN: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_6$0$0 == .
__str_6:
	.ascii "FREQ_2A: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_7$0$0 == .
__str_7:
	.ascii "FREQ_1A: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_8$0$0 == .
__str_8:
	.ascii "FREQ_0A: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_9$0$0 == .
__str_9:
	.ascii "FREQ_2B: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_10$0$0 == .
__str_10:
	.ascii "FREQ_1B: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_11$0$0 == .
__str_11:
	.ascii "FREQ_0B: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_12$0$0 == .
__str_12:
	.ascii "FSEP1: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_13$0$0 == .
__str_13:
	.ascii "FSEP0: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_14$0$0 == .
__str_14:
	.ascii "CURRENT: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_15$0$0 == .
__str_15:
	.ascii "FRONT_END: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_16$0$0 == .
__str_16:
	.ascii "PA_POW: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_17$0$0 == .
__str_17:
	.ascii "PLL: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_18$0$0 == .
__str_18:
	.ascii "LOCK: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_19$0$0 == .
__str_19:
	.ascii "CAL: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_20$0$0 == .
__str_20:
	.ascii "MODEM2: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_21$0$0 == .
__str_21:
	.ascii "MODEM1: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_22$0$0 == .
__str_22:
	.ascii "MODEM0: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_23$0$0 == .
__str_23:
	.ascii "MATCH: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_24$0$0 == .
__str_24:
	.ascii "FSCTRL: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_25$0$0 == .
__str_25:
	.ascii "PRESCALER: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_26$0$0 == .
__str_26:
	.ascii "TEST6: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_27$0$0 == .
__str_27:
	.ascii "TEST5: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_28$0$0 == .
__str_28:
	.ascii "TEST4: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_29$0$0 == .
__str_29:
	.ascii "TEST3: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_30$0$0 == .
__str_30:
	.ascii "TEST2: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_31$0$0 == .
__str_31:
	.ascii "TEST1: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_32$0$0 == .
__str_32:
	.ascii "TEST0: 0x%x "
	.db 0x0A
	.db 0x00
Fwsnms4$_str_33$0$0 == .
__str_33:
	.db 0x0A
	.ascii "N=%x F=%x SI=%x "
	.db 0x00
Fwsnms4$_str_34$0$0 == .
__str_34:
	.ascii "%x"
	.db 0x00
Fwsnms4$_str_35$0$0 == .
__str_35:
	.db 0x0A
	.ascii "S=%x F=%x SI=%x SN=xx "
	.db 0x00
Fwsnms4$_str_36$0$0 == .
__str_36:
	.ascii "Wybrany tryb %d"
	.db 0x00
Fwsnms4$_str_37$0$0 == .
__str_37:
	.ascii "Wybrano tryb z potwierdzeniem"
	.db 0x00
Fwsnms4$_str_38$0$0 == .
__str_38:
	.ascii "Wybrano tryb bez potwierdzenia"
	.db 0x00
Fwsnms4$_str_39$0$0 == .
__str_39:
	.ascii "Wybrany wezel w trybie slave, adres: %x"
	.db 0x0A
	.db 0x00
Fwsnms4$_str_40$0$0 == .
__str_40:
	.ascii "Wybrany wezel w trybie master"
	.db 0x0A
	.db 0x00
	.area XINIT   (CODE)
	.area CABS    (ABS,CODE)
