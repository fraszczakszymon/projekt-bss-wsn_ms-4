#include "compiler_defs.h"
#include "C8051F020_defs.h"
#include "dev.h"
#include "uart.h"
#include "cc1000.h"
#include "crc8.h"
#include "aes128.h"
#include "wsn.h"

int main()
{
    int deviceId = -1;

    Init_Device();
    P5 = 0x0F;

    do {
        Wait_for_button_release();
        Wait_for_button_pressed();
        if (B1_ON()) {
            deviceId = P4; // Adres slave, odczytany z mikroprzelacznika P4
            LED1_BLINK();
        }
        if (B2_ON()) {
            deviceId = 0;
            LED2_BLINK();
        }
    } while (deviceId == -1);

    initializeCC1000();

    if (deviceId > 0) {
        printf("Wybrany wezel w trybie slave, adres: %x\n", deviceId);
        runSlaveMode(deviceId);
    } else if (deviceId == 0) {
        printf("Wybrany wezel w trybie master\n");
        runMasterMode();
    }

    /* ###########################  CRC8  ###########################

        #define crcLen 14

        U8 kodcrc = 0x0000, litery[] = { 'M', 'a', 'r', 'c', 'i', 'n', 'S', 'z', 'y', 'm', 'o', 'n', '!', '!' };
        printf("Kod CRC: %x, dlugosc %d", crc8(litery, kodcrc, crcLen), crcLen);
    */

    /* ########################### AES128 ###########################

        __xdata U8 slowo[DATA_LENGTH];
        __xdata U8 szyfr[DATA_LENGTH];
        int i=0, j=0;

        while(1)
        {  
            U8 slowo[] = { 'M', 'a', 'r', 'c', 'i', 'n', 'S', 'z', 'y', 'm', 'o', 'n', 'B', 'S', 'S', '!' };
            printf("Wpisane dane:\n");
            for (i=0; i < j; i++) {
                putchar(slowo[i]);
            }

            KeyExpansion();
            printf("\n\n");

            for (i=0; i<4*Nb*(Nr+1); i++) {
                printf("%x ", EXP_KEYS[i]);
                if ((i+1)%(4*Nb) == 0)
                    printf("\n");
            }

            Cipher(&slowo[0], &szyfr[0]);
            
            printf("\n\nZaszyfrowane dane:\n");
            for (i=0; i < j; i++) {
                printf("%x ", szyfr[i]);
            }

            InvCipher(&szyfr[0], &slowo[0]);
            printf("\n\nOdszyfrowane dane:\n");
            for (i=0; i < j; i++) {
                putchar(slowo[i]);
            }
        }
    */


    /* ########################### CC1000 ###########################

        U8 mode = 0, i, frcnt = 0, size, a = 0, frame[16];

        do {
            Wait_for_button_release (); // Czekaj na zwolnienie przyciskow
            Wait_for_button_pressed (); // Czekaj na nacisniecie przycisku
            // Odbiornik
            if (B1_ON()) {
                mode = 1;                   
                LED1_BLINK();
            }
            // Nadajnik
            if (B2_ON()) {
                mode = 2;
                LED2_BLINK();
            }
        } while (mode == 0x00);

        CC1000_Initialize();
        CC1000_DisplayAllRegisters();

        while(1) {
            // Odbiornik
            if (mode==1) {
                CC1000_WakeUpToRX(CC1000_RX_CURRENT); // Wybudzenie do trybu RX
                CC1000_SetupRX(CC1000_RX_CURRENT);
                P0MDOUT &= ~(0x08);                   // Ustaw (P0.3) DIO open-drain
                DIO = 1;                              // (P0 |= 0x08;) Przygotowanie do odbioru
                PREAMBULE = 0;

                while(!CC1000_Sync());
                    frame[0]=CC1000_ReceiveByte();

                if(frame[0] == 0x20) {
                    printf("%x ", frame[i]);
                    for (i=1; i<16; i++) {
                        frame[i] = CC1000_ReceiveByte();  // Funkcja odbiera poszczegolne bajty ramki.
                        printf("%x ", frame[i]);
                    }
                }

                printf("\n");
                printf("Dlugosc: %x \n",frame[0]);
                printf("BS: %x \n",frame[1]);
                printf("SN: %x \n",frame[2]);
                printf("FF: %x \n",frame[3]);
                printf("Dane: ");
                for (i=4; i<16; i++) {
                    putchar(frame[i]);
                }
                printf("\n");

                if(frame[1] & 1)
                    LED1_ON();
                else
                    LED1_OFF();
                if(frame[1] & 2)
                    LED2_ON();
                else
                    LED2_OFF();
                if(frame[1] & 4)
                    LED3_ON();
                else
                    LED3_OFF();

                // Oczekiwanie na synchronizacje i odbior danych
                // Stan z ladunku B1-B3 przepisac na LED1-LED3
                // Wyslanie tresci ladunku na terminal
                LED4_BLINK();
            }
            // Nadajnik (Przelaczenie DIO do push-pull jest w RFSendPacket(..))
            if (mode == 2) {
                // Przygotowanie ladunku: TxBuffer[0] = dlugosc ladunku,
                // TxBuffer[1] = stan 3 przyciskow modulu TOOLSTICK UNI DC (B1-B3)
                // TxBuffer[2] = sekwencyjny numer pakietu
                // TxBuffer[3] = staly bajt 0xFF
                Delay_x100us(10000);
                CC1000_WakeUpToTX(CC1000_TX_CURRENT);            // wybudzenie do TX
                CC1000_SetupTX(CC1000_TX_CURRENT);
                P0MDOUT |= 0x08;                                 // wyjscie P0.3 (DIO) push-pull
                printf("\nWybudzenie do TX ok");
                for (i = 0; i < CC1000_PREAMBULE_LENGTH; ++i)    // wysylanie preambuly
                    CC1000_SendByte(CC1000_PREAMBULE);

                CC1000_SendByte(CC1000_SYNC_BYTE);               // bajt synchronizacyjny (SFD)

                CC1000_SendByte(0x20);
                CC1000_SendByte(0x00);
                CC1000_SendByte(0xDA);
                CC1000_SendByte(0xD9);
                CC1000_SendByte('S');
                CC1000_SendByte('z');
                CC1000_SendByte('y');
                CC1000_SendByte('m');
                CC1000_SendByte('o');
                CC1000_SendByte('n');
                CC1000_SendByte('M');
                CC1000_SendByte('a');
                CC1000_SendByte('r');
                CC1000_SendByte('c');
                CC1000_SendByte('i');
                CC1000_SetupPD();

                LED4_BLINK();
            }
        }
    */

    getchar();
}