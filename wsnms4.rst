                              1 ;--------------------------------------------------------
                              2 ; File Created by SDCC : free open source ANSI-C Compiler
                              3 ; Version 3.3.0 #8604 (May 11 2013) (MINGW32)
                              4 ; This file was generated Tue Jan 28 20:56:30 2014
                              5 ;--------------------------------------------------------
                              6 	.module wsnms4
                              7 	.optsdcc -mmcs51 --model-small
                              8 	
                              9 ;--------------------------------------------------------
                             10 ; Public variables in this module
                             11 ;--------------------------------------------------------
                             12 	.globl _CC1000_WriteToRegister_PARM_2
                             13 	.globl _crc8_PARM_3
                             14 	.globl _crc8_PARM_2
                             15 	.globl _main
                             16 	.globl _crc8
                             17 	.globl _printf
                             18 	.globl _printBCD
                             19 	.globl _gethex
                             20 	.globl _printc
                             21 	.globl _getchar
                             22 	.globl _putchar
                             23 	.globl _UART_SetBaudRate
                             24 	.globl _B4_PRESSED
                             25 	.globl _B3_PRESSED
                             26 	.globl _B2_PRESSED
                             27 	.globl _B1_PRESSED
                             28 	.globl _Wait_for_button_pressed
                             29 	.globl _Wait_for_button_release
                             30 	.globl _Delay_x100us
                             31 	.globl _Tus100_ISR
                             32 	.globl _Init_Device
                             33 	.globl _Interrupts_Init
                             34 	.globl _Oscillator_Init
                             35 	.globl _Port_IO_Init
                             36 	.globl _UART_Init
                             37 	.globl _Timer_Init
                             38 	.globl _Reset_Sources_Init
                             39 	.globl _PALE
                             40 	.globl _PDATA
                             41 	.globl _PCLK
                             42 	.globl _DIO
                             43 	.globl _DCLK
                             44 	.globl _SPIEN
                             45 	.globl _MSTEN
                             46 	.globl _SLVSEL
                             47 	.globl _TXBSY
                             48 	.globl _RXOVRN
                             49 	.globl _MODF
                             50 	.globl _WCOL
                             51 	.globl _SPIF
                             52 	.globl _AD0LJST
                             53 	.globl _AD0WINT
                             54 	.globl _AD0CM0
                             55 	.globl _AD0CM1
                             56 	.globl _AD0BUSY
                             57 	.globl _AD0INT
                             58 	.globl _AD0TM
                             59 	.globl _AD0EN
                             60 	.globl _CCF0
                             61 	.globl _CCF1
                             62 	.globl _CCF2
                             63 	.globl _CCF3
                             64 	.globl _CCF4
                             65 	.globl _CR
                             66 	.globl _CF
                             67 	.globl _P
                             68 	.globl _F1
                             69 	.globl _OV
                             70 	.globl _RS0
                             71 	.globl _RS1
                             72 	.globl _F0
                             73 	.globl _AC
                             74 	.globl _CY
                             75 	.globl _CPRL2
                             76 	.globl _CT2
                             77 	.globl _TR2
                             78 	.globl _EXEN2
                             79 	.globl _TCLK0
                             80 	.globl _RCLK0
                             81 	.globl _EXF2
                             82 	.globl _TF2
                             83 	.globl _SMBTOE
                             84 	.globl _SMBFTE
                             85 	.globl _AA
                             86 	.globl _SI
                             87 	.globl _STO
                             88 	.globl _STA
                             89 	.globl _ENSMB
                             90 	.globl _BUSY
                             91 	.globl _PX0
                             92 	.globl _PT0
                             93 	.globl _PX1
                             94 	.globl _PT1
                             95 	.globl _PS
                             96 	.globl _PT2
                             97 	.globl _EX0
                             98 	.globl _ET0
                             99 	.globl _EX1
                            100 	.globl _ET1
                            101 	.globl _ES0
                            102 	.globl _ET2
                            103 	.globl _IEGF0
                            104 	.globl _EA
                            105 	.globl _RI0
                            106 	.globl _TI0
                            107 	.globl _RB80
                            108 	.globl _TB80
                            109 	.globl _REN0
                            110 	.globl _SM20
                            111 	.globl _SM10
                            112 	.globl _SM00
                            113 	.globl _IT0
                            114 	.globl _IE0
                            115 	.globl _IT1
                            116 	.globl _IE1
                            117 	.globl _TR0
                            118 	.globl _TF0
                            119 	.globl _TR1
                            120 	.globl _TF1
                            121 	.globl __XPAGE
                            122 	.globl _DAC1
                            123 	.globl _DAC0
                            124 	.globl _TMR4
                            125 	.globl _TMR4RL
                            126 	.globl _T4
                            127 	.globl _RCAP4
                            128 	.globl _TMR2
                            129 	.globl _TMR2RL
                            130 	.globl _T2
                            131 	.globl _RCAP2
                            132 	.globl _ADC0LT
                            133 	.globl _ADC0GT
                            134 	.globl _ADC0
                            135 	.globl _TMR3
                            136 	.globl _TMR3RL
                            137 	.globl _DP
                            138 	.globl _WDTCN
                            139 	.globl _PCA0CPH4
                            140 	.globl _PCA0CPH3
                            141 	.globl _PCA0CPH2
                            142 	.globl _PCA0CPH1
                            143 	.globl _PCA0CPH0
                            144 	.globl _PCA0H
                            145 	.globl _SPI0CN
                            146 	.globl _EIP2
                            147 	.globl _EIP1
                            148 	.globl _TH4
                            149 	.globl _TL4
                            150 	.globl _SADDR1
                            151 	.globl _SBUF1
                            152 	.globl _SCON1
                            153 	.globl _B
                            154 	.globl _RSTSRC
                            155 	.globl _PCA0CPL4
                            156 	.globl _PCA0CPL3
                            157 	.globl _PCA0CPL2
                            158 	.globl _PCA0CPL1
                            159 	.globl _PCA0CPL0
                            160 	.globl _PCA0L
                            161 	.globl _ADC0CN
                            162 	.globl _EIE2
                            163 	.globl _EIE1
                            164 	.globl _RCAP4H
                            165 	.globl _RCAP4L
                            166 	.globl _XBR2
                            167 	.globl _XBR1
                            168 	.globl _XBR0
                            169 	.globl _ACC
                            170 	.globl _PCA0CPM4
                            171 	.globl _PCA0CPM3
                            172 	.globl _PCA0CPM2
                            173 	.globl _PCA0CPM1
                            174 	.globl _PCA0CPM0
                            175 	.globl _PCA0MD
                            176 	.globl _PCA0CN
                            177 	.globl _DAC1CN
                            178 	.globl _DAC1H
                            179 	.globl _DAC1L
                            180 	.globl _DAC0CN
                            181 	.globl _DAC0H
                            182 	.globl _DAC0L
                            183 	.globl _REF0CN
                            184 	.globl _PSW
                            185 	.globl _SMB0CR
                            186 	.globl _TH2
                            187 	.globl _TL2
                            188 	.globl _RCAP2H
                            189 	.globl _RCAP2L
                            190 	.globl _T4CON
                            191 	.globl _T2CON
                            192 	.globl _ADC0LTH
                            193 	.globl _ADC0LTL
                            194 	.globl _ADC0GTH
                            195 	.globl _ADC0GTL
                            196 	.globl _SMB0ADR
                            197 	.globl _SMB0DAT
                            198 	.globl _SMB0STA
                            199 	.globl _SMB0CN
                            200 	.globl _ADC0H
                            201 	.globl _ADC0L
                            202 	.globl _P1MDIN
                            203 	.globl _ADC0CF
                            204 	.globl _AMX0SL
                            205 	.globl _AMX0CF
                            206 	.globl _SADEN0
                            207 	.globl _IP
                            208 	.globl _FLACL
                            209 	.globl _FLSCL
                            210 	.globl _P74OUT
                            211 	.globl _OSCICN
                            212 	.globl _OSCXCN
                            213 	.globl _P3
                            214 	.globl _EMI0CN
                            215 	.globl _SADEN1
                            216 	.globl _P3IF
                            217 	.globl _AMX1SL
                            218 	.globl _ADC1CF
                            219 	.globl _ADC1CN
                            220 	.globl _SADDR0
                            221 	.globl _IE
                            222 	.globl _P3MDOUT
                            223 	.globl _P2MDOUT
                            224 	.globl _P1MDOUT
                            225 	.globl _P0MDOUT
                            226 	.globl _EMI0CF
                            227 	.globl _EMI0TC
                            228 	.globl _P2
                            229 	.globl _CPT1CN
                            230 	.globl _CPT0CN
                            231 	.globl _SPI0CKR
                            232 	.globl _ADC1
                            233 	.globl _SPI0DAT
                            234 	.globl _SPI0CFG
                            235 	.globl _SBUF0
                            236 	.globl _SCON0
                            237 	.globl _P7
                            238 	.globl _TMR3H
                            239 	.globl _TMR3L
                            240 	.globl _TMR3RLH
                            241 	.globl _TMR3RLL
                            242 	.globl _TMR3CN
                            243 	.globl _P1
                            244 	.globl _PSCTL
                            245 	.globl _CKCON
                            246 	.globl _TH1
                            247 	.globl _TH0
                            248 	.globl _TL1
                            249 	.globl _TL0
                            250 	.globl _TMOD
                            251 	.globl _TCON
                            252 	.globl _PCON
                            253 	.globl _P6
                            254 	.globl _P5
                            255 	.globl _P4
                            256 	.globl _DPH
                            257 	.globl _DPL
                            258 	.globl _SP
                            259 	.globl _P0
                            260 	.globl _acknowledge
                            261 	.globl _mode
                            262 	.globl _i
                            263 	.globl _slaves
                            264 	.globl _slaveAddress
                            265 	.globl _crc
                            266 	.globl _frame
                            267 	.globl _PREAMBULE
                            268 	.globl _printBCD_PARM_2
                            269 	.globl _T_1s
                            270 	.globl _T_1ms
                            271 	.globl _hus
                            272 	.globl _us100
                            273 	.globl _CC1000_WriteToRegister
                            274 	.globl _CC1000_ReadFromRegister
                            275 	.globl _BitReceive
                            276 	.globl _CC1000_ReceiveByte
                            277 	.globl _BitSend
                            278 	.globl _CC1000_SendByte
                            279 	.globl _CC1000_Reset
                            280 	.globl _CC1000_Calibrate
                            281 	.globl _CC1000_SetupRX
                            282 	.globl _CC1000_SetupTX
                            283 	.globl _CC1000_SetupPD
                            284 	.globl _CC1000_WakeUpToRX
                            285 	.globl _CC1000_WakeUpToTX
                            286 	.globl _CC1000_ConfigureRegistersRx
                            287 	.globl _CC1000_ConfigureRegistersTx
                            288 	.globl _CC1000_ResetFreqSynth
                            289 	.globl _CC1000_Initialize
                            290 	.globl _CC1000_ConfigureRegisters
                            291 	.globl _CC1000_DisplayAllRegisters
                            292 	.globl _CC1000_Sync
                            293 	.globl _clearFrame
                            294 	.globl _sendFrame
                            295 	.globl _sendData
                            296 	.globl _waitForFrame
                            297 	.globl _runSlaveMode
                            298 	.globl _sendDataWithoutSecurityToMaster
                            299 	.globl _sendDataWithCRC8ToMaster
                            300 	.globl _runMasterMode
                            301 	.globl _getMode
                            302 	.globl _sendTestFrame
                            303 	.globl _sendDataWithoutSecurity
                            304 	.globl _sendDataWithCRC8
                            305 ;--------------------------------------------------------
                            306 ; special function registers
                            307 ;--------------------------------------------------------
                            308 	.area RSEG    (ABS,DATA)
   0000                     309 	.org 0x0000
                     0080   310 G$P0$0$0 == 0x0080
                     0080   311 _P0	=	0x0080
                     0081   312 G$SP$0$0 == 0x0081
                     0081   313 _SP	=	0x0081
                     0082   314 G$DPL$0$0 == 0x0082
                     0082   315 _DPL	=	0x0082
                     0083   316 G$DPH$0$0 == 0x0083
                     0083   317 _DPH	=	0x0083
                     0084   318 G$P4$0$0 == 0x0084
                     0084   319 _P4	=	0x0084
                     0085   320 G$P5$0$0 == 0x0085
                     0085   321 _P5	=	0x0085
                     0086   322 G$P6$0$0 == 0x0086
                     0086   323 _P6	=	0x0086
                     0087   324 G$PCON$0$0 == 0x0087
                     0087   325 _PCON	=	0x0087
                     0088   326 G$TCON$0$0 == 0x0088
                     0088   327 _TCON	=	0x0088
                     0089   328 G$TMOD$0$0 == 0x0089
                     0089   329 _TMOD	=	0x0089
                     008A   330 G$TL0$0$0 == 0x008a
                     008A   331 _TL0	=	0x008a
                     008B   332 G$TL1$0$0 == 0x008b
                     008B   333 _TL1	=	0x008b
                     008C   334 G$TH0$0$0 == 0x008c
                     008C   335 _TH0	=	0x008c
                     008D   336 G$TH1$0$0 == 0x008d
                     008D   337 _TH1	=	0x008d
                     008E   338 G$CKCON$0$0 == 0x008e
                     008E   339 _CKCON	=	0x008e
                     008F   340 G$PSCTL$0$0 == 0x008f
                     008F   341 _PSCTL	=	0x008f
                     0090   342 G$P1$0$0 == 0x0090
                     0090   343 _P1	=	0x0090
                     0091   344 G$TMR3CN$0$0 == 0x0091
                     0091   345 _TMR3CN	=	0x0091
                     0092   346 G$TMR3RLL$0$0 == 0x0092
                     0092   347 _TMR3RLL	=	0x0092
                     0093   348 G$TMR3RLH$0$0 == 0x0093
                     0093   349 _TMR3RLH	=	0x0093
                     0094   350 G$TMR3L$0$0 == 0x0094
                     0094   351 _TMR3L	=	0x0094
                     0095   352 G$TMR3H$0$0 == 0x0095
                     0095   353 _TMR3H	=	0x0095
                     0096   354 G$P7$0$0 == 0x0096
                     0096   355 _P7	=	0x0096
                     0098   356 G$SCON0$0$0 == 0x0098
                     0098   357 _SCON0	=	0x0098
                     0099   358 G$SBUF0$0$0 == 0x0099
                     0099   359 _SBUF0	=	0x0099
                     009A   360 G$SPI0CFG$0$0 == 0x009a
                     009A   361 _SPI0CFG	=	0x009a
                     009B   362 G$SPI0DAT$0$0 == 0x009b
                     009B   363 _SPI0DAT	=	0x009b
                     009C   364 G$ADC1$0$0 == 0x009c
                     009C   365 _ADC1	=	0x009c
                     009D   366 G$SPI0CKR$0$0 == 0x009d
                     009D   367 _SPI0CKR	=	0x009d
                     009E   368 G$CPT0CN$0$0 == 0x009e
                     009E   369 _CPT0CN	=	0x009e
                     009F   370 G$CPT1CN$0$0 == 0x009f
                     009F   371 _CPT1CN	=	0x009f
                     00A0   372 G$P2$0$0 == 0x00a0
                     00A0   373 _P2	=	0x00a0
                     00A1   374 G$EMI0TC$0$0 == 0x00a1
                     00A1   375 _EMI0TC	=	0x00a1
                     00A3   376 G$EMI0CF$0$0 == 0x00a3
                     00A3   377 _EMI0CF	=	0x00a3
                     00A4   378 G$P0MDOUT$0$0 == 0x00a4
                     00A4   379 _P0MDOUT	=	0x00a4
                     00A5   380 G$P1MDOUT$0$0 == 0x00a5
                     00A5   381 _P1MDOUT	=	0x00a5
                     00A6   382 G$P2MDOUT$0$0 == 0x00a6
                     00A6   383 _P2MDOUT	=	0x00a6
                     00A7   384 G$P3MDOUT$0$0 == 0x00a7
                     00A7   385 _P3MDOUT	=	0x00a7
                     00A8   386 G$IE$0$0 == 0x00a8
                     00A8   387 _IE	=	0x00a8
                     00A9   388 G$SADDR0$0$0 == 0x00a9
                     00A9   389 _SADDR0	=	0x00a9
                     00AA   390 G$ADC1CN$0$0 == 0x00aa
                     00AA   391 _ADC1CN	=	0x00aa
                     00AB   392 G$ADC1CF$0$0 == 0x00ab
                     00AB   393 _ADC1CF	=	0x00ab
                     00AC   394 G$AMX1SL$0$0 == 0x00ac
                     00AC   395 _AMX1SL	=	0x00ac
                     00AD   396 G$P3IF$0$0 == 0x00ad
                     00AD   397 _P3IF	=	0x00ad
                     00AE   398 G$SADEN1$0$0 == 0x00ae
                     00AE   399 _SADEN1	=	0x00ae
                     00AF   400 G$EMI0CN$0$0 == 0x00af
                     00AF   401 _EMI0CN	=	0x00af
                     00B0   402 G$P3$0$0 == 0x00b0
                     00B0   403 _P3	=	0x00b0
                     00B1   404 G$OSCXCN$0$0 == 0x00b1
                     00B1   405 _OSCXCN	=	0x00b1
                     00B2   406 G$OSCICN$0$0 == 0x00b2
                     00B2   407 _OSCICN	=	0x00b2
                     00B5   408 G$P74OUT$0$0 == 0x00b5
                     00B5   409 _P74OUT	=	0x00b5
                     00B6   410 G$FLSCL$0$0 == 0x00b6
                     00B6   411 _FLSCL	=	0x00b6
                     00B7   412 G$FLACL$0$0 == 0x00b7
                     00B7   413 _FLACL	=	0x00b7
                     00B8   414 G$IP$0$0 == 0x00b8
                     00B8   415 _IP	=	0x00b8
                     00B9   416 G$SADEN0$0$0 == 0x00b9
                     00B9   417 _SADEN0	=	0x00b9
                     00BA   418 G$AMX0CF$0$0 == 0x00ba
                     00BA   419 _AMX0CF	=	0x00ba
                     00BB   420 G$AMX0SL$0$0 == 0x00bb
                     00BB   421 _AMX0SL	=	0x00bb
                     00BC   422 G$ADC0CF$0$0 == 0x00bc
                     00BC   423 _ADC0CF	=	0x00bc
                     00BD   424 G$P1MDIN$0$0 == 0x00bd
                     00BD   425 _P1MDIN	=	0x00bd
                     00BE   426 G$ADC0L$0$0 == 0x00be
                     00BE   427 _ADC0L	=	0x00be
                     00BF   428 G$ADC0H$0$0 == 0x00bf
                     00BF   429 _ADC0H	=	0x00bf
                     00C0   430 G$SMB0CN$0$0 == 0x00c0
                     00C0   431 _SMB0CN	=	0x00c0
                     00C1   432 G$SMB0STA$0$0 == 0x00c1
                     00C1   433 _SMB0STA	=	0x00c1
                     00C2   434 G$SMB0DAT$0$0 == 0x00c2
                     00C2   435 _SMB0DAT	=	0x00c2
                     00C3   436 G$SMB0ADR$0$0 == 0x00c3
                     00C3   437 _SMB0ADR	=	0x00c3
                     00C4   438 G$ADC0GTL$0$0 == 0x00c4
                     00C4   439 _ADC0GTL	=	0x00c4
                     00C5   440 G$ADC0GTH$0$0 == 0x00c5
                     00C5   441 _ADC0GTH	=	0x00c5
                     00C6   442 G$ADC0LTL$0$0 == 0x00c6
                     00C6   443 _ADC0LTL	=	0x00c6
                     00C7   444 G$ADC0LTH$0$0 == 0x00c7
                     00C7   445 _ADC0LTH	=	0x00c7
                     00C8   446 G$T2CON$0$0 == 0x00c8
                     00C8   447 _T2CON	=	0x00c8
                     00C9   448 G$T4CON$0$0 == 0x00c9
                     00C9   449 _T4CON	=	0x00c9
                     00CA   450 G$RCAP2L$0$0 == 0x00ca
                     00CA   451 _RCAP2L	=	0x00ca
                     00CB   452 G$RCAP2H$0$0 == 0x00cb
                     00CB   453 _RCAP2H	=	0x00cb
                     00CC   454 G$TL2$0$0 == 0x00cc
                     00CC   455 _TL2	=	0x00cc
                     00CD   456 G$TH2$0$0 == 0x00cd
                     00CD   457 _TH2	=	0x00cd
                     00CF   458 G$SMB0CR$0$0 == 0x00cf
                     00CF   459 _SMB0CR	=	0x00cf
                     00D0   460 G$PSW$0$0 == 0x00d0
                     00D0   461 _PSW	=	0x00d0
                     00D1   462 G$REF0CN$0$0 == 0x00d1
                     00D1   463 _REF0CN	=	0x00d1
                     00D2   464 G$DAC0L$0$0 == 0x00d2
                     00D2   465 _DAC0L	=	0x00d2
                     00D3   466 G$DAC0H$0$0 == 0x00d3
                     00D3   467 _DAC0H	=	0x00d3
                     00D4   468 G$DAC0CN$0$0 == 0x00d4
                     00D4   469 _DAC0CN	=	0x00d4
                     00D5   470 G$DAC1L$0$0 == 0x00d5
                     00D5   471 _DAC1L	=	0x00d5
                     00D6   472 G$DAC1H$0$0 == 0x00d6
                     00D6   473 _DAC1H	=	0x00d6
                     00D7   474 G$DAC1CN$0$0 == 0x00d7
                     00D7   475 _DAC1CN	=	0x00d7
                     00D8   476 G$PCA0CN$0$0 == 0x00d8
                     00D8   477 _PCA0CN	=	0x00d8
                     00D9   478 G$PCA0MD$0$0 == 0x00d9
                     00D9   479 _PCA0MD	=	0x00d9
                     00DA   480 G$PCA0CPM0$0$0 == 0x00da
                     00DA   481 _PCA0CPM0	=	0x00da
                     00DB   482 G$PCA0CPM1$0$0 == 0x00db
                     00DB   483 _PCA0CPM1	=	0x00db
                     00DC   484 G$PCA0CPM2$0$0 == 0x00dc
                     00DC   485 _PCA0CPM2	=	0x00dc
                     00DD   486 G$PCA0CPM3$0$0 == 0x00dd
                     00DD   487 _PCA0CPM3	=	0x00dd
                     00DE   488 G$PCA0CPM4$0$0 == 0x00de
                     00DE   489 _PCA0CPM4	=	0x00de
                     00E0   490 G$ACC$0$0 == 0x00e0
                     00E0   491 _ACC	=	0x00e0
                     00E1   492 G$XBR0$0$0 == 0x00e1
                     00E1   493 _XBR0	=	0x00e1
                     00E2   494 G$XBR1$0$0 == 0x00e2
                     00E2   495 _XBR1	=	0x00e2
                     00E3   496 G$XBR2$0$0 == 0x00e3
                     00E3   497 _XBR2	=	0x00e3
                     00E4   498 G$RCAP4L$0$0 == 0x00e4
                     00E4   499 _RCAP4L	=	0x00e4
                     00E5   500 G$RCAP4H$0$0 == 0x00e5
                     00E5   501 _RCAP4H	=	0x00e5
                     00E6   502 G$EIE1$0$0 == 0x00e6
                     00E6   503 _EIE1	=	0x00e6
                     00E7   504 G$EIE2$0$0 == 0x00e7
                     00E7   505 _EIE2	=	0x00e7
                     00E8   506 G$ADC0CN$0$0 == 0x00e8
                     00E8   507 _ADC0CN	=	0x00e8
                     00E9   508 G$PCA0L$0$0 == 0x00e9
                     00E9   509 _PCA0L	=	0x00e9
                     00EA   510 G$PCA0CPL0$0$0 == 0x00ea
                     00EA   511 _PCA0CPL0	=	0x00ea
                     00EB   512 G$PCA0CPL1$0$0 == 0x00eb
                     00EB   513 _PCA0CPL1	=	0x00eb
                     00EC   514 G$PCA0CPL2$0$0 == 0x00ec
                     00EC   515 _PCA0CPL2	=	0x00ec
                     00ED   516 G$PCA0CPL3$0$0 == 0x00ed
                     00ED   517 _PCA0CPL3	=	0x00ed
                     00EE   518 G$PCA0CPL4$0$0 == 0x00ee
                     00EE   519 _PCA0CPL4	=	0x00ee
                     00EF   520 G$RSTSRC$0$0 == 0x00ef
                     00EF   521 _RSTSRC	=	0x00ef
                     00F0   522 G$B$0$0 == 0x00f0
                     00F0   523 _B	=	0x00f0
                     00F1   524 G$SCON1$0$0 == 0x00f1
                     00F1   525 _SCON1	=	0x00f1
                     00F2   526 G$SBUF1$0$0 == 0x00f2
                     00F2   527 _SBUF1	=	0x00f2
                     00F3   528 G$SADDR1$0$0 == 0x00f3
                     00F3   529 _SADDR1	=	0x00f3
                     00F4   530 G$TL4$0$0 == 0x00f4
                     00F4   531 _TL4	=	0x00f4
                     00F5   532 G$TH4$0$0 == 0x00f5
                     00F5   533 _TH4	=	0x00f5
                     00F6   534 G$EIP1$0$0 == 0x00f6
                     00F6   535 _EIP1	=	0x00f6
                     00F7   536 G$EIP2$0$0 == 0x00f7
                     00F7   537 _EIP2	=	0x00f7
                     00F8   538 G$SPI0CN$0$0 == 0x00f8
                     00F8   539 _SPI0CN	=	0x00f8
                     00F9   540 G$PCA0H$0$0 == 0x00f9
                     00F9   541 _PCA0H	=	0x00f9
                     00FA   542 G$PCA0CPH0$0$0 == 0x00fa
                     00FA   543 _PCA0CPH0	=	0x00fa
                     00FB   544 G$PCA0CPH1$0$0 == 0x00fb
                     00FB   545 _PCA0CPH1	=	0x00fb
                     00FC   546 G$PCA0CPH2$0$0 == 0x00fc
                     00FC   547 _PCA0CPH2	=	0x00fc
                     00FD   548 G$PCA0CPH3$0$0 == 0x00fd
                     00FD   549 _PCA0CPH3	=	0x00fd
                     00FE   550 G$PCA0CPH4$0$0 == 0x00fe
                     00FE   551 _PCA0CPH4	=	0x00fe
                     00FF   552 G$WDTCN$0$0 == 0x00ff
                     00FF   553 _WDTCN	=	0x00ff
                     8382   554 G$DP$0$0 == 0x8382
                     8382   555 _DP	=	0x8382
                     9392   556 G$TMR3RL$0$0 == 0x9392
                     9392   557 _TMR3RL	=	0x9392
                     9594   558 G$TMR3$0$0 == 0x9594
                     9594   559 _TMR3	=	0x9594
                     BFBE   560 G$ADC0$0$0 == 0xbfbe
                     BFBE   561 _ADC0	=	0xbfbe
                     C5C4   562 G$ADC0GT$0$0 == 0xc5c4
                     C5C4   563 _ADC0GT	=	0xc5c4
                     C7C6   564 G$ADC0LT$0$0 == 0xc7c6
                     C7C6   565 _ADC0LT	=	0xc7c6
                     CBCA   566 G$RCAP2$0$0 == 0xcbca
                     CBCA   567 _RCAP2	=	0xcbca
                     CDCC   568 G$T2$0$0 == 0xcdcc
                     CDCC   569 _T2	=	0xcdcc
                     CBCA   570 G$TMR2RL$0$0 == 0xcbca
                     CBCA   571 _TMR2RL	=	0xcbca
                     CDCC   572 G$TMR2$0$0 == 0xcdcc
                     CDCC   573 _TMR2	=	0xcdcc
                     E5E4   574 G$RCAP4$0$0 == 0xe5e4
                     E5E4   575 _RCAP4	=	0xe5e4
                     F5F4   576 G$T4$0$0 == 0xf5f4
                     F5F4   577 _T4	=	0xf5f4
                     E5E4   578 G$TMR4RL$0$0 == 0xe5e4
                     E5E4   579 _TMR4RL	=	0xe5e4
                     F5F4   580 G$TMR4$0$0 == 0xf5f4
                     F5F4   581 _TMR4	=	0xf5f4
                     D3D2   582 G$DAC0$0$0 == 0xd3d2
                     D3D2   583 _DAC0	=	0xd3d2
                     D6D5   584 G$DAC1$0$0 == 0xd6d5
                     D6D5   585 _DAC1	=	0xd6d5
                     00AF   586 G$_XPAGE$0$0 == 0x00af
                     00AF   587 __XPAGE	=	0x00af
                            588 ;--------------------------------------------------------
                            589 ; special function bits
                            590 ;--------------------------------------------------------
                            591 	.area RSEG    (ABS,DATA)
   0000                     592 	.org 0x0000
                     008F   593 G$TF1$0$0 == 0x008f
                     008F   594 _TF1	=	0x008f
                     008E   595 G$TR1$0$0 == 0x008e
                     008E   596 _TR1	=	0x008e
                     008D   597 G$TF0$0$0 == 0x008d
                     008D   598 _TF0	=	0x008d
                     008C   599 G$TR0$0$0 == 0x008c
                     008C   600 _TR0	=	0x008c
                     008B   601 G$IE1$0$0 == 0x008b
                     008B   602 _IE1	=	0x008b
                     008A   603 G$IT1$0$0 == 0x008a
                     008A   604 _IT1	=	0x008a
                     0089   605 G$IE0$0$0 == 0x0089
                     0089   606 _IE0	=	0x0089
                     0088   607 G$IT0$0$0 == 0x0088
                     0088   608 _IT0	=	0x0088
                     009F   609 G$SM00$0$0 == 0x009f
                     009F   610 _SM00	=	0x009f
                     009E   611 G$SM10$0$0 == 0x009e
                     009E   612 _SM10	=	0x009e
                     009D   613 G$SM20$0$0 == 0x009d
                     009D   614 _SM20	=	0x009d
                     009C   615 G$REN0$0$0 == 0x009c
                     009C   616 _REN0	=	0x009c
                     009B   617 G$TB80$0$0 == 0x009b
                     009B   618 _TB80	=	0x009b
                     009A   619 G$RB80$0$0 == 0x009a
                     009A   620 _RB80	=	0x009a
                     0099   621 G$TI0$0$0 == 0x0099
                     0099   622 _TI0	=	0x0099
                     0098   623 G$RI0$0$0 == 0x0098
                     0098   624 _RI0	=	0x0098
                     00AF   625 G$EA$0$0 == 0x00af
                     00AF   626 _EA	=	0x00af
                     00AE   627 G$IEGF0$0$0 == 0x00ae
                     00AE   628 _IEGF0	=	0x00ae
                     00AD   629 G$ET2$0$0 == 0x00ad
                     00AD   630 _ET2	=	0x00ad
                     00AC   631 G$ES0$0$0 == 0x00ac
                     00AC   632 _ES0	=	0x00ac
                     00AB   633 G$ET1$0$0 == 0x00ab
                     00AB   634 _ET1	=	0x00ab
                     00AA   635 G$EX1$0$0 == 0x00aa
                     00AA   636 _EX1	=	0x00aa
                     00A9   637 G$ET0$0$0 == 0x00a9
                     00A9   638 _ET0	=	0x00a9
                     00A8   639 G$EX0$0$0 == 0x00a8
                     00A8   640 _EX0	=	0x00a8
                     00BD   641 G$PT2$0$0 == 0x00bd
                     00BD   642 _PT2	=	0x00bd
                     00BC   643 G$PS$0$0 == 0x00bc
                     00BC   644 _PS	=	0x00bc
                     00BB   645 G$PT1$0$0 == 0x00bb
                     00BB   646 _PT1	=	0x00bb
                     00BA   647 G$PX1$0$0 == 0x00ba
                     00BA   648 _PX1	=	0x00ba
                     00B9   649 G$PT0$0$0 == 0x00b9
                     00B9   650 _PT0	=	0x00b9
                     00B8   651 G$PX0$0$0 == 0x00b8
                     00B8   652 _PX0	=	0x00b8
                     00C7   653 G$BUSY$0$0 == 0x00c7
                     00C7   654 _BUSY	=	0x00c7
                     00C6   655 G$ENSMB$0$0 == 0x00c6
                     00C6   656 _ENSMB	=	0x00c6
                     00C5   657 G$STA$0$0 == 0x00c5
                     00C5   658 _STA	=	0x00c5
                     00C4   659 G$STO$0$0 == 0x00c4
                     00C4   660 _STO	=	0x00c4
                     00C3   661 G$SI$0$0 == 0x00c3
                     00C3   662 _SI	=	0x00c3
                     00C2   663 G$AA$0$0 == 0x00c2
                     00C2   664 _AA	=	0x00c2
                     00C1   665 G$SMBFTE$0$0 == 0x00c1
                     00C1   666 _SMBFTE	=	0x00c1
                     00C0   667 G$SMBTOE$0$0 == 0x00c0
                     00C0   668 _SMBTOE	=	0x00c0
                     00CF   669 G$TF2$0$0 == 0x00cf
                     00CF   670 _TF2	=	0x00cf
                     00CE   671 G$EXF2$0$0 == 0x00ce
                     00CE   672 _EXF2	=	0x00ce
                     00CD   673 G$RCLK0$0$0 == 0x00cd
                     00CD   674 _RCLK0	=	0x00cd
                     00CC   675 G$TCLK0$0$0 == 0x00cc
                     00CC   676 _TCLK0	=	0x00cc
                     00CB   677 G$EXEN2$0$0 == 0x00cb
                     00CB   678 _EXEN2	=	0x00cb
                     00CA   679 G$TR2$0$0 == 0x00ca
                     00CA   680 _TR2	=	0x00ca
                     00C9   681 G$CT2$0$0 == 0x00c9
                     00C9   682 _CT2	=	0x00c9
                     00C8   683 G$CPRL2$0$0 == 0x00c8
                     00C8   684 _CPRL2	=	0x00c8
                     00D7   685 G$CY$0$0 == 0x00d7
                     00D7   686 _CY	=	0x00d7
                     00D6   687 G$AC$0$0 == 0x00d6
                     00D6   688 _AC	=	0x00d6
                     00D5   689 G$F0$0$0 == 0x00d5
                     00D5   690 _F0	=	0x00d5
                     00D4   691 G$RS1$0$0 == 0x00d4
                     00D4   692 _RS1	=	0x00d4
                     00D3   693 G$RS0$0$0 == 0x00d3
                     00D3   694 _RS0	=	0x00d3
                     00D2   695 G$OV$0$0 == 0x00d2
                     00D2   696 _OV	=	0x00d2
                     00D1   697 G$F1$0$0 == 0x00d1
                     00D1   698 _F1	=	0x00d1
                     00D0   699 G$P$0$0 == 0x00d0
                     00D0   700 _P	=	0x00d0
                     00DF   701 G$CF$0$0 == 0x00df
                     00DF   702 _CF	=	0x00df
                     00DE   703 G$CR$0$0 == 0x00de
                     00DE   704 _CR	=	0x00de
                     00DC   705 G$CCF4$0$0 == 0x00dc
                     00DC   706 _CCF4	=	0x00dc
                     00DB   707 G$CCF3$0$0 == 0x00db
                     00DB   708 _CCF3	=	0x00db
                     00DA   709 G$CCF2$0$0 == 0x00da
                     00DA   710 _CCF2	=	0x00da
                     00D9   711 G$CCF1$0$0 == 0x00d9
                     00D9   712 _CCF1	=	0x00d9
                     00D8   713 G$CCF0$0$0 == 0x00d8
                     00D8   714 _CCF0	=	0x00d8
                     00EF   715 G$AD0EN$0$0 == 0x00ef
                     00EF   716 _AD0EN	=	0x00ef
                     00EE   717 G$AD0TM$0$0 == 0x00ee
                     00EE   718 _AD0TM	=	0x00ee
                     00ED   719 G$AD0INT$0$0 == 0x00ed
                     00ED   720 _AD0INT	=	0x00ed
                     00EC   721 G$AD0BUSY$0$0 == 0x00ec
                     00EC   722 _AD0BUSY	=	0x00ec
                     00EB   723 G$AD0CM1$0$0 == 0x00eb
                     00EB   724 _AD0CM1	=	0x00eb
                     00EA   725 G$AD0CM0$0$0 == 0x00ea
                     00EA   726 _AD0CM0	=	0x00ea
                     00E9   727 G$AD0WINT$0$0 == 0x00e9
                     00E9   728 _AD0WINT	=	0x00e9
                     00E8   729 G$AD0LJST$0$0 == 0x00e8
                     00E8   730 _AD0LJST	=	0x00e8
                     00FF   731 G$SPIF$0$0 == 0x00ff
                     00FF   732 _SPIF	=	0x00ff
                     00FE   733 G$WCOL$0$0 == 0x00fe
                     00FE   734 _WCOL	=	0x00fe
                     00FD   735 G$MODF$0$0 == 0x00fd
                     00FD   736 _MODF	=	0x00fd
                     00FC   737 G$RXOVRN$0$0 == 0x00fc
                     00FC   738 _RXOVRN	=	0x00fc
                     00FB   739 G$TXBSY$0$0 == 0x00fb
                     00FB   740 _TXBSY	=	0x00fb
                     00FA   741 G$SLVSEL$0$0 == 0x00fa
                     00FA   742 _SLVSEL	=	0x00fa
                     00F9   743 G$MSTEN$0$0 == 0x00f9
                     00F9   744 _MSTEN	=	0x00f9
                     00F8   745 G$SPIEN$0$0 == 0x00f8
                     00F8   746 _SPIEN	=	0x00f8
                     0082   747 G$DCLK$0$0 == 0x0082
                     0082   748 _DCLK	=	0x0082
                     0083   749 G$DIO$0$0 == 0x0083
                     0083   750 _DIO	=	0x0083
                     0085   751 G$PCLK$0$0 == 0x0085
                     0085   752 _PCLK	=	0x0085
                     0084   753 G$PDATA$0$0 == 0x0084
                     0084   754 _PDATA	=	0x0084
                     0087   755 G$PALE$0$0 == 0x0087
                     0087   756 _PALE	=	0x0087
                            757 ;--------------------------------------------------------
                            758 ; overlayable register banks
                            759 ;--------------------------------------------------------
                            760 	.area REG_BANK_0	(REL,OVR,DATA)
   0000                     761 	.ds 8
                            762 ;--------------------------------------------------------
                            763 ; internal ram data
                            764 ;--------------------------------------------------------
                            765 	.area DSEG    (DATA)
                     0000   766 G$us100$0$0==.
   0021                     767 _us100::
   0021                     768 	.ds 1
                     0001   769 G$hus$0$0==.
   0022                     770 _hus::
   0022                     771 	.ds 2
                     0003   772 G$T_1ms$0$0==.
   0024                     773 _T_1ms::
   0024                     774 	.ds 2
                     0005   775 G$T_1s$0$0==.
   0026                     776 _T_1s::
   0026                     777 	.ds 2
                     0007   778 Lwsnms4.printBCD$precission$1$44==.
   0028                     779 _printBCD_PARM_2:
   0028                     780 	.ds 1
                     0008   781 Lwsnms4.printBCD$tmp$1$45==.
   0029                     782 _printBCD_tmp_1_45:
   0029                     783 	.ds 5
                     000D   784 Fwsnms4$Transceiver_State$0$0==.
   002E                     785 _Transceiver_State:
   002E                     786 	.ds 1
                     000E   787 Fwsnms4$Transceiver_Timeout$0$0==.
   002F                     788 _Transceiver_Timeout:
   002F                     789 	.ds 1
                     000F   790 Fwsnms4$NrSekw$0$0==.
   0030                     791 _NrSekw:
   0030                     792 	.ds 1
                     0010   793 G$PREAMBULE$0$0==.
   0031                     794 _PREAMBULE::
   0031                     795 	.ds 4
                     0014   796 G$frame$0$0==.
   0035                     797 _frame::
   0035                     798 	.ds 19
                     0027   799 G$crc$0$0==.
   0048                     800 _crc::
   0048                     801 	.ds 1
                     0028   802 G$slaveAddress$0$0==.
   0049                     803 _slaveAddress::
   0049                     804 	.ds 2
                     002A   805 G$slaves$0$0==.
   004B                     806 _slaves::
   004B                     807 	.ds 6
                     0030   808 G$i$0$0==.
   0051                     809 _i::
   0051                     810 	.ds 2
                     0032   811 G$mode$0$0==.
   0053                     812 _mode::
   0053                     813 	.ds 2
                     0034   814 G$acknowledge$0$0==.
   0055                     815 _acknowledge::
   0055                     816 	.ds 2
                     0036   817 Lwsnms4.sendData$frame$1$141==.
   0057                     818 _sendData_frame_1_141:
   0057                     819 	.ds 3
                            820 ;--------------------------------------------------------
                            821 ; overlayable items in internal ram 
                            822 ;--------------------------------------------------------
                            823 	.area	OSEG    (OVR,DATA)
                            824 	.area	OSEG    (OVR,DATA)
                            825 	.area	OSEG    (OVR,DATA)
                            826 	.area	OSEG    (OVR,DATA)
                            827 	.area	OSEG    (OVR,DATA)
                            828 	.area	OSEG    (OVR,DATA)
                     0000   829 Lwsnms4.crc8$crc$1$56==.
   0009                     830 _crc8_PARM_2:
   0009                     831 	.ds 1
                     0001   832 Lwsnms4.crc8$len$1$56==.
   000A                     833 _crc8_PARM_3:
   000A                     834 	.ds 2
                            835 	.area	OSEG    (OVR,DATA)
                     0000   836 Lwsnms4.CC1000_WriteToRegister$value$1$80==.
   0009                     837 _CC1000_WriteToRegister_PARM_2:
   0009                     838 	.ds 1
                            839 	.area	OSEG    (OVR,DATA)
                            840 	.area	OSEG    (OVR,DATA)
                            841 ;--------------------------------------------------------
                            842 ; Stack segment in internal ram 
                            843 ;--------------------------------------------------------
                            844 	.area	SSEG	(DATA)
   005A                     845 __start__stack:
   005A                     846 	.ds	1
                            847 
                            848 ;--------------------------------------------------------
                            849 ; indirectly addressable internal ram data
                            850 ;--------------------------------------------------------
                            851 	.area ISEG    (DATA)
                            852 ;--------------------------------------------------------
                            853 ; absolute internal ram data
                            854 ;--------------------------------------------------------
                            855 	.area IABS    (ABS,DATA)
                            856 	.area IABS    (ABS,DATA)
                            857 ;--------------------------------------------------------
                            858 ; bit data
                            859 ;--------------------------------------------------------
                            860 	.area BSEG    (BIT)
                     0000   861 Lwsnms4.printBCD$s$1$45==.
   0000                     862 _printBCD_s_1_45:
   0000                     863 	.ds 1
                            864 ;--------------------------------------------------------
                            865 ; paged external ram data
                            866 ;--------------------------------------------------------
                            867 	.area PSEG    (PAG,XDATA)
                            868 ;--------------------------------------------------------
                            869 ; external ram data
                            870 ;--------------------------------------------------------
                            871 	.area XSEG    (XDATA)
                            872 ;--------------------------------------------------------
                            873 ; absolute external ram data
                            874 ;--------------------------------------------------------
                            875 	.area XABS    (ABS,XDATA)
                            876 ;--------------------------------------------------------
                            877 ; external initialized ram data
                            878 ;--------------------------------------------------------
                            879 	.area XISEG   (XDATA)
                            880 	.area HOME    (CODE)
                            881 	.area GSINIT0 (CODE)
                            882 	.area GSINIT1 (CODE)
                            883 	.area GSINIT2 (CODE)
                            884 	.area GSINIT3 (CODE)
                            885 	.area GSINIT4 (CODE)
                            886 	.area GSINIT5 (CODE)
                            887 	.area GSINIT  (CODE)
                            888 	.area GSFINAL (CODE)
                            889 	.area CSEG    (CODE)
                            890 ;--------------------------------------------------------
                            891 ; interrupt vector 
                            892 ;--------------------------------------------------------
                            893 	.area HOME    (CODE)
   0000                     894 __interrupt_vect:
   0000 02 00 31      [24]  895 	ljmp	__sdcc_gsinit_startup
   0003 32            [24]  896 	reti
   0004                     897 	.ds	7
   000B 32            [24]  898 	reti
   000C                     899 	.ds	7
   0013 32            [24]  900 	reti
   0014                     901 	.ds	7
   001B 32            [24]  902 	reti
   001C                     903 	.ds	7
   0023 32            [24]  904 	reti
   0024                     905 	.ds	7
   002B 02 01 33      [24]  906 	ljmp	_Tus100_ISR
                            907 ;--------------------------------------------------------
                            908 ; global & static initialisations
                            909 ;--------------------------------------------------------
                            910 	.area HOME    (CODE)
                            911 	.area GSINIT  (CODE)
                            912 	.area GSFINAL (CODE)
                            913 	.area GSINIT  (CODE)
                            914 	.globl __sdcc_gsinit_startup
                            915 	.globl __sdcc_program_startup
                            916 	.globl __start__stack
                            917 	.globl __mcs51_genXINIT
                            918 	.globl __mcs51_genXRAMCLEAR
                            919 	.globl __mcs51_genRAMCLEAR
                     0000   920 	C$dev.h$108$1$185 ==.
                            921 ;	C:\Workspace\WSN_MS-4\/dev.h:108: volatile U8   us100  = 0;		
   008A 75 21 00      [24]  922 	mov	_us100,#0x00
                     0003   923 	C$dev.h$109$1$185 ==.
                            924 ;	C:\Workspace\WSN_MS-4\/dev.h:109: volatile U16  hus    = 0;	// us x 100
   008D E4            [12]  925 	clr	a
   008E F5 22         [12]  926 	mov	_hus,a
   0090 F5 23         [12]  927 	mov	(_hus + 1),a
                     0008   928 	C$dev.h$110$1$185 ==.
                            929 ;	C:\Workspace\WSN_MS-4\/dev.h:110: volatile U16  T_1ms  = 0; 	// TIME_MS   = 0;
   0092 E4            [12]  930 	clr	a
   0093 F5 24         [12]  931 	mov	_T_1ms,a
   0095 F5 25         [12]  932 	mov	(_T_1ms + 1),a
                     000D   933 	C$dev.h$111$1$185 ==.
                            934 ;	C:\Workspace\WSN_MS-4\/dev.h:111: volatile U16  T_1s   = 0;	// TIME      = 0;
   0097 E4            [12]  935 	clr	a
   0098 F5 26         [12]  936 	mov	_T_1s,a
   009A F5 27         [12]  937 	mov	(_T_1s + 1),a
                     0012   938 	C$cc1000.h$136$1$185 ==.
                            939 ;	C:\Workspace\WSN_MS-4\/cc1000.h:136: static U8 Transceiver_State = 0;
   009C 75 2E 00      [24]  940 	mov	_Transceiver_State,#0x00
                     0015   941 	C$cc1000.h$137$1$185 ==.
                            942 ;	C:\Workspace\WSN_MS-4\/cc1000.h:137: static U8 Transceiver_Timeout = 10;
   009F 75 2F 0A      [24]  943 	mov	_Transceiver_Timeout,#0x0A
                     0018   944 	C$cc1000.h$138$1$185 ==.
                            945 ;	C:\Workspace\WSN_MS-4\/cc1000.h:138: static U8 NrSekw = 0;
   00A2 75 30 00      [24]  946 	mov	_NrSekw,#0x00
                     001B   947 	C$wsn.h$14$1$185 ==.
                            948 ;	C:\Workspace\WSN_MS-4\/wsn.h:14: U8 crc = 0x0000;
   00A5 75 48 00      [24]  949 	mov	_crc,#0x00
                     001E   950 	C$wsn.h$15$1$185 ==.
                            951 ;	C:\Workspace\WSN_MS-4\/wsn.h:15: int slaveAddress = 0x00;
   00A8 E4            [12]  952 	clr	a
   00A9 F5 49         [12]  953 	mov	_slaveAddress,a
   00AB F5 4A         [12]  954 	mov	(_slaveAddress + 1),a
                     0023   955 	C$wsn.h$16$1$185 ==.
                            956 ;	C:\Workspace\WSN_MS-4\/wsn.h:16: int slaves[] = {0, 0, 0};
   00AD E4            [12]  957 	clr	a
   00AE F5 4B         [12]  958 	mov	(_slaves + 0),a
   00B0 F5 4C         [12]  959 	mov	(_slaves + 1),a
   00B2 F5 4D         [12]  960 	mov	((_slaves + 0x0002) + 0),a
   00B4 F5 4E         [12]  961 	mov	((_slaves + 0x0002) + 1),a
   00B6 F5 4F         [12]  962 	mov	((_slaves + 0x0004) + 0),a
   00B8 F5 50         [12]  963 	mov	((_slaves + 0x0004) + 1),a
                     0030   964 	C$wsn.h$17$1$185 ==.
                            965 ;	C:\Workspace\WSN_MS-4\/wsn.h:17: int i, mode = 0;
   00BA E4            [12]  966 	clr	a
   00BB F5 53         [12]  967 	mov	_mode,a
   00BD F5 54         [12]  968 	mov	(_mode + 1),a
                     0035   969 	C$wsn.h$18$1$185 ==.
                            970 ;	C:\Workspace\WSN_MS-4\/wsn.h:18: int acknowledge = 0;
   00BF E4            [12]  971 	clr	a
   00C0 F5 55         [12]  972 	mov	_acknowledge,a
   00C2 F5 56         [12]  973 	mov	(_acknowledge + 1),a
                            974 	.area GSFINAL (CODE)
   00C4 02 00 2E      [24]  975 	ljmp	__sdcc_program_startup
                            976 ;--------------------------------------------------------
                            977 ; Home
                            978 ;--------------------------------------------------------
                            979 	.area HOME    (CODE)
                            980 	.area HOME    (CODE)
   002E                     981 __sdcc_program_startup:
   002E 02 16 E9      [24]  982 	ljmp	_main
                            983 ;	return from main will return to caller
                            984 ;--------------------------------------------------------
                            985 ; code
                            986 ;--------------------------------------------------------
                            987 	.area CSEG    (CODE)
                            988 ;------------------------------------------------------------
                            989 ;Allocation info for local variables in function 'Reset_Sources_Init'
                            990 ;------------------------------------------------------------
                     0000   991 	G$Reset_Sources_Init$0$0 ==.
                     0000   992 	C$dev.h$41$0$0 ==.
                            993 ;	C:\Workspace\WSN_MS-4\/dev.h:41: void Reset_Sources_Init()
                            994 ;	-----------------------------------------
                            995 ;	 function Reset_Sources_Init
                            996 ;	-----------------------------------------
   00C7                     997 _Reset_Sources_Init:
                     0007   998 	ar7 = 0x07
                     0006   999 	ar6 = 0x06
                     0005  1000 	ar5 = 0x05
                     0004  1001 	ar4 = 0x04
                     0003  1002 	ar3 = 0x03
                     0002  1003 	ar2 = 0x02
                     0001  1004 	ar1 = 0x01
                     0000  1005 	ar0 = 0x00
                     0000  1006 	C$dev.h$43$1$1 ==.
                           1007 ;	C:\Workspace\WSN_MS-4\/dev.h:43: WDTCN     = 0xDE;
   00C7 75 FF DE      [24] 1008 	mov	_WDTCN,#0xDE
                     0003  1009 	C$dev.h$44$1$1 ==.
                           1010 ;	C:\Workspace\WSN_MS-4\/dev.h:44: WDTCN     = 0xAD;
   00CA 75 FF AD      [24] 1011 	mov	_WDTCN,#0xAD
                     0006  1012 	C$dev.h$45$1$1 ==.
                           1013 ;	C:\Workspace\WSN_MS-4\/dev.h:45: RSTSRC    = 0x04;
   00CD 75 EF 04      [24] 1014 	mov	_RSTSRC,#0x04
                     0009  1015 	C$dev.h$46$1$1 ==.
                     0009  1016 	XG$Reset_Sources_Init$0$0 ==.
   00D0 22            [24] 1017 	ret
                           1018 ;------------------------------------------------------------
                           1019 ;Allocation info for local variables in function 'Timer_Init'
                           1020 ;------------------------------------------------------------
                     000A  1021 	G$Timer_Init$0$0 ==.
                     000A  1022 	C$dev.h$48$1$1 ==.
                           1023 ;	C:\Workspace\WSN_MS-4\/dev.h:48: void Timer_Init()
                           1024 ;	-----------------------------------------
                           1025 ;	 function Timer_Init
                           1026 ;	-----------------------------------------
   00D1                    1027 _Timer_Init:
                     000A  1028 	C$dev.h$50$1$2 ==.
                           1029 ;	C:\Workspace\WSN_MS-4\/dev.h:50: CKCON     = 0x38;   // Timer0, Timer1 i Timer2 u�ywaj� SYSCLK
   00D1 75 8E 38      [24] 1030 	mov	_CKCON,#0x38
                     000D  1031 	C$dev.h$51$1$2 ==.
                           1032 ;	C:\Workspace\WSN_MS-4\/dev.h:51: TCON      = 0x50;   // Zezwolenie (start) dla Timer0 (TR0) i Timer1 (TR1)
   00D4 75 88 50      [24] 1033 	mov	_TCON,#0x50
                     0010  1034 	C$dev.h$52$1$2 ==.
                           1035 ;	C:\Workspace\WSN_MS-4\/dev.h:52: TMOD      = 0x21;   // Timer0 - tryb 16-bitowy, Timer1 - tryb 8-bitowy z autoprze�adowaniem
   00D7 75 89 21      [24] 1036 	mov	_TMOD,#0x21
                     0013  1037 	C$dev.h$53$1$2 ==.
                           1038 ;	C:\Workspace\WSN_MS-4\/dev.h:53: TH1       = 0xFA;   // Warto�� prze�adowania Timera1 dla pr�dkosci 115200 bit/s
   00DA 75 8D FA      [24] 1039 	mov	_TH1,#0xFA
                     0016  1040 	C$dev.h$54$1$2 ==.
                           1041 ;	C:\Workspace\WSN_MS-4\/dev.h:54: T2CON     = 0x04;   // Start Timera2
   00DD 75 C8 04      [24] 1042 	mov	_T2CON,#0x04
                     0019  1043 	C$dev.h$55$1$2 ==.
                           1044 ;	C:\Workspace\WSN_MS-4\/dev.h:55: RCAP2L = (U8) (65536-(SYSCLK/10000)); 		// Dla przerwa� 100us z T2L
   00E0 75 CA 5D      [24] 1045 	mov	_RCAP2L,#0x5D
                     001C  1046 	C$dev.h$56$1$2 ==.
                           1047 ;	C:\Workspace\WSN_MS-4\/dev.h:56: RCAP2H = (U8) ((65536-(SYSCLK/10000)) >>8);
   00E3 75 CB F7      [24] 1048 	mov	_RCAP2H,#0xF7
                     001F  1049 	C$dev.h$57$1$2 ==.
                           1050 ;	C:\Workspace\WSN_MS-4\/dev.h:57: TL2 = RCAP2L;   // Warto�� prze�adowania Timera2
   00E6 85 CA CC      [24] 1051 	mov	_TL2,_RCAP2L
                     0022  1052 	C$dev.h$58$1$2 ==.
                           1053 ;	C:\Workspace\WSN_MS-4\/dev.h:58: TH2 = RCAP2H;   //   "           "          " 
   00E9 85 CB CD      [24] 1054 	mov	_TH2,_RCAP2H
                     0025  1055 	C$dev.h$59$1$2 ==.
                     0025  1056 	XG$Timer_Init$0$0 ==.
   00EC 22            [24] 1057 	ret
                           1058 ;------------------------------------------------------------
                           1059 ;Allocation info for local variables in function 'UART_Init'
                           1060 ;------------------------------------------------------------
                     0026  1061 	G$UART_Init$0$0 ==.
                     0026  1062 	C$dev.h$61$1$2 ==.
                           1063 ;	C:\Workspace\WSN_MS-4\/dev.h:61: void UART_Init()
                           1064 ;	-----------------------------------------
                           1065 ;	 function UART_Init
                           1066 ;	-----------------------------------------
   00ED                    1067 _UART_Init:
                     0026  1068 	C$dev.h$63$1$3 ==.
                           1069 ;	C:\Workspace\WSN_MS-4\/dev.h:63: SCON0     = 0x50;	// Tryb 8-bitowy; zezwolenie na odbi�r
   00ED 75 98 50      [24] 1070 	mov	_SCON0,#0x50
                     0029  1071 	C$dev.h$64$1$3 ==.
                           1072 ;	C:\Workspace\WSN_MS-4\/dev.h:64: TI0		  = 1;		// Wa�ny; Transmit Interrupt Flag;  	
   00F0 D2 99         [12] 1073 	setb	_TI0
                     002B  1074 	C$dev.h$65$1$3 ==.
                           1075 ;	C:\Workspace\WSN_MS-4\/dev.h:65: SCON1     = 0x50;	// Tryb 8-bitowy; zezwolenie na odbi�r
   00F2 75 F1 50      [24] 1076 	mov	_SCON1,#0x50
                     002E  1077 	C$dev.h$67$1$3 ==.
                     002E  1078 	XG$UART_Init$0$0 ==.
   00F5 22            [24] 1079 	ret
                           1080 ;------------------------------------------------------------
                           1081 ;Allocation info for local variables in function 'Port_IO_Init'
                           1082 ;------------------------------------------------------------
                     002F  1083 	G$Port_IO_Init$0$0 ==.
                     002F  1084 	C$dev.h$69$1$3 ==.
                           1085 ;	C:\Workspace\WSN_MS-4\/dev.h:69: void Port_IO_Init()
                           1086 ;	-----------------------------------------
                           1087 ;	 function Port_IO_Init
                           1088 ;	-----------------------------------------
   00F6                    1089 _Port_IO_Init:
                     002F  1090 	C$dev.h$71$1$4 ==.
                           1091 ;	C:\Workspace\WSN_MS-4\/dev.h:71: P0MDOUT   = 0xA1;	// Port P0.0(TX0), P0.5(PCLK), P0.7(PALE) - wyj. push-pull
   00F6 75 A4 A1      [24] 1092 	mov	_P0MDOUT,#0xA1
                     0032  1093 	C$dev.h$72$1$4 ==.
                           1094 ;	C:\Workspace\WSN_MS-4\/dev.h:72: P74OUT    = 0x08;   // Port P5.4-P5.7 (LED1-4) - wyj. push-pull  
   00F9 75 B5 08      [24] 1095 	mov	_P74OUT,#0x08
                     0035  1096 	C$dev.h$73$1$4 ==.
                           1097 ;	C:\Workspace\WSN_MS-4\/dev.h:73: XBR0      = 0x04;   // Pod��czenie do port�w UART0
   00FC 75 E1 04      [24] 1098 	mov	_XBR0,#0x04
                     0038  1099 	C$dev.h$74$1$4 ==.
                           1100 ;	C:\Workspace\WSN_MS-4\/dev.h:74: XBR2      = 0x40;   // Zezwolenie Crossbar 
   00FF 75 E3 40      [24] 1101 	mov	_XBR2,#0x40
                     003B  1102 	C$dev.h$75$1$4 ==.
                     003B  1103 	XG$Port_IO_Init$0$0 ==.
   0102 22            [24] 1104 	ret
                           1105 ;------------------------------------------------------------
                           1106 ;Allocation info for local variables in function 'Oscillator_Init'
                           1107 ;------------------------------------------------------------
                           1108 ;i                         Allocated to registers r6 r7 
                           1109 ;------------------------------------------------------------
                     003C  1110 	G$Oscillator_Init$0$0 ==.
                     003C  1111 	C$dev.h$77$1$4 ==.
                           1112 ;	C:\Workspace\WSN_MS-4\/dev.h:77: void Oscillator_Init()
                           1113 ;	-----------------------------------------
                           1114 ;	 function Oscillator_Init
                           1115 ;	-----------------------------------------
   0103                    1116 _Oscillator_Init:
                     003C  1117 	C$dev.h$80$1$5 ==.
                           1118 ;	C:\Workspace\WSN_MS-4\/dev.h:80: OSCXCN    = 0x67;
   0103 75 B1 67      [24] 1119 	mov	_OSCXCN,#0x67
                     003F  1120 	C$dev.h$81$1$5 ==.
                           1121 ;	C:\Workspace\WSN_MS-4\/dev.h:81: for (i = 0; i < 3000; i++);  // Wait 1ms for initialization
   0106 7E B8         [12] 1122 	mov	r6,#0xB8
   0108 7F 0B         [12] 1123 	mov	r7,#0x0B
   010A                    1124 00107$:
   010A 1E            [12] 1125 	dec	r6
   010B BE FF 01      [24] 1126 	cjne	r6,#0xFF,00121$
   010E 1F            [12] 1127 	dec	r7
   010F                    1128 00121$:
   010F EE            [12] 1129 	mov	a,r6
   0110 4F            [12] 1130 	orl	a,r7
   0111 70 F7         [24] 1131 	jnz	00107$
                     004C  1132 	C$dev.h$82$1$5 ==.
                           1133 ;	C:\Workspace\WSN_MS-4\/dev.h:82: while ((OSCXCN & 0x80) == 0);
   0113                    1134 00102$:
   0113 E5 B1         [12] 1135 	mov	a,_OSCXCN
   0115 30 E7 FB      [24] 1136 	jnb	acc.7,00102$
                     0051  1137 	C$dev.h$83$1$5 ==.
                           1138 ;	C:\Workspace\WSN_MS-4\/dev.h:83: OSCICN    = 0x08;
   0118 75 B2 08      [24] 1139 	mov	_OSCICN,#0x08
                     0054  1140 	C$dev.h$84$1$5 ==.
                     0054  1141 	XG$Oscillator_Init$0$0 ==.
   011B 22            [24] 1142 	ret
                           1143 ;------------------------------------------------------------
                           1144 ;Allocation info for local variables in function 'Interrupts_Init'
                           1145 ;------------------------------------------------------------
                     0055  1146 	G$Interrupts_Init$0$0 ==.
                     0055  1147 	C$dev.h$86$1$5 ==.
                           1148 ;	C:\Workspace\WSN_MS-4\/dev.h:86: void Interrupts_Init()
                           1149 ;	-----------------------------------------
                           1150 ;	 function Interrupts_Init
                           1151 ;	-----------------------------------------
   011C                    1152 _Interrupts_Init:
                     0055  1153 	C$dev.h$88$1$6 ==.
                           1154 ;	C:\Workspace\WSN_MS-4\/dev.h:88: IE        = 0xA0;   // Zezwolenia na przerwania globalne i od Timera2 
   011C 75 A8 A0      [24] 1155 	mov	_IE,#0xA0
                     0058  1156 	C$dev.h$89$1$6 ==.
                     0058  1157 	XG$Interrupts_Init$0$0 ==.
   011F 22            [24] 1158 	ret
                           1159 ;------------------------------------------------------------
                           1160 ;Allocation info for local variables in function 'Init_Device'
                           1161 ;------------------------------------------------------------
                     0059  1162 	G$Init_Device$0$0 ==.
                     0059  1163 	C$dev.h$93$1$6 ==.
                           1164 ;	C:\Workspace\WSN_MS-4\/dev.h:93: void Init_Device(void)
                           1165 ;	-----------------------------------------
                           1166 ;	 function Init_Device
                           1167 ;	-----------------------------------------
   0120                    1168 _Init_Device:
                     0059  1169 	C$dev.h$95$1$8 ==.
                           1170 ;	C:\Workspace\WSN_MS-4\/dev.h:95: Reset_Sources_Init();
   0120 12 00 C7      [24] 1171 	lcall	_Reset_Sources_Init
                     005C  1172 	C$dev.h$96$1$8 ==.
                           1173 ;	C:\Workspace\WSN_MS-4\/dev.h:96: Timer_Init();
   0123 12 00 D1      [24] 1174 	lcall	_Timer_Init
                     005F  1175 	C$dev.h$97$1$8 ==.
                           1176 ;	C:\Workspace\WSN_MS-4\/dev.h:97: UART_Init();
   0126 12 00 ED      [24] 1177 	lcall	_UART_Init
                     0062  1178 	C$dev.h$98$1$8 ==.
                           1179 ;	C:\Workspace\WSN_MS-4\/dev.h:98: Port_IO_Init();
   0129 12 00 F6      [24] 1180 	lcall	_Port_IO_Init
                     0065  1181 	C$dev.h$99$1$8 ==.
                           1182 ;	C:\Workspace\WSN_MS-4\/dev.h:99: Oscillator_Init();
   012C 12 01 03      [24] 1183 	lcall	_Oscillator_Init
                     0068  1184 	C$dev.h$100$1$8 ==.
                           1185 ;	C:\Workspace\WSN_MS-4\/dev.h:100: Interrupts_Init();
   012F 12 01 1C      [24] 1186 	lcall	_Interrupts_Init
                     006B  1187 	C$dev.h$101$1$8 ==.
                     006B  1188 	XG$Init_Device$0$0 ==.
   0132 22            [24] 1189 	ret
                           1190 ;------------------------------------------------------------
                           1191 ;Allocation info for local variables in function 'Tus100_ISR'
                           1192 ;------------------------------------------------------------
                     006C  1193 	G$Tus100_ISR$0$0 ==.
                     006C  1194 	C$dev.h$115$1$8 ==.
                           1195 ;	C:\Workspace\WSN_MS-4\/dev.h:115: void Tus100_ISR (void) __interrupt 5    // Przerwania 100us
                           1196 ;	-----------------------------------------
                           1197 ;	 function Tus100_ISR
                           1198 ;	-----------------------------------------
   0133                    1199 _Tus100_ISR:
   0133 C0 E0         [24] 1200 	push	acc
   0135 C0 D0         [24] 1201 	push	psw
                     0070  1202 	C$dev.h$117$1$10 ==.
                           1203 ;	C:\Workspace\WSN_MS-4\/dev.h:117: TF2 = 0;
   0137 C2 CF         [12] 1204 	clr	_TF2
                     0072  1205 	C$dev.h$118$1$10 ==.
                           1206 ;	C:\Workspace\WSN_MS-4\/dev.h:118: ++us100;
   0139 05 21         [12] 1207 	inc	_us100
                     0074  1208 	C$dev.h$119$1$10 ==.
                           1209 ;	C:\Workspace\WSN_MS-4\/dev.h:119: ++hus;
   013B 74 01         [12] 1210 	mov	a,#0x01
   013D 25 22         [12] 1211 	add	a,_hus
   013F F5 22         [12] 1212 	mov	_hus,a
   0141 E4            [12] 1213 	clr	a
   0142 35 23         [12] 1214 	addc	a,(_hus + 1)
   0144 F5 23         [12] 1215 	mov	(_hus + 1),a
                     007F  1216 	C$dev.h$120$1$10 ==.
                           1217 ;	C:\Workspace\WSN_MS-4\/dev.h:120: if (us100 >= 10) 	
   0146 74 F6         [12] 1218 	mov	a,#0x100 - 0x0A
   0148 25 21         [12] 1219 	add	a,_us100
   014A 50 0E         [24] 1220 	jnc	00102$
                     0085  1221 	C$dev.h$121$2$11 ==.
                           1222 ;	C:\Workspace\WSN_MS-4\/dev.h:121: {	us100 = 0;
   014C 75 21 00      [24] 1223 	mov	_us100,#0x00
                     0088  1224 	C$dev.h$122$2$11 ==.
                           1225 ;	C:\Workspace\WSN_MS-4\/dev.h:122: ++T_1ms; 
   014F 74 01         [12] 1226 	mov	a,#0x01
   0151 25 24         [12] 1227 	add	a,_T_1ms
   0153 F5 24         [12] 1228 	mov	_T_1ms,a
   0155 E4            [12] 1229 	clr	a
   0156 35 25         [12] 1230 	addc	a,(_T_1ms + 1)
   0158 F5 25         [12] 1231 	mov	(_T_1ms + 1),a
   015A                    1232 00102$:
                     0093  1233 	C$dev.h$124$1$10 ==.
                           1234 ;	C:\Workspace\WSN_MS-4\/dev.h:124: if (T_1ms >= 1000)          //  Modulo 1000
   015A C3            [12] 1235 	clr	c
   015B E5 24         [12] 1236 	mov	a,_T_1ms
   015D 94 E8         [12] 1237 	subb	a,#0xE8
   015F E5 25         [12] 1238 	mov	a,(_T_1ms + 1)
   0161 94 03         [12] 1239 	subb	a,#0x03
   0163 40 10         [24] 1240 	jc	00105$
                     009E  1241 	C$dev.h$125$2$12 ==.
                           1242 ;	C:\Workspace\WSN_MS-4\/dev.h:125: {	T_1ms = 0;
   0165 E4            [12] 1243 	clr	a
   0166 F5 24         [12] 1244 	mov	_T_1ms,a
   0168 F5 25         [12] 1245 	mov	(_T_1ms + 1),a
                     00A3  1246 	C$dev.h$126$2$12 ==.
                           1247 ;	C:\Workspace\WSN_MS-4\/dev.h:126: ++T_1s; 
   016A 74 01         [12] 1248 	mov	a,#0x01
   016C 25 26         [12] 1249 	add	a,_T_1s
   016E F5 26         [12] 1250 	mov	_T_1s,a
   0170 E4            [12] 1251 	clr	a
   0171 35 27         [12] 1252 	addc	a,(_T_1s + 1)
   0173 F5 27         [12] 1253 	mov	(_T_1s + 1),a
   0175                    1254 00105$:
   0175 D0 D0         [24] 1255 	pop	psw
   0177 D0 E0         [24] 1256 	pop	acc
                     00B2  1257 	C$dev.h$128$1$10 ==.
                     00B2  1258 	XG$Tus100_ISR$0$0 ==.
   0179 32            [24] 1259 	reti
                           1260 ;	eliminated unneeded mov psw,# (no regs used in bank)
                           1261 ;	eliminated unneeded push/pop dpl
                           1262 ;	eliminated unneeded push/pop dph
                           1263 ;	eliminated unneeded push/pop b
                           1264 ;------------------------------------------------------------
                           1265 ;Allocation info for local variables in function 'Delay_x100us'
                           1266 ;------------------------------------------------------------
                           1267 ;n100us                    Allocated to registers r6 r7 
                           1268 ;------------------------------------------------------------
                     00B3  1269 	G$Delay_x100us$0$0 ==.
                     00B3  1270 	C$dev.h$130$1$10 ==.
                           1271 ;	C:\Workspace\WSN_MS-4\/dev.h:130: void Delay_x100us(U16 n100us)	// W 100 x us
                           1272 ;	-----------------------------------------
                           1273 ;	 function Delay_x100us
                           1274 ;	-----------------------------------------
   017A                    1275 _Delay_x100us:
   017A AE 82         [24] 1276 	mov	r6,dpl
   017C AF 83         [24] 1277 	mov	r7,dph
                     00B7  1278 	C$dev.h$132$1$14 ==.
                           1279 ;	C:\Workspace\WSN_MS-4\/dev.h:132: hus = 0;
   017E E4            [12] 1280 	clr	a
   017F F5 22         [12] 1281 	mov	_hus,a
   0181 F5 23         [12] 1282 	mov	(_hus + 1),a
                     00BC  1283 	C$dev.h$133$1$14 ==.
                           1284 ;	C:\Workspace\WSN_MS-4\/dev.h:133: while (hus <= n100us);
   0183                    1285 00101$:
   0183 C3            [12] 1286 	clr	c
   0184 EE            [12] 1287 	mov	a,r6
   0185 95 22         [12] 1288 	subb	a,_hus
   0187 EF            [12] 1289 	mov	a,r7
   0188 95 23         [12] 1290 	subb	a,(_hus + 1)
   018A 50 F7         [24] 1291 	jnc	00101$
                     00C5  1292 	C$dev.h$134$1$14 ==.
                     00C5  1293 	XG$Delay_x100us$0$0 ==.
   018C 22            [24] 1294 	ret
                           1295 ;------------------------------------------------------------
                           1296 ;Allocation info for local variables in function 'Wait_for_button_release'
                           1297 ;------------------------------------------------------------
                     00C6  1298 	G$Wait_for_button_release$0$0 ==.
                     00C6  1299 	C$dev.h$143$1$14 ==.
                           1300 ;	C:\Workspace\WSN_MS-4\/dev.h:143: void Wait_for_button_release (void) 
                           1301 ;	-----------------------------------------
                           1302 ;	 function Wait_for_button_release
                           1303 ;	-----------------------------------------
   018D                    1304 _Wait_for_button_release:
                     00C6  1305 	C$dev.h$145$1$16 ==.
                           1306 ;	C:\Workspace\WSN_MS-4\/dev.h:145: while (B1_ON() || B2_ON() || B3_ON() || B4_ON());  
   018D                    1307 00104$:
   018D E5 85         [12] 1308 	mov	a,_P5
   018F 30 E0 FB      [24] 1309 	jnb	acc.0,00104$
   0192 E5 85         [12] 1310 	mov	a,_P5
   0194 30 E1 F6      [24] 1311 	jnb	acc.1,00104$
   0197 E5 85         [12] 1312 	mov	a,_P5
   0199 30 E2 F1      [24] 1313 	jnb	acc.2,00104$
   019C E5 85         [12] 1314 	mov	a,_P5
   019E 30 E3 EC      [24] 1315 	jnb	acc.3,00104$
                     00DA  1316 	C$dev.h$146$1$16 ==.
                           1317 ;	C:\Workspace\WSN_MS-4\/dev.h:146: Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w                                         
   01A1 90 00 64      [24] 1318 	mov	dptr,#0x0064
   01A4 12 01 7A      [24] 1319 	lcall	_Delay_x100us
                     00E0  1320 	C$dev.h$147$1$16 ==.
                           1321 ;	C:\Workspace\WSN_MS-4\/dev.h:147: while (B1_ON() || B2_ON() || B3_ON() || B4_ON());  
   01A7                    1322 00110$:
   01A7 E5 85         [12] 1323 	mov	a,_P5
   01A9 30 E0 FB      [24] 1324 	jnb	acc.0,00110$
   01AC E5 85         [12] 1325 	mov	a,_P5
   01AE 30 E1 F6      [24] 1326 	jnb	acc.1,00110$
   01B1 E5 85         [12] 1327 	mov	a,_P5
   01B3 30 E2 F1      [24] 1328 	jnb	acc.2,00110$
   01B6 E5 85         [12] 1329 	mov	a,_P5
   01B8 30 E3 EC      [24] 1330 	jnb	acc.3,00110$
                     00F4  1331 	C$dev.h$148$1$16 ==.
                     00F4  1332 	XG$Wait_for_button_release$0$0 ==.
   01BB 22            [24] 1333 	ret
                           1334 ;------------------------------------------------------------
                           1335 ;Allocation info for local variables in function 'Wait_for_button_pressed'
                           1336 ;------------------------------------------------------------
                     00F5  1337 	G$Wait_for_button_pressed$0$0 ==.
                     00F5  1338 	C$dev.h$151$1$16 ==.
                           1339 ;	C:\Workspace\WSN_MS-4\/dev.h:151: void Wait_for_button_pressed (void) 
                           1340 ;	-----------------------------------------
                           1341 ;	 function Wait_for_button_pressed
                           1342 ;	-----------------------------------------
   01BC                    1343 _Wait_for_button_pressed:
                     00F5  1344 	C$dev.h$153$1$18 ==.
                           1345 ;	C:\Workspace\WSN_MS-4\/dev.h:153: while (!(B1_ON() || B2_ON() || B3_ON() || B4_ON()));  
   01BC                    1346 00104$:
   01BC E5 85         [12] 1347 	mov	a,_P5
   01BE 30 E0 0F      [24] 1348 	jnb	acc.0,00106$
   01C1 E5 85         [12] 1349 	mov	a,_P5
   01C3 30 E1 0A      [24] 1350 	jnb	acc.1,00106$
   01C6 E5 85         [12] 1351 	mov	a,_P5
   01C8 30 E2 05      [24] 1352 	jnb	acc.2,00106$
   01CB E5 85         [12] 1353 	mov	a,_P5
   01CD 20 E3 EC      [24] 1354 	jb	acc.3,00104$
   01D0                    1355 00106$:
                     0109  1356 	C$dev.h$154$1$18 ==.
                           1357 ;	C:\Workspace\WSN_MS-4\/dev.h:154: Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w                                         
   01D0 90 00 64      [24] 1358 	mov	dptr,#0x0064
   01D3 12 01 7A      [24] 1359 	lcall	_Delay_x100us
                     010F  1360 	C$dev.h$155$1$18 ==.
                           1361 ;	C:\Workspace\WSN_MS-4\/dev.h:155: while (!(B1_ON() || B2_ON() || B3_ON() || B4_ON()));  
   01D6                    1362 00110$:
   01D6 E5 85         [12] 1363 	mov	a,_P5
   01D8 30 E0 0F      [24] 1364 	jnb	acc.0,00113$
   01DB E5 85         [12] 1365 	mov	a,_P5
   01DD 30 E1 0A      [24] 1366 	jnb	acc.1,00113$
   01E0 E5 85         [12] 1367 	mov	a,_P5
   01E2 30 E2 05      [24] 1368 	jnb	acc.2,00113$
   01E5 E5 85         [12] 1369 	mov	a,_P5
   01E7 20 E3 EC      [24] 1370 	jb	acc.3,00110$
   01EA                    1371 00113$:
                     0123  1372 	C$dev.h$156$1$18 ==.
                     0123  1373 	XG$Wait_for_button_pressed$0$0 ==.
   01EA 22            [24] 1374 	ret
                           1375 ;------------------------------------------------------------
                           1376 ;Allocation info for local variables in function 'B1_PRESSED'
                           1377 ;------------------------------------------------------------
                     0124  1378 	G$B1_PRESSED$0$0 ==.
                     0124  1379 	C$dev.h$159$1$18 ==.
                           1380 ;	C:\Workspace\WSN_MS-4\/dev.h:159: U8 B1_PRESSED(void)
                           1381 ;	-----------------------------------------
                           1382 ;	 function B1_PRESSED
                           1383 ;	-----------------------------------------
   01EB                    1384 _B1_PRESSED:
                     0124  1385 	C$dev.h$161$1$20 ==.
                           1386 ;	C:\Workspace\WSN_MS-4\/dev.h:161: if(B1_ON())
   01EB E5 85         [12] 1387 	mov	a,_P5
   01ED 20 E0 10      [24] 1388 	jb	acc.0,00104$
                     0129  1389 	C$dev.h$163$2$21 ==.
                           1390 ;	C:\Workspace\WSN_MS-4\/dev.h:163: Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w 
   01F0 90 00 64      [24] 1391 	mov	dptr,#0x0064
   01F3 12 01 7A      [24] 1392 	lcall	_Delay_x100us
                     012F  1393 	C$dev.h$164$2$21 ==.
                           1394 ;	C:\Workspace\WSN_MS-4\/dev.h:164: if(B1_ON())     return 1;   // Naci�ni�ty
   01F6 E5 85         [12] 1395 	mov	a,_P5
   01F8 20 E0 05      [24] 1396 	jb	acc.0,00104$
   01FB 75 82 01      [24] 1397 	mov	dpl,#0x01
   01FE 80 03         [24] 1398 	sjmp	00105$
   0200                    1399 00104$:
                     0139  1400 	C$dev.h$166$1$20 ==.
                           1401 ;	C:\Workspace\WSN_MS-4\/dev.h:166: return 0;
   0200 75 82 00      [24] 1402 	mov	dpl,#0x00
   0203                    1403 00105$:
                     013C  1404 	C$dev.h$167$1$20 ==.
                     013C  1405 	XG$B1_PRESSED$0$0 ==.
   0203 22            [24] 1406 	ret
                           1407 ;------------------------------------------------------------
                           1408 ;Allocation info for local variables in function 'B2_PRESSED'
                           1409 ;------------------------------------------------------------
                     013D  1410 	G$B2_PRESSED$0$0 ==.
                     013D  1411 	C$dev.h$169$1$20 ==.
                           1412 ;	C:\Workspace\WSN_MS-4\/dev.h:169: U8 B2_PRESSED(void)
                           1413 ;	-----------------------------------------
                           1414 ;	 function B2_PRESSED
                           1415 ;	-----------------------------------------
   0204                    1416 _B2_PRESSED:
                     013D  1417 	C$dev.h$171$1$23 ==.
                           1418 ;	C:\Workspace\WSN_MS-4\/dev.h:171: if(B2_ON())
   0204 E5 85         [12] 1419 	mov	a,_P5
   0206 20 E1 10      [24] 1420 	jb	acc.1,00104$
                     0142  1421 	C$dev.h$173$2$24 ==.
                           1422 ;	C:\Workspace\WSN_MS-4\/dev.h:173: Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w 
   0209 90 00 64      [24] 1423 	mov	dptr,#0x0064
   020C 12 01 7A      [24] 1424 	lcall	_Delay_x100us
                     0148  1425 	C$dev.h$174$2$24 ==.
                           1426 ;	C:\Workspace\WSN_MS-4\/dev.h:174: if(B2_ON())     return 1;   // Naci�ni�ty
   020F E5 85         [12] 1427 	mov	a,_P5
   0211 20 E1 05      [24] 1428 	jb	acc.1,00104$
   0214 75 82 01      [24] 1429 	mov	dpl,#0x01
   0217 80 03         [24] 1430 	sjmp	00105$
   0219                    1431 00104$:
                     0152  1432 	C$dev.h$176$1$23 ==.
                           1433 ;	C:\Workspace\WSN_MS-4\/dev.h:176: return 0;
   0219 75 82 00      [24] 1434 	mov	dpl,#0x00
   021C                    1435 00105$:
                     0155  1436 	C$dev.h$177$1$23 ==.
                     0155  1437 	XG$B2_PRESSED$0$0 ==.
   021C 22            [24] 1438 	ret
                           1439 ;------------------------------------------------------------
                           1440 ;Allocation info for local variables in function 'B3_PRESSED'
                           1441 ;------------------------------------------------------------
                     0156  1442 	G$B3_PRESSED$0$0 ==.
                     0156  1443 	C$dev.h$179$1$23 ==.
                           1444 ;	C:\Workspace\WSN_MS-4\/dev.h:179: U8 B3_PRESSED(void)
                           1445 ;	-----------------------------------------
                           1446 ;	 function B3_PRESSED
                           1447 ;	-----------------------------------------
   021D                    1448 _B3_PRESSED:
                     0156  1449 	C$dev.h$181$1$26 ==.
                           1450 ;	C:\Workspace\WSN_MS-4\/dev.h:181: if(B3_ON())
   021D E5 85         [12] 1451 	mov	a,_P5
   021F 20 E2 10      [24] 1452 	jb	acc.2,00104$
                     015B  1453 	C$dev.h$183$2$27 ==.
                           1454 ;	C:\Workspace\WSN_MS-4\/dev.h:183: Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w 
   0222 90 00 64      [24] 1455 	mov	dptr,#0x0064
   0225 12 01 7A      [24] 1456 	lcall	_Delay_x100us
                     0161  1457 	C$dev.h$184$2$27 ==.
                           1458 ;	C:\Workspace\WSN_MS-4\/dev.h:184: if(B3_ON())     return 1;   // Naci�ni�ty
   0228 E5 85         [12] 1459 	mov	a,_P5
   022A 20 E2 05      [24] 1460 	jb	acc.2,00104$
   022D 75 82 01      [24] 1461 	mov	dpl,#0x01
   0230 80 03         [24] 1462 	sjmp	00105$
   0232                    1463 00104$:
                     016B  1464 	C$dev.h$186$1$26 ==.
                           1465 ;	C:\Workspace\WSN_MS-4\/dev.h:186: return 0;
   0232 75 82 00      [24] 1466 	mov	dpl,#0x00
   0235                    1467 00105$:
                     016E  1468 	C$dev.h$187$1$26 ==.
                     016E  1469 	XG$B3_PRESSED$0$0 ==.
   0235 22            [24] 1470 	ret
                           1471 ;------------------------------------------------------------
                           1472 ;Allocation info for local variables in function 'B4_PRESSED'
                           1473 ;------------------------------------------------------------
                     016F  1474 	G$B4_PRESSED$0$0 ==.
                     016F  1475 	C$dev.h$189$1$26 ==.
                           1476 ;	C:\Workspace\WSN_MS-4\/dev.h:189: U8 B4_PRESSED(void)
                           1477 ;	-----------------------------------------
                           1478 ;	 function B4_PRESSED
                           1479 ;	-----------------------------------------
   0236                    1480 _B4_PRESSED:
                     016F  1481 	C$dev.h$191$1$29 ==.
                           1482 ;	C:\Workspace\WSN_MS-4\/dev.h:191: if(B4_ON())
   0236 E5 85         [12] 1483 	mov	a,_P5
   0238 20 E3 10      [24] 1484 	jb	acc.3,00104$
                     0174  1485 	C$dev.h$193$2$30 ==.
                           1486 ;	C:\Workspace\WSN_MS-4\/dev.h:193: Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w 
   023B 90 00 64      [24] 1487 	mov	dptr,#0x0064
   023E 12 01 7A      [24] 1488 	lcall	_Delay_x100us
                     017A  1489 	C$dev.h$194$2$30 ==.
                           1490 ;	C:\Workspace\WSN_MS-4\/dev.h:194: if(B4_ON())     return 1;   // Naci�ni�ty
   0241 E5 85         [12] 1491 	mov	a,_P5
   0243 20 E3 05      [24] 1492 	jb	acc.3,00104$
   0246 75 82 01      [24] 1493 	mov	dpl,#0x01
   0249 80 03         [24] 1494 	sjmp	00105$
   024B                    1495 00104$:
                     0184  1496 	C$dev.h$196$1$29 ==.
                           1497 ;	C:\Workspace\WSN_MS-4\/dev.h:196: return 0;
   024B 75 82 00      [24] 1498 	mov	dpl,#0x00
   024E                    1499 00105$:
                     0187  1500 	C$dev.h$197$1$29 ==.
                     0187  1501 	XG$B4_PRESSED$0$0 ==.
   024E 22            [24] 1502 	ret
                           1503 ;------------------------------------------------------------
                           1504 ;Allocation info for local variables in function 'UART_SetBaudRate'
                           1505 ;------------------------------------------------------------
                           1506 ;baudRate                  Allocated to registers r7 
                           1507 ;------------------------------------------------------------
                     0188  1508 	G$UART_SetBaudRate$0$0 ==.
                     0188  1509 	C$uart.h$16$1$29 ==.
                           1510 ;	C:\Workspace\WSN_MS-4\/uart.h:16: void UART_SetBaudRate(U8 baudRate)
                           1511 ;	-----------------------------------------
                           1512 ;	 function UART_SetBaudRate
                           1513 ;	-----------------------------------------
   024F                    1514 _UART_SetBaudRate:
   024F AF 82         [24] 1515 	mov	r7,dpl
                     018A  1516 	C$uart.h$18$1$32 ==.
                           1517 ;	C:\Workspace\WSN_MS-4\/uart.h:18: switch(baudRate)
   0251 BF 01 02      [24] 1518 	cjne	r7,#0x01,00120$
   0254 80 0F         [24] 1519 	sjmp	00101$
   0256                    1520 00120$:
   0256 BF 02 02      [24] 1521 	cjne	r7,#0x02,00121$
   0259 80 12         [24] 1522 	sjmp	00102$
   025B                    1523 00121$:
   025B BF 03 02      [24] 1524 	cjne	r7,#0x03,00122$
   025E 80 18         [24] 1525 	sjmp	00103$
   0260                    1526 00122$:
                     0199  1527 	C$uart.h$20$2$33 ==.
                           1528 ;	C:\Workspace\WSN_MS-4\/uart.h:20: case UART_BAUDRATE_2400:	//?
   0260 BF 04 23      [24] 1529 	cjne	r7,#0x04,00106$
   0263 80 1B         [24] 1530 	sjmp	00104$
   0265                    1531 00101$:
                     019E  1532 	C$uart.h$21$2$33 ==.
                           1533 ;	C:\Workspace\WSN_MS-4\/uart.h:21: CKCON &= 0xF4;
   0265 53 8E F4      [24] 1534 	anl	_CKCON,#0xF4
                     01A1  1535 	C$uart.h$22$2$33 ==.
                           1536 ;	C:\Workspace\WSN_MS-4\/uart.h:22: TH1    = 0x2B;
   0268 75 8D 2B      [24] 1537 	mov	_TH1,#0x2B
                     01A4  1538 	C$uart.h$23$2$33 ==.
                           1539 ;	C:\Workspace\WSN_MS-4\/uart.h:23: break;
                     01A4  1540 	C$uart.h$25$2$33 ==.
                           1541 ;	C:\Workspace\WSN_MS-4\/uart.h:25: case UART_BAUDRATE_9600:
   026B 80 19         [24] 1542 	sjmp	00106$
   026D                    1543 00102$:
                     01A6  1544 	C$uart.h$26$2$33 ==.
                           1545 ;	C:\Workspace\WSN_MS-4\/uart.h:26: CKCON &= 0xF4;
   026D 53 8E F4      [24] 1546 	anl	_CKCON,#0xF4
                     01A9  1547 	C$uart.h$27$2$33 ==.
                           1548 ;	C:\Workspace\WSN_MS-4\/uart.h:27: CKCON |= 0x10;
   0270 43 8E 10      [24] 1549 	orl	_CKCON,#0x10
                     01AC  1550 	C$uart.h$28$2$33 ==.
                           1551 ;	C:\Workspace\WSN_MS-4\/uart.h:28: TH1    = 0x96;
   0273 75 8D 96      [24] 1552 	mov	_TH1,#0x96
                     01AF  1553 	C$uart.h$29$2$33 ==.
                           1554 ;	C:\Workspace\WSN_MS-4\/uart.h:29: break;
                     01AF  1555 	C$uart.h$31$2$33 ==.
                           1556 ;	C:\Workspace\WSN_MS-4\/uart.h:31: case UART_BAUDRATE_56000:
   0276 80 0E         [24] 1557 	sjmp	00106$
   0278                    1558 00103$:
                     01B1  1559 	C$uart.h$32$2$33 ==.
                           1560 ;	C:\Workspace\WSN_MS-4\/uart.h:32: CKCON |= 0x08;
   0278 43 8E 08      [24] 1561 	orl	_CKCON,#0x08
                     01B4  1562 	C$uart.h$33$2$33 ==.
                           1563 ;	C:\Workspace\WSN_MS-4\/uart.h:33: TH1    = 0x25;
   027B 75 8D 25      [24] 1564 	mov	_TH1,#0x25
                     01B7  1565 	C$uart.h$34$2$33 ==.
                           1566 ;	C:\Workspace\WSN_MS-4\/uart.h:34: break;
                     01B7  1567 	C$uart.h$36$2$33 ==.
                           1568 ;	C:\Workspace\WSN_MS-4\/uart.h:36: case UART_BAUDRATE_115200:
   027E 80 06         [24] 1569 	sjmp	00106$
   0280                    1570 00104$:
                     01B9  1571 	C$uart.h$37$2$33 ==.
                           1572 ;	C:\Workspace\WSN_MS-4\/uart.h:37: CKCON |= 0x08;
   0280 43 8E 08      [24] 1573 	orl	_CKCON,#0x08
                     01BC  1574 	C$uart.h$38$2$33 ==.
                           1575 ;	C:\Workspace\WSN_MS-4\/uart.h:38: TH1    = 0x96;
   0283 75 8D 96      [24] 1576 	mov	_TH1,#0x96
                     01BF  1577 	C$uart.h$40$1$32 ==.
                           1578 ;	C:\Workspace\WSN_MS-4\/uart.h:40: }
   0286                    1579 00106$:
                     01BF  1580 	C$uart.h$41$1$32 ==.
                     01BF  1581 	XG$UART_SetBaudRate$0$0 ==.
   0286 22            [24] 1582 	ret
                           1583 ;------------------------------------------------------------
                           1584 ;Allocation info for local variables in function 'putchar'
                           1585 ;------------------------------------------------------------
                           1586 ;c                         Allocated to registers r7 
                           1587 ;------------------------------------------------------------
                     01C0  1588 	G$putchar$0$0 ==.
                     01C0  1589 	C$uart.h$51$1$32 ==.
                           1590 ;	C:\Workspace\WSN_MS-4\/uart.h:51: void putchar(S8 c)
                           1591 ;	-----------------------------------------
                           1592 ;	 function putchar
                           1593 ;	-----------------------------------------
   0287                    1594 _putchar:
   0287 AF 82         [24] 1595 	mov	r7,dpl
                     01C2  1596 	C$uart.h$53$1$35 ==.
                           1597 ;	C:\Workspace\WSN_MS-4\/uart.h:53: while (!TI0);
   0289                    1598 00101$:
                     01C2  1599 	C$uart.h$54$1$35 ==.
                           1600 ;	C:\Workspace\WSN_MS-4\/uart.h:54: TI0   = 0;
   0289 10 99 02      [24] 1601 	jbc	_TI0,00112$
   028C 80 FB         [24] 1602 	sjmp	00101$
   028E                    1603 00112$:
                     01C7  1604 	C$uart.h$55$1$35 ==.
                           1605 ;	C:\Workspace\WSN_MS-4\/uart.h:55: SBUF0 = c;
   028E 8F 99         [24] 1606 	mov	_SBUF0,r7
                     01C9  1607 	C$uart.h$56$1$35 ==.
                     01C9  1608 	XG$putchar$0$0 ==.
   0290 22            [24] 1609 	ret
                           1610 ;------------------------------------------------------------
                           1611 ;Allocation info for local variables in function 'getchar'
                           1612 ;------------------------------------------------------------
                     01CA  1613 	G$getchar$0$0 ==.
                     01CA  1614 	C$uart.h$63$1$35 ==.
                           1615 ;	C:\Workspace\WSN_MS-4\/uart.h:63: S8 getchar(void)
                           1616 ;	-----------------------------------------
                           1617 ;	 function getchar
                           1618 ;	-----------------------------------------
   0291                    1619 _getchar:
                     01CA  1620 	C$uart.h$65$1$37 ==.
                           1621 ;	C:\Workspace\WSN_MS-4\/uart.h:65: while (!RI0);
   0291                    1622 00101$:
                     01CA  1623 	C$uart.h$66$1$37 ==.
                           1624 ;	C:\Workspace\WSN_MS-4\/uart.h:66: RI0 = 0;
   0291 10 98 02      [24] 1625 	jbc	_RI0,00112$
   0294 80 FB         [24] 1626 	sjmp	00101$
   0296                    1627 00112$:
                     01CF  1628 	C$uart.h$67$1$37 ==.
                           1629 ;	C:\Workspace\WSN_MS-4\/uart.h:67: return SBUF0;
   0296 85 99 82      [24] 1630 	mov	dpl,_SBUF0
                     01D2  1631 	C$uart.h$68$1$37 ==.
                     01D2  1632 	XG$getchar$0$0 ==.
   0299 22            [24] 1633 	ret
                           1634 ;------------------------------------------------------------
                           1635 ;Allocation info for local variables in function 'printc'
                           1636 ;------------------------------------------------------------
                           1637 ;val                       Allocated to registers r7 
                           1638 ;------------------------------------------------------------
                     01D3  1639 	G$printc$0$0 ==.
                     01D3  1640 	C$uart.h$75$1$37 ==.
                           1641 ;	C:\Workspace\WSN_MS-4\/uart.h:75: void printc(U8 val)
                           1642 ;	-----------------------------------------
                           1643 ;	 function printc
                           1644 ;	-----------------------------------------
   029A                    1645 _printc:
                     01D3  1646 	C$uart.h$78$1$39 ==.
                           1647 ;	C:\Workspace\WSN_MS-4\/uart.h:78: val += (val>0x09) ? 0x37 : 0x30;
   029A E5 82         [12] 1648 	mov	a,dpl
   029C FF            [12] 1649 	mov	r7,a
   029D 24 F6         [12] 1650 	add	a,#0xff - 0x09
   029F 50 04         [24] 1651 	jnc	00103$
   02A1 7E 37         [12] 1652 	mov	r6,#0x37
   02A3 80 02         [24] 1653 	sjmp	00104$
   02A5                    1654 00103$:
   02A5 7E 30         [12] 1655 	mov	r6,#0x30
   02A7                    1656 00104$:
   02A7 EE            [12] 1657 	mov	a,r6
   02A8 2F            [12] 1658 	add	a,r7
                     01E2  1659 	C$uart.h$79$1$39 ==.
                           1660 ;	C:\Workspace\WSN_MS-4\/uart.h:79: putchar(val);    
   02A9 F5 82         [12] 1661 	mov	dpl,a
   02AB 12 02 87      [24] 1662 	lcall	_putchar
                     01E7  1663 	C$uart.h$80$1$39 ==.
                     01E7  1664 	XG$printc$0$0 ==.
   02AE 22            [24] 1665 	ret
                           1666 ;------------------------------------------------------------
                           1667 ;Allocation info for local variables in function 'gethex'
                           1668 ;------------------------------------------------------------
                           1669 ;result                    Allocated to registers r7 
                           1670 ;tmp                       Allocated to registers r6 
                           1671 ;------------------------------------------------------------
                     01E8  1672 	G$gethex$0$0 ==.
                     01E8  1673 	C$uart.h$88$1$39 ==.
                           1674 ;	C:\Workspace\WSN_MS-4\/uart.h:88: U8 gethex(void)
                           1675 ;	-----------------------------------------
                           1676 ;	 function gethex
                           1677 ;	-----------------------------------------
   02AF                    1678 _gethex:
                     01E8  1679 	C$uart.h$90$1$39 ==.
                           1680 ;	C:\Workspace\WSN_MS-4\/uart.h:90: S8 result = 0;
   02AF 7F 00         [12] 1681 	mov	r7,#0x00
                     01EA  1682 	C$uart.h$93$1$41 ==.
                           1683 ;	C:\Workspace\WSN_MS-4\/uart.h:93: while (!RI0);
   02B1                    1684 00101$:
                     01EA  1685 	C$uart.h$94$1$41 ==.
                           1686 ;	C:\Workspace\WSN_MS-4\/uart.h:94: RI0 = 0;
   02B1 10 98 02      [24] 1687 	jbc	_RI0,00169$
   02B4 80 FB         [24] 1688 	sjmp	00101$
   02B6                    1689 00169$:
                     01EF  1690 	C$uart.h$95$1$41 ==.
                           1691 ;	C:\Workspace\WSN_MS-4\/uart.h:95: tmp = SBUF0;
   02B6 AE 99         [24] 1692 	mov	r6,_SBUF0
                     01F1  1693 	C$uart.h$97$1$41 ==.
                           1694 ;	C:\Workspace\WSN_MS-4\/uart.h:97: if (tmp != 0x5C) {
   02B8 BE 5C 02      [24] 1695 	cjne	r6,#0x5C,00170$
   02BB 80 69         [24] 1696 	sjmp	00113$
   02BD                    1697 00170$:
                     01F6  1698 	C$uart.h$99$2$42 ==.
                           1699 ;	C:\Workspace\WSN_MS-4\/uart.h:99: if (tmp > 0x60 && tmp < 0x67)
   02BD C3            [12] 1700 	clr	c
   02BE 74 E0         [12] 1701 	mov	a,#(0x60 ^ 0x80)
   02C0 8E F0         [24] 1702 	mov	b,r6
   02C2 63 F0 80      [24] 1703 	xrl	b,#0x80
   02C5 95 F0         [12] 1704 	subb	a,b
   02C7 50 0C         [24] 1705 	jnc	00105$
   02C9 C3            [12] 1706 	clr	c
   02CA EE            [12] 1707 	mov	a,r6
   02CB 64 80         [12] 1708 	xrl	a,#0x80
   02CD 94 E7         [12] 1709 	subb	a,#0xe7
   02CF 50 04         [24] 1710 	jnc	00105$
                     020A  1711 	C$uart.h$100$2$42 ==.
                           1712 ;	C:\Workspace\WSN_MS-4\/uart.h:100: tmp -= 0x20;
   02D1 EE            [12] 1713 	mov	a,r6
   02D2 24 E0         [12] 1714 	add	a,#0xE0
   02D4 FE            [12] 1715 	mov	r6,a
   02D5                    1716 00105$:
                     020E  1717 	C$uart.h$102$2$42 ==.
                           1718 ;	C:\Workspace\WSN_MS-4\/uart.h:102: result = (tmp<0x40) ? (tmp-0x30)<<4 : (tmp-55)<<4;
   02D5 C3            [12] 1719 	clr	c
   02D6 EE            [12] 1720 	mov	a,r6
   02D7 64 80         [12] 1721 	xrl	a,#0x80
   02D9 94 C0         [12] 1722 	subb	a,#0xc0
   02DB 50 09         [24] 1723 	jnc	00123$
   02DD EE            [12] 1724 	mov	a,r6
   02DE 24 D0         [12] 1725 	add	a,#0xD0
   02E0 C4            [12] 1726 	swap	a
   02E1 54 F0         [12] 1727 	anl	a,#0xF0
   02E3 FD            [12] 1728 	mov	r5,a
   02E4 80 08         [24] 1729 	sjmp	00124$
   02E6                    1730 00123$:
   02E6 EE            [12] 1731 	mov	a,r6
   02E7 24 C9         [12] 1732 	add	a,#0xC9
   02E9 FC            [12] 1733 	mov	r4,a
   02EA C4            [12] 1734 	swap	a
   02EB 54 F0         [12] 1735 	anl	a,#0xF0
   02ED FD            [12] 1736 	mov	r5,a
   02EE                    1737 00124$:
   02EE 8D 07         [24] 1738 	mov	ar7,r5
                     0229  1739 	C$uart.h$104$2$42 ==.
                           1740 ;	C:\Workspace\WSN_MS-4\/uart.h:104: while (!RI0);
   02F0                    1741 00107$:
                     0229  1742 	C$uart.h$105$2$42 ==.
                           1743 ;	C:\Workspace\WSN_MS-4\/uart.h:105: RI0 = 0;
   02F0 10 98 02      [24] 1744 	jbc	_RI0,00174$
   02F3 80 FB         [24] 1745 	sjmp	00107$
   02F5                    1746 00174$:
                     022E  1747 	C$uart.h$106$2$42 ==.
                           1748 ;	C:\Workspace\WSN_MS-4\/uart.h:106: tmp = SBUF0;
   02F5 AE 99         [24] 1749 	mov	r6,_SBUF0
                     0230  1750 	C$uart.h$108$2$42 ==.
                           1751 ;	C:\Workspace\WSN_MS-4\/uart.h:108: if (tmp > 0x60 && tmp < 0x67)
   02F7 C3            [12] 1752 	clr	c
   02F8 74 E0         [12] 1753 	mov	a,#(0x60 ^ 0x80)
   02FA 8E F0         [24] 1754 	mov	b,r6
   02FC 63 F0 80      [24] 1755 	xrl	b,#0x80
   02FF 95 F0         [12] 1756 	subb	a,b
   0301 50 0C         [24] 1757 	jnc	00111$
   0303 C3            [12] 1758 	clr	c
   0304 EE            [12] 1759 	mov	a,r6
   0305 64 80         [12] 1760 	xrl	a,#0x80
   0307 94 E7         [12] 1761 	subb	a,#0xe7
   0309 50 04         [24] 1762 	jnc	00111$
                     0244  1763 	C$uart.h$109$2$42 ==.
                           1764 ;	C:\Workspace\WSN_MS-4\/uart.h:109: tmp -= 0x20;
   030B EE            [12] 1765 	mov	a,r6
   030C 24 E0         [12] 1766 	add	a,#0xE0
   030E FE            [12] 1767 	mov	r6,a
   030F                    1768 00111$:
                     0248  1769 	C$uart.h$111$2$42 ==.
                           1770 ;	C:\Workspace\WSN_MS-4\/uart.h:111: result |= (tmp<0x40) ? (tmp-0x30) : (tmp-55);
   030F C3            [12] 1771 	clr	c
   0310 EE            [12] 1772 	mov	a,r6
   0311 64 80         [12] 1773 	xrl	a,#0x80
   0313 94 C0         [12] 1774 	subb	a,#0xc0
   0315 50 06         [24] 1775 	jnc	00125$
   0317 EE            [12] 1776 	mov	a,r6
   0318 24 D0         [12] 1777 	add	a,#0xD0
   031A FD            [12] 1778 	mov	r5,a
   031B 80 04         [24] 1779 	sjmp	00126$
   031D                    1780 00125$:
   031D EE            [12] 1781 	mov	a,r6
   031E 24 C9         [12] 1782 	add	a,#0xC9
   0320 FD            [12] 1783 	mov	r5,a
   0321                    1784 00126$:
   0321 ED            [12] 1785 	mov	a,r5
   0322 42 07         [12] 1786 	orl	ar7,a
                     025D  1787 	C$uart.h$114$2$43 ==.
                           1788 ;	C:\Workspace\WSN_MS-4\/uart.h:114: while (!RI0);
   0324 80 0F         [24] 1789 	sjmp	00120$
   0326                    1790 00113$:
                     025F  1791 	C$uart.h$115$2$43 ==.
                           1792 ;	C:\Workspace\WSN_MS-4\/uart.h:115: RI0 = 0;
   0326 10 98 02      [24] 1793 	jbc	_RI0,00178$
   0329 80 FB         [24] 1794 	sjmp	00113$
   032B                    1795 00178$:
                     0264  1796 	C$uart.h$117$2$43 ==.
                           1797 ;	C:\Workspace\WSN_MS-4\/uart.h:117: if (SBUF0 == 'n')
   032B 74 6E         [12] 1798 	mov	a,#0x6E
   032D B5 99 05      [24] 1799 	cjne	a,_SBUF0,00120$
                     0269  1800 	C$uart.h$118$2$43 ==.
                           1801 ;	C:\Workspace\WSN_MS-4\/uart.h:118: return '\n';
   0330 75 82 0A      [24] 1802 	mov	dpl,#0x0A
   0333 80 02         [24] 1803 	sjmp	00121$
   0335                    1804 00120$:
                     026E  1805 	C$uart.h$120$1$41 ==.
                           1806 ;	C:\Workspace\WSN_MS-4\/uart.h:120: return result;
   0335 8F 82         [24] 1807 	mov	dpl,r7
   0337                    1808 00121$:
                     0270  1809 	C$uart.h$121$1$41 ==.
                     0270  1810 	XG$gethex$0$0 ==.
   0337 22            [24] 1811 	ret
                           1812 ;------------------------------------------------------------
                           1813 ;Allocation info for local variables in function 'printBCD'
                           1814 ;------------------------------------------------------------
                           1815 ;precission                Allocated with name '_printBCD_PARM_2'
                           1816 ;value                     Allocated to registers r6 r7 
                           1817 ;i                         Allocated to registers r5 
                           1818 ;tmp                       Allocated with name '_printBCD_tmp_1_45'
                           1819 ;------------------------------------------------------------
                     0271  1820 	G$printBCD$0$0 ==.
                     0271  1821 	C$uart.h$132$1$41 ==.
                           1822 ;	C:\Workspace\WSN_MS-4\/uart.h:132: void printBCD(S16 value, U8 precission)
                           1823 ;	-----------------------------------------
                           1824 ;	 function printBCD
                           1825 ;	-----------------------------------------
   0338                    1826 _printBCD:
   0338 AE 82         [24] 1827 	mov	r6,dpl
   033A AF 83         [24] 1828 	mov	r7,dph
                     0275  1829 	C$uart.h$135$1$41 ==.
                           1830 ;	C:\Workspace\WSN_MS-4\/uart.h:135: __bit  s = 0;
   033C C2 00         [12] 1831 	clr	_printBCD_s_1_45
                     0277  1832 	C$uart.h$137$1$45 ==.
                           1833 ;	C:\Workspace\WSN_MS-4\/uart.h:137: if (value < 0)
   033E EF            [12] 1834 	mov	a,r7
   033F 30 E7 0E      [24] 1835 	jnb	acc.7,00121$
                     027B  1836 	C$uart.h$138$1$45 ==.
                           1837 ;	C:\Workspace\WSN_MS-4\/uart.h:138: putchar('-');
   0342 75 82 2D      [24] 1838 	mov	dpl,#0x2D
   0345 C0 07         [24] 1839 	push	ar7
   0347 C0 06         [24] 1840 	push	ar6
   0349 12 02 87      [24] 1841 	lcall	_putchar
   034C D0 06         [24] 1842 	pop	ar6
   034E D0 07         [24] 1843 	pop	ar7
                     0289  1844 	C$uart.h$140$1$45 ==.
                           1845 ;	C:\Workspace\WSN_MS-4\/uart.h:140: for (i = 4; i >= 0; --i) {
   0350                    1846 00121$:
   0350 7D 04         [12] 1847 	mov	r5,#0x04
   0352                    1848 00114$:
                     028B  1849 	C$uart.h$141$2$46 ==.
                           1850 ;	C:\Workspace\WSN_MS-4\/uart.h:141: tmp[i] = (value%10);
   0352 ED            [12] 1851 	mov	a,r5
   0353 24 29         [12] 1852 	add	a,#_printBCD_tmp_1_45
   0355 F9            [12] 1853 	mov	r1,a
   0356 75 09 0A      [24] 1854 	mov	__modsint_PARM_2,#0x0A
   0359 75 0A 00      [24] 1855 	mov	(__modsint_PARM_2 + 1),#0x00
   035C 8E 82         [24] 1856 	mov	dpl,r6
   035E 8F 83         [24] 1857 	mov	dph,r7
   0360 C0 07         [24] 1858 	push	ar7
   0362 C0 06         [24] 1859 	push	ar6
   0364 C0 05         [24] 1860 	push	ar5
   0366 C0 01         [24] 1861 	push	ar1
   0368 12 18 3B      [24] 1862 	lcall	__modsint
   036B AB 82         [24] 1863 	mov	r3,dpl
   036D D0 01         [24] 1864 	pop	ar1
   036F D0 05         [24] 1865 	pop	ar5
   0371 D0 06         [24] 1866 	pop	ar6
   0373 D0 07         [24] 1867 	pop	ar7
   0375 A7 03         [24] 1868 	mov	@r1,ar3
                     02B0  1869 	C$uart.h$142$1$45 ==.
                           1870 ;	C:\Workspace\WSN_MS-4\/uart.h:142: value /= 10;
   0377 75 09 0A      [24] 1871 	mov	__divsint_PARM_2,#0x0A
   037A 75 0A 00      [24] 1872 	mov	(__divsint_PARM_2 + 1),#0x00
   037D 8E 82         [24] 1873 	mov	dpl,r6
   037F 8F 83         [24] 1874 	mov	dph,r7
   0381 C0 05         [24] 1875 	push	ar5
   0383 12 18 71      [24] 1876 	lcall	__divsint
   0386 AE 82         [24] 1877 	mov	r6,dpl
   0388 AF 83         [24] 1878 	mov	r7,dph
   038A D0 05         [24] 1879 	pop	ar5
                     02C5  1880 	C$uart.h$140$1$45 ==.
                           1881 ;	C:\Workspace\WSN_MS-4\/uart.h:140: for (i = 4; i >= 0; --i) {
   038C 1D            [12] 1882 	dec	r5
   038D ED            [12] 1883 	mov	a,r5
   038E 30 E7 C1      [24] 1884 	jnb	acc.7,00114$
                     02CA  1885 	C$uart.h$145$1$45 ==.
                           1886 ;	C:\Workspace\WSN_MS-4\/uart.h:145: for (i = 0; i < 5; ++i) {
   0391 7F 00         [12] 1887 	mov	r7,#0x00
   0393                    1888 00116$:
                     02CC  1889 	C$uart.h$147$2$47 ==.
                           1890 ;	C:\Workspace\WSN_MS-4\/uart.h:147: if (tmp[i] != 0)
   0393 EF            [12] 1891 	mov	a,r7
   0394 24 29         [12] 1892 	add	a,#_printBCD_tmp_1_45
   0396 F9            [12] 1893 	mov	r1,a
   0397 E7            [12] 1894 	mov	a,@r1
   0398 60 02         [24] 1895 	jz	00105$
                     02D3  1896 	C$uart.h$148$2$47 ==.
                           1897 ;	C:\Workspace\WSN_MS-4\/uart.h:148: s = 1;
   039A D2 00         [12] 1898 	setb	_printBCD_s_1_45
   039C                    1899 00105$:
                     02D5  1900 	C$uart.h$150$2$47 ==.
                           1901 ;	C:\Workspace\WSN_MS-4\/uart.h:150: if ( (5-i) == precission) {
   039C EF            [12] 1902 	mov	a,r7
   039D FD            [12] 1903 	mov	r5,a
   039E 33            [12] 1904 	rlc	a
   039F 95 E0         [12] 1905 	subb	a,acc
   03A1 FE            [12] 1906 	mov	r6,a
   03A2 74 05         [12] 1907 	mov	a,#0x05
   03A4 C3            [12] 1908 	clr	c
   03A5 9D            [12] 1909 	subb	a,r5
   03A6 FD            [12] 1910 	mov	r5,a
   03A7 E4            [12] 1911 	clr	a
   03A8 9E            [12] 1912 	subb	a,r6
   03A9 FE            [12] 1913 	mov	r6,a
   03AA AB 28         [24] 1914 	mov	r3,_printBCD_PARM_2
   03AC 7C 00         [12] 1915 	mov	r4,#0x00
   03AE ED            [12] 1916 	mov	a,r5
   03AF B5 03 1B      [24] 1917 	cjne	a,ar3,00109$
   03B2 EE            [12] 1918 	mov	a,r6
   03B3 B5 04 17      [24] 1919 	cjne	a,ar4,00109$
                     02EF  1920 	C$uart.h$151$3$48 ==.
                           1921 ;	C:\Workspace\WSN_MS-4\/uart.h:151: if (!s)
   03B6 20 00 0A      [24] 1922 	jb	_printBCD_s_1_45,00107$
                     02F2  1923 	C$uart.h$152$3$48 ==.
                           1924 ;	C:\Workspace\WSN_MS-4\/uart.h:152: putchar('0');
   03B9 75 82 30      [24] 1925 	mov	dpl,#0x30
   03BC C0 07         [24] 1926 	push	ar7
   03BE 12 02 87      [24] 1927 	lcall	_putchar
   03C1 D0 07         [24] 1928 	pop	ar7
   03C3                    1929 00107$:
                     02FC  1930 	C$uart.h$153$3$48 ==.
                           1931 ;	C:\Workspace\WSN_MS-4\/uart.h:153: putchar(',');
   03C3 75 82 2C      [24] 1932 	mov	dpl,#0x2C
   03C6 C0 07         [24] 1933 	push	ar7
   03C8 12 02 87      [24] 1934 	lcall	_putchar
   03CB D0 07         [24] 1935 	pop	ar7
   03CD                    1936 00109$:
                     0306  1937 	C$uart.h$156$2$47 ==.
                           1938 ;	C:\Workspace\WSN_MS-4\/uart.h:156: if (s || i == 4)
   03CD 20 00 03      [24] 1939 	jb	_printBCD_s_1_45,00110$
   03D0 BF 04 0D      [24] 1940 	cjne	r7,#0x04,00117$
   03D3                    1941 00110$:
                     030C  1942 	C$uart.h$157$2$47 ==.
                           1943 ;	C:\Workspace\WSN_MS-4\/uart.h:157: printc(tmp[i]);
   03D3 EF            [12] 1944 	mov	a,r7
   03D4 24 29         [12] 1945 	add	a,#_printBCD_tmp_1_45
   03D6 F9            [12] 1946 	mov	r1,a
   03D7 87 82         [24] 1947 	mov	dpl,@r1
   03D9 C0 07         [24] 1948 	push	ar7
   03DB 12 02 9A      [24] 1949 	lcall	_printc
   03DE D0 07         [24] 1950 	pop	ar7
   03E0                    1951 00117$:
                     0319  1952 	C$uart.h$145$1$45 ==.
                           1953 ;	C:\Workspace\WSN_MS-4\/uart.h:145: for (i = 0; i < 5; ++i) {
   03E0 0F            [12] 1954 	inc	r7
   03E1 C3            [12] 1955 	clr	c
   03E2 EF            [12] 1956 	mov	a,r7
   03E3 64 80         [12] 1957 	xrl	a,#0x80
   03E5 94 85         [12] 1958 	subb	a,#0x85
   03E7 40 AA         [24] 1959 	jc	00116$
                     0322  1960 	C$uart.h$159$1$45 ==.
                     0322  1961 	XG$printBCD$0$0 ==.
   03E9 22            [24] 1962 	ret
                           1963 ;------------------------------------------------------------
                           1964 ;Allocation info for local variables in function 'printf'
                           1965 ;------------------------------------------------------------
                           1966 ;string                    Allocated to stack - _bp -5
                           1967 ;ap                        Allocated to stack - _bp +1
                           1968 ;ival                      Allocated to registers r4 r3 
                           1969 ;precission                Allocated to registers r7 
                           1970 ;sloc0                     Allocated to stack - _bp +5
                           1971 ;sloc1                     Allocated to stack - _bp +6
                           1972 ;------------------------------------------------------------
                     0323  1973 	G$printf$0$0 ==.
                     0323  1974 	C$uart.h$167$1$45 ==.
                           1975 ;	C:\Workspace\WSN_MS-4\/uart.h:167: void printf(const S8* string, ...)
                           1976 ;	-----------------------------------------
                           1977 ;	 function printf
                           1978 ;	-----------------------------------------
   03EA                    1979 _printf:
   03EA C0 08         [24] 1980 	push	_bp
   03EC 85 81 08      [24] 1981 	mov	_bp,sp
   03EF 05 81         [12] 1982 	inc	sp
                     032A  1983 	C$uart.h$171$1$45 ==.
                           1984 ;	C:\Workspace\WSN_MS-4\/uart.h:171: U8 precission = 0;
   03F1 7F 00         [12] 1985 	mov	r7,#0x00
                     032C  1986 	C$uart.h$173$2$51 ==.
                           1987 ;	C:\Workspace\WSN_MS-4\/uart.h:173: va_start(ap,string);
   03F3 E5 08         [12] 1988 	mov	a,_bp
   03F5 24 FB         [12] 1989 	add	a,#0xFB
   03F7 FE            [12] 1990 	mov	r6,a
   03F8 A8 08         [24] 1991 	mov	r0,_bp
   03FA 08            [12] 1992 	inc	r0
   03FB A6 06         [24] 1993 	mov	@r0,ar6
   03FD                    1994 00114$:
                     0336  1995 	C$uart.h$175$1$50 ==.
                           1996 ;	C:\Workspace\WSN_MS-4\/uart.h:175: for (; *string; ++string) {
   03FD E5 08         [12] 1997 	mov	a,_bp
   03FF 24 FB         [12] 1998 	add	a,#0xfb
   0401 F8            [12] 1999 	mov	r0,a
   0402 86 03         [24] 2000 	mov	ar3,@r0
   0404 08            [12] 2001 	inc	r0
   0405 86 04         [24] 2002 	mov	ar4,@r0
   0407 08            [12] 2003 	inc	r0
   0408 86 05         [24] 2004 	mov	ar5,@r0
   040A 8B 82         [24] 2005 	mov	dpl,r3
   040C 8C 83         [24] 2006 	mov	dph,r4
   040E 8D F0         [24] 2007 	mov	b,r5
   0410 12 18 1F      [24] 2008 	lcall	__gptrget
   0413 FA            [12] 2009 	mov	r2,a
   0414 70 03         [24] 2010 	jnz	00142$
   0416 02 06 03      [24] 2011 	ljmp	00116$
   0419                    2012 00142$:
                     0352  2013 	C$uart.h$176$2$52 ==.
                           2014 ;	C:\Workspace\WSN_MS-4\/uart.h:176: if (*string == '%') {
   0419 BA 25 02      [24] 2015 	cjne	r2,#0x25,00143$
   041C 80 03         [24] 2016 	sjmp	00144$
   041E                    2017 00143$:
   041E 02 05 C9      [24] 2018 	ljmp	00110$
   0421                    2019 00144$:
                     035A  2020 	C$uart.h$177$3$53 ==.
                           2021 ;	C:\Workspace\WSN_MS-4\/uart.h:177: ++string;
   0421 E5 08         [12] 2022 	mov	a,_bp
   0423 24 FB         [12] 2023 	add	a,#0xfb
   0425 F8            [12] 2024 	mov	r0,a
   0426 74 01         [12] 2025 	mov	a,#0x01
   0428 2B            [12] 2026 	add	a,r3
   0429 F6            [12] 2027 	mov	@r0,a
   042A E4            [12] 2028 	clr	a
   042B 3C            [12] 2029 	addc	a,r4
   042C 08            [12] 2030 	inc	r0
   042D F6            [12] 2031 	mov	@r0,a
   042E 08            [12] 2032 	inc	r0
   042F A6 05         [24] 2033 	mov	@r0,ar5
                     036A  2034 	C$uart.h$178$3$53 ==.
                           2035 ;	C:\Workspace\WSN_MS-4\/uart.h:178: switch (*string) {
   0431 E5 08         [12] 2036 	mov	a,_bp
   0433 24 FB         [12] 2037 	add	a,#0xfb
   0435 F8            [12] 2038 	mov	r0,a
   0436 86 03         [24] 2039 	mov	ar3,@r0
   0438 08            [12] 2040 	inc	r0
   0439 86 04         [24] 2041 	mov	ar4,@r0
   043B 08            [12] 2042 	inc	r0
   043C 86 05         [24] 2043 	mov	ar5,@r0
   043E 8B 82         [24] 2044 	mov	dpl,r3
   0440 8C 83         [24] 2045 	mov	dph,r4
   0442 8D F0         [24] 2046 	mov	b,r5
   0444 12 18 1F      [24] 2047 	lcall	__gptrget
   0447 FE            [12] 2048 	mov	r6,a
   0448 BE 2E 03      [24] 2049 	cjne	r6,#0x2E,00145$
   044B 02 05 29      [24] 2050 	ljmp	00103$
   044E                    2051 00145$:
   044E BE 58 02      [24] 2052 	cjne	r6,#0x58,00146$
   0451 80 59         [24] 2053 	sjmp	00102$
   0453                    2054 00146$:
   0453 BE 64 03      [24] 2055 	cjne	r6,#0x64,00147$
   0456 02 05 68      [24] 2056 	ljmp	00104$
   0459                    2057 00147$:
   0459 BE 78 02      [24] 2058 	cjne	r6,#0x78,00148$
   045C 80 03         [24] 2059 	sjmp	00149$
   045E                    2060 00148$:
   045E 02 05 8B      [24] 2061 	ljmp	00105$
   0461                    2062 00149$:
                     039A  2063 	C$uart.h$181$1$50 ==.
                           2064 ;	C:\Workspace\WSN_MS-4\/uart.h:181: ival = va_arg(ap, U16);
   0461 C0 07         [24] 2065 	push	ar7
   0463 A8 08         [24] 2066 	mov	r0,_bp
   0465 08            [12] 2067 	inc	r0
   0466 E6            [12] 2068 	mov	a,@r0
   0467 24 FE         [12] 2069 	add	a,#0xFE
   0469 FF            [12] 2070 	mov	r7,a
   046A A8 08         [24] 2071 	mov	r0,_bp
   046C 08            [12] 2072 	inc	r0
   046D A6 07         [24] 2073 	mov	@r0,ar7
   046F 8F 01         [24] 2074 	mov	ar1,r7
   0471 87 04         [24] 2075 	mov	ar4,@r1
   0473 09            [12] 2076 	inc	r1
   0474 87 03         [24] 2077 	mov	ar3,@r1
   0476 19            [12] 2078 	dec	r1
                     03B0  2079 	C$uart.h$183$4$54 ==.
                           2080 ;	C:\Workspace\WSN_MS-4\/uart.h:183: printc(ival>>4);
   0477 8C 06         [24] 2081 	mov	ar6,r4
   0479 EB            [12] 2082 	mov	a,r3
   047A C4            [12] 2083 	swap	a
   047B CE            [12] 2084 	xch	a,r6
   047C C4            [12] 2085 	swap	a
   047D 54 0F         [12] 2086 	anl	a,#0x0F
   047F 6E            [12] 2087 	xrl	a,r6
   0480 CE            [12] 2088 	xch	a,r6
   0481 54 0F         [12] 2089 	anl	a,#0x0F
   0483 CE            [12] 2090 	xch	a,r6
   0484 6E            [12] 2091 	xrl	a,r6
   0485 CE            [12] 2092 	xch	a,r6
   0486 FF            [12] 2093 	mov	r7,a
   0487 8E 82         [24] 2094 	mov	dpl,r6
   0489 C0 07         [24] 2095 	push	ar7
   048B C0 04         [24] 2096 	push	ar4
   048D C0 03         [24] 2097 	push	ar3
   048F 12 02 9A      [24] 2098 	lcall	_printc
   0492 D0 03         [24] 2099 	pop	ar3
   0494 D0 04         [24] 2100 	pop	ar4
   0496 D0 07         [24] 2101 	pop	ar7
                     03D1  2102 	C$uart.h$184$4$54 ==.
                           2103 ;	C:\Workspace\WSN_MS-4\/uart.h:184: printc(ival&0x000F);
   0498 74 0F         [12] 2104 	mov	a,#0x0F
   049A 5C            [12] 2105 	anl	a,r4
   049B FE            [12] 2106 	mov	r6,a
   049C 7F 00         [12] 2107 	mov	r7,#0x00
   049E 8E 82         [24] 2108 	mov	dpl,r6
   04A0 C0 07         [24] 2109 	push	ar7
   04A2 12 02 9A      [24] 2110 	lcall	_printc
   04A5 D0 07         [24] 2111 	pop	ar7
                     03E0  2112 	C$uart.h$186$1$50 ==.
                           2113 ;	C:\Workspace\WSN_MS-4\/uart.h:186: break;
   04A7 D0 07         [24] 2114 	pop	ar7
   04A9 02 05 F5      [24] 2115 	ljmp	00115$
                     03E5  2116 	C$uart.h$188$4$54 ==.
                           2117 ;	C:\Workspace\WSN_MS-4\/uart.h:188: case 'X':
   04AC                    2118 00102$:
                     03E5  2119 	C$uart.h$190$1$50 ==.
                           2120 ;	C:\Workspace\WSN_MS-4\/uart.h:190: ival = va_arg(ap, U16);
   04AC C0 07         [24] 2121 	push	ar7
   04AE A8 08         [24] 2122 	mov	r0,_bp
   04B0 08            [12] 2123 	inc	r0
   04B1 E6            [12] 2124 	mov	a,@r0
   04B2 24 FE         [12] 2125 	add	a,#0xFE
   04B4 FE            [12] 2126 	mov	r6,a
   04B5 A8 08         [24] 2127 	mov	r0,_bp
   04B7 08            [12] 2128 	inc	r0
   04B8 A6 06         [24] 2129 	mov	@r0,ar6
   04BA 8E 01         [24] 2130 	mov	ar1,r6
   04BC 87 04         [24] 2131 	mov	ar4,@r1
   04BE 09            [12] 2132 	inc	r1
   04BF 87 03         [24] 2133 	mov	ar3,@r1
   04C1 19            [12] 2134 	dec	r1
                     03FB  2135 	C$uart.h$192$4$54 ==.
                           2136 ;	C:\Workspace\WSN_MS-4\/uart.h:192: printc(ival>>12);
   04C2 EB            [12] 2137 	mov	a,r3
   04C3 C4            [12] 2138 	swap	a
   04C4 54 0F         [12] 2139 	anl	a,#0x0F
   04C6 FE            [12] 2140 	mov	r6,a
   04C7 7F 00         [12] 2141 	mov	r7,#0x00
   04C9 8E 82         [24] 2142 	mov	dpl,r6
   04CB C0 07         [24] 2143 	push	ar7
   04CD C0 04         [24] 2144 	push	ar4
   04CF C0 03         [24] 2145 	push	ar3
   04D1 12 02 9A      [24] 2146 	lcall	_printc
   04D4 D0 03         [24] 2147 	pop	ar3
   04D6 D0 04         [24] 2148 	pop	ar4
   04D8 D0 07         [24] 2149 	pop	ar7
                     0413  2150 	C$uart.h$193$4$54 ==.
                           2151 ;	C:\Workspace\WSN_MS-4\/uart.h:193: printc((ival>>8)&0x000F);
   04DA 8B 07         [24] 2152 	mov	ar7,r3
   04DC 74 0F         [12] 2153 	mov	a,#0x0F
   04DE 5F            [12] 2154 	anl	a,r7
   04DF F5 82         [12] 2155 	mov	dpl,a
   04E1 C0 07         [24] 2156 	push	ar7
   04E3 C0 04         [24] 2157 	push	ar4
   04E5 C0 03         [24] 2158 	push	ar3
   04E7 12 02 9A      [24] 2159 	lcall	_printc
   04EA D0 03         [24] 2160 	pop	ar3
   04EC D0 04         [24] 2161 	pop	ar4
   04EE D0 07         [24] 2162 	pop	ar7
                     0429  2163 	C$uart.h$194$4$54 ==.
                           2164 ;	C:\Workspace\WSN_MS-4\/uart.h:194: printc((ival>>4)&0x000F);
   04F0 8C 06         [24] 2165 	mov	ar6,r4
   04F2 EB            [12] 2166 	mov	a,r3
   04F3 C4            [12] 2167 	swap	a
   04F4 CE            [12] 2168 	xch	a,r6
   04F5 C4            [12] 2169 	swap	a
   04F6 54 0F         [12] 2170 	anl	a,#0x0F
   04F8 6E            [12] 2171 	xrl	a,r6
   04F9 CE            [12] 2172 	xch	a,r6
   04FA 54 0F         [12] 2173 	anl	a,#0x0F
   04FC CE            [12] 2174 	xch	a,r6
   04FD 6E            [12] 2175 	xrl	a,r6
   04FE CE            [12] 2176 	xch	a,r6
   04FF 53 06 0F      [24] 2177 	anl	ar6,#0x0F
   0502 7F 00         [12] 2178 	mov	r7,#0x00
   0504 8E 82         [24] 2179 	mov	dpl,r6
   0506 C0 07         [24] 2180 	push	ar7
   0508 C0 04         [24] 2181 	push	ar4
   050A C0 03         [24] 2182 	push	ar3
   050C 12 02 9A      [24] 2183 	lcall	_printc
   050F D0 03         [24] 2184 	pop	ar3
   0511 D0 04         [24] 2185 	pop	ar4
   0513 D0 07         [24] 2186 	pop	ar7
                     044E  2187 	C$uart.h$195$4$54 ==.
                           2188 ;	C:\Workspace\WSN_MS-4\/uart.h:195: printc(ival&0x000F);
   0515 74 0F         [12] 2189 	mov	a,#0x0F
   0517 5C            [12] 2190 	anl	a,r4
   0518 FE            [12] 2191 	mov	r6,a
   0519 7F 00         [12] 2192 	mov	r7,#0x00
   051B 8E 82         [24] 2193 	mov	dpl,r6
   051D C0 07         [24] 2194 	push	ar7
   051F 12 02 9A      [24] 2195 	lcall	_printc
   0522 D0 07         [24] 2196 	pop	ar7
                     045D  2197 	C$uart.h$197$1$50 ==.
                           2198 ;	C:\Workspace\WSN_MS-4\/uart.h:197: break;
   0524 D0 07         [24] 2199 	pop	ar7
   0526 02 05 F5      [24] 2200 	ljmp	00115$
                     0462  2201 	C$uart.h$199$4$54 ==.
                           2202 ;	C:\Workspace\WSN_MS-4\/uart.h:199: case '.':
   0529                    2203 00103$:
                     0462  2204 	C$uart.h$201$4$54 ==.
                           2205 ;	C:\Workspace\WSN_MS-4\/uart.h:201: ++string;
   0529 E5 08         [12] 2206 	mov	a,_bp
   052B 24 FB         [12] 2207 	add	a,#0xfb
   052D F8            [12] 2208 	mov	r0,a
   052E 74 01         [12] 2209 	mov	a,#0x01
   0530 2B            [12] 2210 	add	a,r3
   0531 F6            [12] 2211 	mov	@r0,a
   0532 E4            [12] 2212 	clr	a
   0533 3C            [12] 2213 	addc	a,r4
   0534 08            [12] 2214 	inc	r0
   0535 F6            [12] 2215 	mov	@r0,a
   0536 08            [12] 2216 	inc	r0
   0537 A6 05         [24] 2217 	mov	@r0,ar5
                     0472  2218 	C$uart.h$202$4$54 ==.
                           2219 ;	C:\Workspace\WSN_MS-4\/uart.h:202: precission = (U16) *string-'0';
   0539 E5 08         [12] 2220 	mov	a,_bp
   053B 24 FB         [12] 2221 	add	a,#0xfb
   053D F8            [12] 2222 	mov	r0,a
   053E 86 05         [24] 2223 	mov	ar5,@r0
   0540 08            [12] 2224 	inc	r0
   0541 86 04         [24] 2225 	mov	ar4,@r0
   0543 08            [12] 2226 	inc	r0
   0544 86 02         [24] 2227 	mov	ar2,@r0
   0546 8D 82         [24] 2228 	mov	dpl,r5
   0548 8C 83         [24] 2229 	mov	dph,r4
   054A 8A F0         [24] 2230 	mov	b,r2
   054C 12 18 1F      [24] 2231 	lcall	__gptrget
   054F FB            [12] 2232 	mov	r3,a
   0550 33            [12] 2233 	rlc	a
   0551 95 E0         [12] 2234 	subb	a,acc
   0553 FE            [12] 2235 	mov	r6,a
   0554 EB            [12] 2236 	mov	a,r3
   0555 24 D0         [12] 2237 	add	a,#0xD0
   0557 FF            [12] 2238 	mov	r7,a
                     0491  2239 	C$uart.h$203$4$54 ==.
                           2240 ;	C:\Workspace\WSN_MS-4\/uart.h:203: ++string;
   0558 E5 08         [12] 2241 	mov	a,_bp
   055A 24 FB         [12] 2242 	add	a,#0xfb
   055C F8            [12] 2243 	mov	r0,a
   055D 74 01         [12] 2244 	mov	a,#0x01
   055F 2D            [12] 2245 	add	a,r5
   0560 F6            [12] 2246 	mov	@r0,a
   0561 E4            [12] 2247 	clr	a
   0562 3C            [12] 2248 	addc	a,r4
   0563 08            [12] 2249 	inc	r0
   0564 F6            [12] 2250 	mov	@r0,a
   0565 08            [12] 2251 	inc	r0
   0566 A6 02         [24] 2252 	mov	@r0,ar2
                     04A1  2253 	C$uart.h$205$4$54 ==.
                           2254 ;	C:\Workspace\WSN_MS-4\/uart.h:205: case 'd':
   0568                    2255 00104$:
                     04A1  2256 	C$uart.h$207$4$54 ==.
                           2257 ;	C:\Workspace\WSN_MS-4\/uart.h:207: ival = va_arg(ap, S16);
   0568 A8 08         [24] 2258 	mov	r0,_bp
   056A 08            [12] 2259 	inc	r0
   056B E6            [12] 2260 	mov	a,@r0
   056C 24 FE         [12] 2261 	add	a,#0xFE
   056E FE            [12] 2262 	mov	r6,a
   056F A8 08         [24] 2263 	mov	r0,_bp
   0571 08            [12] 2264 	inc	r0
   0572 A6 06         [24] 2265 	mov	@r0,ar6
   0574 8E 01         [24] 2266 	mov	ar1,r6
   0576 87 04         [24] 2267 	mov	ar4,@r1
   0578 09            [12] 2268 	inc	r1
   0579 87 03         [24] 2269 	mov	ar3,@r1
   057B 19            [12] 2270 	dec	r1
                     04B5  2271 	C$uart.h$209$4$54 ==.
                           2272 ;	C:\Workspace\WSN_MS-4\/uart.h:209: printBCD(ival, precission);
   057C 8F 28         [24] 2273 	mov	_printBCD_PARM_2,r7
   057E 8C 82         [24] 2274 	mov	dpl,r4
   0580 8B 83         [24] 2275 	mov	dph,r3
   0582 C0 07         [24] 2276 	push	ar7
   0584 12 03 38      [24] 2277 	lcall	_printBCD
   0587 D0 07         [24] 2278 	pop	ar7
                     04C2  2279 	C$uart.h$211$4$54 ==.
                           2280 ;	C:\Workspace\WSN_MS-4\/uart.h:211: break;
                     04C2  2281 	C$uart.h$213$4$54 ==.
                           2282 ;	C:\Workspace\WSN_MS-4\/uart.h:213: default:
   0589 80 6A         [24] 2283 	sjmp	00115$
   058B                    2284 00105$:
                     04C4  2285 	C$uart.h$214$4$54 ==.
                           2286 ;	C:\Workspace\WSN_MS-4\/uart.h:214: ival = va_arg(ap, U16);
   058B A8 08         [24] 2287 	mov	r0,_bp
   058D 08            [12] 2288 	inc	r0
   058E E6            [12] 2289 	mov	a,@r0
   058F 24 FE         [12] 2290 	add	a,#0xFE
   0591 FE            [12] 2291 	mov	r6,a
   0592 A8 08         [24] 2292 	mov	r0,_bp
   0594 08            [12] 2293 	inc	r0
   0595 A6 06         [24] 2294 	mov	@r0,ar6
   0597 8E 01         [24] 2295 	mov	ar1,r6
   0599 87 04         [24] 2296 	mov	ar4,@r1
   059B 09            [12] 2297 	inc	r1
   059C 87 03         [24] 2298 	mov	ar3,@r1
   059E 19            [12] 2299 	dec	r1
                     04D8  2300 	C$uart.h$216$4$54 ==.
                           2301 ;	C:\Workspace\WSN_MS-4\/uart.h:216: printc(ival>>4);
   059F 8C 05         [24] 2302 	mov	ar5,r4
   05A1 EB            [12] 2303 	mov	a,r3
   05A2 C4            [12] 2304 	swap	a
   05A3 CD            [12] 2305 	xch	a,r5
   05A4 C4            [12] 2306 	swap	a
   05A5 54 0F         [12] 2307 	anl	a,#0x0F
   05A7 6D            [12] 2308 	xrl	a,r5
   05A8 CD            [12] 2309 	xch	a,r5
   05A9 54 0F         [12] 2310 	anl	a,#0x0F
   05AB CD            [12] 2311 	xch	a,r5
   05AC 6D            [12] 2312 	xrl	a,r5
   05AD CD            [12] 2313 	xch	a,r5
   05AE 8D 82         [24] 2314 	mov	dpl,r5
   05B0 C0 07         [24] 2315 	push	ar7
   05B2 C0 04         [24] 2316 	push	ar4
   05B4 C0 03         [24] 2317 	push	ar3
   05B6 12 02 9A      [24] 2318 	lcall	_printc
   05B9 D0 03         [24] 2319 	pop	ar3
   05BB D0 04         [24] 2320 	pop	ar4
                     04F6  2321 	C$uart.h$217$4$54 ==.
                           2322 ;	C:\Workspace\WSN_MS-4\/uart.h:217: printc(ival&0x000F);
   05BD 74 0F         [12] 2323 	mov	a,#0x0F
   05BF 5C            [12] 2324 	anl	a,r4
   05C0 F5 82         [12] 2325 	mov	dpl,a
   05C2 12 02 9A      [24] 2326 	lcall	_printc
   05C5 D0 07         [24] 2327 	pop	ar7
                     0500  2328 	C$uart.h$218$2$52 ==.
                           2329 ;	C:\Workspace\WSN_MS-4\/uart.h:218: }
   05C7 80 2C         [24] 2330 	sjmp	00115$
   05C9                    2331 00110$:
                     0502  2332 	C$uart.h$220$3$55 ==.
                           2333 ;	C:\Workspace\WSN_MS-4\/uart.h:220: if (*string == '\n')
   05C9 BA 0A 0A      [24] 2334 	cjne	r2,#0x0A,00108$
                     0505  2335 	C$uart.h$221$3$55 ==.
                           2336 ;	C:\Workspace\WSN_MS-4\/uart.h:221: putchar(0x0D);
   05CC 75 82 0D      [24] 2337 	mov	dpl,#0x0D
   05CF C0 07         [24] 2338 	push	ar7
   05D1 12 02 87      [24] 2339 	lcall	_putchar
   05D4 D0 07         [24] 2340 	pop	ar7
   05D6                    2341 00108$:
                     050F  2342 	C$uart.h$223$3$55 ==.
                           2343 ;	C:\Workspace\WSN_MS-4\/uart.h:223: putchar(*string);
   05D6 E5 08         [12] 2344 	mov	a,_bp
   05D8 24 FB         [12] 2345 	add	a,#0xfb
   05DA F8            [12] 2346 	mov	r0,a
   05DB 86 04         [24] 2347 	mov	ar4,@r0
   05DD 08            [12] 2348 	inc	r0
   05DE 86 05         [24] 2349 	mov	ar5,@r0
   05E0 08            [12] 2350 	inc	r0
   05E1 86 06         [24] 2351 	mov	ar6,@r0
   05E3 8C 82         [24] 2352 	mov	dpl,r4
   05E5 8D 83         [24] 2353 	mov	dph,r5
   05E7 8E F0         [24] 2354 	mov	b,r6
   05E9 12 18 1F      [24] 2355 	lcall	__gptrget
   05EC F5 82         [12] 2356 	mov	dpl,a
   05EE C0 07         [24] 2357 	push	ar7
   05F0 12 02 87      [24] 2358 	lcall	_putchar
   05F3 D0 07         [24] 2359 	pop	ar7
   05F5                    2360 00115$:
                     052E  2361 	C$uart.h$175$1$50 ==.
                           2362 ;	C:\Workspace\WSN_MS-4\/uart.h:175: for (; *string; ++string) {
   05F5 E5 08         [12] 2363 	mov	a,_bp
   05F7 24 FB         [12] 2364 	add	a,#0xfb
   05F9 F8            [12] 2365 	mov	r0,a
   05FA 06            [12] 2366 	inc	@r0
   05FB B6 00 02      [24] 2367 	cjne	@r0,#0x00,00152$
   05FE 08            [12] 2368 	inc	r0
   05FF 06            [12] 2369 	inc	@r0
   0600                    2370 00152$:
   0600 02 03 FD      [24] 2371 	ljmp	00114$
   0603                    2372 00116$:
   0603 15 81         [12] 2373 	dec	sp
   0605 D0 08         [24] 2374 	pop	_bp
                     0540  2375 	C$uart.h$226$1$50 ==.
                     0540  2376 	XG$printf$0$0 ==.
   0607 22            [24] 2377 	ret
                           2378 ;------------------------------------------------------------
                           2379 ;Allocation info for local variables in function 'crc8'
                           2380 ;------------------------------------------------------------
                           2381 ;crc                       Allocated with name '_crc8_PARM_2'
                           2382 ;len                       Allocated with name '_crc8_PARM_3'
                           2383 ;data                      Allocated to registers r5 r6 r7 
                           2384 ;i                         Allocated to registers r3 r4 
                           2385 ;j                         Allocated to registers r1 r2 
                           2386 ;------------------------------------------------------------
                     0541  2387 	G$crc8$0$0 ==.
                     0541  2388 	C$crc8.h$11$1$50 ==.
                           2389 ;	C:\Workspace\WSN_MS-4\/crc8.h:11: U8 crc8(U8* data, U8 crc, U16 len) {
                           2390 ;	-----------------------------------------
                           2391 ;	 function crc8
                           2392 ;	-----------------------------------------
   0608                    2393 _crc8:
   0608 AD 82         [24] 2394 	mov	r5,dpl
   060A AE 83         [24] 2395 	mov	r6,dph
   060C AF F0         [24] 2396 	mov	r7,b
                     0547  2397 	C$crc8.h$15$1$57 ==.
                           2398 ;	C:\Workspace\WSN_MS-4\/crc8.h:15: for (i = 0; i < len; i++) {
   060E 7B 00         [12] 2399 	mov	r3,#0x00
   0610 7C 00         [12] 2400 	mov	r4,#0x00
   0612                    2401 00109$:
   0612 C3            [12] 2402 	clr	c
   0613 EB            [12] 2403 	mov	a,r3
   0614 95 0A         [12] 2404 	subb	a,_crc8_PARM_3
   0616 EC            [12] 2405 	mov	a,r4
   0617 95 0B         [12] 2406 	subb	a,(_crc8_PARM_3 + 1)
   0619 50 44         [24] 2407 	jnc	00105$
                     0554  2408 	C$crc8.h$16$2$58 ==.
                           2409 ;	C:\Workspace\WSN_MS-4\/crc8.h:16: crc ^= data[i];
   061B EB            [12] 2410 	mov	a,r3
   061C 2D            [12] 2411 	add	a,r5
   061D F8            [12] 2412 	mov	r0,a
   061E EC            [12] 2413 	mov	a,r4
   061F 3E            [12] 2414 	addc	a,r6
   0620 F9            [12] 2415 	mov	r1,a
   0621 8F 02         [24] 2416 	mov	ar2,r7
   0623 88 82         [24] 2417 	mov	dpl,r0
   0625 89 83         [24] 2418 	mov	dph,r1
   0627 8A F0         [24] 2419 	mov	b,r2
   0629 12 18 1F      [24] 2420 	lcall	__gptrget
   062C F8            [12] 2421 	mov	r0,a
   062D 62 09         [12] 2422 	xrl	_crc8_PARM_2,a
                     0568  2423 	C$crc8.h$17$1$57 ==.
                           2424 ;	C:\Workspace\WSN_MS-4\/crc8.h:17: for (j = 0; j < 8; j++) {
   062F 79 00         [12] 2425 	mov	r1,#0x00
   0631 7A 00         [12] 2426 	mov	r2,#0x00
   0633                    2427 00106$:
                     056C  2428 	C$crc8.h$18$3$59 ==.
                           2429 ;	C:\Workspace\WSN_MS-4\/crc8.h:18: if ((crc & 0x80) != 0)
   0633 E5 09         [12] 2430 	mov	a,_crc8_PARM_2
   0635 30 E7 0C      [24] 2431 	jnb	acc.7,00102$
                     0571  2432 	C$crc8.h$19$3$59 ==.
                           2433 ;	C:\Workspace\WSN_MS-4\/crc8.h:19: crc = (crc << 1) ^ crc8polynom; 
   0638 E5 09         [12] 2434 	mov	a,_crc8_PARM_2
   063A 25 09         [12] 2435 	add	a,_crc8_PARM_2
   063C F8            [12] 2436 	mov	r0,a
   063D 74 07         [12] 2437 	mov	a,#0x07
   063F 68            [12] 2438 	xrl	a,r0
   0640 F5 09         [12] 2439 	mov	_crc8_PARM_2,a
   0642 80 06         [24] 2440 	sjmp	00107$
   0644                    2441 00102$:
                     057D  2442 	C$crc8.h$21$3$59 ==.
                           2443 ;	C:\Workspace\WSN_MS-4\/crc8.h:21: crc <<=1;
   0644 E5 09         [12] 2444 	mov	a,_crc8_PARM_2
   0646 25 09         [12] 2445 	add	a,_crc8_PARM_2
   0648 F5 09         [12] 2446 	mov	_crc8_PARM_2,a
   064A                    2447 00107$:
                     0583  2448 	C$crc8.h$17$2$58 ==.
                           2449 ;	C:\Workspace\WSN_MS-4\/crc8.h:17: for (j = 0; j < 8; j++) {
   064A 09            [12] 2450 	inc	r1
   064B B9 00 01      [24] 2451 	cjne	r1,#0x00,00132$
   064E 0A            [12] 2452 	inc	r2
   064F                    2453 00132$:
   064F C3            [12] 2454 	clr	c
   0650 E9            [12] 2455 	mov	a,r1
   0651 94 08         [12] 2456 	subb	a,#0x08
   0653 EA            [12] 2457 	mov	a,r2
   0654 94 00         [12] 2458 	subb	a,#0x00
   0656 40 DB         [24] 2459 	jc	00106$
                     0591  2460 	C$crc8.h$15$1$57 ==.
                           2461 ;	C:\Workspace\WSN_MS-4\/crc8.h:15: for (i = 0; i < len; i++) {
   0658 0B            [12] 2462 	inc	r3
   0659 BB 00 B6      [24] 2463 	cjne	r3,#0x00,00109$
   065C 0C            [12] 2464 	inc	r4
   065D 80 B3         [24] 2465 	sjmp	00109$
   065F                    2466 00105$:
                     0598  2467 	C$crc8.h$25$1$57 ==.
                           2468 ;	C:\Workspace\WSN_MS-4\/crc8.h:25: return crc;
   065F 85 09 82      [24] 2469 	mov	dpl,_crc8_PARM_2
                     059B  2470 	C$crc8.h$26$1$57 ==.
                     059B  2471 	XG$crc8$0$0 ==.
   0662 22            [24] 2472 	ret
                           2473 ;------------------------------------------------------------
                           2474 ;Allocation info for local variables in function 'CC1000_WriteToRegister'
                           2475 ;------------------------------------------------------------
                           2476 ;value                     Allocated with name '_CC1000_WriteToRegister_PARM_2'
                           2477 ;address                   Allocated to registers r7 
                           2478 ;BitCnt                    Allocated to registers r6 
                           2479 ;i                         Allocated to registers r5 
                           2480 ;------------------------------------------------------------
                     059C  2481 	G$CC1000_WriteToRegister$0$0 ==.
                     059C  2482 	C$cc1000.h$149$1$57 ==.
                           2483 ;	C:\Workspace\WSN_MS-4\/cc1000.h:149: void CC1000_WriteToRegister(U8 address, U8 value)   
                           2484 ;	-----------------------------------------
                           2485 ;	 function CC1000_WriteToRegister
                           2486 ;	-----------------------------------------
   0663                    2487 _CC1000_WriteToRegister:
   0663 AF 82         [24] 2488 	mov	r7,dpl
                     059E  2489 	C$cc1000.h$153$1$81 ==.
                           2490 ;	C:\Workspace\WSN_MS-4\/cc1000.h:153: P0MDOUT |= 0x10;                // Wyj�cie P0.4 (PDATA) jako push-pull
   0665 43 A4 10      [24] 2491 	orl	_P0MDOUT,#0x10
                     05A1  2492 	C$cc1000.h$154$1$81 ==.
                           2493 ;	C:\Workspace\WSN_MS-4\/cc1000.h:154: PCLK = 1;
   0668 D2 85         [12] 2494 	setb	_PCLK
                     05A3  2495 	C$cc1000.h$155$1$81 ==.
                           2496 ;	C:\Workspace\WSN_MS-4\/cc1000.h:155: PALE = 1;
   066A D2 87         [12] 2497 	setb	_PALE
                     05A5  2498 	C$cc1000.h$156$1$81 ==.
                           2499 ;	C:\Workspace\WSN_MS-4\/cc1000.h:156: address = (address<<1)|0x01;    // W lewo bo adr. 7-bit; (LSB = 1 to zapis)
   066C EF            [12] 2500 	mov	a,r7
   066D 2F            [12] 2501 	add	a,r7
   066E FE            [12] 2502 	mov	r6,a
   066F 74 01         [12] 2503 	mov	a,#0x01
   0671 4E            [12] 2504 	orl	a,r6
   0672 FF            [12] 2505 	mov	r7,a
                     05AC  2506 	C$cc1000.h$157$1$81 ==.
                           2507 ;	C:\Workspace\WSN_MS-4\/cc1000.h:157: PALE = 0;                               // Przygotowanie zapisu adresu
   0673 C2 87         [12] 2508 	clr	_PALE
                     05AE  2509 	C$cc1000.h$158$1$81 ==.
                           2510 ;	C:\Workspace\WSN_MS-4\/cc1000.h:158: for (BitCnt = 0; BitCnt < 8; BitCnt++)	// Wysy�anie adresu po jednym bicie (szeregowo)
   0675 7E 00         [12] 2511 	mov	r6,#0x00
   0677                    2512 00115$:
                     05B0  2513 	C$cc1000.h$160$2$82 ==.
                           2514 ;	C:\Workspace\WSN_MS-4\/cc1000.h:160: PCLK = 1;
   0677 D2 85         [12] 2515 	setb	_PCLK
                     05B2  2516 	C$cc1000.h$161$2$82 ==.
                           2517 ;	C:\Workspace\WSN_MS-4\/cc1000.h:161: if (address & 0x80)         // Test MSB 
   0679 EF            [12] 2518 	mov	a,r7
   067A 30 E7 04      [24] 2519 	jnb	acc.7,00102$
                     05B6  2520 	C$cc1000.h$162$2$82 ==.
                           2521 ;	C:\Workspace\WSN_MS-4\/cc1000.h:162: PDATA = 1;
   067D D2 84         [12] 2522 	setb	_PDATA
   067F 80 02         [24] 2523 	sjmp	00103$
   0681                    2524 00102$:
                     05BA  2525 	C$cc1000.h$164$2$82 ==.
                           2526 ;	C:\Workspace\WSN_MS-4\/cc1000.h:164: PDATA = 0;
   0681 C2 84         [12] 2527 	clr	_PDATA
   0683                    2528 00103$:
                     05BC  2529 	C$cc1000.h$165$2$82 ==.
                           2530 ;	C:\Workspace\WSN_MS-4\/cc1000.h:165: address = address<<1;		// Nast�pny bit  
   0683 EF            [12] 2531 	mov	a,r7
   0684 2F            [12] 2532 	add	a,r7
   0685 FF            [12] 2533 	mov	r7,a
                     05BF  2534 	C$cc1000.h$166$2$82 ==.
                           2535 ;	C:\Workspace\WSN_MS-4\/cc1000.h:166: PCLK = 0;                   // Zapis bitu
   0686 C2 85         [12] 2536 	clr	_PCLK
                     05C1  2537 	C$cc1000.h$167$2$82 ==.
                           2538 ;	C:\Workspace\WSN_MS-4\/cc1000.h:167: for (i=0; i<0x0F; i++);     // Op�nienie
   0688 7D 0F         [12] 2539 	mov	r5,#0x0F
   068A                    2540 00114$:
   068A 8D 04         [24] 2541 	mov	ar4,r5
   068C EC            [12] 2542 	mov	a,r4
   068D 14            [12] 2543 	dec	a
   068E FD            [12] 2544 	mov	r5,a
   068F 70 F9         [24] 2545 	jnz	00114$
                     05CA  2546 	C$cc1000.h$158$1$81 ==.
                           2547 ;	C:\Workspace\WSN_MS-4\/cc1000.h:158: for (BitCnt = 0; BitCnt < 8; BitCnt++)	// Wysy�anie adresu po jednym bicie (szeregowo)
   0691 0E            [12] 2548 	inc	r6
   0692 BE 08 00      [24] 2549 	cjne	r6,#0x08,00167$
   0695                    2550 00167$:
   0695 40 E0         [24] 2551 	jc	00115$
                     05D0  2552 	C$cc1000.h$169$1$81 ==.
                           2553 ;	C:\Workspace\WSN_MS-4\/cc1000.h:169: PCLK = 1;
   0697 D2 85         [12] 2554 	setb	_PCLK
                     05D2  2555 	C$cc1000.h$170$1$81 ==.
                           2556 ;	C:\Workspace\WSN_MS-4\/cc1000.h:170: for (i=0; i<0x0F; i++);         // Op�nienie
   0699 7D 0F         [12] 2557 	mov	r5,#0x0F
   069B                    2558 00119$:
   069B 8D 07         [24] 2559 	mov	ar7,r5
   069D EF            [12] 2560 	mov	a,r7
   069E 14            [12] 2561 	dec	a
   069F FD            [12] 2562 	mov	r5,a
   06A0 70 F9         [24] 2563 	jnz	00119$
                     05DB  2564 	C$cc1000.h$171$1$81 ==.
                           2565 ;	C:\Workspace\WSN_MS-4\/cc1000.h:171: PALE = 1;                               // Przygotowanie zapisu danej
   06A2 D2 87         [12] 2566 	setb	_PALE
                     05DD  2567 	C$cc1000.h$172$1$81 ==.
                           2568 ;	C:\Workspace\WSN_MS-4\/cc1000.h:172: for (BitCnt = 0; BitCnt < 8; BitCnt++)	// wysy�anie danych po jednym bicie
   06A4 7F 00         [12] 2569 	mov	r7,#0x00
   06A6                    2570 00123$:
                     05DF  2571 	C$cc1000.h$174$2$83 ==.
                           2572 ;	C:\Workspace\WSN_MS-4\/cc1000.h:174: PCLK = 1;
   06A6 D2 85         [12] 2573 	setb	_PCLK
                     05E1  2574 	C$cc1000.h$175$2$83 ==.
                           2575 ;	C:\Workspace\WSN_MS-4\/cc1000.h:175: if (value & 0x80)           // Test MSB 
   06A8 E5 09         [12] 2576 	mov	a,_CC1000_WriteToRegister_PARM_2
   06AA 30 E7 04      [24] 2577 	jnb	acc.7,00108$
                     05E6  2578 	C$cc1000.h$176$2$83 ==.
                           2579 ;	C:\Workspace\WSN_MS-4\/cc1000.h:176: PDATA = 1;
   06AD D2 84         [12] 2580 	setb	_PDATA
   06AF 80 02         [24] 2581 	sjmp	00109$
   06B1                    2582 00108$:
                     05EA  2583 	C$cc1000.h$178$2$83 ==.
                           2584 ;	C:\Workspace\WSN_MS-4\/cc1000.h:178: PDATA = 0;
   06B1 C2 84         [12] 2585 	clr	_PDATA
   06B3                    2586 00109$:
                     05EC  2587 	C$cc1000.h$179$2$83 ==.
                           2588 ;	C:\Workspace\WSN_MS-4\/cc1000.h:179: value = value<<1;		    // Nast�pny bit
   06B3 E5 09         [12] 2589 	mov	a,_CC1000_WriteToRegister_PARM_2
   06B5 25 09         [12] 2590 	add	a,_CC1000_WriteToRegister_PARM_2
   06B7 F5 09         [12] 2591 	mov	_CC1000_WriteToRegister_PARM_2,a
                     05F2  2592 	C$cc1000.h$180$2$83 ==.
                           2593 ;	C:\Workspace\WSN_MS-4\/cc1000.h:180: PCLK = 0;
   06B9 C2 85         [12] 2594 	clr	_PCLK
                     05F4  2595 	C$cc1000.h$181$2$83 ==.
                           2596 ;	C:\Workspace\WSN_MS-4\/cc1000.h:181: for (i=0; i<0x0F; i++);     // Op�nienie
   06BB 7D 0F         [12] 2597 	mov	r5,#0x0F
   06BD                    2598 00122$:
   06BD 8D 06         [24] 2599 	mov	ar6,r5
   06BF EE            [12] 2600 	mov	a,r6
   06C0 14            [12] 2601 	dec	a
   06C1 FD            [12] 2602 	mov	r5,a
   06C2 70 F9         [24] 2603 	jnz	00122$
                     05FD  2604 	C$cc1000.h$172$1$81 ==.
                           2605 ;	C:\Workspace\WSN_MS-4\/cc1000.h:172: for (BitCnt = 0; BitCnt < 8; BitCnt++)	// wysy�anie danych po jednym bicie
   06C4 0F            [12] 2606 	inc	r7
   06C5 BF 08 00      [24] 2607 	cjne	r7,#0x08,00172$
   06C8                    2608 00172$:
   06C8 40 DC         [24] 2609 	jc	00123$
                     0603  2610 	C$cc1000.h$183$1$81 ==.
                           2611 ;	C:\Workspace\WSN_MS-4\/cc1000.h:183: PCLK  = 1;
   06CA D2 85         [12] 2612 	setb	_PCLK
                     0605  2613 	C$cc1000.h$184$1$81 ==.
                           2614 ;	C:\Workspace\WSN_MS-4\/cc1000.h:184: PDATA = 1;  
   06CC D2 84         [12] 2615 	setb	_PDATA
                     0607  2616 	C$cc1000.h$185$1$81 ==.
                     0607  2617 	XG$CC1000_WriteToRegister$0$0 ==.
   06CE 22            [24] 2618 	ret
                           2619 ;------------------------------------------------------------
                           2620 ;Allocation info for local variables in function 'CC1000_ReadFromRegister'
                           2621 ;------------------------------------------------------------
                           2622 ;address                   Allocated to registers r7 
                           2623 ;Value                     Allocated to registers r6 
                           2624 ;BitCnt                    Allocated to registers r5 
                           2625 ;i                         Allocated to registers r4 
                           2626 ;------------------------------------------------------------
                     0608  2627 	G$CC1000_ReadFromRegister$0$0 ==.
                     0608  2628 	C$cc1000.h$190$1$81 ==.
                           2629 ;	C:\Workspace\WSN_MS-4\/cc1000.h:190: U8 CC1000_ReadFromRegister(U8 address) 
                           2630 ;	-----------------------------------------
                           2631 ;	 function CC1000_ReadFromRegister
                           2632 ;	-----------------------------------------
   06CF                    2633 _CC1000_ReadFromRegister:
   06CF AF 82         [24] 2634 	mov	r7,dpl
                     060A  2635 	C$cc1000.h$192$1$81 ==.
                           2636 ;	C:\Workspace\WSN_MS-4\/cc1000.h:192: U8 Value = 0;
   06D1 7E 00         [12] 2637 	mov	r6,#0x00
                     060C  2638 	C$cc1000.h$195$1$85 ==.
                           2639 ;	C:\Workspace\WSN_MS-4\/cc1000.h:195: P0MDOUT |= 0x10;            // Wyj�cie P0.4 (PDATA) jako push-pull
   06D3 43 A4 10      [24] 2640 	orl	_P0MDOUT,#0x10
                     060F  2641 	C$cc1000.h$196$1$85 ==.
                           2642 ;	C:\Workspace\WSN_MS-4\/cc1000.h:196: PCLK = 1;
   06D6 D2 85         [12] 2643 	setb	_PCLK
                     0611  2644 	C$cc1000.h$197$1$85 ==.
                           2645 ;	C:\Workspace\WSN_MS-4\/cc1000.h:197: PALE = 1;
   06D8 D2 87         [12] 2646 	setb	_PALE
                     0613  2647 	C$cc1000.h$198$1$85 ==.
                           2648 ;	C:\Workspace\WSN_MS-4\/cc1000.h:198: address = (address<<1)&0xFE;	        // Adres 7-bit; (LSB = 0 - odczyt);
   06DA EF            [12] 2649 	mov	a,r7
   06DB 2F            [12] 2650 	add	a,r7
   06DC FD            [12] 2651 	mov	r5,a
   06DD 74 FE         [12] 2652 	mov	a,#0xFE
   06DF 5D            [12] 2653 	anl	a,r5
   06E0 FF            [12] 2654 	mov	r7,a
                     061A  2655 	C$cc1000.h$199$1$85 ==.
                           2656 ;	C:\Workspace\WSN_MS-4\/cc1000.h:199: PALE = 0;
   06E1 C2 87         [12] 2657 	clr	_PALE
                     061C  2658 	C$cc1000.h$200$1$85 ==.
                           2659 ;	C:\Workspace\WSN_MS-4\/cc1000.h:200: for (BitCnt = 0; BitCnt < 8; BitCnt++)	// Wysy�anie adresu po jednym bicie (szeregowo)
   06E3 7D 00         [12] 2660 	mov	r5,#0x00
   06E5                    2661 00115$:
                     061E  2662 	C$cc1000.h$202$2$86 ==.
                           2663 ;	C:\Workspace\WSN_MS-4\/cc1000.h:202: PCLK = 1;
   06E5 D2 85         [12] 2664 	setb	_PCLK
                     0620  2665 	C$cc1000.h$203$2$86 ==.
                           2666 ;	C:\Workspace\WSN_MS-4\/cc1000.h:203: if (address & 0x80)
   06E7 EF            [12] 2667 	mov	a,r7
   06E8 30 E7 04      [24] 2668 	jnb	acc.7,00102$
                     0624  2669 	C$cc1000.h$204$2$86 ==.
                           2670 ;	C:\Workspace\WSN_MS-4\/cc1000.h:204: PDATA = 1; 
   06EB D2 84         [12] 2671 	setb	_PDATA
   06ED 80 02         [24] 2672 	sjmp	00103$
   06EF                    2673 00102$:
                     0628  2674 	C$cc1000.h$206$2$86 ==.
                           2675 ;	C:\Workspace\WSN_MS-4\/cc1000.h:206: PDATA = 0; 
   06EF C2 84         [12] 2676 	clr	_PDATA
   06F1                    2677 00103$:
                     062A  2678 	C$cc1000.h$207$2$86 ==.
                           2679 ;	C:\Workspace\WSN_MS-4\/cc1000.h:207: address = address<<1;   // Nast�pny bit
   06F1 EF            [12] 2680 	mov	a,r7
   06F2 2F            [12] 2681 	add	a,r7
   06F3 FF            [12] 2682 	mov	r7,a
                     062D  2683 	C$cc1000.h$208$2$86 ==.
                           2684 ;	C:\Workspace\WSN_MS-4\/cc1000.h:208: PCLK = 0;
   06F4 C2 85         [12] 2685 	clr	_PCLK
                     062F  2686 	C$cc1000.h$209$2$86 ==.
                           2687 ;	C:\Workspace\WSN_MS-4\/cc1000.h:209: for (i=0; i<0x0F; i++); // Op�nienie
   06F6 7C 0F         [12] 2688 	mov	r4,#0x0F
   06F8                    2689 00114$:
   06F8 8C 03         [24] 2690 	mov	ar3,r4
   06FA EB            [12] 2691 	mov	a,r3
   06FB 14            [12] 2692 	dec	a
   06FC FC            [12] 2693 	mov	r4,a
   06FD 70 F9         [24] 2694 	jnz	00114$
                     0638  2695 	C$cc1000.h$200$1$85 ==.
                           2696 ;	C:\Workspace\WSN_MS-4\/cc1000.h:200: for (BitCnt = 0; BitCnt < 8; BitCnt++)	// Wysy�anie adresu po jednym bicie (szeregowo)
   06FF 0D            [12] 2697 	inc	r5
   0700 BD 08 00      [24] 2698 	cjne	r5,#0x08,00167$
   0703                    2699 00167$:
   0703 40 E0         [24] 2700 	jc	00115$
                     063E  2701 	C$cc1000.h$211$1$85 ==.
                           2702 ;	C:\Workspace\WSN_MS-4\/cc1000.h:211: PCLK = 1;
   0705 D2 85         [12] 2703 	setb	_PCLK
                     0640  2704 	C$cc1000.h$212$1$85 ==.
                           2705 ;	C:\Workspace\WSN_MS-4\/cc1000.h:212: for (i=0; i<0x0F; i++);     // Op�nienie
   0707 7C 0F         [12] 2706 	mov	r4,#0x0F
   0709                    2707 00119$:
   0709 8C 07         [24] 2708 	mov	ar7,r4
   070B EF            [12] 2709 	mov	a,r7
   070C 14            [12] 2710 	dec	a
   070D FC            [12] 2711 	mov	r4,a
   070E 70 F9         [24] 2712 	jnz	00119$
                     0649  2713 	C$cc1000.h$213$1$85 ==.
                           2714 ;	C:\Workspace\WSN_MS-4\/cc1000.h:213: PALE  = 1;
   0710 D2 87         [12] 2715 	setb	_PALE
                     064B  2716 	C$cc1000.h$214$1$85 ==.
                           2717 ;	C:\Workspace\WSN_MS-4\/cc1000.h:214: P0MDOUT &= 0xEF;            // Port P0.4 (PDATA) jako open-drain (wej�cie)
   0712 53 A4 EF      [24] 2718 	anl	_P0MDOUT,#0xEF
                     064E  2719 	C$cc1000.h$215$1$85 ==.
                           2720 ;	C:\Workspace\WSN_MS-4\/cc1000.h:215: PDATA = 1;                  // Wa�ne aby PDATA by�o wej�ciem
   0715 D2 84         [12] 2721 	setb	_PDATA
                     0650  2722 	C$cc1000.h$216$1$85 ==.
                           2723 ;	C:\Workspace\WSN_MS-4\/cc1000.h:216: for (BitCnt = 0; BitCnt < 8; BitCnt++)	
   0717 7F 00         [12] 2724 	mov	r7,#0x00
   0719                    2725 00123$:
                     0652  2726 	C$cc1000.h$218$2$87 ==.
                           2727 ;	C:\Workspace\WSN_MS-4\/cc1000.h:218: PCLK = 0;
   0719 C2 85         [12] 2728 	clr	_PCLK
                     0654  2729 	C$cc1000.h$219$2$87 ==.
                           2730 ;	C:\Workspace\WSN_MS-4\/cc1000.h:219: Value = Value<<1;
   071B EE            [12] 2731 	mov	a,r6
   071C 2E            [12] 2732 	add	a,r6
   071D FE            [12] 2733 	mov	r6,a
                     0657  2734 	C$cc1000.h$220$2$87 ==.
                           2735 ;	C:\Workspace\WSN_MS-4\/cc1000.h:220: if (PDATA) 
   071E 30 84 05      [24] 2736 	jnb	_PDATA,00108$
                     065A  2737 	C$cc1000.h$221$2$87 ==.
                           2738 ;	C:\Workspace\WSN_MS-4\/cc1000.h:221: Value |= 0x01;  
   0721 43 06 01      [24] 2739 	orl	ar6,#0x01
   0724 80 03         [24] 2740 	sjmp	00109$
   0726                    2741 00108$:
                     065F  2742 	C$cc1000.h$223$2$87 ==.
                           2743 ;	C:\Workspace\WSN_MS-4\/cc1000.h:223: Value &= 0xFE;  
   0726 53 06 FE      [24] 2744 	anl	ar6,#0xFE
   0729                    2745 00109$:
                     0662  2746 	C$cc1000.h$224$2$87 ==.
                           2747 ;	C:\Workspace\WSN_MS-4\/cc1000.h:224: PCLK = 1;
   0729 D2 85         [12] 2748 	setb	_PCLK
                     0664  2749 	C$cc1000.h$225$2$87 ==.
                           2750 ;	C:\Workspace\WSN_MS-4\/cc1000.h:225: for (i=0; i<0x0F; i++); // Op�nienie
   072B 7C 0F         [12] 2751 	mov	r4,#0x0F
   072D                    2752 00122$:
   072D 8C 05         [24] 2753 	mov	ar5,r4
   072F ED            [12] 2754 	mov	a,r5
   0730 14            [12] 2755 	dec	a
   0731 FC            [12] 2756 	mov	r4,a
   0732 70 F9         [24] 2757 	jnz	00122$
                     066D  2758 	C$cc1000.h$216$1$85 ==.
                           2759 ;	C:\Workspace\WSN_MS-4\/cc1000.h:216: for (BitCnt = 0; BitCnt < 8; BitCnt++)	
   0734 0F            [12] 2760 	inc	r7
   0735 BF 08 00      [24] 2761 	cjne	r7,#0x08,00172$
   0738                    2762 00172$:
   0738 40 DF         [24] 2763 	jc	00123$
                     0673  2764 	C$cc1000.h$227$1$85 ==.
                           2765 ;	C:\Workspace\WSN_MS-4\/cc1000.h:227: PDATA = 1;
   073A D2 84         [12] 2766 	setb	_PDATA
                     0675  2767 	C$cc1000.h$228$1$85 ==.
                           2768 ;	C:\Workspace\WSN_MS-4\/cc1000.h:228: return Value;
   073C 8E 82         [24] 2769 	mov	dpl,r6
                     0677  2770 	C$cc1000.h$229$1$85 ==.
                     0677  2771 	XG$CC1000_ReadFromRegister$0$0 ==.
   073E 22            [24] 2772 	ret
                           2773 ;------------------------------------------------------------
                           2774 ;Allocation info for local variables in function 'BitReceive'
                           2775 ;------------------------------------------------------------
                     0678  2776 	G$BitReceive$0$0 ==.
                     0678  2777 	C$cc1000.h$239$1$85 ==.
                           2778 ;	C:\Workspace\WSN_MS-4\/cc1000.h:239: U8 BitReceive(void) // Odczyt przy zboczu narastaj�cym 
                           2779 ;	-----------------------------------------
                           2780 ;	 function BitReceive
                           2781 ;	-----------------------------------------
   073F                    2782 _BitReceive:
                     0678  2783 	C$cc1000.h$241$1$89 ==.
                           2784 ;	C:\Workspace\WSN_MS-4\/cc1000.h:241: while(DCLK);    // Czekaj a� DCLK zmieni stan z "1" -> "0"
   073F                    2785 00101$:
   073F 20 82 FD      [24] 2786 	jb	_DCLK,00101$
                     067B  2787 	C$cc1000.h$242$1$89 ==.
                           2788 ;	C:\Workspace\WSN_MS-4\/cc1000.h:242: while(!DCLK);   // Czekaj a� DCLK zmieni stan z "0" -> "1" 
   0742                    2789 00104$:
   0742 30 82 FD      [24] 2790 	jnb	_DCLK,00104$
                     067E  2791 	C$cc1000.h$244$1$89 ==.
                           2792 ;	C:\Workspace\WSN_MS-4\/cc1000.h:244: return DIO;     // Odczyt przy zboczu narastaj�cym
   0745 A2 83         [12] 2793 	mov	c,_DIO
   0747 E4            [12] 2794 	clr	a
   0748 33            [12] 2795 	rlc	a
   0749 F5 82         [12] 2796 	mov	dpl,a
                     0684  2797 	C$cc1000.h$245$1$89 ==.
                     0684  2798 	XG$BitReceive$0$0 ==.
   074B 22            [24] 2799 	ret
                           2800 ;------------------------------------------------------------
                           2801 ;Allocation info for local variables in function 'CC1000_ReceiveByte'
                           2802 ;------------------------------------------------------------
                           2803 ;i                         Allocated to registers r6 
                           2804 ;DATA                      Allocated to registers r7 
                           2805 ;------------------------------------------------------------
                     0685  2806 	G$CC1000_ReceiveByte$0$0 ==.
                     0685  2807 	C$cc1000.h$248$1$89 ==.
                           2808 ;	C:\Workspace\WSN_MS-4\/cc1000.h:248: U8 CC1000_ReceiveByte(void) 
                           2809 ;	-----------------------------------------
                           2810 ;	 function CC1000_ReceiveByte
                           2811 ;	-----------------------------------------
   074C                    2812 _CC1000_ReceiveByte:
                     0685  2813 	C$cc1000.h$250$1$89 ==.
                           2814 ;	C:\Workspace\WSN_MS-4\/cc1000.h:250: U8 i, DATA = 0;
   074C 7F 00         [12] 2815 	mov	r7,#0x00
                     0687  2816 	C$cc1000.h$251$1$91 ==.
                           2817 ;	C:\Workspace\WSN_MS-4\/cc1000.h:251: for(i=0; i<8; i++) {
   074E 7E 08         [12] 2818 	mov	r6,#0x08
   0750                    2819 00106$:
                     0689  2820 	C$cc1000.h$252$2$92 ==.
                           2821 ;	C:\Workspace\WSN_MS-4\/cc1000.h:252: DATA >>= 1;
   0750 EF            [12] 2822 	mov	a,r7
   0751 C3            [12] 2823 	clr	c
   0752 13            [12] 2824 	rrc	a
   0753 FF            [12] 2825 	mov	r7,a
                     068D  2826 	C$cc1000.h$253$2$92 ==.
                           2827 ;	C:\Workspace\WSN_MS-4\/cc1000.h:253: if(!BitReceive()) DATA |= 0x80;     // Odbi�r bit�w z negacj�
   0754 C0 07         [24] 2828 	push	ar7
   0756 C0 06         [24] 2829 	push	ar6
   0758 12 07 3F      [24] 2830 	lcall	_BitReceive
   075B E5 82         [12] 2831 	mov	a,dpl
   075D D0 06         [24] 2832 	pop	ar6
   075F D0 07         [24] 2833 	pop	ar7
   0761 70 03         [24] 2834 	jnz	00102$
   0763 43 07 80      [24] 2835 	orl	ar7,#0x80
   0766                    2836 00102$:
   0766 8E 05         [24] 2837 	mov	ar5,r6
   0768 ED            [12] 2838 	mov	a,r5
   0769 14            [12] 2839 	dec	a
                     06A3  2840 	C$cc1000.h$251$2$92 ==.
                           2841 ;	C:\Workspace\WSN_MS-4\/cc1000.h:251: for(i=0; i<8; i++) {
   076A FE            [12] 2842 	mov	r6,a
   076B 70 E3         [24] 2843 	jnz	00106$
                     06A6  2844 	C$cc1000.h$256$1$91 ==.
                           2845 ;	C:\Workspace\WSN_MS-4\/cc1000.h:256: return DATA;
   076D 8F 82         [24] 2846 	mov	dpl,r7
                     06A8  2847 	C$cc1000.h$257$1$91 ==.
                     06A8  2848 	XG$CC1000_ReceiveByte$0$0 ==.
   076F 22            [24] 2849 	ret
                           2850 ;------------------------------------------------------------
                           2851 ;Allocation info for local variables in function 'BitSend'
                           2852 ;------------------------------------------------------------
                           2853 ;bit0                      Allocated to registers r7 
                           2854 ;------------------------------------------------------------
                     06A9  2855 	G$BitSend$0$0 ==.
                     06A9  2856 	C$cc1000.h$260$1$91 ==.
                           2857 ;	C:\Workspace\WSN_MS-4\/cc1000.h:260: void BitSend(U8 bit0)   // Zapis przy zboczu opadaj�cym 
                           2858 ;	-----------------------------------------
                           2859 ;	 function BitSend
                           2860 ;	-----------------------------------------
   0770                    2861 _BitSend:
   0770 AF 82         [24] 2862 	mov	r7,dpl
                     06AB  2863 	C$cc1000.h$262$1$94 ==.
                           2864 ;	C:\Workspace\WSN_MS-4\/cc1000.h:262: while(!DCLK);   // Czekaj a� DCLK zmieni stan z "0" -> "1"
   0772                    2865 00101$:
   0772 30 82 FD      [24] 2866 	jnb	_DCLK,00101$
                     06AE  2867 	C$cc1000.h$263$1$94 ==.
                           2868 ;	C:\Workspace\WSN_MS-4\/cc1000.h:263: while(DCLK);    // Czekaj a� DCLK zmieni stan z "1" -> "0"
   0775                    2869 00104$:
   0775 20 82 FD      [24] 2870 	jb	_DCLK,00104$
                     06B1  2871 	C$cc1000.h$264$1$94 ==.
                           2872 ;	C:\Workspace\WSN_MS-4\/cc1000.h:264: if(bit0) DIO = 1; else DIO = 0; // Zapis przy zboczu opadaj�cym
   0778 EF            [12] 2873 	mov	a,r7
   0779 60 04         [24] 2874 	jz	00108$
   077B D2 83         [12] 2875 	setb	_DIO
   077D 80 02         [24] 2876 	sjmp	00110$
   077F                    2877 00108$:
   077F C2 83         [12] 2878 	clr	_DIO
   0781                    2879 00110$:
                     06BA  2880 	C$cc1000.h$265$1$94 ==.
                     06BA  2881 	XG$BitSend$0$0 ==.
   0781 22            [24] 2882 	ret
                           2883 ;------------------------------------------------------------
                           2884 ;Allocation info for local variables in function 'CC1000_SendByte'
                           2885 ;------------------------------------------------------------
                           2886 ;byte                      Allocated to registers r7 
                           2887 ;i                         Allocated to registers r6 
                           2888 ;------------------------------------------------------------
                     06BB  2889 	G$CC1000_SendByte$0$0 ==.
                     06BB  2890 	C$cc1000.h$268$1$94 ==.
                           2891 ;	C:\Workspace\WSN_MS-4\/cc1000.h:268: void CC1000_SendByte(U8 byte) 
                           2892 ;	-----------------------------------------
                           2893 ;	 function CC1000_SendByte
                           2894 ;	-----------------------------------------
   0782                    2895 _CC1000_SendByte:
   0782 AF 82         [24] 2896 	mov	r7,dpl
                     06BD  2897 	C$cc1000.h$271$1$96 ==.
                           2898 ;	C:\Workspace\WSN_MS-4\/cc1000.h:271: for(i=0; i<8; i++) {
   0784 7E 00         [12] 2899 	mov	r6,#0x00
   0786                    2900 00102$:
                     06BF  2901 	C$cc1000.h$272$2$97 ==.
                           2902 ;	C:\Workspace\WSN_MS-4\/cc1000.h:272: BitSend(byte & 0x01);
   0786 74 01         [12] 2903 	mov	a,#0x01
   0788 5F            [12] 2904 	anl	a,r7
   0789 F5 82         [12] 2905 	mov	dpl,a
   078B C0 07         [24] 2906 	push	ar7
   078D C0 06         [24] 2907 	push	ar6
   078F 12 07 70      [24] 2908 	lcall	_BitSend
   0792 D0 06         [24] 2909 	pop	ar6
   0794 D0 07         [24] 2910 	pop	ar7
                     06CF  2911 	C$cc1000.h$273$2$97 ==.
                           2912 ;	C:\Workspace\WSN_MS-4\/cc1000.h:273: byte >>= 1;
   0796 EF            [12] 2913 	mov	a,r7
   0797 C3            [12] 2914 	clr	c
   0798 13            [12] 2915 	rrc	a
   0799 FF            [12] 2916 	mov	r7,a
                     06D3  2917 	C$cc1000.h$271$1$96 ==.
                           2918 ;	C:\Workspace\WSN_MS-4\/cc1000.h:271: for(i=0; i<8; i++) {
   079A 0E            [12] 2919 	inc	r6
   079B BE 08 00      [24] 2920 	cjne	r6,#0x08,00110$
   079E                    2921 00110$:
   079E 40 E6         [24] 2922 	jc	00102$
                     06D9  2923 	C$cc1000.h$275$1$96 ==.
                     06D9  2924 	XG$CC1000_SendByte$0$0 ==.
   07A0 22            [24] 2925 	ret
                           2926 ;------------------------------------------------------------
                           2927 ;Allocation info for local variables in function 'CC1000_Reset'
                           2928 ;------------------------------------------------------------
                           2929 ;MainValue                 Allocated to registers r7 
                           2930 ;------------------------------------------------------------
                     06DA  2931 	G$CC1000_Reset$0$0 ==.
                     06DA  2932 	C$cc1000.h$284$1$96 ==.
                           2933 ;	C:\Workspace\WSN_MS-4\/cc1000.h:284: void CC1000_Reset(void)
                           2934 ;	-----------------------------------------
                           2935 ;	 function CC1000_Reset
                           2936 ;	-----------------------------------------
   07A1                    2937 _CC1000_Reset:
                     06DA  2938 	C$cc1000.h$288$1$99 ==.
                           2939 ;	C:\Workspace\WSN_MS-4\/cc1000.h:288: MainValue = CC1000_ReadFromRegister(CC1000_MAIN);
   07A1 75 82 00      [24] 2940 	mov	dpl,#0x00
   07A4 12 06 CF      [24] 2941 	lcall	_CC1000_ReadFromRegister
   07A7 AF 82         [24] 2942 	mov	r7,dpl
                     06E2  2943 	C$cc1000.h$289$1$99 ==.
                           2944 ;	C:\Workspace\WSN_MS-4\/cc1000.h:289: CC1000_WriteToRegister(CC1000_MAIN,MainValue & 0xFE);   
   07A9 74 FE         [12] 2945 	mov	a,#0xFE
   07AB 5F            [12] 2946 	anl	a,r7
   07AC F5 09         [12] 2947 	mov	_CC1000_WriteToRegister_PARM_2,a
   07AE 75 82 00      [24] 2948 	mov	dpl,#0x00
   07B1 C0 07         [24] 2949 	push	ar7
   07B3 12 06 63      [24] 2950 	lcall	_CC1000_WriteToRegister
   07B6 D0 07         [24] 2951 	pop	ar7
                     06F1  2952 	C$cc1000.h$290$1$99 ==.
                           2953 ;	C:\Workspace\WSN_MS-4\/cc1000.h:290: CC1000_WriteToRegister(CC1000_MAIN,MainValue | 0x01);  
   07B8 74 01         [12] 2954 	mov	a,#0x01
   07BA 4F            [12] 2955 	orl	a,r7
   07BB F5 09         [12] 2956 	mov	_CC1000_WriteToRegister_PARM_2,a
   07BD 75 82 00      [24] 2957 	mov	dpl,#0x00
   07C0 12 06 63      [24] 2958 	lcall	_CC1000_WriteToRegister
                     06FC  2959 	C$cc1000.h$291$1$99 ==.
                     06FC  2960 	XG$CC1000_Reset$0$0 ==.
   07C3 22            [24] 2961 	ret
                           2962 ;------------------------------------------------------------
                           2963 ;Allocation info for local variables in function 'CC1000_Calibrate'
                           2964 ;------------------------------------------------------------
                           2965 ;TimeOutCounter            Allocated to registers r6 r7 
                           2966 ;------------------------------------------------------------
                     06FD  2967 	G$CC1000_Calibrate$0$0 ==.
                     06FD  2968 	C$cc1000.h$296$1$99 ==.
                           2969 ;	C:\Workspace\WSN_MS-4\/cc1000.h:296: U8 CC1000_Calibrate(void)   
                           2970 ;	-----------------------------------------
                           2971 ;	 function CC1000_Calibrate
                           2972 ;	-----------------------------------------
   07C4                    2973 _CC1000_Calibrate:
                     06FD  2974 	C$cc1000.h$299$1$101 ==.
                           2975 ;	C:\Workspace\WSN_MS-4\/cc1000.h:299: printf("\nKalibracja...");
   07C4 74 AD         [12] 2976 	mov	a,#__str_0
   07C6 C0 E0         [24] 2977 	push	acc
   07C8 74 18         [12] 2978 	mov	a,#(__str_0 >> 8)
   07CA C0 E0         [24] 2979 	push	acc
   07CC 74 80         [12] 2980 	mov	a,#0x80
   07CE C0 E0         [24] 2981 	push	acc
   07D0 12 03 EA      [24] 2982 	lcall	_printf
   07D3 15 81         [12] 2983 	dec	sp
   07D5 15 81         [12] 2984 	dec	sp
   07D7 15 81         [12] 2985 	dec	sp
                     0712  2986 	C$cc1000.h$301$1$101 ==.
                           2987 ;	C:\Workspace\WSN_MS-4\/cc1000.h:301: CC1000_WriteToRegister(CC1000_PA_POW,0x00);
   07D9 75 09 00      [24] 2988 	mov	_CC1000_WriteToRegister_PARM_2,#0x00
   07DC 75 82 0B      [24] 2989 	mov	dpl,#0x0B
   07DF 12 06 63      [24] 2990 	lcall	_CC1000_WriteToRegister
                     071B  2991 	C$cc1000.h$302$1$101 ==.
                           2992 ;	C:\Workspace\WSN_MS-4\/cc1000.h:302: CC1000_WriteToRegister(CC1000_CAL,0xA6);    // Start kalibracji                                    
   07E2 75 09 A6      [24] 2993 	mov	_CC1000_WriteToRegister_PARM_2,#0xA6
   07E5 75 82 0E      [24] 2994 	mov	dpl,#0x0E
   07E8 12 06 63      [24] 2995 	lcall	_CC1000_WriteToRegister
                     0724  2996 	C$cc1000.h$303$1$101 ==.
                           2997 ;	C:\Workspace\WSN_MS-4\/cc1000.h:303: Delay_x100us(200);
   07EB 90 00 C8      [24] 2998 	mov	dptr,#0x00C8
   07EE 12 01 7A      [24] 2999 	lcall	_Delay_x100us
                     072A  3000 	C$cc1000.h$305$1$101 ==.
                           3001 ;	C:\Workspace\WSN_MS-4\/cc1000.h:305: for (TimeOutCounter=CC1000_CAL_TIMEOUT; ((CC1000_ReadFromRegister(CC1000_CAL)&0x08)==0)&&(TimeOutCounter>0); TimeOutCounter--);        
   07F1 7E FE         [12] 3002 	mov	r6,#0xFE
   07F3 7F 7F         [12] 3003 	mov	r7,#0x7F
   07F5                    3004 00105$:
   07F5 75 82 0E      [24] 3005 	mov	dpl,#0x0E
   07F8 C0 07         [24] 3006 	push	ar7
   07FA C0 06         [24] 3007 	push	ar6
   07FC 12 06 CF      [24] 3008 	lcall	_CC1000_ReadFromRegister
   07FF E5 82         [12] 3009 	mov	a,dpl
   0801 D0 06         [24] 3010 	pop	ar6
   0803 D0 07         [24] 3011 	pop	ar7
   0805 54 08         [12] 3012 	anl	a,#0x08
   0807 FD            [12] 3013 	mov	r5,a
   0808 60 02         [24] 3014 	jz	00136$
   080A 80 0B         [24] 3015 	sjmp	00122$
   080C                    3016 00136$:
   080C EE            [12] 3017 	mov	a,r6
   080D 4F            [12] 3018 	orl	a,r7
   080E 60 07         [24] 3019 	jz	00122$
   0810 1E            [12] 3020 	dec	r6
   0811 BE FF 01      [24] 3021 	cjne	r6,#0xFF,00138$
   0814 1F            [12] 3022 	dec	r7
   0815                    3023 00138$:
                     074E  3024 	C$cc1000.h$307$1$101 ==.
                           3025 ;	C:\Workspace\WSN_MS-4\/cc1000.h:307: for (TimeOutCounter=CC1000_LOCK_TIMEOUT; ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0)&&(TimeOutCounter>0); TimeOutCounter--);
   0815 80 DE         [24] 3026 	sjmp	00105$
   0817                    3027 00122$:
   0817 7E FE         [12] 3028 	mov	r6,#0xFE
   0819 7F 7F         [12] 3029 	mov	r7,#0x7F
   081B                    3030 00109$:
   081B 75 82 0D      [24] 3031 	mov	dpl,#0x0D
   081E C0 07         [24] 3032 	push	ar7
   0820 C0 06         [24] 3033 	push	ar6
   0822 12 06 CF      [24] 3034 	lcall	_CC1000_ReadFromRegister
   0825 E5 82         [12] 3035 	mov	a,dpl
   0827 D0 06         [24] 3036 	pop	ar6
   0829 D0 07         [24] 3037 	pop	ar7
   082B 54 01         [12] 3038 	anl	a,#0x01
   082D FD            [12] 3039 	mov	r5,a
   082E 60 02         [24] 3040 	jz	00140$
   0830 80 0B         [24] 3041 	sjmp	00102$
   0832                    3042 00140$:
   0832 EE            [12] 3043 	mov	a,r6
   0833 4F            [12] 3044 	orl	a,r7
   0834 60 07         [24] 3045 	jz	00102$
   0836 1E            [12] 3046 	dec	r6
   0837 BE FF 01      [24] 3047 	cjne	r6,#0xFF,00142$
   083A 1F            [12] 3048 	dec	r7
   083B                    3049 00142$:
   083B 80 DE         [24] 3050 	sjmp	00109$
   083D                    3051 00102$:
                     0776  3052 	C$cc1000.h$308$1$101 ==.
                           3053 ;	C:\Workspace\WSN_MS-4\/cc1000.h:308: CC1000_WriteToRegister(CC1000_CAL, 0x26);     // Zako�czenie kalibracji
   083D 75 09 26      [24] 3054 	mov	_CC1000_WriteToRegister_PARM_2,#0x26
   0840 75 82 0E      [24] 3055 	mov	dpl,#0x0E
   0843 12 06 63      [24] 3056 	lcall	_CC1000_WriteToRegister
                     077F  3057 	C$cc1000.h$309$1$101 ==.
                           3058 ;	C:\Workspace\WSN_MS-4\/cc1000.h:309: CC1000_WriteToRegister(CC1000_PA_POW, CC1000_PA_VALUE);    // W��czenie PA
   0846 75 09 F0      [24] 3059 	mov	_CC1000_WriteToRegister_PARM_2,#0xF0
   0849 75 82 0B      [24] 3060 	mov	dpl,#0x0B
   084C 12 06 63      [24] 3061 	lcall	_CC1000_WriteToRegister
                     0788  3062 	C$cc1000.h$310$1$101 ==.
                           3063 ;	C:\Workspace\WSN_MS-4\/cc1000.h:310: return ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01) == 1);
   084F 75 82 0D      [24] 3064 	mov	dpl,#0x0D
   0852 12 06 CF      [24] 3065 	lcall	_CC1000_ReadFromRegister
   0855 E5 82         [12] 3066 	mov	a,dpl
   0857 54 01         [12] 3067 	anl	a,#0x01
   0859 FF            [12] 3068 	mov	r7,a
   085A E4            [12] 3069 	clr	a
   085B BF 01 01      [24] 3070 	cjne	r7,#0x01,00143$
   085E 04            [12] 3071 	inc	a
   085F                    3072 00143$:
   085F F5 82         [12] 3073 	mov	dpl,a
                     079A  3074 	C$cc1000.h$311$1$101 ==.
                     079A  3075 	XG$CC1000_Calibrate$0$0 ==.
   0861 22            [24] 3076 	ret
                           3077 ;------------------------------------------------------------
                           3078 ;Allocation info for local variables in function 'CC1000_SetupRX'
                           3079 ;------------------------------------------------------------
                           3080 ;RXCurrent                 Allocated to registers r7 
                           3081 ;i                         Allocated to registers r6 r7 
                           3082 ;lock_status               Allocated to registers r7 
                           3083 ;------------------------------------------------------------
                     079B  3084 	G$CC1000_SetupRX$0$0 ==.
                     079B  3085 	C$cc1000.h$316$1$101 ==.
                           3086 ;	C:\Workspace\WSN_MS-4\/cc1000.h:316: U8 CC1000_SetupRX(U8 RXCurrent) 
                           3087 ;	-----------------------------------------
                           3088 ;	 function CC1000_SetupRX
                           3089 ;	-----------------------------------------
   0862                    3090 _CC1000_SetupRX:
   0862 AF 82         [24] 3091 	mov	r7,dpl
                     079D  3092 	C$cc1000.h$321$1$103 ==.
                           3093 ;	C:\Workspace\WSN_MS-4\/cc1000.h:321: CC1000_WriteToRegister(CC1000_MAIN,0x11);           // Prze��cz. do RX, cz�stotl. rej. A
   0864 75 09 11      [24] 3094 	mov	_CC1000_WriteToRegister_PARM_2,#0x11
   0867 75 82 00      [24] 3095 	mov	dpl,#0x00
   086A C0 07         [24] 3096 	push	ar7
   086C 12 06 63      [24] 3097 	lcall	_CC1000_WriteToRegister
                     07A8  3098 	C$cc1000.h$322$1$103 ==.
                           3099 ;	C:\Workspace\WSN_MS-4\/cc1000.h:322: CC1000_WriteToRegister(CC1000_PLL, CC1000_RX_PLL); 
   086F 75 09 58      [24] 3100 	mov	_CC1000_WriteToRegister_PARM_2,#0x58
   0872 75 82 0C      [24] 3101 	mov	dpl,#0x0C
   0875 12 06 63      [24] 3102 	lcall	_CC1000_WriteToRegister
   0878 D0 07         [24] 3103 	pop	ar7
                     07B3  3104 	C$cc1000.h$323$1$103 ==.
                           3105 ;	C:\Workspace\WSN_MS-4\/cc1000.h:323: CC1000_WriteToRegister(CC1000_CURRENT, RXCurrent); 
   087A 8F 09         [24] 3106 	mov	_CC1000_WriteToRegister_PARM_2,r7
   087C 75 82 09      [24] 3107 	mov	dpl,#0x09
   087F 12 06 63      [24] 3108 	lcall	_CC1000_WriteToRegister
                     07BB  3109 	C$cc1000.h$324$1$103 ==.
                           3110 ;	C:\Workspace\WSN_MS-4\/cc1000.h:324: Delay_x100us(3);                                    // Oczekiwanie 300us 
   0882 90 00 03      [24] 3111 	mov	dptr,#0x0003
   0885 12 01 7A      [24] 3112 	lcall	_Delay_x100us
                     07C1  3113 	C$cc1000.h$326$1$103 ==.
                           3114 ;	C:\Workspace\WSN_MS-4\/cc1000.h:326: for (i=CC1000_LOCK_TIMEOUT; ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0)&&(i>0); i--);
   0888 7E FE         [12] 3115 	mov	r6,#0xFE
   088A 7F 7F         [12] 3116 	mov	r7,#0x7F
   088C                    3117 00110$:
   088C 75 82 0D      [24] 3118 	mov	dpl,#0x0D
   088F C0 07         [24] 3119 	push	ar7
   0891 C0 06         [24] 3120 	push	ar6
   0893 12 06 CF      [24] 3121 	lcall	_CC1000_ReadFromRegister
   0896 E5 82         [12] 3122 	mov	a,dpl
   0898 D0 06         [24] 3123 	pop	ar6
   089A D0 07         [24] 3124 	pop	ar7
   089C 54 01         [12] 3125 	anl	a,#0x01
   089E FD            [12] 3126 	mov	r5,a
   089F 70 0B         [24] 3127 	jnz	00101$
   08A1 EE            [12] 3128 	mov	a,r6
   08A2 4F            [12] 3129 	orl	a,r7
   08A3 60 07         [24] 3130 	jz	00101$
   08A5 1E            [12] 3131 	dec	r6
   08A6 BE FF 01      [24] 3132 	cjne	r6,#0xFF,00134$
   08A9 1F            [12] 3133 	dec	r7
   08AA                    3134 00134$:
   08AA 80 E0         [24] 3135 	sjmp	00110$
   08AC                    3136 00101$:
                     07E5  3137 	C$cc1000.h$327$1$103 ==.
                           3138 ;	C:\Workspace\WSN_MS-4\/cc1000.h:327: if ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0x01) 
   08AC 75 82 0D      [24] 3139 	mov	dpl,#0x0D
   08AF 12 06 CF      [24] 3140 	lcall	_CC1000_ReadFromRegister
   08B2 E5 82         [12] 3141 	mov	a,dpl
   08B4 54 01         [12] 3142 	anl	a,#0x01
   08B6 FF            [12] 3143 	mov	r7,a
   08B7 BF 01 04      [24] 3144 	cjne	r7,#0x01,00106$
                     07F3  3145 	C$cc1000.h$328$1$103 ==.
                           3146 ;	C:\Workspace\WSN_MS-4\/cc1000.h:328: lock_status = CC1000_LOCK_OK;  // PLL zamkni�ta
   08BA 7F 01         [12] 3147 	mov	r7,#0x01
   08BC 80 10         [24] 3148 	sjmp	00107$
   08BE                    3149 00106$:
                     07F7  3150 	C$cc1000.h$331$2$104 ==.
                           3151 ;	C:\Workspace\WSN_MS-4\/cc1000.h:331: if (CC1000_Calibrate()) // Je�li kalibracja z sukcesem
   08BE 12 07 C4      [24] 3152 	lcall	_CC1000_Calibrate
   08C1 E5 82         [12] 3153 	mov	a,dpl
   08C3 60 04         [24] 3154 	jz	00103$
                     07FE  3155 	C$cc1000.h$332$2$104 ==.
                           3156 ;	C:\Workspace\WSN_MS-4\/cc1000.h:332: lock_status = CC1000_LOCK_RECAL_OK;  
   08C5 7F 02         [12] 3157 	mov	r7,#0x02
   08C7 80 05         [24] 3158 	sjmp	00107$
   08C9                    3159 00103$:
                     0802  3160 	C$cc1000.h$335$3$105 ==.
                           3161 ;	C:\Workspace\WSN_MS-4\/cc1000.h:335: CC1000_ResetFreqSynth();
   08C9 12 0A 1C      [24] 3162 	lcall	_CC1000_ResetFreqSynth
                     0805  3163 	C$cc1000.h$336$3$105 ==.
                           3164 ;	C:\Workspace\WSN_MS-4\/cc1000.h:336: lock_status = CC1000_LOCK_NOK; 
   08CC 7F 00         [12] 3165 	mov	r7,#0x00
   08CE                    3166 00107$:
                     0807  3167 	C$cc1000.h$339$1$103 ==.
                           3168 ;	C:\Workspace\WSN_MS-4\/cc1000.h:339: return (lock_status);  // zwrot statusu syntezatora
   08CE 8F 82         [24] 3169 	mov	dpl,r7
                     0809  3170 	C$cc1000.h$340$1$103 ==.
                     0809  3171 	XG$CC1000_SetupRX$0$0 ==.
   08D0 22            [24] 3172 	ret
                           3173 ;------------------------------------------------------------
                           3174 ;Allocation info for local variables in function 'CC1000_SetupTX'
                           3175 ;------------------------------------------------------------
                           3176 ;TXCurrent                 Allocated to registers r7 
                           3177 ;i                         Allocated to registers r6 r7 
                           3178 ;lock_status               Allocated to registers r7 
                           3179 ;------------------------------------------------------------
                     080A  3180 	G$CC1000_SetupTX$0$0 ==.
                     080A  3181 	C$cc1000.h$345$1$103 ==.
                           3182 ;	C:\Workspace\WSN_MS-4\/cc1000.h:345: U8 CC1000_SetupTX(U8 TXCurrent) 
                           3183 ;	-----------------------------------------
                           3184 ;	 function CC1000_SetupTX
                           3185 ;	-----------------------------------------
   08D1                    3186 _CC1000_SetupTX:
   08D1 AF 82         [24] 3187 	mov	r7,dpl
                     080C  3188 	C$cc1000.h$350$1$107 ==.
                           3189 ;	C:\Workspace\WSN_MS-4\/cc1000.h:350: CC1000_WriteToRegister(CC1000_PA_POW,0x00); // Wy��czenie PA 
   08D3 75 09 00      [24] 3190 	mov	_CC1000_WriteToRegister_PARM_2,#0x00
   08D6 75 82 0B      [24] 3191 	mov	dpl,#0x0B
   08D9 C0 07         [24] 3192 	push	ar7
   08DB 12 06 63      [24] 3193 	lcall	_CC1000_WriteToRegister
                     0817  3194 	C$cc1000.h$351$1$107 ==.
                           3195 ;	C:\Workspace\WSN_MS-4\/cc1000.h:351: CC1000_WriteToRegister(CC1000_MAIN,0xE1);   // Prze�. do TX, do cz�stotl. rej. B
   08DE 75 09 E1      [24] 3196 	mov	_CC1000_WriteToRegister_PARM_2,#0xE1
   08E1 75 82 00      [24] 3197 	mov	dpl,#0x00
   08E4 12 06 63      [24] 3198 	lcall	_CC1000_WriteToRegister
                     0820  3199 	C$cc1000.h$352$1$107 ==.
                           3200 ;	C:\Workspace\WSN_MS-4\/cc1000.h:352: CC1000_WriteToRegister(CC1000_PLL, CC1000_TX_PLL);
   08E7 75 09 58      [24] 3201 	mov	_CC1000_WriteToRegister_PARM_2,#0x58
   08EA 75 82 0C      [24] 3202 	mov	dpl,#0x0C
   08ED 12 06 63      [24] 3203 	lcall	_CC1000_WriteToRegister
   08F0 D0 07         [24] 3204 	pop	ar7
                     082B  3205 	C$cc1000.h$353$1$107 ==.
                           3206 ;	C:\Workspace\WSN_MS-4\/cc1000.h:353: CC1000_WriteToRegister(CC1000_CURRENT, TXCurrent);
   08F2 8F 09         [24] 3207 	mov	_CC1000_WriteToRegister_PARM_2,r7
   08F4 75 82 09      [24] 3208 	mov	dpl,#0x09
   08F7 12 06 63      [24] 3209 	lcall	_CC1000_WriteToRegister
                     0833  3210 	C$cc1000.h$354$1$107 ==.
                           3211 ;	C:\Workspace\WSN_MS-4\/cc1000.h:354: Delay_x100us(3);                            // Czekaj 250us
   08FA 90 00 03      [24] 3212 	mov	dptr,#0x0003
   08FD 12 01 7A      [24] 3213 	lcall	_Delay_x100us
                     0839  3214 	C$cc1000.h$356$1$107 ==.
                           3215 ;	C:\Workspace\WSN_MS-4\/cc1000.h:356: for (i=CC1000_LOCK_TIMEOUT; ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0)&&(i>0); i--);
   0900 7E FE         [12] 3216 	mov	r6,#0xFE
   0902 7F 7F         [12] 3217 	mov	r7,#0x7F
   0904                    3218 00110$:
   0904 75 82 0D      [24] 3219 	mov	dpl,#0x0D
   0907 C0 07         [24] 3220 	push	ar7
   0909 C0 06         [24] 3221 	push	ar6
   090B 12 06 CF      [24] 3222 	lcall	_CC1000_ReadFromRegister
   090E E5 82         [12] 3223 	mov	a,dpl
   0910 D0 06         [24] 3224 	pop	ar6
   0912 D0 07         [24] 3225 	pop	ar7
   0914 54 01         [12] 3226 	anl	a,#0x01
   0916 FD            [12] 3227 	mov	r5,a
   0917 70 0B         [24] 3228 	jnz	00101$
   0919 EE            [12] 3229 	mov	a,r6
   091A 4F            [12] 3230 	orl	a,r7
   091B 60 07         [24] 3231 	jz	00101$
   091D 1E            [12] 3232 	dec	r6
   091E BE FF 01      [24] 3233 	cjne	r6,#0xFF,00134$
   0921 1F            [12] 3234 	dec	r7
   0922                    3235 00134$:
   0922 80 E0         [24] 3236 	sjmp	00110$
   0924                    3237 00101$:
                     085D  3238 	C$cc1000.h$357$1$107 ==.
                           3239 ;	C:\Workspace\WSN_MS-4\/cc1000.h:357: if ((CC1000_ReadFromRegister(CC1000_LOCK)&0x01)==0x01)
   0924 75 82 0D      [24] 3240 	mov	dpl,#0x0D
   0927 12 06 CF      [24] 3241 	lcall	_CC1000_ReadFromRegister
   092A E5 82         [12] 3242 	mov	a,dpl
   092C 54 01         [12] 3243 	anl	a,#0x01
   092E FF            [12] 3244 	mov	r7,a
   092F BF 01 04      [24] 3245 	cjne	r7,#0x01,00106$
                     086B  3246 	C$cc1000.h$358$1$107 ==.
                           3247 ;	C:\Workspace\WSN_MS-4\/cc1000.h:358: lock_status = CC1000_LOCK_OK; // PLL stabilna
   0932 7F 01         [12] 3248 	mov	r7,#0x01
   0934 80 10         [24] 3249 	sjmp	00107$
   0936                    3250 00106$:
                     086F  3251 	C$cc1000.h$361$2$108 ==.
                           3252 ;	C:\Workspace\WSN_MS-4\/cc1000.h:361: if (CC1000_Calibrate()) // Je�li kalibracja poprawna
   0936 12 07 C4      [24] 3253 	lcall	_CC1000_Calibrate
   0939 E5 82         [12] 3254 	mov	a,dpl
   093B 60 04         [24] 3255 	jz	00103$
                     0876  3256 	C$cc1000.h$362$2$108 ==.
                           3257 ;	C:\Workspace\WSN_MS-4\/cc1000.h:362: lock_status = CC1000_LOCK_RECAL_OK; 
   093D 7F 02         [12] 3258 	mov	r7,#0x02
   093F 80 05         [24] 3259 	sjmp	00107$
   0941                    3260 00103$:
                     087A  3261 	C$cc1000.h$365$3$109 ==.
                           3262 ;	C:\Workspace\WSN_MS-4\/cc1000.h:365: CC1000_ResetFreqSynth();
   0941 12 0A 1C      [24] 3263 	lcall	_CC1000_ResetFreqSynth
                     087D  3264 	C$cc1000.h$366$3$109 ==.
                           3265 ;	C:\Workspace\WSN_MS-4\/cc1000.h:366: lock_status = CC1000_LOCK_NOK;  
   0944 7F 00         [12] 3266 	mov	r7,#0x00
   0946                    3267 00107$:
                     087F  3268 	C$cc1000.h$370$1$107 ==.
                           3269 ;	C:\Workspace\WSN_MS-4\/cc1000.h:370: CC1000_WriteToRegister(CC1000_PA_POW, CC1000_PA_VALUE); // W��czenie PA
   0946 75 09 F0      [24] 3270 	mov	_CC1000_WriteToRegister_PARM_2,#0xF0
   0949 75 82 0B      [24] 3271 	mov	dpl,#0x0B
   094C C0 07         [24] 3272 	push	ar7
   094E 12 06 63      [24] 3273 	lcall	_CC1000_WriteToRegister
   0951 D0 07         [24] 3274 	pop	ar7
                     088C  3275 	C$cc1000.h$371$1$107 ==.
                           3276 ;	C:\Workspace\WSN_MS-4\/cc1000.h:371: return (lock_status);   // zwrot statusu syntezatora
   0953 8F 82         [24] 3277 	mov	dpl,r7
                     088E  3278 	C$cc1000.h$372$1$107 ==.
                     088E  3279 	XG$CC1000_SetupTX$0$0 ==.
   0955 22            [24] 3280 	ret
                           3281 ;------------------------------------------------------------
                           3282 ;Allocation info for local variables in function 'CC1000_SetupPD'
                           3283 ;------------------------------------------------------------
                     088F  3284 	G$CC1000_SetupPD$0$0 ==.
                     088F  3285 	C$cc1000.h$376$1$107 ==.
                           3286 ;	C:\Workspace\WSN_MS-4\/cc1000.h:376: void CC1000_SetupPD(void)
                           3287 ;	-----------------------------------------
                           3288 ;	 function CC1000_SetupPD
                           3289 ;	-----------------------------------------
   0956                    3290 _CC1000_SetupPD:
                     088F  3291 	C$cc1000.h$378$1$111 ==.
                           3292 ;	C:\Workspace\WSN_MS-4\/cc1000.h:378: CC1000_WriteToRegister(CC1000_MAIN,0x3F);    // CC1000 do power-down
   0956 75 09 3F      [24] 3293 	mov	_CC1000_WriteToRegister_PARM_2,#0x3F
   0959 75 82 00      [24] 3294 	mov	dpl,#0x00
   095C 12 06 63      [24] 3295 	lcall	_CC1000_WriteToRegister
                     0898  3296 	C$cc1000.h$379$1$111 ==.
                           3297 ;	C:\Workspace\WSN_MS-4\/cc1000.h:379: CC1000_WriteToRegister(CC1000_PA_POW,0x00);  // Wy��czenie PA dla ograniczenia pr�du
   095F 75 09 00      [24] 3298 	mov	_CC1000_WriteToRegister_PARM_2,#0x00
   0962 75 82 0B      [24] 3299 	mov	dpl,#0x0B
   0965 12 06 63      [24] 3300 	lcall	_CC1000_WriteToRegister
                     08A1  3301 	C$cc1000.h$380$1$111 ==.
                     08A1  3302 	XG$CC1000_SetupPD$0$0 ==.
   0968 22            [24] 3303 	ret
                           3304 ;------------------------------------------------------------
                           3305 ;Allocation info for local variables in function 'CC1000_WakeUpToRX'
                           3306 ;------------------------------------------------------------
                           3307 ;RXCurrent                 Allocated to registers r7 
                           3308 ;------------------------------------------------------------
                     08A2  3309 	G$CC1000_WakeUpToRX$0$0 ==.
                     08A2  3310 	C$cc1000.h$386$1$111 ==.
                           3311 ;	C:\Workspace\WSN_MS-4\/cc1000.h:386: void CC1000_WakeUpToRX(U8 RXCurrent)  
                           3312 ;	-----------------------------------------
                           3313 ;	 function CC1000_WakeUpToRX
                           3314 ;	-----------------------------------------
   0969                    3315 _CC1000_WakeUpToRX:
   0969 AF 82         [24] 3316 	mov	r7,dpl
                     08A4  3317 	C$cc1000.h$388$1$113 ==.
                           3318 ;	C:\Workspace\WSN_MS-4\/cc1000.h:388: CC1000_WriteToRegister(CC1000_MAIN,0x3B);    // W��czenie oscylatora xtal
   096B 75 09 3B      [24] 3319 	mov	_CC1000_WriteToRegister_PARM_2,#0x3B
   096E 75 82 00      [24] 3320 	mov	dpl,#0x00
   0971 C0 07         [24] 3321 	push	ar7
   0973 12 06 63      [24] 3322 	lcall	_CC1000_WriteToRegister
   0976 D0 07         [24] 3323 	pop	ar7
                     08B1  3324 	C$cc1000.h$389$1$113 ==.
                           3325 ;	C:\Workspace\WSN_MS-4\/cc1000.h:389: CC1000_WriteToRegister(CC1000_CURRENT,RXCurrent);   
   0978 8F 09         [24] 3326 	mov	_CC1000_WriteToRegister_PARM_2,r7
   097A 75 82 09      [24] 3327 	mov	dpl,#0x09
   097D 12 06 63      [24] 3328 	lcall	_CC1000_WriteToRegister
                     08B9  3329 	C$cc1000.h$390$1$113 ==.
                           3330 ;	C:\Workspace\WSN_MS-4\/cc1000.h:390: CC1000_WriteToRegister(CC1000_PLL, CC1000_RX_PLL); 
   0980 75 09 58      [24] 3331 	mov	_CC1000_WriteToRegister_PARM_2,#0x58
   0983 75 82 0C      [24] 3332 	mov	dpl,#0x0C
   0986 12 06 63      [24] 3333 	lcall	_CC1000_WriteToRegister
                     08C2  3334 	C$cc1000.h$392$1$113 ==.
                           3335 ;	C:\Workspace\WSN_MS-4\/cc1000.h:392: Delay_x100us(50);   // typowo 2-5ms
   0989 90 00 32      [24] 3336 	mov	dptr,#0x0032
   098C 12 01 7A      [24] 3337 	lcall	_Delay_x100us
                     08C8  3338 	C$cc1000.h$393$1$113 ==.
                           3339 ;	C:\Workspace\WSN_MS-4\/cc1000.h:393: CC1000_WriteToRegister(CC1000_MAIN,0x39);    
   098F 75 09 39      [24] 3340 	mov	_CC1000_WriteToRegister_PARM_2,#0x39
   0992 75 82 00      [24] 3341 	mov	dpl,#0x00
   0995 12 06 63      [24] 3342 	lcall	_CC1000_WriteToRegister
                     08D1  3343 	C$cc1000.h$394$1$113 ==.
                           3344 ;	C:\Workspace\WSN_MS-4\/cc1000.h:394: Delay_x100us(3);     // Oczekiwanie 250us 
   0998 90 00 03      [24] 3345 	mov	dptr,#0x0003
   099B 12 01 7A      [24] 3346 	lcall	_Delay_x100us
                     08D7  3347 	C$cc1000.h$395$1$113 ==.
                           3348 ;	C:\Workspace\WSN_MS-4\/cc1000.h:395: CC1000_WriteToRegister(CC1000_MAIN,0x31);    // W��czenie syntezatora cz�stotliwo�ci
   099E 75 09 31      [24] 3349 	mov	_CC1000_WriteToRegister_PARM_2,#0x31
   09A1 75 82 00      [24] 3350 	mov	dpl,#0x00
   09A4 12 06 63      [24] 3351 	lcall	_CC1000_WriteToRegister
                     08E0  3352 	C$cc1000.h$396$1$113 ==.
                     08E0  3353 	XG$CC1000_WakeUpToRX$0$0 ==.
   09A7 22            [24] 3354 	ret
                           3355 ;------------------------------------------------------------
                           3356 ;Allocation info for local variables in function 'CC1000_WakeUpToTX'
                           3357 ;------------------------------------------------------------
                           3358 ;TXCurrent                 Allocated to registers r7 
                           3359 ;------------------------------------------------------------
                     08E1  3360 	G$CC1000_WakeUpToTX$0$0 ==.
                     08E1  3361 	C$cc1000.h$401$1$113 ==.
                           3362 ;	C:\Workspace\WSN_MS-4\/cc1000.h:401: void CC1000_WakeUpToTX(U8 TXCurrent)  
                           3363 ;	-----------------------------------------
                           3364 ;	 function CC1000_WakeUpToTX
                           3365 ;	-----------------------------------------
   09A8                    3366 _CC1000_WakeUpToTX:
   09A8 AF 82         [24] 3367 	mov	r7,dpl
                     08E3  3368 	C$cc1000.h$403$1$115 ==.
                           3369 ;	C:\Workspace\WSN_MS-4\/cc1000.h:403: CC1000_WriteToRegister(CC1000_MAIN,0xFB);  // W��czenie oscylatora xtal
   09AA 75 09 FB      [24] 3370 	mov	_CC1000_WriteToRegister_PARM_2,#0xFB
   09AD 75 82 00      [24] 3371 	mov	dpl,#0x00
   09B0 C0 07         [24] 3372 	push	ar7
   09B2 12 06 63      [24] 3373 	lcall	_CC1000_WriteToRegister
   09B5 D0 07         [24] 3374 	pop	ar7
                     08F0  3375 	C$cc1000.h$404$1$115 ==.
                           3376 ;	C:\Workspace\WSN_MS-4\/cc1000.h:404: CC1000_WriteToRegister(CC1000_CURRENT,TXCurrent);
   09B7 8F 09         [24] 3377 	mov	_CC1000_WriteToRegister_PARM_2,r7
   09B9 75 82 09      [24] 3378 	mov	dpl,#0x09
   09BC 12 06 63      [24] 3379 	lcall	_CC1000_WriteToRegister
                     08F8  3380 	C$cc1000.h$405$1$115 ==.
                           3381 ;	C:\Workspace\WSN_MS-4\/cc1000.h:405: CC1000_WriteToRegister(CC1000_PLL, CC1000_TX_PLL);
   09BF 75 09 58      [24] 3382 	mov	_CC1000_WriteToRegister_PARM_2,#0x58
   09C2 75 82 0C      [24] 3383 	mov	dpl,#0x0C
   09C5 12 06 63      [24] 3384 	lcall	_CC1000_WriteToRegister
                     0901  3385 	C$cc1000.h$407$1$115 ==.
                           3386 ;	C:\Workspace\WSN_MS-4\/cc1000.h:407: Delay_x100us(50);    // typowo 2-5ms 
   09C8 90 00 32      [24] 3387 	mov	dptr,#0x0032
   09CB 12 01 7A      [24] 3388 	lcall	_Delay_x100us
                     0907  3389 	C$cc1000.h$408$1$115 ==.
                           3390 ;	C:\Workspace\WSN_MS-4\/cc1000.h:408: CC1000_WriteToRegister(CC1000_MAIN,0xF9); 
   09CE 75 09 F9      [24] 3391 	mov	_CC1000_WriteToRegister_PARM_2,#0xF9
   09D1 75 82 00      [24] 3392 	mov	dpl,#0x00
   09D4 12 06 63      [24] 3393 	lcall	_CC1000_WriteToRegister
                     0910  3394 	C$cc1000.h$409$1$115 ==.
                           3395 ;	C:\Workspace\WSN_MS-4\/cc1000.h:409: Delay_x100us(3);     // Oczekiwanie 250us
   09D7 90 00 03      [24] 3396 	mov	dptr,#0x0003
   09DA 12 01 7A      [24] 3397 	lcall	_Delay_x100us
                     0916  3398 	C$cc1000.h$410$1$115 ==.
                           3399 ;	C:\Workspace\WSN_MS-4\/cc1000.h:410: CC1000_WriteToRegister(CC1000_PA_POW, CC1000_PA_VALUE);    // W��czenie PA
   09DD 75 09 F0      [24] 3400 	mov	_CC1000_WriteToRegister_PARM_2,#0xF0
   09E0 75 82 0B      [24] 3401 	mov	dpl,#0x0B
   09E3 12 06 63      [24] 3402 	lcall	_CC1000_WriteToRegister
                     091F  3403 	C$cc1000.h$411$1$115 ==.
                           3404 ;	C:\Workspace\WSN_MS-4\/cc1000.h:411: CC1000_WriteToRegister(CC1000_MAIN,0xF1);    // W��czenie syntezatora cz�stotliwo�ci  
   09E6 75 09 F1      [24] 3405 	mov	_CC1000_WriteToRegister_PARM_2,#0xF1
   09E9 75 82 00      [24] 3406 	mov	dpl,#0x00
   09EC 12 06 63      [24] 3407 	lcall	_CC1000_WriteToRegister
                     0928  3408 	C$cc1000.h$412$1$115 ==.
                     0928  3409 	XG$CC1000_WakeUpToTX$0$0 ==.
   09EF 22            [24] 3410 	ret
                           3411 ;------------------------------------------------------------
                           3412 ;Allocation info for local variables in function 'CC1000_ConfigureRegistersRx'
                           3413 ;------------------------------------------------------------
                     0929  3414 	G$CC1000_ConfigureRegistersRx$0$0 ==.
                     0929  3415 	C$cc1000.h$415$1$115 ==.
                           3416 ;	C:\Workspace\WSN_MS-4\/cc1000.h:415: void CC1000_ConfigureRegistersRx(void)
                           3417 ;	-----------------------------------------
                           3418 ;	 function CC1000_ConfigureRegistersRx
                           3419 ;	-----------------------------------------
   09F0                    3420 _CC1000_ConfigureRegistersRx:
                     0929  3421 	C$cc1000.h$417$1$117 ==.
                           3422 ;	C:\Workspace\WSN_MS-4\/cc1000.h:417: CC1000_ConfigureRegisters();
   09F0 12 0A D4      [24] 3423 	lcall	_CC1000_ConfigureRegisters
                     092C  3424 	C$cc1000.h$418$1$117 ==.
                           3425 ;	C:\Workspace\WSN_MS-4\/cc1000.h:418: CC1000_WriteToRegister(CC1000_CURRENT, CC1000_RX_CURRENT);
   09F3 75 09 8C      [24] 3426 	mov	_CC1000_WriteToRegister_PARM_2,#0x8C
   09F6 75 82 09      [24] 3427 	mov	dpl,#0x09
   09F9 12 06 63      [24] 3428 	lcall	_CC1000_WriteToRegister
                     0935  3429 	C$cc1000.h$419$1$117 ==.
                           3430 ;	C:\Workspace\WSN_MS-4\/cc1000.h:419: CC1000_WriteToRegister(CC1000_PLL, CC1000_RX_PLL);
   09FC 75 09 58      [24] 3431 	mov	_CC1000_WriteToRegister_PARM_2,#0x58
   09FF 75 82 0C      [24] 3432 	mov	dpl,#0x0C
   0A02 12 06 63      [24] 3433 	lcall	_CC1000_WriteToRegister
                     093E  3434 	C$cc1000.h$420$1$117 ==.
                     093E  3435 	XG$CC1000_ConfigureRegistersRx$0$0 ==.
   0A05 22            [24] 3436 	ret
                           3437 ;------------------------------------------------------------
                           3438 ;Allocation info for local variables in function 'CC1000_ConfigureRegistersTx'
                           3439 ;------------------------------------------------------------
                     093F  3440 	G$CC1000_ConfigureRegistersTx$0$0 ==.
                     093F  3441 	C$cc1000.h$423$1$117 ==.
                           3442 ;	C:\Workspace\WSN_MS-4\/cc1000.h:423: void CC1000_ConfigureRegistersTx(void)
                           3443 ;	-----------------------------------------
                           3444 ;	 function CC1000_ConfigureRegistersTx
                           3445 ;	-----------------------------------------
   0A06                    3446 _CC1000_ConfigureRegistersTx:
                     093F  3447 	C$cc1000.h$425$1$119 ==.
                           3448 ;	C:\Workspace\WSN_MS-4\/cc1000.h:425: CC1000_ConfigureRegisters();
   0A06 12 0A D4      [24] 3449 	lcall	_CC1000_ConfigureRegisters
                     0942  3450 	C$cc1000.h$426$1$119 ==.
                           3451 ;	C:\Workspace\WSN_MS-4\/cc1000.h:426: CC1000_WriteToRegister(CC1000_CURRENT, CC1000_TX_CURRENT);
   0A09 75 09 F3      [24] 3452 	mov	_CC1000_WriteToRegister_PARM_2,#0xF3
   0A0C 75 82 09      [24] 3453 	mov	dpl,#0x09
   0A0F 12 06 63      [24] 3454 	lcall	_CC1000_WriteToRegister
                     094B  3455 	C$cc1000.h$427$1$119 ==.
                           3456 ;	C:\Workspace\WSN_MS-4\/cc1000.h:427: CC1000_WriteToRegister(CC1000_PLL, CC1000_TX_PLL);
   0A12 75 09 58      [24] 3457 	mov	_CC1000_WriteToRegister_PARM_2,#0x58
   0A15 75 82 0C      [24] 3458 	mov	dpl,#0x0C
   0A18 12 06 63      [24] 3459 	lcall	_CC1000_WriteToRegister
                     0954  3460 	C$cc1000.h$428$1$119 ==.
                     0954  3461 	XG$CC1000_ConfigureRegistersTx$0$0 ==.
   0A1B 22            [24] 3462 	ret
                           3463 ;------------------------------------------------------------
                           3464 ;Allocation info for local variables in function 'CC1000_ResetFreqSynth'
                           3465 ;------------------------------------------------------------
                           3466 ;modem1_value              Allocated to registers r7 
                           3467 ;------------------------------------------------------------
                     0955  3468 	G$CC1000_ResetFreqSynth$0$0 ==.
                     0955  3469 	C$cc1000.h$431$1$119 ==.
                           3470 ;	C:\Workspace\WSN_MS-4\/cc1000.h:431: void CC1000_ResetFreqSynth(void)
                           3471 ;	-----------------------------------------
                           3472 ;	 function CC1000_ResetFreqSynth
                           3473 ;	-----------------------------------------
   0A1C                    3474 _CC1000_ResetFreqSynth:
                     0955  3475 	C$cc1000.h$434$1$121 ==.
                           3476 ;	C:\Workspace\WSN_MS-4\/cc1000.h:434: modem1_value = CC1000_ReadFromRegister(CC1000_MODEM1)&~0x01;
   0A1C 75 82 10      [24] 3477 	mov	dpl,#0x10
   0A1F 12 06 CF      [24] 3478 	lcall	_CC1000_ReadFromRegister
   0A22 AF 82         [24] 3479 	mov	r7,dpl
   0A24 53 07 FE      [24] 3480 	anl	ar7,#0xFE
                     0960  3481 	C$cc1000.h$435$1$121 ==.
                           3482 ;	C:\Workspace\WSN_MS-4\/cc1000.h:435: CC1000_WriteToRegister(CC1000_MODEM1,modem1_value);
   0A27 8F 09         [24] 3483 	mov	_CC1000_WriteToRegister_PARM_2,r7
   0A29 75 82 10      [24] 3484 	mov	dpl,#0x10
   0A2C C0 07         [24] 3485 	push	ar7
   0A2E 12 06 63      [24] 3486 	lcall	_CC1000_WriteToRegister
   0A31 D0 07         [24] 3487 	pop	ar7
                     096C  3488 	C$cc1000.h$436$1$121 ==.
                           3489 ;	C:\Workspace\WSN_MS-4\/cc1000.h:436: CC1000_WriteToRegister(CC1000_MODEM1,modem1_value|0x01);
   0A33 74 01         [12] 3490 	mov	a,#0x01
   0A35 4F            [12] 3491 	orl	a,r7
   0A36 F5 09         [12] 3492 	mov	_CC1000_WriteToRegister_PARM_2,a
   0A38 75 82 10      [24] 3493 	mov	dpl,#0x10
   0A3B 12 06 63      [24] 3494 	lcall	_CC1000_WriteToRegister
                     0977  3495 	C$cc1000.h$437$1$121 ==.
                     0977  3496 	XG$CC1000_ResetFreqSynth$0$0 ==.
   0A3E 22            [24] 3497 	ret
                           3498 ;------------------------------------------------------------
                           3499 ;Allocation info for local variables in function 'CC1000_Initialize'
                           3500 ;------------------------------------------------------------
                           3501 ;cal                       Allocated to registers r7 
                           3502 ;------------------------------------------------------------
                     0978  3503 	G$CC1000_Initialize$0$0 ==.
                     0978  3504 	C$cc1000.h$442$1$121 ==.
                           3505 ;	C:\Workspace\WSN_MS-4\/cc1000.h:442: void CC1000_Initialize(void)
                           3506 ;	-----------------------------------------
                           3507 ;	 function CC1000_Initialize
                           3508 ;	-----------------------------------------
   0A3F                    3509 _CC1000_Initialize:
                     0978  3510 	C$cc1000.h$446$1$123 ==.
                           3511 ;	C:\Workspace\WSN_MS-4\/cc1000.h:446: P0MDOUT |= 0x10;    // PDATA (P0.4 jako push-pull)
   0A3F 43 A4 10      [24] 3512 	orl	_P0MDOUT,#0x10
                     097B  3513 	C$cc1000.h$447$1$123 ==.
                           3514 ;	C:\Workspace\WSN_MS-4\/cc1000.h:447: PCLK = 1;           // Przy braku transmisji parametr�w: PCLK = 1
   0A42 D2 85         [12] 3515 	setb	_PCLK
                     097D  3516 	C$cc1000.h$448$1$123 ==.
                           3517 ;	C:\Workspace\WSN_MS-4\/cc1000.h:448: CC1000_SetupPD();
   0A44 12 09 56      [24] 3518 	lcall	_CC1000_SetupPD
                     0980  3519 	C$cc1000.h$449$1$123 ==.
                           3520 ;	C:\Workspace\WSN_MS-4\/cc1000.h:449: CC1000_Reset();	
   0A47 12 07 A1      [24] 3521 	lcall	_CC1000_Reset
                     0983  3522 	C$cc1000.h$450$1$123 ==.
                           3523 ;	C:\Workspace\WSN_MS-4\/cc1000.h:450: Delay_x100us(20);	// czekaj 2 ms
   0A4A 90 00 14      [24] 3524 	mov	dptr,#0x0014
   0A4D 12 01 7A      [24] 3525 	lcall	_Delay_x100us
                     0989  3526 	C$cc1000.h$451$1$123 ==.
                           3527 ;	C:\Workspace\WSN_MS-4\/cc1000.h:451: CC1000_WriteToRegister(CC1000_MAIN,0x11);	// Kalibracja trybu RX
   0A50 75 09 11      [24] 3528 	mov	_CC1000_WriteToRegister_PARM_2,#0x11
   0A53 75 82 00      [24] 3529 	mov	dpl,#0x00
   0A56 12 06 63      [24] 3530 	lcall	_CC1000_WriteToRegister
                     0992  3531 	C$cc1000.h$452$1$123 ==.
                           3532 ;	C:\Workspace\WSN_MS-4\/cc1000.h:452: CC1000_ConfigureRegistersRx();
   0A59 12 09 F0      [24] 3533 	lcall	_CC1000_ConfigureRegistersRx
                     0995  3534 	C$cc1000.h$453$1$123 ==.
                           3535 ;	C:\Workspace\WSN_MS-4\/cc1000.h:453: cal = CC1000_Calibrate();
   0A5C 12 07 C4      [24] 3536 	lcall	_CC1000_Calibrate
                     0998  3537 	C$cc1000.h$454$1$123 ==.
                           3538 ;	C:\Workspace\WSN_MS-4\/cc1000.h:454: if (cal == 0)
   0A5F E5 82         [12] 3539 	mov	a,dpl
   0A61 FF            [12] 3540 	mov	r7,a
   0A62 70 17         [24] 3541 	jnz	00102$
                     099D  3542 	C$cc1000.h$457$2$124 ==.
                           3543 ;	C:\Workspace\WSN_MS-4\/cc1000.h:457: printf("B��d kalibracji trybu RX\n");
   0A64 74 BC         [12] 3544 	mov	a,#__str_1
   0A66 C0 E0         [24] 3545 	push	acc
   0A68 74 18         [12] 3546 	mov	a,#(__str_1 >> 8)
   0A6A C0 E0         [24] 3547 	push	acc
   0A6C 74 80         [12] 3548 	mov	a,#0x80
   0A6E C0 E0         [24] 3549 	push	acc
   0A70 12 03 EA      [24] 3550 	lcall	_printf
   0A73 15 81         [12] 3551 	dec	sp
   0A75 15 81         [12] 3552 	dec	sp
   0A77 15 81         [12] 3553 	dec	sp
   0A79 80 15         [24] 3554 	sjmp	00103$
   0A7B                    3555 00102$:
                     09B4  3556 	C$cc1000.h$463$2$125 ==.
                           3557 ;	C:\Workspace\WSN_MS-4\/cc1000.h:463: printf("Poprawna kalibracja trybu RX\n"); 
   0A7B 74 D6         [12] 3558 	mov	a,#__str_2
   0A7D C0 E0         [24] 3559 	push	acc
   0A7F 74 18         [12] 3560 	mov	a,#(__str_2 >> 8)
   0A81 C0 E0         [24] 3561 	push	acc
   0A83 74 80         [12] 3562 	mov	a,#0x80
   0A85 C0 E0         [24] 3563 	push	acc
   0A87 12 03 EA      [24] 3564 	lcall	_printf
   0A8A 15 81         [12] 3565 	dec	sp
   0A8C 15 81         [12] 3566 	dec	sp
   0A8E 15 81         [12] 3567 	dec	sp
   0A90                    3568 00103$:
                     09C9  3569 	C$cc1000.h$467$1$123 ==.
                           3570 ;	C:\Workspace\WSN_MS-4\/cc1000.h:467: CC1000_WriteToRegister(CC1000_MAIN,0xA1);	// Kalibracja trybu TX
   0A90 75 09 A1      [24] 3571 	mov	_CC1000_WriteToRegister_PARM_2,#0xA1
   0A93 75 82 00      [24] 3572 	mov	dpl,#0x00
   0A96 12 06 63      [24] 3573 	lcall	_CC1000_WriteToRegister
                     09D2  3574 	C$cc1000.h$468$1$123 ==.
                           3575 ;	C:\Workspace\WSN_MS-4\/cc1000.h:468: CC1000_ConfigureRegistersTx();
   0A99 12 0A 06      [24] 3576 	lcall	_CC1000_ConfigureRegistersTx
                     09D5  3577 	C$cc1000.h$469$1$123 ==.
                           3578 ;	C:\Workspace\WSN_MS-4\/cc1000.h:469: cal = CC1000_Calibrate();
   0A9C 12 07 C4      [24] 3579 	lcall	_CC1000_Calibrate
                     09D8  3580 	C$cc1000.h$470$1$123 ==.
                           3581 ;	C:\Workspace\WSN_MS-4\/cc1000.h:470: if (cal == 0)
   0A9F E5 82         [12] 3582 	mov	a,dpl
   0AA1 FF            [12] 3583 	mov	r7,a
   0AA2 70 17         [24] 3584 	jnz	00105$
                     09DD  3585 	C$cc1000.h$473$2$126 ==.
                           3586 ;	C:\Workspace\WSN_MS-4\/cc1000.h:473: printf("B��d kalibracji trybu TX\n");            
   0AA4 74 F4         [12] 3587 	mov	a,#__str_3
   0AA6 C0 E0         [24] 3588 	push	acc
   0AA8 74 18         [12] 3589 	mov	a,#(__str_3 >> 8)
   0AAA C0 E0         [24] 3590 	push	acc
   0AAC 74 80         [12] 3591 	mov	a,#0x80
   0AAE C0 E0         [24] 3592 	push	acc
   0AB0 12 03 EA      [24] 3593 	lcall	_printf
   0AB3 15 81         [12] 3594 	dec	sp
   0AB5 15 81         [12] 3595 	dec	sp
   0AB7 15 81         [12] 3596 	dec	sp
   0AB9 80 15         [24] 3597 	sjmp	00106$
   0ABB                    3598 00105$:
                     09F4  3599 	C$cc1000.h$479$2$127 ==.
                           3600 ;	C:\Workspace\WSN_MS-4\/cc1000.h:479: printf("Poprawna kalibracja trybu TX\n"); 
   0ABB 74 0E         [12] 3601 	mov	a,#__str_4
   0ABD C0 E0         [24] 3602 	push	acc
   0ABF 74 19         [12] 3603 	mov	a,#(__str_4 >> 8)
   0AC1 C0 E0         [24] 3604 	push	acc
   0AC3 74 80         [12] 3605 	mov	a,#0x80
   0AC5 C0 E0         [24] 3606 	push	acc
   0AC7 12 03 EA      [24] 3607 	lcall	_printf
   0ACA 15 81         [12] 3608 	dec	sp
   0ACC 15 81         [12] 3609 	dec	sp
   0ACE 15 81         [12] 3610 	dec	sp
   0AD0                    3611 00106$:
                     0A09  3612 	C$cc1000.h$482$1$123 ==.
                           3613 ;	C:\Workspace\WSN_MS-4\/cc1000.h:482: CC1000_SetupPD();	
   0AD0 12 09 56      [24] 3614 	lcall	_CC1000_SetupPD
                     0A0C  3615 	C$cc1000.h$483$1$123 ==.
                     0A0C  3616 	XG$CC1000_Initialize$0$0 ==.
   0AD3 22            [24] 3617 	ret
                           3618 ;------------------------------------------------------------
                           3619 ;Allocation info for local variables in function 'CC1000_ConfigureRegisters'
                           3620 ;------------------------------------------------------------
                     0A0D  3621 	G$CC1000_ConfigureRegisters$0$0 ==.
                     0A0D  3622 	C$cc1000.h$486$1$123 ==.
                           3623 ;	C:\Workspace\WSN_MS-4\/cc1000.h:486: void CC1000_ConfigureRegisters(void)
                           3624 ;	-----------------------------------------
                           3625 ;	 function CC1000_ConfigureRegisters
                           3626 ;	-----------------------------------------
   0AD4                    3627 _CC1000_ConfigureRegisters:
                     0A0D  3628 	C$cc1000.h$529$1$129 ==.
                           3629 ;	C:\Workspace\WSN_MS-4\/cc1000.h:529: CC1000_WriteToRegister(CC1000_MAIN,	0x11);      //0xE1
   0AD4 75 09 11      [24] 3630 	mov	_CC1000_WriteToRegister_PARM_2,#0x11
   0AD7 75 82 00      [24] 3631 	mov	dpl,#0x00
   0ADA 12 06 63      [24] 3632 	lcall	_CC1000_WriteToRegister
                     0A16  3633 	C$cc1000.h$530$1$129 ==.
                           3634 ;	C:\Workspace\WSN_MS-4\/cc1000.h:530: CC1000_WriteToRegister(CC1000_FREQ_2A,	0x50);
   0ADD 75 09 50      [24] 3635 	mov	_CC1000_WriteToRegister_PARM_2,#0x50
   0AE0 75 82 01      [24] 3636 	mov	dpl,#0x01
   0AE3 12 06 63      [24] 3637 	lcall	_CC1000_WriteToRegister
                     0A1F  3638 	C$cc1000.h$531$1$129 ==.
                           3639 ;	C:\Workspace\WSN_MS-4\/cc1000.h:531: CC1000_WriteToRegister(CC1000_FREQ_1A,	0xC0);
   0AE6 75 09 C0      [24] 3640 	mov	_CC1000_WriteToRegister_PARM_2,#0xC0
   0AE9 75 82 02      [24] 3641 	mov	dpl,#0x02
   0AEC 12 06 63      [24] 3642 	lcall	_CC1000_WriteToRegister
                     0A28  3643 	C$cc1000.h$532$1$129 ==.
                           3644 ;	C:\Workspace\WSN_MS-4\/cc1000.h:532: CC1000_WriteToRegister(CC1000_FREQ_0A,	0x00);
   0AEF 75 09 00      [24] 3645 	mov	_CC1000_WriteToRegister_PARM_2,#0x00
   0AF2 75 82 03      [24] 3646 	mov	dpl,#0x03
   0AF5 12 06 63      [24] 3647 	lcall	_CC1000_WriteToRegister
                     0A31  3648 	C$cc1000.h$533$1$129 ==.
                           3649 ;	C:\Workspace\WSN_MS-4\/cc1000.h:533: CC1000_WriteToRegister(CC1000_FREQ_2B,	0x50);
   0AF8 75 09 50      [24] 3650 	mov	_CC1000_WriteToRegister_PARM_2,#0x50
   0AFB 75 82 04      [24] 3651 	mov	dpl,#0x04
   0AFE 12 06 63      [24] 3652 	lcall	_CC1000_WriteToRegister
                     0A3A  3653 	C$cc1000.h$534$1$129 ==.
                           3654 ;	C:\Workspace\WSN_MS-4\/cc1000.h:534: CC1000_WriteToRegister(CC1000_FREQ_1B,	0xB7);
   0B01 75 09 B7      [24] 3655 	mov	_CC1000_WriteToRegister_PARM_2,#0xB7
   0B04 75 82 05      [24] 3656 	mov	dpl,#0x05
   0B07 12 06 63      [24] 3657 	lcall	_CC1000_WriteToRegister
                     0A43  3658 	C$cc1000.h$535$1$129 ==.
                           3659 ;	C:\Workspace\WSN_MS-4\/cc1000.h:535: CC1000_WriteToRegister(CC1000_FREQ_0B,	0x4F);
   0B0A 75 09 4F      [24] 3660 	mov	_CC1000_WriteToRegister_PARM_2,#0x4F
   0B0D 75 82 06      [24] 3661 	mov	dpl,#0x06
   0B10 12 06 63      [24] 3662 	lcall	_CC1000_WriteToRegister
                     0A4C  3663 	C$cc1000.h$536$1$129 ==.
                           3664 ;	C:\Workspace\WSN_MS-4\/cc1000.h:536: CC1000_WriteToRegister(CC1000_FSEP1,  	0x03);
   0B13 75 09 03      [24] 3665 	mov	_CC1000_WriteToRegister_PARM_2,#0x03
   0B16 75 82 07      [24] 3666 	mov	dpl,#0x07
   0B19 12 06 63      [24] 3667 	lcall	_CC1000_WriteToRegister
                     0A55  3668 	C$cc1000.h$537$1$129 ==.
                           3669 ;	C:\Workspace\WSN_MS-4\/cc1000.h:537: CC1000_WriteToRegister(CC1000_FSEP0,  	0x0E);
   0B1C 75 09 0E      [24] 3670 	mov	_CC1000_WriteToRegister_PARM_2,#0x0E
   0B1F 75 82 08      [24] 3671 	mov	dpl,#0x08
   0B22 12 06 63      [24] 3672 	lcall	_CC1000_WriteToRegister
                     0A5E  3673 	C$cc1000.h$538$1$129 ==.
                           3674 ;	C:\Workspace\WSN_MS-4\/cc1000.h:538: CC1000_WriteToRegister(CC1000_FRONT_END,0x12);
   0B25 75 09 12      [24] 3675 	mov	_CC1000_WriteToRegister_PARM_2,#0x12
   0B28 75 82 0A      [24] 3676 	mov	dpl,#0x0A
   0B2B 12 06 63      [24] 3677 	lcall	_CC1000_WriteToRegister
                     0A67  3678 	C$cc1000.h$539$1$129 ==.
                           3679 ;	C:\Workspace\WSN_MS-4\/cc1000.h:539: CC1000_WriteToRegister(CC1000_PA_POW, 	0x70);       
   0B2E 75 09 70      [24] 3680 	mov	_CC1000_WriteToRegister_PARM_2,#0x70
   0B31 75 82 0B      [24] 3681 	mov	dpl,#0x0B
   0B34 12 06 63      [24] 3682 	lcall	_CC1000_WriteToRegister
                     0A70  3683 	C$cc1000.h$540$1$129 ==.
                           3684 ;	C:\Workspace\WSN_MS-4\/cc1000.h:540: CC1000_WriteToRegister(CC1000_PLL,   	0x58);  // REFDIV=11
   0B37 75 09 58      [24] 3685 	mov	_CC1000_WriteToRegister_PARM_2,#0x58
   0B3A 75 82 0C      [24] 3686 	mov	dpl,#0x0C
   0B3D 12 06 63      [24] 3687 	lcall	_CC1000_WriteToRegister
                     0A79  3688 	C$cc1000.h$541$1$129 ==.
                           3689 ;	C:\Workspace\WSN_MS-4\/cc1000.h:541: CC1000_WriteToRegister(CC1000_LOCK,   	0x10);
   0B40 75 09 10      [24] 3690 	mov	_CC1000_WriteToRegister_PARM_2,#0x10
   0B43 75 82 0D      [24] 3691 	mov	dpl,#0x0D
   0B46 12 06 63      [24] 3692 	lcall	_CC1000_WriteToRegister
                     0A82  3693 	C$cc1000.h$542$1$129 ==.
                           3694 ;	C:\Workspace\WSN_MS-4\/cc1000.h:542: CC1000_WriteToRegister(CC1000_CAL,    	0x26);
   0B49 75 09 26      [24] 3695 	mov	_CC1000_WriteToRegister_PARM_2,#0x26
   0B4C 75 82 0E      [24] 3696 	mov	dpl,#0x0E
   0B4F 12 06 63      [24] 3697 	lcall	_CC1000_WriteToRegister
                     0A8B  3698 	C$cc1000.h$543$1$129 ==.
                           3699 ;	C:\Workspace\WSN_MS-4\/cc1000.h:543: CC1000_WriteToRegister(CC1000_MODEM2, 	0xB7);  // 0x90
   0B52 75 09 B7      [24] 3700 	mov	_CC1000_WriteToRegister_PARM_2,#0xB7
   0B55 75 82 0F      [24] 3701 	mov	dpl,#0x0F
   0B58 12 06 63      [24] 3702 	lcall	_CC1000_WriteToRegister
                     0A94  3703 	C$cc1000.h$544$1$129 ==.
                           3704 ;	C:\Workspace\WSN_MS-4\/cc1000.h:544: CC1000_WriteToRegister(CC1000_MODEM1, 	0x6F);  // 0x69 
   0B5B 75 09 6F      [24] 3705 	mov	_CC1000_WriteToRegister_PARM_2,#0x6F
   0B5E 75 82 10      [24] 3706 	mov	dpl,#0x10
   0B61 12 06 63      [24] 3707 	lcall	_CC1000_WriteToRegister
                     0A9D  3708 	C$cc1000.h$545$1$129 ==.
                           3709 ;	C:\Workspace\WSN_MS-4\/cc1000.h:545: CC1000_WriteToRegister(CC1000_MODEM0, 	0x37);  // 2,4 (Manchester)
   0B64 75 09 37      [24] 3710 	mov	_CC1000_WriteToRegister_PARM_2,#0x37
   0B67 75 82 11      [24] 3711 	mov	dpl,#0x11
   0B6A 12 06 63      [24] 3712 	lcall	_CC1000_WriteToRegister
                     0AA6  3713 	C$cc1000.h$546$1$129 ==.
                           3714 ;	C:\Workspace\WSN_MS-4\/cc1000.h:546: CC1000_WriteToRegister(CC1000_MATCH,  	0x70);
   0B6D 75 09 70      [24] 3715 	mov	_CC1000_WriteToRegister_PARM_2,#0x70
   0B70 75 82 12      [24] 3716 	mov	dpl,#0x12
   0B73 12 06 63      [24] 3717 	lcall	_CC1000_WriteToRegister
                     0AAF  3718 	C$cc1000.h$547$1$129 ==.
                           3719 ;	C:\Workspace\WSN_MS-4\/cc1000.h:547: CC1000_WriteToRegister(CC1000_FSCTRL, 	0x01);
   0B76 75 09 01      [24] 3720 	mov	_CC1000_WriteToRegister_PARM_2,#0x01
   0B79 75 82 13      [24] 3721 	mov	dpl,#0x13
   0B7C 12 06 63      [24] 3722 	lcall	_CC1000_WriteToRegister
                     0AB8  3723 	C$cc1000.h$550$1$129 ==.
                     0AB8  3724 	XG$CC1000_ConfigureRegisters$0$0 ==.
   0B7F 22            [24] 3725 	ret
                           3726 ;------------------------------------------------------------
                           3727 ;Allocation info for local variables in function 'CC1000_DisplayAllRegisters'
                           3728 ;------------------------------------------------------------
                     0AB9  3729 	G$CC1000_DisplayAllRegisters$0$0 ==.
                     0AB9  3730 	C$cc1000.h$555$1$129 ==.
                           3731 ;	C:\Workspace\WSN_MS-4\/cc1000.h:555: void CC1000_DisplayAllRegisters(void)
                           3732 ;	-----------------------------------------
                           3733 ;	 function CC1000_DisplayAllRegisters
                           3734 ;	-----------------------------------------
   0B80                    3735 _CC1000_DisplayAllRegisters:
                     0AB9  3736 	C$cc1000.h$557$1$131 ==.
                           3737 ;	C:\Workspace\WSN_MS-4\/cc1000.h:557: printf("MAIN: 0x%x \n", CC1000_ReadFromRegister(CC1000_MAIN));
   0B80 75 82 00      [24] 3738 	mov	dpl,#0x00
   0B83 12 06 CF      [24] 3739 	lcall	_CC1000_ReadFromRegister
   0B86 AF 82         [24] 3740 	mov	r7,dpl
   0B88 7E 00         [12] 3741 	mov	r6,#0x00
   0B8A C0 07         [24] 3742 	push	ar7
   0B8C C0 06         [24] 3743 	push	ar6
   0B8E 74 2C         [12] 3744 	mov	a,#__str_5
   0B90 C0 E0         [24] 3745 	push	acc
   0B92 74 19         [12] 3746 	mov	a,#(__str_5 >> 8)
   0B94 C0 E0         [24] 3747 	push	acc
   0B96 74 80         [12] 3748 	mov	a,#0x80
   0B98 C0 E0         [24] 3749 	push	acc
   0B9A 12 03 EA      [24] 3750 	lcall	_printf
   0B9D E5 81         [12] 3751 	mov	a,sp
   0B9F 24 FB         [12] 3752 	add	a,#0xfb
   0BA1 F5 81         [12] 3753 	mov	sp,a
                     0ADC  3754 	C$cc1000.h$558$1$131 ==.
                           3755 ;	C:\Workspace\WSN_MS-4\/cc1000.h:558: printf("FREQ_2A: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_2A));    // 0x75
   0BA3 75 82 01      [24] 3756 	mov	dpl,#0x01
   0BA6 12 06 CF      [24] 3757 	lcall	_CC1000_ReadFromRegister
   0BA9 AF 82         [24] 3758 	mov	r7,dpl
   0BAB 7E 00         [12] 3759 	mov	r6,#0x00
   0BAD C0 07         [24] 3760 	push	ar7
   0BAF C0 06         [24] 3761 	push	ar6
   0BB1 74 39         [12] 3762 	mov	a,#__str_6
   0BB3 C0 E0         [24] 3763 	push	acc
   0BB5 74 19         [12] 3764 	mov	a,#(__str_6 >> 8)
   0BB7 C0 E0         [24] 3765 	push	acc
   0BB9 74 80         [12] 3766 	mov	a,#0x80
   0BBB C0 E0         [24] 3767 	push	acc
   0BBD 12 03 EA      [24] 3768 	lcall	_printf
   0BC0 E5 81         [12] 3769 	mov	a,sp
   0BC2 24 FB         [12] 3770 	add	a,#0xfb
   0BC4 F5 81         [12] 3771 	mov	sp,a
                     0AFF  3772 	C$cc1000.h$559$1$131 ==.
                           3773 ;	C:\Workspace\WSN_MS-4\/cc1000.h:559: printf("FREQ_1A: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_1A));    // 0xA0    
   0BC6 75 82 02      [24] 3774 	mov	dpl,#0x02
   0BC9 12 06 CF      [24] 3775 	lcall	_CC1000_ReadFromRegister
   0BCC AF 82         [24] 3776 	mov	r7,dpl
   0BCE 7E 00         [12] 3777 	mov	r6,#0x00
   0BD0 C0 07         [24] 3778 	push	ar7
   0BD2 C0 06         [24] 3779 	push	ar6
   0BD4 74 49         [12] 3780 	mov	a,#__str_7
   0BD6 C0 E0         [24] 3781 	push	acc
   0BD8 74 19         [12] 3782 	mov	a,#(__str_7 >> 8)
   0BDA C0 E0         [24] 3783 	push	acc
   0BDC 74 80         [12] 3784 	mov	a,#0x80
   0BDE C0 E0         [24] 3785 	push	acc
   0BE0 12 03 EA      [24] 3786 	lcall	_printf
   0BE3 E5 81         [12] 3787 	mov	a,sp
   0BE5 24 FB         [12] 3788 	add	a,#0xfb
   0BE7 F5 81         [12] 3789 	mov	sp,a
                     0B22  3790 	C$cc1000.h$560$1$131 ==.
                           3791 ;	C:\Workspace\WSN_MS-4\/cc1000.h:560: printf("FREQ_0A: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_0A));    // 0xCB
   0BE9 75 82 03      [24] 3792 	mov	dpl,#0x03
   0BEC 12 06 CF      [24] 3793 	lcall	_CC1000_ReadFromRegister
   0BEF AF 82         [24] 3794 	mov	r7,dpl
   0BF1 7E 00         [12] 3795 	mov	r6,#0x00
   0BF3 C0 07         [24] 3796 	push	ar7
   0BF5 C0 06         [24] 3797 	push	ar6
   0BF7 74 59         [12] 3798 	mov	a,#__str_8
   0BF9 C0 E0         [24] 3799 	push	acc
   0BFB 74 19         [12] 3800 	mov	a,#(__str_8 >> 8)
   0BFD C0 E0         [24] 3801 	push	acc
   0BFF 74 80         [12] 3802 	mov	a,#0x80
   0C01 C0 E0         [24] 3803 	push	acc
   0C03 12 03 EA      [24] 3804 	lcall	_printf
   0C06 E5 81         [12] 3805 	mov	a,sp
   0C08 24 FB         [12] 3806 	add	a,#0xfb
   0C0A F5 81         [12] 3807 	mov	sp,a
                     0B45  3808 	C$cc1000.h$561$1$131 ==.
                           3809 ;	C:\Workspace\WSN_MS-4\/cc1000.h:561: printf("FREQ_2B: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_2B));    // 0x75
   0C0C 75 82 04      [24] 3810 	mov	dpl,#0x04
   0C0F 12 06 CF      [24] 3811 	lcall	_CC1000_ReadFromRegister
   0C12 AF 82         [24] 3812 	mov	r7,dpl
   0C14 7E 00         [12] 3813 	mov	r6,#0x00
   0C16 C0 07         [24] 3814 	push	ar7
   0C18 C0 06         [24] 3815 	push	ar6
   0C1A 74 69         [12] 3816 	mov	a,#__str_9
   0C1C C0 E0         [24] 3817 	push	acc
   0C1E 74 19         [12] 3818 	mov	a,#(__str_9 >> 8)
   0C20 C0 E0         [24] 3819 	push	acc
   0C22 74 80         [12] 3820 	mov	a,#0x80
   0C24 C0 E0         [24] 3821 	push	acc
   0C26 12 03 EA      [24] 3822 	lcall	_printf
   0C29 E5 81         [12] 3823 	mov	a,sp
   0C2B 24 FB         [12] 3824 	add	a,#0xfb
   0C2D F5 81         [12] 3825 	mov	sp,a
                     0B68  3826 	C$cc1000.h$562$1$131 ==.
                           3827 ;	C:\Workspace\WSN_MS-4\/cc1000.h:562: printf("FREQ_1B: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_1B));    // 0xA5
   0C2F 75 82 05      [24] 3828 	mov	dpl,#0x05
   0C32 12 06 CF      [24] 3829 	lcall	_CC1000_ReadFromRegister
   0C35 AF 82         [24] 3830 	mov	r7,dpl
   0C37 7E 00         [12] 3831 	mov	r6,#0x00
   0C39 C0 07         [24] 3832 	push	ar7
   0C3B C0 06         [24] 3833 	push	ar6
   0C3D 74 79         [12] 3834 	mov	a,#__str_10
   0C3F C0 E0         [24] 3835 	push	acc
   0C41 74 19         [12] 3836 	mov	a,#(__str_10 >> 8)
   0C43 C0 E0         [24] 3837 	push	acc
   0C45 74 80         [12] 3838 	mov	a,#0x80
   0C47 C0 E0         [24] 3839 	push	acc
   0C49 12 03 EA      [24] 3840 	lcall	_printf
   0C4C E5 81         [12] 3841 	mov	a,sp
   0C4E 24 FB         [12] 3842 	add	a,#0xfb
   0C50 F5 81         [12] 3843 	mov	sp,a
                     0B8B  3844 	C$cc1000.h$563$1$131 ==.
                           3845 ;	C:\Workspace\WSN_MS-4\/cc1000.h:563: printf("FREQ_0B: 0x%x \n", CC1000_ReadFromRegister(CC1000_FREQ_0B));    // 0x4E
   0C52 75 82 06      [24] 3846 	mov	dpl,#0x06
   0C55 12 06 CF      [24] 3847 	lcall	_CC1000_ReadFromRegister
   0C58 AF 82         [24] 3848 	mov	r7,dpl
   0C5A 7E 00         [12] 3849 	mov	r6,#0x00
   0C5C C0 07         [24] 3850 	push	ar7
   0C5E C0 06         [24] 3851 	push	ar6
   0C60 74 89         [12] 3852 	mov	a,#__str_11
   0C62 C0 E0         [24] 3853 	push	acc
   0C64 74 19         [12] 3854 	mov	a,#(__str_11 >> 8)
   0C66 C0 E0         [24] 3855 	push	acc
   0C68 74 80         [12] 3856 	mov	a,#0x80
   0C6A C0 E0         [24] 3857 	push	acc
   0C6C 12 03 EA      [24] 3858 	lcall	_printf
   0C6F E5 81         [12] 3859 	mov	a,sp
   0C71 24 FB         [12] 3860 	add	a,#0xfb
   0C73 F5 81         [12] 3861 	mov	sp,a
                     0BAE  3862 	C$cc1000.h$564$1$131 ==.
                           3863 ;	C:\Workspace\WSN_MS-4\/cc1000.h:564: printf("FSEP1: 0x%x \n", CC1000_ReadFromRegister(CC1000_FSEP1));        // 0x00
   0C75 75 82 07      [24] 3864 	mov	dpl,#0x07
   0C78 12 06 CF      [24] 3865 	lcall	_CC1000_ReadFromRegister
   0C7B AF 82         [24] 3866 	mov	r7,dpl
   0C7D 7E 00         [12] 3867 	mov	r6,#0x00
   0C7F C0 07         [24] 3868 	push	ar7
   0C81 C0 06         [24] 3869 	push	ar6
   0C83 74 99         [12] 3870 	mov	a,#__str_12
   0C85 C0 E0         [24] 3871 	push	acc
   0C87 74 19         [12] 3872 	mov	a,#(__str_12 >> 8)
   0C89 C0 E0         [24] 3873 	push	acc
   0C8B 74 80         [12] 3874 	mov	a,#0x80
   0C8D C0 E0         [24] 3875 	push	acc
   0C8F 12 03 EA      [24] 3876 	lcall	_printf
   0C92 E5 81         [12] 3877 	mov	a,sp
   0C94 24 FB         [12] 3878 	add	a,#0xfb
   0C96 F5 81         [12] 3879 	mov	sp,a
                     0BD1  3880 	C$cc1000.h$565$1$131 ==.
                           3881 ;	C:\Workspace\WSN_MS-4\/cc1000.h:565: printf("FSEP0: 0x%x \n", CC1000_ReadFromRegister(CC1000_FSEP0));        // 0x59 
   0C98 75 82 08      [24] 3882 	mov	dpl,#0x08
   0C9B 12 06 CF      [24] 3883 	lcall	_CC1000_ReadFromRegister
   0C9E AF 82         [24] 3884 	mov	r7,dpl
   0CA0 7E 00         [12] 3885 	mov	r6,#0x00
   0CA2 C0 07         [24] 3886 	push	ar7
   0CA4 C0 06         [24] 3887 	push	ar6
   0CA6 74 A7         [12] 3888 	mov	a,#__str_13
   0CA8 C0 E0         [24] 3889 	push	acc
   0CAA 74 19         [12] 3890 	mov	a,#(__str_13 >> 8)
   0CAC C0 E0         [24] 3891 	push	acc
   0CAE 74 80         [12] 3892 	mov	a,#0x80
   0CB0 C0 E0         [24] 3893 	push	acc
   0CB2 12 03 EA      [24] 3894 	lcall	_printf
   0CB5 E5 81         [12] 3895 	mov	a,sp
   0CB7 24 FB         [12] 3896 	add	a,#0xfb
   0CB9 F5 81         [12] 3897 	mov	sp,a
                     0BF4  3898 	C$cc1000.h$566$1$131 ==.
                           3899 ;	C:\Workspace\WSN_MS-4\/cc1000.h:566: printf("CURRENT: 0x%x \n", CC1000_ReadFromRegister(CC1000_CURRENT));    // 0xCA
   0CBB 75 82 09      [24] 3900 	mov	dpl,#0x09
   0CBE 12 06 CF      [24] 3901 	lcall	_CC1000_ReadFromRegister
   0CC1 AF 82         [24] 3902 	mov	r7,dpl
   0CC3 7E 00         [12] 3903 	mov	r6,#0x00
   0CC5 C0 07         [24] 3904 	push	ar7
   0CC7 C0 06         [24] 3905 	push	ar6
   0CC9 74 B5         [12] 3906 	mov	a,#__str_14
   0CCB C0 E0         [24] 3907 	push	acc
   0CCD 74 19         [12] 3908 	mov	a,#(__str_14 >> 8)
   0CCF C0 E0         [24] 3909 	push	acc
   0CD1 74 80         [12] 3910 	mov	a,#0x80
   0CD3 C0 E0         [24] 3911 	push	acc
   0CD5 12 03 EA      [24] 3912 	lcall	_printf
   0CD8 E5 81         [12] 3913 	mov	a,sp
   0CDA 24 FB         [12] 3914 	add	a,#0xfb
   0CDC F5 81         [12] 3915 	mov	sp,a
                     0C17  3916 	C$cc1000.h$567$1$131 ==.
                           3917 ;	C:\Workspace\WSN_MS-4\/cc1000.h:567: printf("FRONT_END: 0x%x \n", CC1000_ReadFromRegister(CC1000_FRONT_END));// 0x08 
   0CDE 75 82 0A      [24] 3918 	mov	dpl,#0x0A
   0CE1 12 06 CF      [24] 3919 	lcall	_CC1000_ReadFromRegister
   0CE4 AF 82         [24] 3920 	mov	r7,dpl
   0CE6 7E 00         [12] 3921 	mov	r6,#0x00
   0CE8 C0 07         [24] 3922 	push	ar7
   0CEA C0 06         [24] 3923 	push	ar6
   0CEC 74 C5         [12] 3924 	mov	a,#__str_15
   0CEE C0 E0         [24] 3925 	push	acc
   0CF0 74 19         [12] 3926 	mov	a,#(__str_15 >> 8)
   0CF2 C0 E0         [24] 3927 	push	acc
   0CF4 74 80         [12] 3928 	mov	a,#0x80
   0CF6 C0 E0         [24] 3929 	push	acc
   0CF8 12 03 EA      [24] 3930 	lcall	_printf
   0CFB E5 81         [12] 3931 	mov	a,sp
   0CFD 24 FB         [12] 3932 	add	a,#0xfb
   0CFF F5 81         [12] 3933 	mov	sp,a
                     0C3A  3934 	C$cc1000.h$568$1$131 ==.
                           3935 ;	C:\Workspace\WSN_MS-4\/cc1000.h:568: printf("PA_POW: 0x%x \n", CC1000_ReadFromRegister(CC1000_PA_POW));      // 0x0F
   0D01 75 82 0B      [24] 3936 	mov	dpl,#0x0B
   0D04 12 06 CF      [24] 3937 	lcall	_CC1000_ReadFromRegister
   0D07 AF 82         [24] 3938 	mov	r7,dpl
   0D09 7E 00         [12] 3939 	mov	r6,#0x00
   0D0B C0 07         [24] 3940 	push	ar7
   0D0D C0 06         [24] 3941 	push	ar6
   0D0F 74 D7         [12] 3942 	mov	a,#__str_16
   0D11 C0 E0         [24] 3943 	push	acc
   0D13 74 19         [12] 3944 	mov	a,#(__str_16 >> 8)
   0D15 C0 E0         [24] 3945 	push	acc
   0D17 74 80         [12] 3946 	mov	a,#0x80
   0D19 C0 E0         [24] 3947 	push	acc
   0D1B 12 03 EA      [24] 3948 	lcall	_printf
   0D1E E5 81         [12] 3949 	mov	a,sp
   0D20 24 FB         [12] 3950 	add	a,#0xfb
   0D22 F5 81         [12] 3951 	mov	sp,a
                     0C5D  3952 	C$cc1000.h$569$1$131 ==.
                           3953 ;	C:\Workspace\WSN_MS-4\/cc1000.h:569: printf("PLL: 0x%x \n", CC1000_ReadFromRegister(CC1000_PLL));            // 0x10 
   0D24 75 82 0C      [24] 3954 	mov	dpl,#0x0C
   0D27 12 06 CF      [24] 3955 	lcall	_CC1000_ReadFromRegister
   0D2A AF 82         [24] 3956 	mov	r7,dpl
   0D2C 7E 00         [12] 3957 	mov	r6,#0x00
   0D2E C0 07         [24] 3958 	push	ar7
   0D30 C0 06         [24] 3959 	push	ar6
   0D32 74 E6         [12] 3960 	mov	a,#__str_17
   0D34 C0 E0         [24] 3961 	push	acc
   0D36 74 19         [12] 3962 	mov	a,#(__str_17 >> 8)
   0D38 C0 E0         [24] 3963 	push	acc
   0D3A 74 80         [12] 3964 	mov	a,#0x80
   0D3C C0 E0         [24] 3965 	push	acc
   0D3E 12 03 EA      [24] 3966 	lcall	_printf
   0D41 E5 81         [12] 3967 	mov	a,sp
   0D43 24 FB         [12] 3968 	add	a,#0xfb
   0D45 F5 81         [12] 3969 	mov	sp,a
                     0C80  3970 	C$cc1000.h$570$1$131 ==.
                           3971 ;	C:\Workspace\WSN_MS-4\/cc1000.h:570: printf("LOCK: 0x%x \n", CC1000_ReadFromRegister(CC1000_LOCK));          // 0x00 
   0D47 75 82 0D      [24] 3972 	mov	dpl,#0x0D
   0D4A 12 06 CF      [24] 3973 	lcall	_CC1000_ReadFromRegister
   0D4D AF 82         [24] 3974 	mov	r7,dpl
   0D4F 7E 00         [12] 3975 	mov	r6,#0x00
   0D51 C0 07         [24] 3976 	push	ar7
   0D53 C0 06         [24] 3977 	push	ar6
   0D55 74 F2         [12] 3978 	mov	a,#__str_18
   0D57 C0 E0         [24] 3979 	push	acc
   0D59 74 19         [12] 3980 	mov	a,#(__str_18 >> 8)
   0D5B C0 E0         [24] 3981 	push	acc
   0D5D 74 80         [12] 3982 	mov	a,#0x80
   0D5F C0 E0         [24] 3983 	push	acc
   0D61 12 03 EA      [24] 3984 	lcall	_printf
   0D64 E5 81         [12] 3985 	mov	a,sp
   0D66 24 FB         [12] 3986 	add	a,#0xfb
   0D68 F5 81         [12] 3987 	mov	sp,a
                     0CA3  3988 	C$cc1000.h$571$1$131 ==.
                           3989 ;	C:\Workspace\WSN_MS-4\/cc1000.h:571: printf("CAL: 0x%x \n", CC1000_ReadFromRegister(CC1000_CAL));            // 0x05
   0D6A 75 82 0E      [24] 3990 	mov	dpl,#0x0E
   0D6D 12 06 CF      [24] 3991 	lcall	_CC1000_ReadFromRegister
   0D70 AF 82         [24] 3992 	mov	r7,dpl
   0D72 7E 00         [12] 3993 	mov	r6,#0x00
   0D74 C0 07         [24] 3994 	push	ar7
   0D76 C0 06         [24] 3995 	push	ar6
   0D78 74 FF         [12] 3996 	mov	a,#__str_19
   0D7A C0 E0         [24] 3997 	push	acc
   0D7C 74 19         [12] 3998 	mov	a,#(__str_19 >> 8)
   0D7E C0 E0         [24] 3999 	push	acc
   0D80 74 80         [12] 4000 	mov	a,#0x80
   0D82 C0 E0         [24] 4001 	push	acc
   0D84 12 03 EA      [24] 4002 	lcall	_printf
   0D87 E5 81         [12] 4003 	mov	a,sp
   0D89 24 FB         [12] 4004 	add	a,#0xfb
   0D8B F5 81         [12] 4005 	mov	sp,a
                     0CC6  4006 	C$cc1000.h$572$1$131 ==.
                           4007 ;	C:\Workspace\WSN_MS-4\/cc1000.h:572: printf("MODEM2: 0x%x \n", CC1000_ReadFromRegister(CC1000_MODEM2));      // 0x96
   0D8D 75 82 0F      [24] 4008 	mov	dpl,#0x0F
   0D90 12 06 CF      [24] 4009 	lcall	_CC1000_ReadFromRegister
   0D93 AF 82         [24] 4010 	mov	r7,dpl
   0D95 7E 00         [12] 4011 	mov	r6,#0x00
   0D97 C0 07         [24] 4012 	push	ar7
   0D99 C0 06         [24] 4013 	push	ar6
   0D9B 74 0B         [12] 4014 	mov	a,#__str_20
   0D9D C0 E0         [24] 4015 	push	acc
   0D9F 74 1A         [12] 4016 	mov	a,#(__str_20 >> 8)
   0DA1 C0 E0         [24] 4017 	push	acc
   0DA3 74 80         [12] 4018 	mov	a,#0x80
   0DA5 C0 E0         [24] 4019 	push	acc
   0DA7 12 03 EA      [24] 4020 	lcall	_printf
   0DAA E5 81         [12] 4021 	mov	a,sp
   0DAC 24 FB         [12] 4022 	add	a,#0xfb
   0DAE F5 81         [12] 4023 	mov	sp,a
                     0CE9  4024 	C$cc1000.h$573$1$131 ==.
                           4025 ;	C:\Workspace\WSN_MS-4\/cc1000.h:573: printf("MODEM1: 0x%x \n", CC1000_ReadFromRegister(CC1000_MODEM1));      // 0x67
   0DB0 75 82 10      [24] 4026 	mov	dpl,#0x10
   0DB3 12 06 CF      [24] 4027 	lcall	_CC1000_ReadFromRegister
   0DB6 AF 82         [24] 4028 	mov	r7,dpl
   0DB8 7E 00         [12] 4029 	mov	r6,#0x00
   0DBA C0 07         [24] 4030 	push	ar7
   0DBC C0 06         [24] 4031 	push	ar6
   0DBE 74 1A         [12] 4032 	mov	a,#__str_21
   0DC0 C0 E0         [24] 4033 	push	acc
   0DC2 74 1A         [12] 4034 	mov	a,#(__str_21 >> 8)
   0DC4 C0 E0         [24] 4035 	push	acc
   0DC6 74 80         [12] 4036 	mov	a,#0x80
   0DC8 C0 E0         [24] 4037 	push	acc
   0DCA 12 03 EA      [24] 4038 	lcall	_printf
   0DCD E5 81         [12] 4039 	mov	a,sp
   0DCF 24 FB         [12] 4040 	add	a,#0xfb
   0DD1 F5 81         [12] 4041 	mov	sp,a
                     0D0C  4042 	C$cc1000.h$574$1$131 ==.
                           4043 ;	C:\Workspace\WSN_MS-4\/cc1000.h:574: printf("MODEM0: 0x%x \n", CC1000_ReadFromRegister(CC1000_MODEM0));      // 0x24
   0DD3 75 82 11      [24] 4044 	mov	dpl,#0x11
   0DD6 12 06 CF      [24] 4045 	lcall	_CC1000_ReadFromRegister
   0DD9 AF 82         [24] 4046 	mov	r7,dpl
   0DDB 7E 00         [12] 4047 	mov	r6,#0x00
   0DDD C0 07         [24] 4048 	push	ar7
   0DDF C0 06         [24] 4049 	push	ar6
   0DE1 74 29         [12] 4050 	mov	a,#__str_22
   0DE3 C0 E0         [24] 4051 	push	acc
   0DE5 74 1A         [12] 4052 	mov	a,#(__str_22 >> 8)
   0DE7 C0 E0         [24] 4053 	push	acc
   0DE9 74 80         [12] 4054 	mov	a,#0x80
   0DEB C0 E0         [24] 4055 	push	acc
   0DED 12 03 EA      [24] 4056 	lcall	_printf
   0DF0 E5 81         [12] 4057 	mov	a,sp
   0DF2 24 FB         [12] 4058 	add	a,#0xfb
   0DF4 F5 81         [12] 4059 	mov	sp,a
                     0D2F  4060 	C$cc1000.h$575$1$131 ==.
                           4061 ;	C:\Workspace\WSN_MS-4\/cc1000.h:575: printf("MATCH: 0x%x \n", CC1000_ReadFromRegister(CC1000_MATCH));        // 0x00
   0DF6 75 82 12      [24] 4062 	mov	dpl,#0x12
   0DF9 12 06 CF      [24] 4063 	lcall	_CC1000_ReadFromRegister
   0DFC AF 82         [24] 4064 	mov	r7,dpl
   0DFE 7E 00         [12] 4065 	mov	r6,#0x00
   0E00 C0 07         [24] 4066 	push	ar7
   0E02 C0 06         [24] 4067 	push	ar6
   0E04 74 38         [12] 4068 	mov	a,#__str_23
   0E06 C0 E0         [24] 4069 	push	acc
   0E08 74 1A         [12] 4070 	mov	a,#(__str_23 >> 8)
   0E0A C0 E0         [24] 4071 	push	acc
   0E0C 74 80         [12] 4072 	mov	a,#0x80
   0E0E C0 E0         [24] 4073 	push	acc
   0E10 12 03 EA      [24] 4074 	lcall	_printf
   0E13 E5 81         [12] 4075 	mov	a,sp
   0E15 24 FB         [12] 4076 	add	a,#0xfb
   0E17 F5 81         [12] 4077 	mov	sp,a
                     0D52  4078 	C$cc1000.h$576$1$131 ==.
                           4079 ;	C:\Workspace\WSN_MS-4\/cc1000.h:576: printf("FSCTRL: 0x%x \n", CC1000_ReadFromRegister(CC1000_FSCTRL));      // 0x01
   0E19 75 82 13      [24] 4080 	mov	dpl,#0x13
   0E1C 12 06 CF      [24] 4081 	lcall	_CC1000_ReadFromRegister
   0E1F AF 82         [24] 4082 	mov	r7,dpl
   0E21 7E 00         [12] 4083 	mov	r6,#0x00
   0E23 C0 07         [24] 4084 	push	ar7
   0E25 C0 06         [24] 4085 	push	ar6
   0E27 74 46         [12] 4086 	mov	a,#__str_24
   0E29 C0 E0         [24] 4087 	push	acc
   0E2B 74 1A         [12] 4088 	mov	a,#(__str_24 >> 8)
   0E2D C0 E0         [24] 4089 	push	acc
   0E2F 74 80         [12] 4090 	mov	a,#0x80
   0E31 C0 E0         [24] 4091 	push	acc
   0E33 12 03 EA      [24] 4092 	lcall	_printf
   0E36 E5 81         [12] 4093 	mov	a,sp
   0E38 24 FB         [12] 4094 	add	a,#0xfb
   0E3A F5 81         [12] 4095 	mov	sp,a
                     0D75  4096 	C$cc1000.h$577$1$131 ==.
                           4097 ;	C:\Workspace\WSN_MS-4\/cc1000.h:577: printf("PRESCALER: 0x%x \n", CC1000_ReadFromRegister(CC1000_PRESCALER));// 0x00 
   0E3C 75 82 1C      [24] 4098 	mov	dpl,#0x1C
   0E3F 12 06 CF      [24] 4099 	lcall	_CC1000_ReadFromRegister
   0E42 AF 82         [24] 4100 	mov	r7,dpl
   0E44 7E 00         [12] 4101 	mov	r6,#0x00
   0E46 C0 07         [24] 4102 	push	ar7
   0E48 C0 06         [24] 4103 	push	ar6
   0E4A 74 55         [12] 4104 	mov	a,#__str_25
   0E4C C0 E0         [24] 4105 	push	acc
   0E4E 74 1A         [12] 4106 	mov	a,#(__str_25 >> 8)
   0E50 C0 E0         [24] 4107 	push	acc
   0E52 74 80         [12] 4108 	mov	a,#0x80
   0E54 C0 E0         [24] 4109 	push	acc
   0E56 12 03 EA      [24] 4110 	lcall	_printf
   0E59 E5 81         [12] 4111 	mov	a,sp
   0E5B 24 FB         [12] 4112 	add	a,#0xfb
   0E5D F5 81         [12] 4113 	mov	sp,a
                     0D98  4114 	C$cc1000.h$578$1$131 ==.
                           4115 ;	C:\Workspace\WSN_MS-4\/cc1000.h:578: printf("TEST6: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST6));        // 0x10
   0E5F 75 82 40      [24] 4116 	mov	dpl,#0x40
   0E62 12 06 CF      [24] 4117 	lcall	_CC1000_ReadFromRegister
   0E65 AF 82         [24] 4118 	mov	r7,dpl
   0E67 7E 00         [12] 4119 	mov	r6,#0x00
   0E69 C0 07         [24] 4120 	push	ar7
   0E6B C0 06         [24] 4121 	push	ar6
   0E6D 74 67         [12] 4122 	mov	a,#__str_26
   0E6F C0 E0         [24] 4123 	push	acc
   0E71 74 1A         [12] 4124 	mov	a,#(__str_26 >> 8)
   0E73 C0 E0         [24] 4125 	push	acc
   0E75 74 80         [12] 4126 	mov	a,#0x80
   0E77 C0 E0         [24] 4127 	push	acc
   0E79 12 03 EA      [24] 4128 	lcall	_printf
   0E7C E5 81         [12] 4129 	mov	a,sp
   0E7E 24 FB         [12] 4130 	add	a,#0xfb
   0E80 F5 81         [12] 4131 	mov	sp,a
                     0DBB  4132 	C$cc1000.h$579$1$131 ==.
                           4133 ;	C:\Workspace\WSN_MS-4\/cc1000.h:579: printf("TEST5: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST5));        // 0x08
   0E82 75 82 41      [24] 4134 	mov	dpl,#0x41
   0E85 12 06 CF      [24] 4135 	lcall	_CC1000_ReadFromRegister
   0E88 AF 82         [24] 4136 	mov	r7,dpl
   0E8A 7E 00         [12] 4137 	mov	r6,#0x00
   0E8C C0 07         [24] 4138 	push	ar7
   0E8E C0 06         [24] 4139 	push	ar6
   0E90 74 75         [12] 4140 	mov	a,#__str_27
   0E92 C0 E0         [24] 4141 	push	acc
   0E94 74 1A         [12] 4142 	mov	a,#(__str_27 >> 8)
   0E96 C0 E0         [24] 4143 	push	acc
   0E98 74 80         [12] 4144 	mov	a,#0x80
   0E9A C0 E0         [24] 4145 	push	acc
   0E9C 12 03 EA      [24] 4146 	lcall	_printf
   0E9F E5 81         [12] 4147 	mov	a,sp
   0EA1 24 FB         [12] 4148 	add	a,#0xfb
   0EA3 F5 81         [12] 4149 	mov	sp,a
                     0DDE  4150 	C$cc1000.h$580$1$131 ==.
                           4151 ;	C:\Workspace\WSN_MS-4\/cc1000.h:580: printf("TEST4: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST4));        // 0x25
   0EA5 75 82 42      [24] 4152 	mov	dpl,#0x42
   0EA8 12 06 CF      [24] 4153 	lcall	_CC1000_ReadFromRegister
   0EAB AF 82         [24] 4154 	mov	r7,dpl
   0EAD 7E 00         [12] 4155 	mov	r6,#0x00
   0EAF C0 07         [24] 4156 	push	ar7
   0EB1 C0 06         [24] 4157 	push	ar6
   0EB3 74 83         [12] 4158 	mov	a,#__str_28
   0EB5 C0 E0         [24] 4159 	push	acc
   0EB7 74 1A         [12] 4160 	mov	a,#(__str_28 >> 8)
   0EB9 C0 E0         [24] 4161 	push	acc
   0EBB 74 80         [12] 4162 	mov	a,#0x80
   0EBD C0 E0         [24] 4163 	push	acc
   0EBF 12 03 EA      [24] 4164 	lcall	_printf
   0EC2 E5 81         [12] 4165 	mov	a,sp
   0EC4 24 FB         [12] 4166 	add	a,#0xfb
   0EC6 F5 81         [12] 4167 	mov	sp,a
                     0E01  4168 	C$cc1000.h$581$1$131 ==.
                           4169 ;	C:\Workspace\WSN_MS-4\/cc1000.h:581: printf("TEST3: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST3));        // 0x04
   0EC8 75 82 43      [24] 4170 	mov	dpl,#0x43
   0ECB 12 06 CF      [24] 4171 	lcall	_CC1000_ReadFromRegister
   0ECE AF 82         [24] 4172 	mov	r7,dpl
   0ED0 7E 00         [12] 4173 	mov	r6,#0x00
   0ED2 C0 07         [24] 4174 	push	ar7
   0ED4 C0 06         [24] 4175 	push	ar6
   0ED6 74 91         [12] 4176 	mov	a,#__str_29
   0ED8 C0 E0         [24] 4177 	push	acc
   0EDA 74 1A         [12] 4178 	mov	a,#(__str_29 >> 8)
   0EDC C0 E0         [24] 4179 	push	acc
   0EDE 74 80         [12] 4180 	mov	a,#0x80
   0EE0 C0 E0         [24] 4181 	push	acc
   0EE2 12 03 EA      [24] 4182 	lcall	_printf
   0EE5 E5 81         [12] 4183 	mov	a,sp
   0EE7 24 FB         [12] 4184 	add	a,#0xfb
   0EE9 F5 81         [12] 4185 	mov	sp,a
                     0E24  4186 	C$cc1000.h$582$1$131 ==.
                           4187 ;	C:\Workspace\WSN_MS-4\/cc1000.h:582: printf("TEST2: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST2));        // 0x00
   0EEB 75 82 44      [24] 4188 	mov	dpl,#0x44
   0EEE 12 06 CF      [24] 4189 	lcall	_CC1000_ReadFromRegister
   0EF1 AF 82         [24] 4190 	mov	r7,dpl
   0EF3 7E 00         [12] 4191 	mov	r6,#0x00
   0EF5 C0 07         [24] 4192 	push	ar7
   0EF7 C0 06         [24] 4193 	push	ar6
   0EF9 74 9F         [12] 4194 	mov	a,#__str_30
   0EFB C0 E0         [24] 4195 	push	acc
   0EFD 74 1A         [12] 4196 	mov	a,#(__str_30 >> 8)
   0EFF C0 E0         [24] 4197 	push	acc
   0F01 74 80         [12] 4198 	mov	a,#0x80
   0F03 C0 E0         [24] 4199 	push	acc
   0F05 12 03 EA      [24] 4200 	lcall	_printf
   0F08 E5 81         [12] 4201 	mov	a,sp
   0F0A 24 FB         [12] 4202 	add	a,#0xfb
   0F0C F5 81         [12] 4203 	mov	sp,a
                     0E47  4204 	C$cc1000.h$583$1$131 ==.
                           4205 ;	C:\Workspace\WSN_MS-4\/cc1000.h:583: printf("TEST1: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST1));        // 0x00 
   0F0E 75 82 45      [24] 4206 	mov	dpl,#0x45
   0F11 12 06 CF      [24] 4207 	lcall	_CC1000_ReadFromRegister
   0F14 AF 82         [24] 4208 	mov	r7,dpl
   0F16 7E 00         [12] 4209 	mov	r6,#0x00
   0F18 C0 07         [24] 4210 	push	ar7
   0F1A C0 06         [24] 4211 	push	ar6
   0F1C 74 AD         [12] 4212 	mov	a,#__str_31
   0F1E C0 E0         [24] 4213 	push	acc
   0F20 74 1A         [12] 4214 	mov	a,#(__str_31 >> 8)
   0F22 C0 E0         [24] 4215 	push	acc
   0F24 74 80         [12] 4216 	mov	a,#0x80
   0F26 C0 E0         [24] 4217 	push	acc
   0F28 12 03 EA      [24] 4218 	lcall	_printf
   0F2B E5 81         [12] 4219 	mov	a,sp
   0F2D 24 FB         [12] 4220 	add	a,#0xfb
   0F2F F5 81         [12] 4221 	mov	sp,a
                     0E6A  4222 	C$cc1000.h$584$1$131 ==.
                           4223 ;	C:\Workspace\WSN_MS-4\/cc1000.h:584: printf("TEST0: 0x%x \n", CC1000_ReadFromRegister(CC1000_TEST0));        // 0x00
   0F31 75 82 46      [24] 4224 	mov	dpl,#0x46
   0F34 12 06 CF      [24] 4225 	lcall	_CC1000_ReadFromRegister
   0F37 AF 82         [24] 4226 	mov	r7,dpl
   0F39 7E 00         [12] 4227 	mov	r6,#0x00
   0F3B C0 07         [24] 4228 	push	ar7
   0F3D C0 06         [24] 4229 	push	ar6
   0F3F 74 BB         [12] 4230 	mov	a,#__str_32
   0F41 C0 E0         [24] 4231 	push	acc
   0F43 74 1A         [12] 4232 	mov	a,#(__str_32 >> 8)
   0F45 C0 E0         [24] 4233 	push	acc
   0F47 74 80         [12] 4234 	mov	a,#0x80
   0F49 C0 E0         [24] 4235 	push	acc
   0F4B 12 03 EA      [24] 4236 	lcall	_printf
   0F4E E5 81         [12] 4237 	mov	a,sp
   0F50 24 FB         [12] 4238 	add	a,#0xfb
   0F52 F5 81         [12] 4239 	mov	sp,a
                     0E8D  4240 	C$cc1000.h$585$1$131 ==.
                     0E8D  4241 	XG$CC1000_DisplayAllRegisters$0$0 ==.
   0F54 22            [24] 4242 	ret
                           4243 ;------------------------------------------------------------
                           4244 ;Allocation info for local variables in function 'CC1000_Sync'
                           4245 ;------------------------------------------------------------
                     0E8E  4246 	G$CC1000_Sync$0$0 ==.
                     0E8E  4247 	C$cc1000.h$593$1$131 ==.
                           4248 ;	C:\Workspace\WSN_MS-4\/cc1000.h:593: U8 CC1000_Sync(void)
                           4249 ;	-----------------------------------------
                           4250 ;	 function CC1000_Sync
                           4251 ;	-----------------------------------------
   0F55                    4252 _CC1000_Sync:
                     0E8E  4253 	C$cc1000.h$595$1$133 ==.
                           4254 ;	C:\Workspace\WSN_MS-4\/cc1000.h:595: PREAMBULE >>= 1;
   0F55 E5 34         [12] 4255 	mov	a,(_PREAMBULE + 3)
   0F57 C3            [12] 4256 	clr	c
   0F58 13            [12] 4257 	rrc	a
   0F59 F5 34         [12] 4258 	mov	(_PREAMBULE + 3),a
   0F5B E5 33         [12] 4259 	mov	a,(_PREAMBULE + 2)
   0F5D 13            [12] 4260 	rrc	a
   0F5E F5 33         [12] 4261 	mov	(_PREAMBULE + 2),a
   0F60 E5 32         [12] 4262 	mov	a,(_PREAMBULE + 1)
   0F62 13            [12] 4263 	rrc	a
   0F63 F5 32         [12] 4264 	mov	(_PREAMBULE + 1),a
   0F65 E5 31         [12] 4265 	mov	a,_PREAMBULE
   0F67 13            [12] 4266 	rrc	a
   0F68 F5 31         [12] 4267 	mov	_PREAMBULE,a
                     0EA3  4268 	C$cc1000.h$596$1$133 ==.
                           4269 ;	C:\Workspace\WSN_MS-4\/cc1000.h:596: if (!BitReceive())          // Odbi�r bit�w z negacj�
   0F6A 12 07 3F      [24] 4270 	lcall	_BitReceive
   0F6D E5 82         [12] 4271 	mov	a,dpl
   0F6F 70 0B         [24] 4272 	jnz	00102$
                     0EAA  4273 	C$cc1000.h$597$1$133 ==.
                           4274 ;	C:\Workspace\WSN_MS-4\/cc1000.h:597: PREAMBULE |= 0x80000000;
   0F71 E5 31         [12] 4275 	mov	a,_PREAMBULE
   0F73 E5 32         [12] 4276 	mov	a,(_PREAMBULE + 1)
   0F75 E5 33         [12] 4277 	mov	a,(_PREAMBULE + 2)
   0F77 43 34 80      [24] 4278 	orl	(_PREAMBULE + 3),#0x80
   0F7A 80 09         [24] 4279 	sjmp	00103$
   0F7C                    4280 00102$:
                     0EB5  4281 	C$cc1000.h$599$1$133 ==.
                           4282 ;	C:\Workspace\WSN_MS-4\/cc1000.h:599: PREAMBULE &= 0x7FFFFFFF; 
   0F7C E5 31         [12] 4283 	mov	a,_PREAMBULE
   0F7E E5 32         [12] 4284 	mov	a,(_PREAMBULE + 1)
   0F80 E5 33         [12] 4285 	mov	a,(_PREAMBULE + 2)
   0F82 53 34 7F      [24] 4286 	anl	(_PREAMBULE + 3),#0x7F
   0F85                    4287 00103$:
                     0EBE  4288 	C$cc1000.h$600$1$133 ==.
                           4289 ;	C:\Workspace\WSN_MS-4\/cc1000.h:600: if (PREAMBULE == SYNC_SEQ)
   0F85 74 55         [12] 4290 	mov	a,#0x55
   0F87 B5 31 14      [24] 4291 	cjne	a,_PREAMBULE,00105$
   0F8A 74 55         [12] 4292 	mov	a,#0x55
   0F8C B5 32 0F      [24] 4293 	cjne	a,(_PREAMBULE + 1),00105$
   0F8F 74 55         [12] 4294 	mov	a,#0x55
   0F91 B5 33 0A      [24] 4295 	cjne	a,(_PREAMBULE + 2),00105$
   0F94 74 71         [12] 4296 	mov	a,#0x71
   0F96 B5 34 05      [24] 4297 	cjne	a,(_PREAMBULE + 3),00105$
                     0ED2  4298 	C$cc1000.h$601$1$133 ==.
                           4299 ;	C:\Workspace\WSN_MS-4\/cc1000.h:601: return 1;               // Jest synchronizacja 
   0F99 75 82 01      [24] 4300 	mov	dpl,#0x01
   0F9C 80 03         [24] 4301 	sjmp	00107$
   0F9E                    4302 00105$:
                     0ED7  4303 	C$cc1000.h$602$1$133 ==.
                           4304 ;	C:\Workspace\WSN_MS-4\/cc1000.h:602: else return 0;              // Brak synchronizacji
   0F9E 75 82 00      [24] 4305 	mov	dpl,#0x00
   0FA1                    4306 00107$:
                     0EDA  4307 	C$cc1000.h$603$1$133 ==.
                     0EDA  4308 	XG$CC1000_Sync$0$0 ==.
   0FA1 22            [24] 4309 	ret
                           4310 ;------------------------------------------------------------
                           4311 ;Allocation info for local variables in function 'clearFrame'
                           4312 ;------------------------------------------------------------
                     0EDB  4313 	G$clearFrame$0$0 ==.
                     0EDB  4314 	C$wsn.h$35$1$133 ==.
                           4315 ;	C:\Workspace\WSN_MS-4\/wsn.h:35: void clearFrame()
                           4316 ;	-----------------------------------------
                           4317 ;	 function clearFrame
                           4318 ;	-----------------------------------------
   0FA2                    4319 _clearFrame:
                     0EDB  4320 	C$wsn.h$37$1$138 ==.
                           4321 ;	C:\Workspace\WSN_MS-4\/wsn.h:37: for (i=0; i<19; i++)
   0FA2 E4            [12] 4322 	clr	a
   0FA3 F5 51         [12] 4323 	mov	_i,a
   0FA5 F5 52         [12] 4324 	mov	(_i + 1),a
   0FA7                    4325 00102$:
                     0EE0  4326 	C$wsn.h$38$1$138 ==.
                           4327 ;	C:\Workspace\WSN_MS-4\/wsn.h:38: frame[i] = 0x00;
   0FA7 E5 51         [12] 4328 	mov	a,_i
   0FA9 24 35         [12] 4329 	add	a,#_frame
   0FAB F8            [12] 4330 	mov	r0,a
   0FAC 76 00         [12] 4331 	mov	@r0,#0x00
                     0EE7  4332 	C$wsn.h$37$1$138 ==.
                           4333 ;	C:\Workspace\WSN_MS-4\/wsn.h:37: for (i=0; i<19; i++)
   0FAE 05 51         [12] 4334 	inc	_i
   0FB0 E4            [12] 4335 	clr	a
   0FB1 B5 51 02      [24] 4336 	cjne	a,_i,00109$
   0FB4 05 52         [12] 4337 	inc	(_i + 1)
   0FB6                    4338 00109$:
   0FB6 C3            [12] 4339 	clr	c
   0FB7 E5 51         [12] 4340 	mov	a,_i
   0FB9 94 13         [12] 4341 	subb	a,#0x13
   0FBB E5 52         [12] 4342 	mov	a,(_i + 1)
   0FBD 64 80         [12] 4343 	xrl	a,#0x80
   0FBF 94 80         [12] 4344 	subb	a,#0x80
   0FC1 40 E4         [24] 4345 	jc	00102$
                     0EFC  4346 	C$wsn.h$39$1$138 ==.
                     0EFC  4347 	XG$clearFrame$0$0 ==.
   0FC3 22            [24] 4348 	ret
                           4349 ;------------------------------------------------------------
                           4350 ;Allocation info for local variables in function 'sendFrame'
                           4351 ;------------------------------------------------------------
                           4352 ;frameType                 Allocated to registers r7 
                           4353 ;------------------------------------------------------------
                     0EFD  4354 	G$sendFrame$0$0 ==.
                     0EFD  4355 	C$wsn.h$41$1$138 ==.
                           4356 ;	C:\Workspace\WSN_MS-4\/wsn.h:41: void sendFrame(U8 frameType)
                           4357 ;	-----------------------------------------
                           4358 ;	 function sendFrame
                           4359 ;	-----------------------------------------
   0FC4                    4360 _sendFrame:
   0FC4 AF 82         [24] 4361 	mov	r7,dpl
                     0EFF  4362 	C$wsn.h$43$1$140 ==.
                           4363 ;	C:\Workspace\WSN_MS-4\/wsn.h:43: CC1000_WakeUpToTX(CC1000_TX_CURRENT);
   0FC6 75 82 F3      [24] 4364 	mov	dpl,#0xF3
   0FC9 C0 07         [24] 4365 	push	ar7
   0FCB 12 09 A8      [24] 4366 	lcall	_CC1000_WakeUpToTX
                     0F07  4367 	C$wsn.h$44$1$140 ==.
                           4368 ;	C:\Workspace\WSN_MS-4\/wsn.h:44: CC1000_SetupTX(CC1000_TX_CURRENT);
   0FCE 75 82 F3      [24] 4369 	mov	dpl,#0xF3
   0FD1 12 08 D1      [24] 4370 	lcall	_CC1000_SetupTX
   0FD4 D0 07         [24] 4371 	pop	ar7
                     0F0F  4372 	C$wsn.h$45$1$140 ==.
                           4373 ;	C:\Workspace\WSN_MS-4\/wsn.h:45: P0MDOUT |= 0x08;
   0FD6 43 A4 08      [24] 4374 	orl	_P0MDOUT,#0x08
                     0F12  4375 	C$wsn.h$46$1$140 ==.
                           4376 ;	C:\Workspace\WSN_MS-4\/wsn.h:46: for (i = 0; i < CC1000_PREAMBULE_LENGTH; ++i)
   0FD9 E4            [12] 4377 	clr	a
   0FDA F5 51         [12] 4378 	mov	_i,a
   0FDC F5 52         [12] 4379 	mov	(_i + 1),a
   0FDE                    4380 00102$:
                     0F17  4381 	C$wsn.h$47$1$140 ==.
                           4382 ;	C:\Workspace\WSN_MS-4\/wsn.h:47: CC1000_SendByte(CC1000_PREAMBULE);
   0FDE 75 82 55      [24] 4383 	mov	dpl,#0x55
   0FE1 C0 07         [24] 4384 	push	ar7
   0FE3 12 07 82      [24] 4385 	lcall	_CC1000_SendByte
   0FE6 D0 07         [24] 4386 	pop	ar7
                     0F21  4387 	C$wsn.h$46$1$140 ==.
                           4388 ;	C:\Workspace\WSN_MS-4\/wsn.h:46: for (i = 0; i < CC1000_PREAMBULE_LENGTH; ++i)
   0FE8 05 51         [12] 4389 	inc	_i
   0FEA E4            [12] 4390 	clr	a
   0FEB B5 51 02      [24] 4391 	cjne	a,_i,00112$
   0FEE 05 52         [12] 4392 	inc	(_i + 1)
   0FF0                    4393 00112$:
   0FF0 C3            [12] 4394 	clr	c
   0FF1 E5 51         [12] 4395 	mov	a,_i
   0FF3 94 20         [12] 4396 	subb	a,#0x20
   0FF5 E5 52         [12] 4397 	mov	a,(_i + 1)
   0FF7 64 80         [12] 4398 	xrl	a,#0x80
   0FF9 94 80         [12] 4399 	subb	a,#0x80
   0FFB 40 E1         [24] 4400 	jc	00102$
                     0F36  4401 	C$wsn.h$49$1$140 ==.
                           4402 ;	C:\Workspace\WSN_MS-4\/wsn.h:49: CC1000_SendByte(CC1000_SYNC_BYTE);
   0FFD 75 82 71      [24] 4403 	mov	dpl,#0x71
   1000 C0 07         [24] 4404 	push	ar7
   1002 12 07 82      [24] 4405 	lcall	_CC1000_SendByte
                     0F3E  4406 	C$wsn.h$50$1$140 ==.
                           4407 ;	C:\Workspace\WSN_MS-4\/wsn.h:50: CC1000_SendByte(NI);
   1005 75 82 B4      [24] 4408 	mov	dpl,#0xB4
   1008 12 07 82      [24] 4409 	lcall	_CC1000_SendByte
                     0F44  4410 	C$wsn.h$51$1$140 ==.
                           4411 ;	C:\Workspace\WSN_MS-4\/wsn.h:51: CC1000_SendByte(slaveAddress);
   100B 85 49 82      [24] 4412 	mov	dpl,_slaveAddress
   100E 12 07 82      [24] 4413 	lcall	_CC1000_SendByte
   1011 D0 07         [24] 4414 	pop	ar7
                     0F4C  4415 	C$wsn.h$52$1$140 ==.
                           4416 ;	C:\Workspace\WSN_MS-4\/wsn.h:52: CC1000_SendByte(frameType);
   1013 8F 82         [24] 4417 	mov	dpl,r7
   1015 C0 07         [24] 4418 	push	ar7
   1017 12 07 82      [24] 4419 	lcall	_CC1000_SendByte
   101A D0 07         [24] 4420 	pop	ar7
                     0F55  4421 	C$wsn.h$54$1$140 ==.
                           4422 ;	C:\Workspace\WSN_MS-4\/wsn.h:54: printf("\nN=%x F=%x SI=%x ", NI, frameType, slaveAddress);
   101C 7E 00         [12] 4423 	mov	r6,#0x00
   101E C0 49         [24] 4424 	push	_slaveAddress
   1020 C0 4A         [24] 4425 	push	(_slaveAddress + 1)
   1022 C0 07         [24] 4426 	push	ar7
   1024 C0 06         [24] 4427 	push	ar6
   1026 74 B4         [12] 4428 	mov	a,#0xB4
   1028 C0 E0         [24] 4429 	push	acc
   102A E4            [12] 4430 	clr	a
   102B C0 E0         [24] 4431 	push	acc
   102D 74 C9         [12] 4432 	mov	a,#__str_33
   102F C0 E0         [24] 4433 	push	acc
   1031 74 1A         [12] 4434 	mov	a,#(__str_33 >> 8)
   1033 C0 E0         [24] 4435 	push	acc
   1035 74 80         [12] 4436 	mov	a,#0x80
   1037 C0 E0         [24] 4437 	push	acc
   1039 12 03 EA      [24] 4438 	lcall	_printf
   103C E5 81         [12] 4439 	mov	a,sp
   103E 24 F7         [12] 4440 	add	a,#0xf7
   1040 F5 81         [12] 4441 	mov	sp,a
                     0F7B  4442 	C$wsn.h$55$1$140 ==.
                     0F7B  4443 	XG$sendFrame$0$0 ==.
   1042 22            [24] 4444 	ret
                           4445 ;------------------------------------------------------------
                           4446 ;Allocation info for local variables in function 'sendData'
                           4447 ;------------------------------------------------------------
                           4448 ;frame                     Allocated with name '_sendData_frame_1_141'
                           4449 ;------------------------------------------------------------
                     0F7C  4450 	G$sendData$0$0 ==.
                     0F7C  4451 	C$wsn.h$57$1$140 ==.
                           4452 ;	C:\Workspace\WSN_MS-4\/wsn.h:57: void sendData(U8* frame)
                           4453 ;	-----------------------------------------
                           4454 ;	 function sendData
                           4455 ;	-----------------------------------------
   1043                    4456 _sendData:
   1043 85 82 57      [24] 4457 	mov	_sendData_frame_1_141,dpl
   1046 85 83 58      [24] 4458 	mov	(_sendData_frame_1_141 + 1),dph
   1049 85 F0 59      [24] 4459 	mov	(_sendData_frame_1_141 + 2),b
                     0F85  4460 	C$wsn.h$59$1$142 ==.
                           4461 ;	C:\Workspace\WSN_MS-4\/wsn.h:59: CC1000_WakeUpToTX(CC1000_TX_CURRENT);
   104C 75 82 F3      [24] 4462 	mov	dpl,#0xF3
   104F 12 09 A8      [24] 4463 	lcall	_CC1000_WakeUpToTX
                     0F8B  4464 	C$wsn.h$60$1$142 ==.
                           4465 ;	C:\Workspace\WSN_MS-4\/wsn.h:60: CC1000_SetupTX(CC1000_TX_CURRENT);
   1052 75 82 F3      [24] 4466 	mov	dpl,#0xF3
   1055 12 08 D1      [24] 4467 	lcall	_CC1000_SetupTX
                     0F91  4468 	C$wsn.h$61$1$142 ==.
                           4469 ;	C:\Workspace\WSN_MS-4\/wsn.h:61: P0MDOUT |= 0x08;
   1058 43 A4 08      [24] 4470 	orl	_P0MDOUT,#0x08
                     0F94  4471 	C$wsn.h$62$1$142 ==.
                           4472 ;	C:\Workspace\WSN_MS-4\/wsn.h:62: for (i=0; i<CC1000_PREAMBULE_LENGTH; ++i)
   105B E4            [12] 4473 	clr	a
   105C F5 51         [12] 4474 	mov	_i,a
   105E F5 52         [12] 4475 	mov	(_i + 1),a
   1060                    4476 00104$:
                     0F99  4477 	C$wsn.h$63$1$142 ==.
                           4478 ;	C:\Workspace\WSN_MS-4\/wsn.h:63: CC1000_SendByte(CC1000_PREAMBULE);
   1060 75 82 55      [24] 4479 	mov	dpl,#0x55
   1063 12 07 82      [24] 4480 	lcall	_CC1000_SendByte
                     0F9F  4481 	C$wsn.h$62$1$142 ==.
                           4482 ;	C:\Workspace\WSN_MS-4\/wsn.h:62: for (i=0; i<CC1000_PREAMBULE_LENGTH; ++i)
   1066 05 51         [12] 4483 	inc	_i
   1068 E4            [12] 4484 	clr	a
   1069 B5 51 02      [24] 4485 	cjne	a,_i,00127$
   106C 05 52         [12] 4486 	inc	(_i + 1)
   106E                    4487 00127$:
   106E C3            [12] 4488 	clr	c
   106F E5 51         [12] 4489 	mov	a,_i
   1071 94 20         [12] 4490 	subb	a,#0x20
   1073 E5 52         [12] 4491 	mov	a,(_i + 1)
   1075 64 80         [12] 4492 	xrl	a,#0x80
   1077 94 80         [12] 4493 	subb	a,#0x80
   1079 40 E5         [24] 4494 	jc	00104$
                     0FB4  4495 	C$wsn.h$65$1$142 ==.
                           4496 ;	C:\Workspace\WSN_MS-4\/wsn.h:65: CC1000_SendByte(CC1000_SYNC_BYTE);
   107B 75 82 71      [24] 4497 	mov	dpl,#0x71
   107E 12 07 82      [24] 4498 	lcall	_CC1000_SendByte
                     0FBA  4499 	C$wsn.h$66$1$142 ==.
                           4500 ;	C:\Workspace\WSN_MS-4\/wsn.h:66: for (i=0; i<19; i++)
   1081 E4            [12] 4501 	clr	a
   1082 F5 51         [12] 4502 	mov	_i,a
   1084 F5 52         [12] 4503 	mov	(_i + 1),a
   1086                    4504 00106$:
                     0FBF  4505 	C$wsn.h$67$1$142 ==.
                           4506 ;	C:\Workspace\WSN_MS-4\/wsn.h:67: CC1000_SendByte(frame[i]);
   1086 E5 51         [12] 4507 	mov	a,_i
   1088 25 57         [12] 4508 	add	a,_sendData_frame_1_141
   108A FA            [12] 4509 	mov	r2,a
   108B E5 52         [12] 4510 	mov	a,(_i + 1)
   108D 35 58         [12] 4511 	addc	a,(_sendData_frame_1_141 + 1)
   108F FB            [12] 4512 	mov	r3,a
   1090 AC 59         [24] 4513 	mov	r4,(_sendData_frame_1_141 + 2)
   1092 8A 82         [24] 4514 	mov	dpl,r2
   1094 8B 83         [24] 4515 	mov	dph,r3
   1096 8C F0         [24] 4516 	mov	b,r4
   1098 12 18 1F      [24] 4517 	lcall	__gptrget
   109B F5 82         [12] 4518 	mov	dpl,a
   109D 12 07 82      [24] 4519 	lcall	_CC1000_SendByte
                     0FD9  4520 	C$wsn.h$66$1$142 ==.
                           4521 ;	C:\Workspace\WSN_MS-4\/wsn.h:66: for (i=0; i<19; i++)
   10A0 05 51         [12] 4522 	inc	_i
   10A2 E4            [12] 4523 	clr	a
   10A3 B5 51 02      [24] 4524 	cjne	a,_i,00129$
   10A6 05 52         [12] 4525 	inc	(_i + 1)
   10A8                    4526 00129$:
   10A8 C3            [12] 4527 	clr	c
   10A9 E5 51         [12] 4528 	mov	a,_i
   10AB 94 13         [12] 4529 	subb	a,#0x13
   10AD E5 52         [12] 4530 	mov	a,(_i + 1)
   10AF 64 80         [12] 4531 	xrl	a,#0x80
   10B1 94 80         [12] 4532 	subb	a,#0x80
   10B3 40 D1         [24] 4533 	jc	00106$
                     0FEE  4534 	C$wsn.h$69$1$142 ==.
                           4535 ;	C:\Workspace\WSN_MS-4\/wsn.h:69: printf("\nN=%x F=%x SI=%x ", frame[0], frame[1], frame[2]);
   10B5 74 02         [12] 4536 	mov	a,#0x02
   10B7 25 57         [12] 4537 	add	a,_sendData_frame_1_141
   10B9 FA            [12] 4538 	mov	r2,a
   10BA E4            [12] 4539 	clr	a
   10BB 35 58         [12] 4540 	addc	a,(_sendData_frame_1_141 + 1)
   10BD FB            [12] 4541 	mov	r3,a
   10BE AC 59         [24] 4542 	mov	r4,(_sendData_frame_1_141 + 2)
   10C0 8A 82         [24] 4543 	mov	dpl,r2
   10C2 8B 83         [24] 4544 	mov	dph,r3
   10C4 8C F0         [24] 4545 	mov	b,r4
   10C6 12 18 1F      [24] 4546 	lcall	__gptrget
   10C9 FA            [12] 4547 	mov	r2,a
   10CA 7C 00         [12] 4548 	mov	r4,#0x00
   10CC 74 01         [12] 4549 	mov	a,#0x01
   10CE 25 57         [12] 4550 	add	a,_sendData_frame_1_141
   10D0 F8            [12] 4551 	mov	r0,a
   10D1 E4            [12] 4552 	clr	a
   10D2 35 58         [12] 4553 	addc	a,(_sendData_frame_1_141 + 1)
   10D4 F9            [12] 4554 	mov	r1,a
   10D5 AB 59         [24] 4555 	mov	r3,(_sendData_frame_1_141 + 2)
   10D7 88 82         [24] 4556 	mov	dpl,r0
   10D9 89 83         [24] 4557 	mov	dph,r1
   10DB 8B F0         [24] 4558 	mov	b,r3
   10DD 12 18 1F      [24] 4559 	lcall	__gptrget
   10E0 F8            [12] 4560 	mov	r0,a
   10E1 7B 00         [12] 4561 	mov	r3,#0x00
   10E3 85 57 82      [24] 4562 	mov	dpl,_sendData_frame_1_141
   10E6 85 58 83      [24] 4563 	mov	dph,(_sendData_frame_1_141 + 1)
   10E9 85 59 F0      [24] 4564 	mov	b,(_sendData_frame_1_141 + 2)
   10EC 12 18 1F      [24] 4565 	lcall	__gptrget
   10EF F9            [12] 4566 	mov	r1,a
   10F0 7F 00         [12] 4567 	mov	r7,#0x00
   10F2 C0 02         [24] 4568 	push	ar2
   10F4 C0 04         [24] 4569 	push	ar4
   10F6 C0 00         [24] 4570 	push	ar0
   10F8 C0 03         [24] 4571 	push	ar3
   10FA C0 01         [24] 4572 	push	ar1
   10FC C0 07         [24] 4573 	push	ar7
   10FE 74 C9         [12] 4574 	mov	a,#__str_33
   1100 C0 E0         [24] 4575 	push	acc
   1102 74 1A         [12] 4576 	mov	a,#(__str_33 >> 8)
   1104 C0 E0         [24] 4577 	push	acc
   1106 74 80         [12] 4578 	mov	a,#0x80
   1108 C0 E0         [24] 4579 	push	acc
   110A 12 03 EA      [24] 4580 	lcall	_printf
   110D E5 81         [12] 4581 	mov	a,sp
   110F 24 F7         [12] 4582 	add	a,#0xf7
   1111 F5 81         [12] 4583 	mov	sp,a
                     104C  4584 	C$wsn.h$71$1$142 ==.
                           4585 ;	C:\Workspace\WSN_MS-4\/wsn.h:71: for (i=3; i<19; i++)
   1113 75 51 03      [24] 4586 	mov	_i,#0x03
   1116 75 52 00      [24] 4587 	mov	(_i + 1),#0x00
   1119                    4588 00108$:
                     1052  4589 	C$wsn.h$72$1$142 ==.
                           4590 ;	C:\Workspace\WSN_MS-4\/wsn.h:72: printf("%x", frame[i]);
   1119 E5 51         [12] 4591 	mov	a,_i
   111B 25 57         [12] 4592 	add	a,_sendData_frame_1_141
   111D FD            [12] 4593 	mov	r5,a
   111E E5 52         [12] 4594 	mov	a,(_i + 1)
   1120 35 58         [12] 4595 	addc	a,(_sendData_frame_1_141 + 1)
   1122 FE            [12] 4596 	mov	r6,a
   1123 AF 59         [24] 4597 	mov	r7,(_sendData_frame_1_141 + 2)
   1125 8D 82         [24] 4598 	mov	dpl,r5
   1127 8E 83         [24] 4599 	mov	dph,r6
   1129 8F F0         [24] 4600 	mov	b,r7
   112B 12 18 1F      [24] 4601 	lcall	__gptrget
   112E FD            [12] 4602 	mov	r5,a
   112F 7F 00         [12] 4603 	mov	r7,#0x00
   1131 C0 05         [24] 4604 	push	ar5
   1133 C0 07         [24] 4605 	push	ar7
   1135 74 DB         [12] 4606 	mov	a,#__str_34
   1137 C0 E0         [24] 4607 	push	acc
   1139 74 1A         [12] 4608 	mov	a,#(__str_34 >> 8)
   113B C0 E0         [24] 4609 	push	acc
   113D 74 80         [12] 4610 	mov	a,#0x80
   113F C0 E0         [24] 4611 	push	acc
   1141 12 03 EA      [24] 4612 	lcall	_printf
   1144 E5 81         [12] 4613 	mov	a,sp
   1146 24 FB         [12] 4614 	add	a,#0xfb
   1148 F5 81         [12] 4615 	mov	sp,a
                     1083  4616 	C$wsn.h$71$1$142 ==.
                           4617 ;	C:\Workspace\WSN_MS-4\/wsn.h:71: for (i=3; i<19; i++)
   114A 05 51         [12] 4618 	inc	_i
   114C E4            [12] 4619 	clr	a
   114D B5 51 02      [24] 4620 	cjne	a,_i,00131$
   1150 05 52         [12] 4621 	inc	(_i + 1)
   1152                    4622 00131$:
   1152 C3            [12] 4623 	clr	c
   1153 E5 51         [12] 4624 	mov	a,_i
   1155 94 13         [12] 4625 	subb	a,#0x13
   1157 E5 52         [12] 4626 	mov	a,(_i + 1)
   1159 64 80         [12] 4627 	xrl	a,#0x80
   115B 94 80         [12] 4628 	subb	a,#0x80
   115D 40 BA         [24] 4629 	jc	00108$
                     1098  4630 	C$wsn.h$73$1$142 ==.
                     1098  4631 	XG$sendData$0$0 ==.
   115F 22            [24] 4632 	ret
                           4633 ;------------------------------------------------------------
                           4634 ;Allocation info for local variables in function 'waitForFrame'
                           4635 ;------------------------------------------------------------
                           4636 ;limitAttempts             Allocated to registers r6 r7 
                           4637 ;attempt                   Allocated to registers r4 r5 
                           4638 ;------------------------------------------------------------
                     1099  4639 	G$waitForFrame$0$0 ==.
                     1099  4640 	C$wsn.h$75$1$142 ==.
                           4641 ;	C:\Workspace\WSN_MS-4\/wsn.h:75: U8 waitForFrame(int limitAttempts)
                           4642 ;	-----------------------------------------
                           4643 ;	 function waitForFrame
                           4644 ;	-----------------------------------------
   1160                    4645 _waitForFrame:
   1160 AE 82         [24] 4646 	mov	r6,dpl
   1162 AF 83         [24] 4647 	mov	r7,dph
                     109D  4648 	C$wsn.h$78$1$144 ==.
                           4649 ;	C:\Workspace\WSN_MS-4\/wsn.h:78: CC1000_WakeUpToRX(CC1000_RX_CURRENT);
   1164 75 82 8C      [24] 4650 	mov	dpl,#0x8C
   1167 C0 07         [24] 4651 	push	ar7
   1169 C0 06         [24] 4652 	push	ar6
   116B 12 09 69      [24] 4653 	lcall	_CC1000_WakeUpToRX
                     10A7  4654 	C$wsn.h$79$1$144 ==.
                           4655 ;	C:\Workspace\WSN_MS-4\/wsn.h:79: CC1000_SetupRX(CC1000_RX_CURRENT);
   116E 75 82 8C      [24] 4656 	mov	dpl,#0x8C
   1171 12 08 62      [24] 4657 	lcall	_CC1000_SetupRX
   1174 D0 06         [24] 4658 	pop	ar6
   1176 D0 07         [24] 4659 	pop	ar7
                     10B1  4660 	C$wsn.h$80$1$144 ==.
                           4661 ;	C:\Workspace\WSN_MS-4\/wsn.h:80: P0MDOUT &= ~(0x08);
   1178 AD A4         [24] 4662 	mov	r5,_P0MDOUT
   117A 74 F7         [12] 4663 	mov	a,#0xF7
   117C 5D            [12] 4664 	anl	a,r5
   117D F5 A4         [12] 4665 	mov	_P0MDOUT,a
                     10B8  4666 	C$wsn.h$81$1$144 ==.
                           4667 ;	C:\Workspace\WSN_MS-4\/wsn.h:81: DIO = 1;
   117F D2 83         [12] 4668 	setb	_DIO
                     10BA  4669 	C$wsn.h$82$1$144 ==.
                           4670 ;	C:\Workspace\WSN_MS-4\/wsn.h:82: PREAMBULE = 0;
                     10BA  4671 	C$wsn.h$83$1$144 ==.
                           4672 ;	C:\Workspace\WSN_MS-4\/wsn.h:83: while(attempt <= limitAttempts) {
   1181 E4            [12] 4673 	clr a
   1182 F5 31         [12] 4674 	mov _PREAMBULE,a
   1184 F5 32         [12] 4675 	mov (_PREAMBULE + 1),a
   1186 F5 33         [12] 4676 	mov (_PREAMBULE + 2),a
   1188 F5 34         [12] 4677 	mov (_PREAMBULE + 3),a
   118A FC            [12] 4678 	mov r4,a
   118B FD            [12] 4679 	mov r5,a
   118C                    4680 00112$:
   118C C3            [12] 4681 	clr	c
   118D EE            [12] 4682 	mov	a,r6
   118E 9C            [12] 4683 	subb	a,r4
   118F EF            [12] 4684 	mov	a,r7
   1190 64 80         [12] 4685 	xrl	a,#0x80
   1192 8D F0         [24] 4686 	mov	b,r5
   1194 63 F0 80      [24] 4687 	xrl	b,#0x80
   1197 95 F0         [12] 4688 	subb	a,b
   1199 50 03         [24] 4689 	jnc	00151$
   119B 02 12 8E      [24] 4690 	ljmp	00114$
   119E                    4691 00151$:
                     10D7  4692 	C$wsn.h$84$2$145 ==.
                           4693 ;	C:\Workspace\WSN_MS-4\/wsn.h:84: while(!CC1000_Sync());
   119E                    4694 00101$:
   119E C0 07         [24] 4695 	push	ar7
   11A0 C0 06         [24] 4696 	push	ar6
   11A2 C0 05         [24] 4697 	push	ar5
   11A4 C0 04         [24] 4698 	push	ar4
   11A6 12 0F 55      [24] 4699 	lcall	_CC1000_Sync
   11A9 E5 82         [12] 4700 	mov	a,dpl
   11AB D0 04         [24] 4701 	pop	ar4
   11AD D0 05         [24] 4702 	pop	ar5
   11AF D0 06         [24] 4703 	pop	ar6
   11B1 D0 07         [24] 4704 	pop	ar7
   11B3 60 E9         [24] 4705 	jz	00101$
                     10EE  4706 	C$wsn.h$85$2$145 ==.
                           4707 ;	C:\Workspace\WSN_MS-4\/wsn.h:85: for (i=0; i<19; i++) {
   11B5 E4            [12] 4708 	clr	a
   11B6 F5 51         [12] 4709 	mov	_i,a
   11B8 F5 52         [12] 4710 	mov	(_i + 1),a
   11BA                    4711 00115$:
                     10F3  4712 	C$wsn.h$86$3$146 ==.
                           4713 ;	C:\Workspace\WSN_MS-4\/wsn.h:86: frame[i] = CC1000_ReceiveByte();
   11BA E5 51         [12] 4714 	mov	a,_i
   11BC 24 35         [12] 4715 	add	a,#_frame
   11BE F9            [12] 4716 	mov	r1,a
   11BF C0 07         [24] 4717 	push	ar7
   11C1 C0 06         [24] 4718 	push	ar6
   11C3 C0 05         [24] 4719 	push	ar5
   11C5 C0 04         [24] 4720 	push	ar4
   11C7 C0 01         [24] 4721 	push	ar1
   11C9 12 07 4C      [24] 4722 	lcall	_CC1000_ReceiveByte
   11CC E5 82         [12] 4723 	mov	a,dpl
   11CE D0 01         [24] 4724 	pop	ar1
   11D0 D0 04         [24] 4725 	pop	ar4
   11D2 D0 05         [24] 4726 	pop	ar5
   11D4 D0 06         [24] 4727 	pop	ar6
   11D6 D0 07         [24] 4728 	pop	ar7
   11D8 F7            [12] 4729 	mov	@r1,a
                     1112  4730 	C$wsn.h$85$2$145 ==.
                           4731 ;	C:\Workspace\WSN_MS-4\/wsn.h:85: for (i=0; i<19; i++) {
   11D9 05 51         [12] 4732 	inc	_i
   11DB E4            [12] 4733 	clr	a
   11DC B5 51 02      [24] 4734 	cjne	a,_i,00153$
   11DF 05 52         [12] 4735 	inc	(_i + 1)
   11E1                    4736 00153$:
   11E1 C3            [12] 4737 	clr	c
   11E2 E5 51         [12] 4738 	mov	a,_i
   11E4 94 13         [12] 4739 	subb	a,#0x13
   11E6 E5 52         [12] 4740 	mov	a,(_i + 1)
   11E8 64 80         [12] 4741 	xrl	a,#0x80
   11EA 94 80         [12] 4742 	subb	a,#0x80
   11EC 40 CC         [24] 4743 	jc	00115$
                     1127  4744 	C$wsn.h$89$2$145 ==.
                           4745 ;	C:\Workspace\WSN_MS-4\/wsn.h:89: if (frame[0] == NI && frame[1] == slaveAddress) {
   11EE 74 B4         [12] 4746 	mov	a,#0xB4
   11F0 B5 35 76      [24] 4747 	cjne	a,_frame,00109$
   11F3 AA 36         [24] 4748 	mov	r2,(_frame + 0x0001)
   11F5 7B 00         [12] 4749 	mov	r3,#0x00
   11F7 EA            [12] 4750 	mov	a,r2
   11F8 B5 49 6E      [24] 4751 	cjne	a,_slaveAddress,00109$
   11FB EB            [12] 4752 	mov	a,r3
   11FC B5 4A 6A      [24] 4753 	cjne	a,(_slaveAddress + 1),00109$
                     1138  4754 	C$wsn.h$90$3$147 ==.
                           4755 ;	C:\Workspace\WSN_MS-4\/wsn.h:90: printf("\nS=%x F=%x SI=%x SN=xx ", CC1000_SYNC_BYTE, frame[2], slaveAddress);
   11FF AA 37         [24] 4756 	mov	r2,(_frame + 0x0002)
   1201 7B 00         [12] 4757 	mov	r3,#0x00
   1203 C0 49         [24] 4758 	push	_slaveAddress
   1205 C0 4A         [24] 4759 	push	(_slaveAddress + 1)
   1207 C0 02         [24] 4760 	push	ar2
   1209 C0 03         [24] 4761 	push	ar3
   120B 74 71         [12] 4762 	mov	a,#0x71
   120D C0 E0         [24] 4763 	push	acc
   120F E4            [12] 4764 	clr	a
   1210 C0 E0         [24] 4765 	push	acc
   1212 74 DE         [12] 4766 	mov	a,#__str_35
   1214 C0 E0         [24] 4767 	push	acc
   1216 74 1A         [12] 4768 	mov	a,#(__str_35 >> 8)
   1218 C0 E0         [24] 4769 	push	acc
   121A 74 80         [12] 4770 	mov	a,#0x80
   121C C0 E0         [24] 4771 	push	acc
   121E 12 03 EA      [24] 4772 	lcall	_printf
   1221 E5 81         [12] 4773 	mov	a,sp
   1223 24 F7         [12] 4774 	add	a,#0xf7
   1225 F5 81         [12] 4775 	mov	sp,a
                     1160  4776 	C$wsn.h$91$3$147 ==.
                           4777 ;	C:\Workspace\WSN_MS-4\/wsn.h:91: for (i=3; i<19; i++)
   1227 75 51 03      [24] 4778 	mov	_i,#0x03
   122A 75 52 00      [24] 4779 	mov	(_i + 1),#0x00
   122D                    4780 00117$:
                     1166  4781 	C$wsn.h$92$3$147 ==.
                           4782 ;	C:\Workspace\WSN_MS-4\/wsn.h:92: printf("%x", frame[i]);
   122D E5 51         [12] 4783 	mov	a,_i
   122F 24 35         [12] 4784 	add	a,#_frame
   1231 F9            [12] 4785 	mov	r1,a
   1232 87 03         [24] 4786 	mov	ar3,@r1
   1234 7A 00         [12] 4787 	mov	r2,#0x00
   1236 C0 03         [24] 4788 	push	ar3
   1238 C0 02         [24] 4789 	push	ar2
   123A 74 DB         [12] 4790 	mov	a,#__str_34
   123C C0 E0         [24] 4791 	push	acc
   123E 74 1A         [12] 4792 	mov	a,#(__str_34 >> 8)
   1240 C0 E0         [24] 4793 	push	acc
   1242 74 80         [12] 4794 	mov	a,#0x80
   1244 C0 E0         [24] 4795 	push	acc
   1246 12 03 EA      [24] 4796 	lcall	_printf
   1249 E5 81         [12] 4797 	mov	a,sp
   124B 24 FB         [12] 4798 	add	a,#0xfb
   124D F5 81         [12] 4799 	mov	sp,a
                     1188  4800 	C$wsn.h$91$3$147 ==.
                           4801 ;	C:\Workspace\WSN_MS-4\/wsn.h:91: for (i=3; i<19; i++)
   124F 05 51         [12] 4802 	inc	_i
   1251 E4            [12] 4803 	clr	a
   1252 B5 51 02      [24] 4804 	cjne	a,_i,00159$
   1255 05 52         [12] 4805 	inc	(_i + 1)
   1257                    4806 00159$:
   1257 C3            [12] 4807 	clr	c
   1258 E5 51         [12] 4808 	mov	a,_i
   125A 94 13         [12] 4809 	subb	a,#0x13
   125C E5 52         [12] 4810 	mov	a,(_i + 1)
   125E 64 80         [12] 4811 	xrl	a,#0x80
   1260 94 80         [12] 4812 	subb	a,#0x80
   1262 40 C9         [24] 4813 	jc	00117$
                     119D  4814 	C$wsn.h$94$3$147 ==.
                           4815 ;	C:\Workspace\WSN_MS-4\/wsn.h:94: return frame[2];
   1264 85 37 82      [24] 4816 	mov	dpl,(_frame + 0x0002)
   1267 80 28         [24] 4817 	sjmp	00119$
   1269                    4818 00109$:
                     11A2  4819 	C$wsn.h$96$3$148 ==.
                           4820 ;	C:\Workspace\WSN_MS-4\/wsn.h:96: if (limitAttempts != 0) {
   1269 EE            [12] 4821 	mov	a,r6
   126A 4F            [12] 4822 	orl	a,r7
   126B 70 03         [24] 4823 	jnz	00161$
   126D 02 11 8C      [24] 4824 	ljmp	00112$
   1270                    4825 00161$:
                     11A9  4826 	C$wsn.h$97$4$149 ==.
                           4827 ;	C:\Workspace\WSN_MS-4\/wsn.h:97: attempt++;
   1270 0C            [12] 4828 	inc	r4
   1271 BC 00 01      [24] 4829 	cjne	r4,#0x00,00162$
   1274 0D            [12] 4830 	inc	r5
   1275                    4831 00162$:
                     11AE  4832 	C$wsn.h$98$4$149 ==.
                           4833 ;	C:\Workspace\WSN_MS-4\/wsn.h:98: Delay_x100us(1000);
   1275 90 03 E8      [24] 4834 	mov	dptr,#0x03E8
   1278 C0 07         [24] 4835 	push	ar7
   127A C0 06         [24] 4836 	push	ar6
   127C C0 05         [24] 4837 	push	ar5
   127E C0 04         [24] 4838 	push	ar4
   1280 12 01 7A      [24] 4839 	lcall	_Delay_x100us
   1283 D0 04         [24] 4840 	pop	ar4
   1285 D0 05         [24] 4841 	pop	ar5
   1287 D0 06         [24] 4842 	pop	ar6
   1289 D0 07         [24] 4843 	pop	ar7
                     11C4  4844 	C$wsn.h$100$3$148 ==.
                           4845 ;	C:\Workspace\WSN_MS-4\/wsn.h:100: continue;
   128B 02 11 8C      [24] 4846 	ljmp	00112$
   128E                    4847 00114$:
                     11C7  4848 	C$wsn.h$104$1$144 ==.
                           4849 ;	C:\Workspace\WSN_MS-4\/wsn.h:104: return 0x00;
   128E 75 82 00      [24] 4850 	mov	dpl,#0x00
   1291                    4851 00119$:
                     11CA  4852 	C$wsn.h$105$1$144 ==.
                     11CA  4853 	XG$waitForFrame$0$0 ==.
   1291 22            [24] 4854 	ret
                           4855 ;------------------------------------------------------------
                           4856 ;Allocation info for local variables in function 'runSlaveMode'
                           4857 ;------------------------------------------------------------
                           4858 ;slaveId                   Allocated to registers r6 r7 
                           4859 ;------------------------------------------------------------
                     11CB  4860 	G$runSlaveMode$0$0 ==.
                     11CB  4861 	C$wsn.h$112$1$144 ==.
                           4862 ;	C:\Workspace\WSN_MS-4\/wsn.h:112: void runSlaveMode(int slaveId)
                           4863 ;	-----------------------------------------
                           4864 ;	 function runSlaveMode
                           4865 ;	-----------------------------------------
   1292                    4866 _runSlaveMode:
   1292 AE 82         [24] 4867 	mov	r6,dpl
   1294 AF 83         [24] 4868 	mov	r7,dph
                     11CF  4869 	C$wsn.h$114$1$151 ==.
                           4870 ;	C:\Workspace\WSN_MS-4\/wsn.h:114: clearFrame();
   1296 C0 07         [24] 4871 	push	ar7
   1298 C0 06         [24] 4872 	push	ar6
   129A 12 0F A2      [24] 4873 	lcall	_clearFrame
   129D D0 06         [24] 4874 	pop	ar6
   129F D0 07         [24] 4875 	pop	ar7
                     11DA  4876 	C$wsn.h$115$1$151 ==.
                           4877 ;	C:\Workspace\WSN_MS-4\/wsn.h:115: slaveAddress = slaveId;
   12A1 8E 49         [24] 4878 	mov	_slaveAddress,r6
   12A3 8F 4A         [24] 4879 	mov	(_slaveAddress + 1),r7
                     11DE  4880 	C$wsn.h$116$1$151 ==.
                           4881 ;	C:\Workspace\WSN_MS-4\/wsn.h:116: CC1000_Initialize();
   12A5 C0 07         [24] 4882 	push	ar7
   12A7 C0 06         [24] 4883 	push	ar6
   12A9 12 0A 3F      [24] 4884 	lcall	_CC1000_Initialize
                     11E5  4885 	C$wsn.h$118$1$151 ==.
                           4886 ;	C:\Workspace\WSN_MS-4\/wsn.h:118: switch (waitForFrame(0)) {
   12AC 90 00 00      [24] 4887 	mov	dptr,#0x0000
   12AF 12 11 60      [24] 4888 	lcall	_waitForFrame
   12B2 AD 82         [24] 4889 	mov	r5,dpl
   12B4 D0 06         [24] 4890 	pop	ar6
   12B6 D0 07         [24] 4891 	pop	ar7
   12B8 BD 01 02      [24] 4892 	cjne	r5,#0x01,00137$
   12BB 80 23         [24] 4893 	sjmp	00101$
   12BD                    4894 00137$:
   12BD BD 03 02      [24] 4895 	cjne	r5,#0x03,00138$
   12C0 80 56         [24] 4896 	sjmp	00110$
   12C2                    4897 00138$:
   12C2 BD 04 02      [24] 4898 	cjne	r5,#0x04,00139$
   12C5 80 39         [24] 4899 	sjmp	00104$
   12C7                    4900 00139$:
   12C7 BD 06 02      [24] 4901 	cjne	r5,#0x06,00140$
   12CA 80 4C         [24] 4902 	sjmp	00110$
   12CC                    4903 00140$:
   12CC BD 07 02      [24] 4904 	cjne	r5,#0x07,00141$
   12CF 80 3C         [24] 4905 	sjmp	00105$
   12D1                    4906 00141$:
   12D1 BD 0D 02      [24] 4907 	cjne	r5,#0x0D,00142$
   12D4 80 42         [24] 4908 	sjmp	00110$
   12D6                    4909 00142$:
   12D6 BD 83 02      [24] 4910 	cjne	r5,#0x83,00143$
   12D9 80 15         [24] 4911 	sjmp	00103$
   12DB                    4912 00143$:
                     1214  4913 	C$wsn.h$119$2$152 ==.
                           4914 ;	C:\Workspace\WSN_MS-4\/wsn.h:119: case 0x01:
   12DB BD 86 3A      [24] 4915 	cjne	r5,#0x86,00110$
   12DE 80 10         [24] 4916 	sjmp	00103$
   12E0                    4917 00101$:
                     1219  4918 	C$wsn.h$120$2$152 ==.
                           4919 ;	C:\Workspace\WSN_MS-4\/wsn.h:120: sendFrame(0x02);
   12E0 75 82 02      [24] 4920 	mov	dpl,#0x02
   12E3 C0 07         [24] 4921 	push	ar7
   12E5 C0 06         [24] 4922 	push	ar6
   12E7 12 0F C4      [24] 4923 	lcall	_sendFrame
   12EA D0 06         [24] 4924 	pop	ar6
   12EC D0 07         [24] 4925 	pop	ar7
                     1227  4926 	C$wsn.h$121$2$152 ==.
                           4927 ;	C:\Workspace\WSN_MS-4\/wsn.h:121: break;
                     1227  4928 	C$wsn.h$123$2$152 ==.
                           4929 ;	C:\Workspace\WSN_MS-4\/wsn.h:123: case 0x86:
   12EE 80 28         [24] 4930 	sjmp	00110$
   12F0                    4931 00103$:
                     1229  4932 	C$wsn.h$124$2$152 ==.
                           4933 ;	C:\Workspace\WSN_MS-4\/wsn.h:124: sendFrame(0x0C);
   12F0 75 82 0C      [24] 4934 	mov	dpl,#0x0C
   12F3 C0 07         [24] 4935 	push	ar7
   12F5 C0 06         [24] 4936 	push	ar6
   12F7 12 0F C4      [24] 4937 	lcall	_sendFrame
   12FA D0 06         [24] 4938 	pop	ar6
   12FC D0 07         [24] 4939 	pop	ar7
                     1237  4940 	C$wsn.h$125$2$152 ==.
                           4941 ;	C:\Workspace\WSN_MS-4\/wsn.h:125: break;
                     1237  4942 	C$wsn.h$126$2$152 ==.
                           4943 ;	C:\Workspace\WSN_MS-4\/wsn.h:126: case 0x04:
   12FE 80 18         [24] 4944 	sjmp	00110$
   1300                    4945 00104$:
                     1239  4946 	C$wsn.h$127$2$152 ==.
                           4947 ;	C:\Workspace\WSN_MS-4\/wsn.h:127: sendDataWithoutSecurityToMaster();
   1300 C0 07         [24] 4948 	push	ar7
   1302 C0 06         [24] 4949 	push	ar6
   1304 12 13 2B      [24] 4950 	lcall	_sendDataWithoutSecurityToMaster
   1307 D0 06         [24] 4951 	pop	ar6
   1309 D0 07         [24] 4952 	pop	ar7
                     1244  4953 	C$wsn.h$128$2$152 ==.
                           4954 ;	C:\Workspace\WSN_MS-4\/wsn.h:128: break;
                     1244  4955 	C$wsn.h$129$2$152 ==.
                           4956 ;	C:\Workspace\WSN_MS-4\/wsn.h:129: case 0x07:
   130B 80 0B         [24] 4957 	sjmp	00110$
   130D                    4958 00105$:
                     1246  4959 	C$wsn.h$130$2$152 ==.
                           4960 ;	C:\Workspace\WSN_MS-4\/wsn.h:130: sendDataWithCRC8ToMaster();
   130D C0 07         [24] 4961 	push	ar7
   130F C0 06         [24] 4962 	push	ar6
   1311 12 13 6F      [24] 4963 	lcall	_sendDataWithCRC8ToMaster
   1314 D0 06         [24] 4964 	pop	ar6
   1316 D0 07         [24] 4965 	pop	ar7
                     1251  4966 	C$wsn.h$137$1$151 ==.
                           4967 ;	C:\Workspace\WSN_MS-4\/wsn.h:137: }
   1318                    4968 00110$:
                     1251  4969 	C$wsn.h$139$1$151 ==.
                           4970 ;	C:\Workspace\WSN_MS-4\/wsn.h:139: CC1000_SetupPD();
   1318 C0 07         [24] 4971 	push	ar7
   131A C0 06         [24] 4972 	push	ar6
   131C 12 09 56      [24] 4973 	lcall	_CC1000_SetupPD
   131F D0 06         [24] 4974 	pop	ar6
   1321 D0 07         [24] 4975 	pop	ar7
                     125C  4976 	C$wsn.h$140$1$151 ==.
                           4977 ;	C:\Workspace\WSN_MS-4\/wsn.h:140: runSlaveMode(slaveId);
   1323 8E 82         [24] 4978 	mov	dpl,r6
   1325 8F 83         [24] 4979 	mov	dph,r7
   1327 12 12 92      [24] 4980 	lcall	_runSlaveMode
                     1263  4981 	C$wsn.h$141$1$151 ==.
                     1263  4982 	XG$runSlaveMode$0$0 ==.
   132A 22            [24] 4983 	ret
                           4984 ;------------------------------------------------------------
                           4985 ;Allocation info for local variables in function 'sendDataWithoutSecurityToMaster'
                           4986 ;------------------------------------------------------------
                     1264  4987 	G$sendDataWithoutSecurityToMaster$0$0 ==.
                     1264  4988 	C$wsn.h$143$1$151 ==.
                           4989 ;	C:\Workspace\WSN_MS-4\/wsn.h:143: void sendDataWithoutSecurityToMaster()
                           4990 ;	-----------------------------------------
                           4991 ;	 function sendDataWithoutSecurityToMaster
                           4992 ;	-----------------------------------------
   132B                    4993 _sendDataWithoutSecurityToMaster:
                     1264  4994 	C$wsn.h$145$1$153 ==.
                           4995 ;	C:\Workspace\WSN_MS-4\/wsn.h:145: frame[0] = NI;
   132B 75 35 B4      [24] 4996 	mov	_frame,#0xB4
                     1267  4997 	C$wsn.h$146$1$153 ==.
                           4998 ;	C:\Workspace\WSN_MS-4\/wsn.h:146: frame[1] = slaveAddress;
   132E AF 49         [24] 4999 	mov	r7,_slaveAddress
   1330 8F 36         [24] 5000 	mov	(_frame + 0x0001),r7
                     126B  5001 	C$wsn.h$147$1$153 ==.
                           5002 ;	C:\Workspace\WSN_MS-4\/wsn.h:147: frame[2] = 0x05;
   1332 75 37 05      [24] 5003 	mov	(_frame + 0x0002),#0x05
                     126E  5004 	C$wsn.h$148$1$153 ==.
                           5005 ;	C:\Workspace\WSN_MS-4\/wsn.h:148: frame[3] = 0x00;
   1335 75 38 00      [24] 5006 	mov	(_frame + 0x0003),#0x00
                     1271  5007 	C$wsn.h$149$1$153 ==.
                           5008 ;	C:\Workspace\WSN_MS-4\/wsn.h:149: frame[4] = 'L';
   1338 75 39 4C      [24] 5009 	mov	(_frame + 0x0004),#0x4C
                     1274  5010 	C$wsn.h$150$1$153 ==.
                           5011 ;	C:\Workspace\WSN_MS-4\/wsn.h:150: frame[5] = 'u';
   133B 75 3A 75      [24] 5012 	mov	(_frame + 0x0005),#0x75
                     1277  5013 	C$wsn.h$151$1$153 ==.
                           5014 ;	C:\Workspace\WSN_MS-4\/wsn.h:151: frame[6] = 'b';
   133E 75 3B 62      [24] 5015 	mov	(_frame + 0x0006),#0x62
                     127A  5016 	C$wsn.h$152$1$153 ==.
                           5017 ;	C:\Workspace\WSN_MS-4\/wsn.h:152: frame[7] = 'i';
   1341 75 3C 69      [24] 5018 	mov	(_frame + 0x0007),#0x69
                     127D  5019 	C$wsn.h$153$1$153 ==.
                           5020 ;	C:\Workspace\WSN_MS-4\/wsn.h:153: frame[8] = 'e';
   1344 75 3D 65      [24] 5021 	mov	(_frame + 0x0008),#0x65
                     1280  5022 	C$wsn.h$154$1$153 ==.
                           5023 ;	C:\Workspace\WSN_MS-4\/wsn.h:154: frame[9] = 'R';
   1347 75 3E 52      [24] 5024 	mov	(_frame + 0x0009),#0x52
                     1283  5025 	C$wsn.h$155$1$153 ==.
                           5026 ;	C:\Workspace\WSN_MS-4\/wsn.h:155: frame[10] = 'a';
   134A 75 3F 61      [24] 5027 	mov	(_frame + 0x000a),#0x61
                     1286  5028 	C$wsn.h$156$1$153 ==.
                           5029 ;	C:\Workspace\WSN_MS-4\/wsn.h:156: frame[11] = 'd';
   134D 75 40 64      [24] 5030 	mov	(_frame + 0x000b),#0x64
                     1289  5031 	C$wsn.h$157$1$153 ==.
                           5032 ;	C:\Workspace\WSN_MS-4\/wsn.h:157: frame[12] = 'i';
   1350 75 41 69      [24] 5033 	mov	(_frame + 0x000c),#0x69
                     128C  5034 	C$wsn.h$158$1$153 ==.
                           5035 ;	C:\Workspace\WSN_MS-4\/wsn.h:158: frame[13] = 'o';
   1353 75 42 6F      [24] 5036 	mov	(_frame + 0x000d),#0x6F
                     128F  5037 	C$wsn.h$159$1$153 ==.
                           5038 ;	C:\Workspace\WSN_MS-4\/wsn.h:159: frame[14] = '!';
   1356 75 43 21      [24] 5039 	mov	(_frame + 0x000e),#0x21
                     1292  5040 	C$wsn.h$160$1$153 ==.
                           5041 ;	C:\Workspace\WSN_MS-4\/wsn.h:160: frame[15] = '!';
   1359 75 44 21      [24] 5042 	mov	(_frame + 0x000f),#0x21
                     1295  5043 	C$wsn.h$161$1$153 ==.
                           5044 ;	C:\Workspace\WSN_MS-4\/wsn.h:161: frame[16] = '!';
   135C 75 45 21      [24] 5045 	mov	(_frame + 0x0010),#0x21
                     1298  5046 	C$wsn.h$162$1$153 ==.
                           5047 ;	C:\Workspace\WSN_MS-4\/wsn.h:162: frame[17] = '!';
   135F 75 46 21      [24] 5048 	mov	(_frame + 0x0011),#0x21
                     129B  5049 	C$wsn.h$163$1$153 ==.
                           5050 ;	C:\Workspace\WSN_MS-4\/wsn.h:163: frame[18] = 0x00;
   1362 75 47 00      [24] 5051 	mov	(_frame + 0x0012),#0x00
                     129E  5052 	C$wsn.h$165$1$153 ==.
                           5053 ;	C:\Workspace\WSN_MS-4\/wsn.h:165: sendData(frame);
   1365 90 00 35      [24] 5054 	mov	dptr,#_frame
   1368 75 F0 40      [24] 5055 	mov	b,#0x40
   136B 12 10 43      [24] 5056 	lcall	_sendData
                     12A7  5057 	C$wsn.h$166$1$153 ==.
                     12A7  5058 	XG$sendDataWithoutSecurityToMaster$0$0 ==.
   136E 22            [24] 5059 	ret
                           5060 ;------------------------------------------------------------
                           5061 ;Allocation info for local variables in function 'sendDataWithCRC8ToMaster'
                           5062 ;------------------------------------------------------------
                     12A8  5063 	G$sendDataWithCRC8ToMaster$0$0 ==.
                     12A8  5064 	C$wsn.h$168$1$153 ==.
                           5065 ;	C:\Workspace\WSN_MS-4\/wsn.h:168: void sendDataWithCRC8ToMaster()
                           5066 ;	-----------------------------------------
                           5067 ;	 function sendDataWithCRC8ToMaster
                           5068 ;	-----------------------------------------
   136F                    5069 _sendDataWithCRC8ToMaster:
                     12A8  5070 	C$wsn.h$170$1$154 ==.
                           5071 ;	C:\Workspace\WSN_MS-4\/wsn.h:170: frame[0] = NI;
   136F 75 35 B4      [24] 5072 	mov	_frame,#0xB4
                     12AB  5073 	C$wsn.h$171$1$154 ==.
                           5074 ;	C:\Workspace\WSN_MS-4\/wsn.h:171: frame[1] = slaveAddress;
   1372 AF 49         [24] 5075 	mov	r7,_slaveAddress
   1374 8F 36         [24] 5076 	mov	(_frame + 0x0001),r7
                     12AF  5077 	C$wsn.h$172$1$154 ==.
                           5078 ;	C:\Workspace\WSN_MS-4\/wsn.h:172: frame[2] = 0x08;
   1376 75 37 08      [24] 5079 	mov	(_frame + 0x0002),#0x08
                     12B2  5080 	C$wsn.h$173$1$154 ==.
                           5081 ;	C:\Workspace\WSN_MS-4\/wsn.h:173: frame[3] = 0x00;
   1379 75 38 00      [24] 5082 	mov	(_frame + 0x0003),#0x00
                     12B5  5083 	C$wsn.h$174$1$154 ==.
                           5084 ;	C:\Workspace\WSN_MS-4\/wsn.h:174: frame[4] = 'L';
   137C 75 39 4C      [24] 5085 	mov	(_frame + 0x0004),#0x4C
                     12B8  5086 	C$wsn.h$175$1$154 ==.
                           5087 ;	C:\Workspace\WSN_MS-4\/wsn.h:175: frame[5] = 'u';
   137F 75 3A 75      [24] 5088 	mov	(_frame + 0x0005),#0x75
                     12BB  5089 	C$wsn.h$176$1$154 ==.
                           5090 ;	C:\Workspace\WSN_MS-4\/wsn.h:176: frame[6] = 'b';
   1382 75 3B 62      [24] 5091 	mov	(_frame + 0x0006),#0x62
                     12BE  5092 	C$wsn.h$177$1$154 ==.
                           5093 ;	C:\Workspace\WSN_MS-4\/wsn.h:177: frame[7] = 'i';
   1385 75 3C 69      [24] 5094 	mov	(_frame + 0x0007),#0x69
                     12C1  5095 	C$wsn.h$178$1$154 ==.
                           5096 ;	C:\Workspace\WSN_MS-4\/wsn.h:178: frame[8] = 'e';
   1388 75 3D 65      [24] 5097 	mov	(_frame + 0x0008),#0x65
                     12C4  5098 	C$wsn.h$179$1$154 ==.
                           5099 ;	C:\Workspace\WSN_MS-4\/wsn.h:179: frame[9] = 'R';
   138B 75 3E 52      [24] 5100 	mov	(_frame + 0x0009),#0x52
                     12C7  5101 	C$wsn.h$180$1$154 ==.
                           5102 ;	C:\Workspace\WSN_MS-4\/wsn.h:180: frame[10] = 'a';
   138E 75 3F 61      [24] 5103 	mov	(_frame + 0x000a),#0x61
                     12CA  5104 	C$wsn.h$181$1$154 ==.
                           5105 ;	C:\Workspace\WSN_MS-4\/wsn.h:181: frame[11] = 'd';
   1391 75 40 64      [24] 5106 	mov	(_frame + 0x000b),#0x64
                     12CD  5107 	C$wsn.h$182$1$154 ==.
                           5108 ;	C:\Workspace\WSN_MS-4\/wsn.h:182: frame[12] = 'i';
   1394 75 41 69      [24] 5109 	mov	(_frame + 0x000c),#0x69
                     12D0  5110 	C$wsn.h$183$1$154 ==.
                           5111 ;	C:\Workspace\WSN_MS-4\/wsn.h:183: frame[13] = 'o';
   1397 75 42 6F      [24] 5112 	mov	(_frame + 0x000d),#0x6F
                     12D3  5113 	C$wsn.h$184$1$154 ==.
                           5114 ;	C:\Workspace\WSN_MS-4\/wsn.h:184: frame[14] = '!';
   139A 75 43 21      [24] 5115 	mov	(_frame + 0x000e),#0x21
                     12D6  5116 	C$wsn.h$185$1$154 ==.
                           5117 ;	C:\Workspace\WSN_MS-4\/wsn.h:185: frame[15] = '!';
   139D 75 44 21      [24] 5118 	mov	(_frame + 0x000f),#0x21
                     12D9  5119 	C$wsn.h$186$1$154 ==.
                           5120 ;	C:\Workspace\WSN_MS-4\/wsn.h:186: frame[16] = '!';
   13A0 75 45 21      [24] 5121 	mov	(_frame + 0x0010),#0x21
                     12DC  5122 	C$wsn.h$187$1$154 ==.
                           5123 ;	C:\Workspace\WSN_MS-4\/wsn.h:187: frame[17] = '!';
   13A3 75 46 21      [24] 5124 	mov	(_frame + 0x0011),#0x21
                     12DF  5125 	C$wsn.h$188$1$154 ==.
                           5126 ;	C:\Workspace\WSN_MS-4\/wsn.h:188: frame[18] = crc8(frame[4], crc, 14);
   13A6 AD 39         [24] 5127 	mov	r5,(_frame + 0x0004)
   13A8 7E 00         [12] 5128 	mov	r6,#0x00
   13AA 7F 00         [12] 5129 	mov	r7,#0x00
   13AC 85 48 09      [24] 5130 	mov	_crc8_PARM_2,_crc
   13AF 75 0A 0E      [24] 5131 	mov	_crc8_PARM_3,#0x0E
   13B2 75 0B 00      [24] 5132 	mov	(_crc8_PARM_3 + 1),#0x00
   13B5 8D 82         [24] 5133 	mov	dpl,r5
   13B7 8E 83         [24] 5134 	mov	dph,r6
   13B9 8F F0         [24] 5135 	mov	b,r7
   13BB 12 06 08      [24] 5136 	lcall	_crc8
   13BE E5 82         [12] 5137 	mov	a,dpl
   13C0 F5 47         [12] 5138 	mov	(_frame + 0x0012),a
                     12FB  5139 	C$wsn.h$190$1$154 ==.
                           5140 ;	C:\Workspace\WSN_MS-4\/wsn.h:190: sendData(frame);
   13C2 90 00 35      [24] 5141 	mov	dptr,#_frame
   13C5 75 F0 40      [24] 5142 	mov	b,#0x40
   13C8 12 10 43      [24] 5143 	lcall	_sendData
                     1304  5144 	C$wsn.h$191$1$154 ==.
                     1304  5145 	XG$sendDataWithCRC8ToMaster$0$0 ==.
   13CB 22            [24] 5146 	ret
                           5147 ;------------------------------------------------------------
                           5148 ;Allocation info for local variables in function 'runMasterMode'
                           5149 ;------------------------------------------------------------
                     1305  5150 	G$runMasterMode$0$0 ==.
                     1305  5151 	C$wsn.h$198$1$154 ==.
                           5152 ;	C:\Workspace\WSN_MS-4\/wsn.h:198: void runMasterMode()
                           5153 ;	-----------------------------------------
                           5154 ;	 function runMasterMode
                           5155 ;	-----------------------------------------
   13CC                    5156 _runMasterMode:
                     1305  5157 	C$wsn.h$200$1$155 ==.
                           5158 ;	C:\Workspace\WSN_MS-4\/wsn.h:200: clearFrame();
   13CC 12 0F A2      [24] 5159 	lcall	_clearFrame
                     1308  5160 	C$wsn.h$201$1$155 ==.
                           5161 ;	C:\Workspace\WSN_MS-4\/wsn.h:201: mode = getMode();
   13CF 12 14 87      [24] 5162 	lcall	_getMode
   13D2 85 82 53      [24] 5163 	mov	_mode,dpl
   13D5 85 83 54      [24] 5164 	mov	(_mode + 1),dph
                     1311  5165 	C$wsn.h$203$1$155 ==.
                           5166 ;	C:\Workspace\WSN_MS-4\/wsn.h:203: if (mode >= 1 && mode <= 3) {
   13D8 C3            [12] 5167 	clr	c
   13D9 E5 53         [12] 5168 	mov	a,_mode
   13DB 94 01         [12] 5169 	subb	a,#0x01
   13DD E5 54         [12] 5170 	mov	a,(_mode + 1)
   13DF 64 80         [12] 5171 	xrl	a,#0x80
   13E1 94 80         [12] 5172 	subb	a,#0x80
   13E3 40 55         [24] 5173 	jc	00111$
   13E5 74 03         [12] 5174 	mov	a,#0x03
   13E7 95 53         [12] 5175 	subb	a,_mode
   13E9 E4            [12] 5176 	clr	a
   13EA 64 80         [12] 5177 	xrl	a,#0x80
   13EC 85 54 F0      [24] 5178 	mov	b,(_mode + 1)
   13EF 63 F0 80      [24] 5179 	xrl	b,#0x80
   13F2 95 F0         [12] 5180 	subb	a,b
   13F4 40 44         [24] 5181 	jc	00111$
                     132F  5182 	C$wsn.h$204$2$156 ==.
                           5183 ;	C:\Workspace\WSN_MS-4\/wsn.h:204: printf("Wybrany tryb %d", mode);
   13F6 C0 53         [24] 5184 	push	_mode
   13F8 C0 54         [24] 5185 	push	(_mode + 1)
   13FA 74 F6         [12] 5186 	mov	a,#__str_36
   13FC C0 E0         [24] 5187 	push	acc
   13FE 74 1A         [12] 5188 	mov	a,#(__str_36 >> 8)
   1400 C0 E0         [24] 5189 	push	acc
   1402 74 80         [12] 5190 	mov	a,#0x80
   1404 C0 E0         [24] 5191 	push	acc
   1406 12 03 EA      [24] 5192 	lcall	_printf
   1409 E5 81         [12] 5193 	mov	a,sp
   140B 24 FB         [12] 5194 	add	a,#0xfb
   140D F5 81         [12] 5195 	mov	sp,a
                     1348  5196 	C$wsn.h$205$2$156 ==.
                           5197 ;	C:\Workspace\WSN_MS-4\/wsn.h:205: CC1000_Initialize();
   140F 12 0A 3F      [24] 5198 	lcall	_CC1000_Initialize
                     134B  5199 	C$wsn.h$207$2$156 ==.
                           5200 ;	C:\Workspace\WSN_MS-4\/wsn.h:207: sendTestFrame();
   1412 12 15 0F      [24] 5201 	lcall	_sendTestFrame
                     134E  5202 	C$wsn.h$209$2$156 ==.
                           5203 ;	C:\Workspace\WSN_MS-4\/wsn.h:209: switch (mode) {
   1415 74 01         [12] 5204 	mov	a,#0x01
   1417 B5 53 06      [24] 5205 	cjne	a,_mode,00136$
   141A E4            [12] 5206 	clr	a
   141B B5 54 02      [24] 5207 	cjne	a,(_mode + 1),00136$
   141E 80 0D         [24] 5208 	sjmp	00101$
   1420                    5209 00136$:
   1420 74 02         [12] 5210 	mov	a,#0x02
   1422 B5 53 06      [24] 5211 	cjne	a,_mode,00137$
   1425 E4            [12] 5212 	clr	a
   1426 B5 54 02      [24] 5213 	cjne	a,(_mode + 1),00137$
   1429 80 07         [24] 5214 	sjmp	00102$
   142B                    5215 00137$:
                     1364  5216 	C$wsn.h$210$3$157 ==.
                           5217 ;	C:\Workspace\WSN_MS-4\/wsn.h:210: case 1:
   142B 80 08         [24] 5218 	sjmp	00104$
   142D                    5219 00101$:
                     1366  5220 	C$wsn.h$211$3$157 ==.
                           5221 ;	C:\Workspace\WSN_MS-4\/wsn.h:211: sendDataWithoutSecurity();
   142D 12 15 52      [24] 5222 	lcall	_sendDataWithoutSecurity
                     1369  5223 	C$wsn.h$212$3$157 ==.
                           5224 ;	C:\Workspace\WSN_MS-4\/wsn.h:212: break;
                     1369  5225 	C$wsn.h$213$3$157 ==.
                           5226 ;	C:\Workspace\WSN_MS-4\/wsn.h:213: case 2:
   1430 80 03         [24] 5227 	sjmp	00104$
   1432                    5228 00102$:
                     136B  5229 	C$wsn.h$214$3$157 ==.
                           5230 ;	C:\Workspace\WSN_MS-4\/wsn.h:214: sendDataWithCRC8();
   1432 12 16 11      [24] 5231 	lcall	_sendDataWithCRC8
                     136E  5232 	C$wsn.h$218$2$156 ==.
                           5233 ;	C:\Workspace\WSN_MS-4\/wsn.h:218: }
   1435                    5234 00104$:
                     136E  5235 	C$wsn.h$219$2$156 ==.
                           5236 ;	C:\Workspace\WSN_MS-4\/wsn.h:219: CC1000_SetupPD();
   1435 12 09 56      [24] 5237 	lcall	_CC1000_SetupPD
   1438 80 49         [24] 5238 	sjmp	00112$
   143A                    5239 00111$:
                     1373  5240 	C$wsn.h$220$1$155 ==.
                           5241 ;	C:\Workspace\WSN_MS-4\/wsn.h:220: } else if (mode == 4) {
   143A 74 04         [12] 5242 	mov	a,#0x04
   143C B5 53 06      [24] 5243 	cjne	a,_mode,00138$
   143F E4            [12] 5244 	clr	a
   1440 B5 54 02      [24] 5245 	cjne	a,(_mode + 1),00138$
   1443 80 02         [24] 5246 	sjmp	00139$
   1445                    5247 00138$:
   1445 80 3C         [24] 5248 	sjmp	00112$
   1447                    5249 00139$:
                     1380  5250 	C$wsn.h$221$2$158 ==.
                           5251 ;	C:\Workspace\WSN_MS-4\/wsn.h:221: if (acknowledge == 0) {
   1447 E5 55         [12] 5252 	mov	a,_acknowledge
   1449 45 56         [12] 5253 	orl	a,(_acknowledge + 1)
                     1384  5254 	C$wsn.h$222$3$159 ==.
                           5255 ;	C:\Workspace\WSN_MS-4\/wsn.h:222: acknowledge = 1;
   144B 70 1C         [24] 5256 	jnz	00106$
   144D 75 55 01      [24] 5257 	mov	_acknowledge,#0x01
   1450 F5 56         [12] 5258 	mov	(_acknowledge + 1),a
                     138B  5259 	C$wsn.h$223$3$159 ==.
                           5260 ;	C:\Workspace\WSN_MS-4\/wsn.h:223: printf("Wybrano tryb z potwierdzeniem");
   1452 74 06         [12] 5261 	mov	a,#__str_37
   1454 C0 E0         [24] 5262 	push	acc
   1456 74 1B         [12] 5263 	mov	a,#(__str_37 >> 8)
   1458 C0 E0         [24] 5264 	push	acc
   145A 74 80         [12] 5265 	mov	a,#0x80
   145C C0 E0         [24] 5266 	push	acc
   145E 12 03 EA      [24] 5267 	lcall	_printf
   1461 15 81         [12] 5268 	dec	sp
   1463 15 81         [12] 5269 	dec	sp
   1465 15 81         [12] 5270 	dec	sp
   1467 80 1A         [24] 5271 	sjmp	00112$
   1469                    5272 00106$:
                     13A2  5273 	C$wsn.h$225$3$160 ==.
                           5274 ;	C:\Workspace\WSN_MS-4\/wsn.h:225: acknowledge = 0;
   1469 E4            [12] 5275 	clr	a
   146A F5 55         [12] 5276 	mov	_acknowledge,a
   146C F5 56         [12] 5277 	mov	(_acknowledge + 1),a
                     13A7  5278 	C$wsn.h$226$3$160 ==.
                           5279 ;	C:\Workspace\WSN_MS-4\/wsn.h:226: printf("Wybrano tryb bez potwierdzenia");
   146E 74 24         [12] 5280 	mov	a,#__str_38
   1470 C0 E0         [24] 5281 	push	acc
   1472 74 1B         [12] 5282 	mov	a,#(__str_38 >> 8)
   1474 C0 E0         [24] 5283 	push	acc
   1476 74 80         [12] 5284 	mov	a,#0x80
   1478 C0 E0         [24] 5285 	push	acc
   147A 12 03 EA      [24] 5286 	lcall	_printf
   147D 15 81         [12] 5287 	dec	sp
   147F 15 81         [12] 5288 	dec	sp
   1481 15 81         [12] 5289 	dec	sp
   1483                    5290 00112$:
                     13BC  5291 	C$wsn.h$229$1$155 ==.
                           5292 ;	C:\Workspace\WSN_MS-4\/wsn.h:229: runMasterMode();
   1483 12 13 CC      [24] 5293 	lcall	_runMasterMode
                     13BF  5294 	C$wsn.h$230$1$155 ==.
                     13BF  5295 	XG$runMasterMode$0$0 ==.
   1486 22            [24] 5296 	ret
                           5297 ;------------------------------------------------------------
                           5298 ;Allocation info for local variables in function 'getMode'
                           5299 ;------------------------------------------------------------
                     13C0  5300 	G$getMode$0$0 ==.
                     13C0  5301 	C$wsn.h$232$1$155 ==.
                           5302 ;	C:\Workspace\WSN_MS-4\/wsn.h:232: int getMode()
                           5303 ;	-----------------------------------------
                           5304 ;	 function getMode
                           5305 ;	-----------------------------------------
   1487                    5306 _getMode:
                     13C0  5307 	C$wsn.h$234$1$161 ==.
                           5308 ;	C:\Workspace\WSN_MS-4\/wsn.h:234: do {
   1487                    5309 00124$:
                     13C0  5310 	C$wsn.h$235$2$162 ==.
                           5311 ;	C:\Workspace\WSN_MS-4\/wsn.h:235: Wait_for_button_release();
   1487 12 01 8D      [24] 5312 	lcall	_Wait_for_button_release
                     13C3  5313 	C$wsn.h$236$2$162 ==.
                           5314 ;	C:\Workspace\WSN_MS-4\/wsn.h:236: Wait_for_button_pressed();
   148A 12 01 BC      [24] 5315 	lcall	_Wait_for_button_pressed
                     13C6  5316 	C$wsn.h$237$2$162 ==.
                           5317 ;	C:\Workspace\WSN_MS-4\/wsn.h:237: if (B1_ON()) {
   148D E5 85         [12] 5318 	mov	a,_P5
   148F 20 E0 18      [24] 5319 	jb	acc.0,00122$
                     13CB  5320 	C$wsn.h$238$3$163 ==.
                           5321 ;	C:\Workspace\WSN_MS-4\/wsn.h:238: mode = 1;
   1492 75 53 01      [24] 5322 	mov	_mode,#0x01
   1495 75 54 00      [24] 5323 	mov	(_mode + 1),#0x00
                     13D1  5324 	C$wsn.h$239$4$164 ==.
                           5325 ;	C:\Workspace\WSN_MS-4\/wsn.h:239: LED1_BLINK();
   1498 43 85 10      [24] 5326 	orl	_P5,#0x10
   149B 90 0B B8      [24] 5327 	mov	dptr,#0x0BB8
   149E 12 01 7A      [24] 5328 	lcall	_Delay_x100us
   14A1 AF 85         [24] 5329 	mov	r7,_P5
   14A3 74 EF         [12] 5330 	mov	a,#0xEF
   14A5 5F            [12] 5331 	anl	a,r7
   14A6 F5 85         [12] 5332 	mov	_P5,a
   14A8 80 55         [24] 5333 	sjmp	00125$
   14AA                    5334 00122$:
                     13E3  5335 	C$wsn.h$240$2$162 ==.
                           5336 ;	C:\Workspace\WSN_MS-4\/wsn.h:240: } else if (B2_ON()) {
   14AA E5 85         [12] 5337 	mov	a,_P5
   14AC 20 E1 18      [24] 5338 	jb	acc.1,00119$
                     13E8  5339 	C$wsn.h$241$3$165 ==.
                           5340 ;	C:\Workspace\WSN_MS-4\/wsn.h:241: mode = 2;
   14AF 75 53 02      [24] 5341 	mov	_mode,#0x02
   14B2 75 54 00      [24] 5342 	mov	(_mode + 1),#0x00
                     13EE  5343 	C$wsn.h$242$4$166 ==.
                           5344 ;	C:\Workspace\WSN_MS-4\/wsn.h:242: LED2_BLINK();
   14B5 43 85 20      [24] 5345 	orl	_P5,#0x20
   14B8 90 0B B8      [24] 5346 	mov	dptr,#0x0BB8
   14BB 12 01 7A      [24] 5347 	lcall	_Delay_x100us
   14BE AF 85         [24] 5348 	mov	r7,_P5
   14C0 74 DF         [12] 5349 	mov	a,#0xDF
   14C2 5F            [12] 5350 	anl	a,r7
   14C3 F5 85         [12] 5351 	mov	_P5,a
   14C5 80 38         [24] 5352 	sjmp	00125$
   14C7                    5353 00119$:
                     1400  5354 	C$wsn.h$243$2$162 ==.
                           5355 ;	C:\Workspace\WSN_MS-4\/wsn.h:243: } else if (B3_ON()) {
   14C7 E5 85         [12] 5356 	mov	a,_P5
   14C9 20 E2 18      [24] 5357 	jb	acc.2,00116$
                     1405  5358 	C$wsn.h$244$3$167 ==.
                           5359 ;	C:\Workspace\WSN_MS-4\/wsn.h:244: mode = 3;
   14CC 75 53 03      [24] 5360 	mov	_mode,#0x03
   14CF 75 54 00      [24] 5361 	mov	(_mode + 1),#0x00
                     140B  5362 	C$wsn.h$245$4$168 ==.
                           5363 ;	C:\Workspace\WSN_MS-4\/wsn.h:245: LED3_BLINK();
   14D2 43 85 40      [24] 5364 	orl	_P5,#0x40
   14D5 90 0B B8      [24] 5365 	mov	dptr,#0x0BB8
   14D8 12 01 7A      [24] 5366 	lcall	_Delay_x100us
   14DB AF 85         [24] 5367 	mov	r7,_P5
   14DD 74 BF         [12] 5368 	mov	a,#0xBF
   14DF 5F            [12] 5369 	anl	a,r7
   14E0 F5 85         [12] 5370 	mov	_P5,a
   14E2 80 1B         [24] 5371 	sjmp	00125$
   14E4                    5372 00116$:
                     141D  5373 	C$wsn.h$246$2$162 ==.
                           5374 ;	C:\Workspace\WSN_MS-4\/wsn.h:246: } else if (B4_ON()) {
   14E4 E5 85         [12] 5375 	mov	a,_P5
   14E6 20 E3 16      [24] 5376 	jb	acc.3,00125$
                     1422  5377 	C$wsn.h$247$3$169 ==.
                           5378 ;	C:\Workspace\WSN_MS-4\/wsn.h:247: mode = 4;
   14E9 75 53 04      [24] 5379 	mov	_mode,#0x04
   14EC 75 54 00      [24] 5380 	mov	(_mode + 1),#0x00
                     1428  5381 	C$wsn.h$248$4$170 ==.
                           5382 ;	C:\Workspace\WSN_MS-4\/wsn.h:248: LED4_BLINK();
   14EF 43 85 80      [24] 5383 	orl	_P5,#0x80
   14F2 90 0B B8      [24] 5384 	mov	dptr,#0x0BB8
   14F5 12 01 7A      [24] 5385 	lcall	_Delay_x100us
   14F8 AF 85         [24] 5386 	mov	r7,_P5
   14FA 74 7F         [12] 5387 	mov	a,#0x7F
   14FC 5F            [12] 5388 	anl	a,r7
   14FD F5 85         [12] 5389 	mov	_P5,a
   14FF                    5390 00125$:
                     1438  5391 	C$wsn.h$250$1$161 ==.
                           5392 ;	C:\Workspace\WSN_MS-4\/wsn.h:250: } while (mode == 0);
   14FF E5 53         [12] 5393 	mov	a,_mode
   1501 45 54         [12] 5394 	orl	a,(_mode + 1)
   1503 70 03         [24] 5395 	jnz	00151$
   1505 02 14 87      [24] 5396 	ljmp	00124$
   1508                    5397 00151$:
                     1441  5398 	C$wsn.h$252$1$161 ==.
                           5399 ;	C:\Workspace\WSN_MS-4\/wsn.h:252: return mode;
   1508 85 53 82      [24] 5400 	mov	dpl,_mode
   150B 85 54 83      [24] 5401 	mov	dph,(_mode + 1)
                     1447  5402 	C$wsn.h$253$1$161 ==.
                     1447  5403 	XG$getMode$0$0 ==.
   150E 22            [24] 5404 	ret
                           5405 ;------------------------------------------------------------
                           5406 ;Allocation info for local variables in function 'sendTestFrame'
                           5407 ;------------------------------------------------------------
                     1448  5408 	G$sendTestFrame$0$0 ==.
                     1448  5409 	C$wsn.h$255$1$161 ==.
                           5410 ;	C:\Workspace\WSN_MS-4\/wsn.h:255: void sendTestFrame()
                           5411 ;	-----------------------------------------
                           5412 ;	 function sendTestFrame
                           5413 ;	-----------------------------------------
   150F                    5414 _sendTestFrame:
                     1448  5415 	C$wsn.h$257$1$171 ==.
                           5416 ;	C:\Workspace\WSN_MS-4\/wsn.h:257: for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
   150F E4            [12] 5417 	clr	a
   1510 F5 49         [12] 5418 	mov	_slaveAddress,a
   1512 F5 4A         [12] 5419 	mov	(_slaveAddress + 1),a
   1514                    5420 00104$:
                     144D  5421 	C$wsn.h$258$2$172 ==.
                           5422 ;	C:\Workspace\WSN_MS-4\/wsn.h:258: sendFrame(0x01);
   1514 75 82 01      [24] 5423 	mov	dpl,#0x01
   1517 12 0F C4      [24] 5424 	lcall	_sendFrame
                     1453  5425 	C$wsn.h$259$2$172 ==.
                           5426 ;	C:\Workspace\WSN_MS-4\/wsn.h:259: if (waitForFrame(4) == 0x02) {
   151A 90 00 04      [24] 5427 	mov	dptr,#0x0004
   151D 12 11 60      [24] 5428 	lcall	_waitForFrame
   1520 AF 82         [24] 5429 	mov	r7,dpl
   1522 BF 02 12      [24] 5430 	cjne	r7,#0x02,00105$
                     145E  5431 	C$wsn.h$260$3$173 ==.
                           5432 ;	C:\Workspace\WSN_MS-4\/wsn.h:260: slaves[slaveAddress] = 1;
   1525 E5 49         [12] 5433 	mov	a,_slaveAddress
   1527 25 49         [12] 5434 	add	a,_slaveAddress
   1529 FE            [12] 5435 	mov	r6,a
   152A E5 4A         [12] 5436 	mov	a,(_slaveAddress + 1)
   152C 33            [12] 5437 	rlc	a
   152D FF            [12] 5438 	mov	r7,a
   152E EE            [12] 5439 	mov	a,r6
   152F 24 4B         [12] 5440 	add	a,#_slaves
   1531 F8            [12] 5441 	mov	r0,a
   1532 76 01         [12] 5442 	mov	@r0,#0x01
   1534 08            [12] 5443 	inc	r0
   1535 76 00         [12] 5444 	mov	@r0,#0x00
   1537                    5445 00105$:
                     1470  5446 	C$wsn.h$257$1$171 ==.
                           5447 ;	C:\Workspace\WSN_MS-4\/wsn.h:257: for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
   1537 05 49         [12] 5448 	inc	_slaveAddress
   1539 E4            [12] 5449 	clr	a
   153A B5 49 02      [24] 5450 	cjne	a,_slaveAddress,00116$
   153D 05 4A         [12] 5451 	inc	(_slaveAddress + 1)
   153F                    5452 00116$:
   153F C3            [12] 5453 	clr	c
   1540 74 02         [12] 5454 	mov	a,#0x02
   1542 95 49         [12] 5455 	subb	a,_slaveAddress
   1544 E4            [12] 5456 	clr	a
   1545 64 80         [12] 5457 	xrl	a,#0x80
   1547 85 4A F0      [24] 5458 	mov	b,(_slaveAddress + 1)
   154A 63 F0 80      [24] 5459 	xrl	b,#0x80
   154D 95 F0         [12] 5460 	subb	a,b
   154F 50 C3         [24] 5461 	jnc	00104$
                     148A  5462 	C$wsn.h$263$1$171 ==.
                     148A  5463 	XG$sendTestFrame$0$0 ==.
   1551 22            [24] 5464 	ret
                           5465 ;------------------------------------------------------------
                           5466 ;Allocation info for local variables in function 'sendDataWithoutSecurity'
                           5467 ;------------------------------------------------------------
                     148B  5468 	G$sendDataWithoutSecurity$0$0 ==.
                     148B  5469 	C$wsn.h$265$1$171 ==.
                           5470 ;	C:\Workspace\WSN_MS-4\/wsn.h:265: void sendDataWithoutSecurity()
                           5471 ;	-----------------------------------------
                           5472 ;	 function sendDataWithoutSecurity
                           5473 ;	-----------------------------------------
   1552                    5474 _sendDataWithoutSecurity:
                     148B  5475 	C$wsn.h$267$1$174 ==.
                           5476 ;	C:\Workspace\WSN_MS-4\/wsn.h:267: for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
   1552 E4            [12] 5477 	clr	a
   1553 F5 49         [12] 5478 	mov	_slaveAddress,a
   1555 F5 4A         [12] 5479 	mov	(_slaveAddress + 1),a
   1557                    5480 00109$:
                     1490  5481 	C$wsn.h$268$2$175 ==.
                           5482 ;	C:\Workspace\WSN_MS-4\/wsn.h:268: if (slaves[slaveAddress] != 1) {
   1557 E5 49         [12] 5483 	mov	a,_slaveAddress
   1559 25 49         [12] 5484 	add	a,_slaveAddress
   155B FE            [12] 5485 	mov	r6,a
   155C E5 4A         [12] 5486 	mov	a,(_slaveAddress + 1)
   155E 33            [12] 5487 	rlc	a
   155F EE            [12] 5488 	mov	a,r6
   1560 24 4B         [12] 5489 	add	a,#_slaves
   1562 F9            [12] 5490 	mov	r1,a
   1563 87 06         [24] 5491 	mov	ar6,@r1
   1565 09            [12] 5492 	inc	r1
   1566 87 07         [24] 5493 	mov	ar7,@r1
   1568 19            [12] 5494 	dec	r1
   1569 BE 01 05      [24] 5495 	cjne	r6,#0x01,00129$
   156C BF 00 02      [24] 5496 	cjne	r7,#0x00,00129$
   156F 80 03         [24] 5497 	sjmp	00130$
   1571                    5498 00129$:
   1571 02 15 F3      [24] 5499 	ljmp	00107$
   1574                    5500 00130$:
                     14AD  5501 	C$wsn.h$271$2$175 ==.
                           5502 ;	C:\Workspace\WSN_MS-4\/wsn.h:271: frame[0] = NI;
   1574 75 35 B4      [24] 5503 	mov	_frame,#0xB4
                     14B0  5504 	C$wsn.h$272$2$175 ==.
                           5505 ;	C:\Workspace\WSN_MS-4\/wsn.h:272: frame[1] = slaveAddress;
   1577 AF 49         [24] 5506 	mov	r7,_slaveAddress
   1579 8F 36         [24] 5507 	mov	(_frame + 0x0001),r7
                     14B4  5508 	C$wsn.h$273$2$175 ==.
                           5509 ;	C:\Workspace\WSN_MS-4\/wsn.h:273: frame[2] = (acknowledge == 1) ? 0x83 : 0x03;
   157B 74 01         [12] 5510 	mov	a,#0x01
   157D B5 55 06      [24] 5511 	cjne	a,_acknowledge,00131$
   1580 E4            [12] 5512 	clr	a
   1581 B5 56 02      [24] 5513 	cjne	a,(_acknowledge + 1),00131$
   1584 80 02         [24] 5514 	sjmp	00132$
   1586                    5515 00131$:
   1586 80 04         [24] 5516 	sjmp	00112$
   1588                    5517 00132$:
   1588 7F 83         [12] 5518 	mov	r7,#0x83
   158A 80 02         [24] 5519 	sjmp	00113$
   158C                    5520 00112$:
   158C 7F 03         [12] 5521 	mov	r7,#0x03
   158E                    5522 00113$:
   158E 8F 37         [24] 5523 	mov	(_frame + 0x0002),r7
                     14C9  5524 	C$wsn.h$274$2$175 ==.
                           5525 ;	C:\Workspace\WSN_MS-4\/wsn.h:274: frame[3] = 0x00;
   1590 75 38 00      [24] 5526 	mov	(_frame + 0x0003),#0x00
                     14CC  5527 	C$wsn.h$275$2$175 ==.
                           5528 ;	C:\Workspace\WSN_MS-4\/wsn.h:275: frame[4] = 'M';
   1593 75 39 4D      [24] 5529 	mov	(_frame + 0x0004),#0x4D
                     14CF  5530 	C$wsn.h$276$2$175 ==.
                           5531 ;	C:\Workspace\WSN_MS-4\/wsn.h:276: frame[5] = 'a';
   1596 75 3A 61      [24] 5532 	mov	(_frame + 0x0005),#0x61
                     14D2  5533 	C$wsn.h$277$2$175 ==.
                           5534 ;	C:\Workspace\WSN_MS-4\/wsn.h:277: frame[6] = 'r';
   1599 75 3B 72      [24] 5535 	mov	(_frame + 0x0006),#0x72
                     14D5  5536 	C$wsn.h$278$2$175 ==.
                           5537 ;	C:\Workspace\WSN_MS-4\/wsn.h:278: frame[7] = 'c';
   159C 75 3C 63      [24] 5538 	mov	(_frame + 0x0007),#0x63
                     14D8  5539 	C$wsn.h$279$2$175 ==.
                           5540 ;	C:\Workspace\WSN_MS-4\/wsn.h:279: frame[8] = 'i';
   159F 75 3D 69      [24] 5541 	mov	(_frame + 0x0008),#0x69
                     14DB  5542 	C$wsn.h$280$2$175 ==.
                           5543 ;	C:\Workspace\WSN_MS-4\/wsn.h:280: frame[9] = 'n';
   15A2 75 3E 6E      [24] 5544 	mov	(_frame + 0x0009),#0x6E
                     14DE  5545 	C$wsn.h$281$2$175 ==.
                           5546 ;	C:\Workspace\WSN_MS-4\/wsn.h:281: frame[10] = 'S';
   15A5 75 3F 53      [24] 5547 	mov	(_frame + 0x000a),#0x53
                     14E1  5548 	C$wsn.h$282$2$175 ==.
                           5549 ;	C:\Workspace\WSN_MS-4\/wsn.h:282: frame[11] = 'z';
   15A8 75 40 7A      [24] 5550 	mov	(_frame + 0x000b),#0x7A
                     14E4  5551 	C$wsn.h$283$2$175 ==.
                           5552 ;	C:\Workspace\WSN_MS-4\/wsn.h:283: frame[12] = 'y';
   15AB 75 41 79      [24] 5553 	mov	(_frame + 0x000c),#0x79
                     14E7  5554 	C$wsn.h$284$2$175 ==.
                           5555 ;	C:\Workspace\WSN_MS-4\/wsn.h:284: frame[13] = 'm';
   15AE 75 42 6D      [24] 5556 	mov	(_frame + 0x000d),#0x6D
                     14EA  5557 	C$wsn.h$285$2$175 ==.
                           5558 ;	C:\Workspace\WSN_MS-4\/wsn.h:285: frame[14] = 'o';
   15B1 75 43 6F      [24] 5559 	mov	(_frame + 0x000e),#0x6F
                     14ED  5560 	C$wsn.h$286$2$175 ==.
                           5561 ;	C:\Workspace\WSN_MS-4\/wsn.h:286: frame[15] = 'n';
   15B4 75 44 6E      [24] 5562 	mov	(_frame + 0x000f),#0x6E
                     14F0  5563 	C$wsn.h$287$2$175 ==.
                           5564 ;	C:\Workspace\WSN_MS-4\/wsn.h:287: frame[16] = '!';
   15B7 75 45 21      [24] 5565 	mov	(_frame + 0x0010),#0x21
                     14F3  5566 	C$wsn.h$288$2$175 ==.
                           5567 ;	C:\Workspace\WSN_MS-4\/wsn.h:288: frame[17] = '!';
   15BA 75 46 21      [24] 5568 	mov	(_frame + 0x0011),#0x21
                     14F6  5569 	C$wsn.h$289$2$175 ==.
                           5570 ;	C:\Workspace\WSN_MS-4\/wsn.h:289: frame[18] = 0x00;
   15BD 75 47 00      [24] 5571 	mov	(_frame + 0x0012),#0x00
                     14F9  5572 	C$wsn.h$291$2$175 ==.
                           5573 ;	C:\Workspace\WSN_MS-4\/wsn.h:291: sendData(frame);
   15C0 90 00 35      [24] 5574 	mov	dptr,#_frame
   15C3 75 F0 40      [24] 5575 	mov	b,#0x40
   15C6 12 10 43      [24] 5576 	lcall	_sendData
                     1502  5577 	C$wsn.h$292$2$175 ==.
                           5578 ;	C:\Workspace\WSN_MS-4\/wsn.h:292: if (acknowledge == 1) {
   15C9 74 01         [12] 5579 	mov	a,#0x01
   15CB B5 55 06      [24] 5580 	cjne	a,_acknowledge,00133$
   15CE E4            [12] 5581 	clr	a
   15CF B5 56 02      [24] 5582 	cjne	a,(_acknowledge + 1),00133$
   15D2 80 02         [24] 5583 	sjmp	00134$
   15D4                    5584 00133$:
   15D4 80 06         [24] 5585 	sjmp	00104$
   15D6                    5586 00134$:
                     150F  5587 	C$wsn.h$293$3$177 ==.
                           5588 ;	C:\Workspace\WSN_MS-4\/wsn.h:293: waitForFrame(4);
   15D6 90 00 04      [24] 5589 	mov	dptr,#0x0004
   15D9 12 11 60      [24] 5590 	lcall	_waitForFrame
   15DC                    5591 00104$:
                     1515  5592 	C$wsn.h$295$2$175 ==.
                           5593 ;	C:\Workspace\WSN_MS-4\/wsn.h:295: sendFrame(0x04);
   15DC 75 82 04      [24] 5594 	mov	dpl,#0x04
   15DF 12 0F C4      [24] 5595 	lcall	_sendFrame
                     151B  5596 	C$wsn.h$296$2$175 ==.
                           5597 ;	C:\Workspace\WSN_MS-4\/wsn.h:296: if (waitForFrame(4) == 0x05) {
   15E2 90 00 04      [24] 5598 	mov	dptr,#0x0004
   15E5 12 11 60      [24] 5599 	lcall	_waitForFrame
   15E8 AF 82         [24] 5600 	mov	r7,dpl
   15EA BF 05 06      [24] 5601 	cjne	r7,#0x05,00107$
                     1526  5602 	C$wsn.h$297$3$178 ==.
                           5603 ;	C:\Workspace\WSN_MS-4\/wsn.h:297: sendFrame(0x0D);
   15ED 75 82 0D      [24] 5604 	mov	dpl,#0x0D
   15F0 12 0F C4      [24] 5605 	lcall	_sendFrame
   15F3                    5606 00107$:
                     152C  5607 	C$wsn.h$267$1$174 ==.
                           5608 ;	C:\Workspace\WSN_MS-4\/wsn.h:267: for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
   15F3 05 49         [12] 5609 	inc	_slaveAddress
   15F5 E4            [12] 5610 	clr	a
   15F6 B5 49 02      [24] 5611 	cjne	a,_slaveAddress,00137$
   15F9 05 4A         [12] 5612 	inc	(_slaveAddress + 1)
   15FB                    5613 00137$:
   15FB C3            [12] 5614 	clr	c
   15FC 74 02         [12] 5615 	mov	a,#0x02
   15FE 95 49         [12] 5616 	subb	a,_slaveAddress
   1600 E4            [12] 5617 	clr	a
   1601 64 80         [12] 5618 	xrl	a,#0x80
   1603 85 4A F0      [24] 5619 	mov	b,(_slaveAddress + 1)
   1606 63 F0 80      [24] 5620 	xrl	b,#0x80
   1609 95 F0         [12] 5621 	subb	a,b
   160B 40 03         [24] 5622 	jc	00138$
   160D 02 15 57      [24] 5623 	ljmp	00109$
   1610                    5624 00138$:
                     1549  5625 	C$wsn.h$300$1$174 ==.
                     1549  5626 	XG$sendDataWithoutSecurity$0$0 ==.
   1610 22            [24] 5627 	ret
                           5628 ;------------------------------------------------------------
                           5629 ;Allocation info for local variables in function 'sendDataWithCRC8'
                           5630 ;------------------------------------------------------------
                     154A  5631 	G$sendDataWithCRC8$0$0 ==.
                     154A  5632 	C$wsn.h$302$1$174 ==.
                           5633 ;	C:\Workspace\WSN_MS-4\/wsn.h:302: void sendDataWithCRC8()
                           5634 ;	-----------------------------------------
                           5635 ;	 function sendDataWithCRC8
                           5636 ;	-----------------------------------------
   1611                    5637 _sendDataWithCRC8:
                     154A  5638 	C$wsn.h$304$1$179 ==.
                           5639 ;	C:\Workspace\WSN_MS-4\/wsn.h:304: for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
   1611 E4            [12] 5640 	clr	a
   1612 F5 49         [12] 5641 	mov	_slaveAddress,a
   1614 F5 4A         [12] 5642 	mov	(_slaveAddress + 1),a
   1616                    5643 00109$:
                     154F  5644 	C$wsn.h$305$2$180 ==.
                           5645 ;	C:\Workspace\WSN_MS-4\/wsn.h:305: if (slaves[slaveAddress] != 1) {
   1616 E5 49         [12] 5646 	mov	a,_slaveAddress
   1618 25 49         [12] 5647 	add	a,_slaveAddress
   161A FE            [12] 5648 	mov	r6,a
   161B E5 4A         [12] 5649 	mov	a,(_slaveAddress + 1)
   161D 33            [12] 5650 	rlc	a
   161E EE            [12] 5651 	mov	a,r6
   161F 24 4B         [12] 5652 	add	a,#_slaves
   1621 F9            [12] 5653 	mov	r1,a
   1622 87 06         [24] 5654 	mov	ar6,@r1
   1624 09            [12] 5655 	inc	r1
   1625 87 07         [24] 5656 	mov	ar7,@r1
   1627 19            [12] 5657 	dec	r1
   1628 BE 01 05      [24] 5658 	cjne	r6,#0x01,00129$
   162B BF 00 02      [24] 5659 	cjne	r7,#0x00,00129$
   162E 80 03         [24] 5660 	sjmp	00130$
   1630                    5661 00129$:
   1630 02 16 CB      [24] 5662 	ljmp	00107$
   1633                    5663 00130$:
                     156C  5664 	C$wsn.h$308$2$180 ==.
                           5665 ;	C:\Workspace\WSN_MS-4\/wsn.h:308: frame[0] = NI;
   1633 75 35 B4      [24] 5666 	mov	_frame,#0xB4
                     156F  5667 	C$wsn.h$309$2$180 ==.
                           5668 ;	C:\Workspace\WSN_MS-4\/wsn.h:309: frame[1] = slaveAddress;
   1636 AF 49         [24] 5669 	mov	r7,_slaveAddress
   1638 8F 36         [24] 5670 	mov	(_frame + 0x0001),r7
                     1573  5671 	C$wsn.h$310$2$180 ==.
                           5672 ;	C:\Workspace\WSN_MS-4\/wsn.h:310: frame[2] = (acknowledge == 1) ? 0x86 : 0x06;
   163A 74 01         [12] 5673 	mov	a,#0x01
   163C B5 55 06      [24] 5674 	cjne	a,_acknowledge,00131$
   163F E4            [12] 5675 	clr	a
   1640 B5 56 02      [24] 5676 	cjne	a,(_acknowledge + 1),00131$
   1643 80 02         [24] 5677 	sjmp	00132$
   1645                    5678 00131$:
   1645 80 04         [24] 5679 	sjmp	00112$
   1647                    5680 00132$:
   1647 7F 86         [12] 5681 	mov	r7,#0x86
   1649 80 02         [24] 5682 	sjmp	00113$
   164B                    5683 00112$:
   164B 7F 06         [12] 5684 	mov	r7,#0x06
   164D                    5685 00113$:
   164D 8F 37         [24] 5686 	mov	(_frame + 0x0002),r7
                     1588  5687 	C$wsn.h$311$2$180 ==.
                           5688 ;	C:\Workspace\WSN_MS-4\/wsn.h:311: frame[3] = 0x00;
   164F 75 38 00      [24] 5689 	mov	(_frame + 0x0003),#0x00
                     158B  5690 	C$wsn.h$312$2$180 ==.
                           5691 ;	C:\Workspace\WSN_MS-4\/wsn.h:312: frame[4] = 'M';
   1652 75 39 4D      [24] 5692 	mov	(_frame + 0x0004),#0x4D
                     158E  5693 	C$wsn.h$313$2$180 ==.
                           5694 ;	C:\Workspace\WSN_MS-4\/wsn.h:313: frame[5] = 'a';
   1655 75 3A 61      [24] 5695 	mov	(_frame + 0x0005),#0x61
                     1591  5696 	C$wsn.h$314$2$180 ==.
                           5697 ;	C:\Workspace\WSN_MS-4\/wsn.h:314: frame[6] = 'r';
   1658 75 3B 72      [24] 5698 	mov	(_frame + 0x0006),#0x72
                     1594  5699 	C$wsn.h$315$2$180 ==.
                           5700 ;	C:\Workspace\WSN_MS-4\/wsn.h:315: frame[7] = 'c';
   165B 75 3C 63      [24] 5701 	mov	(_frame + 0x0007),#0x63
                     1597  5702 	C$wsn.h$316$2$180 ==.
                           5703 ;	C:\Workspace\WSN_MS-4\/wsn.h:316: frame[8] = 'i';
   165E 75 3D 69      [24] 5704 	mov	(_frame + 0x0008),#0x69
                     159A  5705 	C$wsn.h$317$2$180 ==.
                           5706 ;	C:\Workspace\WSN_MS-4\/wsn.h:317: frame[9] = 'n';
   1661 75 3E 6E      [24] 5707 	mov	(_frame + 0x0009),#0x6E
                     159D  5708 	C$wsn.h$318$2$180 ==.
                           5709 ;	C:\Workspace\WSN_MS-4\/wsn.h:318: frame[10] = 'S';
   1664 75 3F 53      [24] 5710 	mov	(_frame + 0x000a),#0x53
                     15A0  5711 	C$wsn.h$319$2$180 ==.
                           5712 ;	C:\Workspace\WSN_MS-4\/wsn.h:319: frame[11] = 'z';
   1667 75 40 7A      [24] 5713 	mov	(_frame + 0x000b),#0x7A
                     15A3  5714 	C$wsn.h$320$2$180 ==.
                           5715 ;	C:\Workspace\WSN_MS-4\/wsn.h:320: frame[12] = 'y';
   166A 75 41 79      [24] 5716 	mov	(_frame + 0x000c),#0x79
                     15A6  5717 	C$wsn.h$321$2$180 ==.
                           5718 ;	C:\Workspace\WSN_MS-4\/wsn.h:321: frame[13] = 'm';
   166D 75 42 6D      [24] 5719 	mov	(_frame + 0x000d),#0x6D
                     15A9  5720 	C$wsn.h$322$2$180 ==.
                           5721 ;	C:\Workspace\WSN_MS-4\/wsn.h:322: frame[14] = 'o';
   1670 75 43 6F      [24] 5722 	mov	(_frame + 0x000e),#0x6F
                     15AC  5723 	C$wsn.h$323$2$180 ==.
                           5724 ;	C:\Workspace\WSN_MS-4\/wsn.h:323: frame[15] = 'n';
   1673 75 44 6E      [24] 5725 	mov	(_frame + 0x000f),#0x6E
                     15AF  5726 	C$wsn.h$324$2$180 ==.
                           5727 ;	C:\Workspace\WSN_MS-4\/wsn.h:324: frame[16] = '!';
   1676 75 45 21      [24] 5728 	mov	(_frame + 0x0010),#0x21
                     15B2  5729 	C$wsn.h$325$2$180 ==.
                           5730 ;	C:\Workspace\WSN_MS-4\/wsn.h:325: frame[17] = '!';
   1679 75 46 21      [24] 5731 	mov	(_frame + 0x0011),#0x21
                     15B5  5732 	C$wsn.h$326$2$180 ==.
                           5733 ;	C:\Workspace\WSN_MS-4\/wsn.h:326: frame[18] = crc8(frame[4], crc, 14);
   167C AD 39         [24] 5734 	mov	r5,(_frame + 0x0004)
   167E 7E 00         [12] 5735 	mov	r6,#0x00
   1680 7F 00         [12] 5736 	mov	r7,#0x00
   1682 85 48 09      [24] 5737 	mov	_crc8_PARM_2,_crc
   1685 75 0A 0E      [24] 5738 	mov	_crc8_PARM_3,#0x0E
   1688 75 0B 00      [24] 5739 	mov	(_crc8_PARM_3 + 1),#0x00
   168B 8D 82         [24] 5740 	mov	dpl,r5
   168D 8E 83         [24] 5741 	mov	dph,r6
   168F 8F F0         [24] 5742 	mov	b,r7
   1691 12 06 08      [24] 5743 	lcall	_crc8
   1694 E5 82         [12] 5744 	mov	a,dpl
   1696 F5 47         [12] 5745 	mov	(_frame + 0x0012),a
                     15D1  5746 	C$wsn.h$328$2$180 ==.
                           5747 ;	C:\Workspace\WSN_MS-4\/wsn.h:328: sendData(frame);
   1698 90 00 35      [24] 5748 	mov	dptr,#_frame
   169B 75 F0 40      [24] 5749 	mov	b,#0x40
   169E 12 10 43      [24] 5750 	lcall	_sendData
                     15DA  5751 	C$wsn.h$329$2$180 ==.
                           5752 ;	C:\Workspace\WSN_MS-4\/wsn.h:329: if (acknowledge == 1) {
   16A1 74 01         [12] 5753 	mov	a,#0x01
   16A3 B5 55 06      [24] 5754 	cjne	a,_acknowledge,00133$
   16A6 E4            [12] 5755 	clr	a
   16A7 B5 56 02      [24] 5756 	cjne	a,(_acknowledge + 1),00133$
   16AA 80 02         [24] 5757 	sjmp	00134$
   16AC                    5758 00133$:
   16AC 80 06         [24] 5759 	sjmp	00104$
   16AE                    5760 00134$:
                     15E7  5761 	C$wsn.h$330$3$182 ==.
                           5762 ;	C:\Workspace\WSN_MS-4\/wsn.h:330: waitForFrame(4);
   16AE 90 00 04      [24] 5763 	mov	dptr,#0x0004
   16B1 12 11 60      [24] 5764 	lcall	_waitForFrame
   16B4                    5765 00104$:
                     15ED  5766 	C$wsn.h$332$2$180 ==.
                           5767 ;	C:\Workspace\WSN_MS-4\/wsn.h:332: sendFrame(0x07);
   16B4 75 82 07      [24] 5768 	mov	dpl,#0x07
   16B7 12 0F C4      [24] 5769 	lcall	_sendFrame
                     15F3  5770 	C$wsn.h$333$2$180 ==.
                           5771 ;	C:\Workspace\WSN_MS-4\/wsn.h:333: if (waitForFrame(4) == 0x08) {
   16BA 90 00 04      [24] 5772 	mov	dptr,#0x0004
   16BD 12 11 60      [24] 5773 	lcall	_waitForFrame
   16C0 AF 82         [24] 5774 	mov	r7,dpl
   16C2 BF 08 06      [24] 5775 	cjne	r7,#0x08,00107$
                     15FE  5776 	C$wsn.h$334$3$183 ==.
                           5777 ;	C:\Workspace\WSN_MS-4\/wsn.h:334: sendFrame(0x0D);
   16C5 75 82 0D      [24] 5778 	mov	dpl,#0x0D
   16C8 12 0F C4      [24] 5779 	lcall	_sendFrame
   16CB                    5780 00107$:
                     1604  5781 	C$wsn.h$304$1$179 ==.
                           5782 ;	C:\Workspace\WSN_MS-4\/wsn.h:304: for (slaveAddress = 0; slaveAddress <= 2; slaveAddress++) {
   16CB 05 49         [12] 5783 	inc	_slaveAddress
   16CD E4            [12] 5784 	clr	a
   16CE B5 49 02      [24] 5785 	cjne	a,_slaveAddress,00137$
   16D1 05 4A         [12] 5786 	inc	(_slaveAddress + 1)
   16D3                    5787 00137$:
   16D3 C3            [12] 5788 	clr	c
   16D4 74 02         [12] 5789 	mov	a,#0x02
   16D6 95 49         [12] 5790 	subb	a,_slaveAddress
   16D8 E4            [12] 5791 	clr	a
   16D9 64 80         [12] 5792 	xrl	a,#0x80
   16DB 85 4A F0      [24] 5793 	mov	b,(_slaveAddress + 1)
   16DE 63 F0 80      [24] 5794 	xrl	b,#0x80
   16E1 95 F0         [12] 5795 	subb	a,b
   16E3 40 03         [24] 5796 	jc	00138$
   16E5 02 16 16      [24] 5797 	ljmp	00109$
   16E8                    5798 00138$:
                     1621  5799 	C$wsn.h$337$1$179 ==.
                     1621  5800 	XG$sendDataWithCRC8$0$0 ==.
   16E8 22            [24] 5801 	ret
                           5802 ;------------------------------------------------------------
                           5803 ;Allocation info for local variables in function 'main'
                           5804 ;------------------------------------------------------------
                           5805 ;deviceId                  Allocated to registers r6 r7 
                           5806 ;------------------------------------------------------------
                     1622  5807 	G$main$0$0 ==.
                     1622  5808 	C$wsnms4.c$8$1$179 ==.
                           5809 ;	C:\Workspace\WSN_MS-4\wsnms4.c:8: void main (void) 
                           5810 ;	-----------------------------------------
                           5811 ;	 function main
                           5812 ;	-----------------------------------------
   16E9                    5813 _main:
                     1622  5814 	C$wsnms4.c$10$1$179 ==.
                           5815 ;	C:\Workspace\WSN_MS-4\wsnms4.c:10: int deviceId = -1;
   16E9 7E FF         [12] 5816 	mov	r6,#0xFF
   16EB 7F FF         [12] 5817 	mov	r7,#0xFF
                     1626  5818 	C$wsnms4.c$11$1$185 ==.
                           5819 ;	C:\Workspace\WSN_MS-4\wsnms4.c:11: Init_Device();
   16ED C0 07         [24] 5820 	push	ar7
   16EF C0 06         [24] 5821 	push	ar6
   16F1 12 01 20      [24] 5822 	lcall	_Init_Device
   16F4 D0 06         [24] 5823 	pop	ar6
   16F6 D0 07         [24] 5824 	pop	ar7
                     1631  5825 	C$wsnms4.c$12$1$185 ==.
                           5826 ;	C:\Workspace\WSN_MS-4\wsnms4.c:12: P5 = 0x0F;
   16F8 75 85 0F      [24] 5827 	mov	_P5,#0x0F
                     1634  5828 	C$wsnms4.c$14$1$185 ==.
                           5829 ;	C:\Workspace\WSN_MS-4\wsnms4.c:14: do {
   16FB                    5830 00111$:
                     1634  5831 	C$wsnms4.c$15$2$186 ==.
                           5832 ;	C:\Workspace\WSN_MS-4\wsnms4.c:15: Wait_for_button_release();
   16FB C0 07         [24] 5833 	push	ar7
   16FD C0 06         [24] 5834 	push	ar6
   16FF 12 01 8D      [24] 5835 	lcall	_Wait_for_button_release
                     163B  5836 	C$wsnms4.c$16$2$186 ==.
                           5837 ;	C:\Workspace\WSN_MS-4\wsnms4.c:16: Wait_for_button_pressed();
   1702 12 01 BC      [24] 5838 	lcall	_Wait_for_button_pressed
   1705 D0 06         [24] 5839 	pop	ar6
   1707 D0 07         [24] 5840 	pop	ar7
                     1642  5841 	C$wsnms4.c$17$2$186 ==.
                           5842 ;	C:\Workspace\WSN_MS-4\wsnms4.c:17: if (B1_ON()) {
   1709 E5 85         [12] 5843 	mov	a,_P5
   170B 20 E0 1C      [24] 5844 	jb	acc.0,00105$
                     1647  5845 	C$wsnms4.c$18$3$187 ==.
                           5846 ;	C:\Workspace\WSN_MS-4\wsnms4.c:18: deviceId = P4; // Adres slave, odczytany z mikroprzelacznika P4
   170E AE 84         [24] 5847 	mov	r6,_P4
   1710 7F 00         [12] 5848 	mov	r7,#0x00
                     164B  5849 	C$wsnms4.c$19$4$188 ==.
                           5850 ;	C:\Workspace\WSN_MS-4\wsnms4.c:19: LED1_BLINK();
   1712 43 85 10      [24] 5851 	orl	_P5,#0x10
   1715 90 0B B8      [24] 5852 	mov	dptr,#0x0BB8
   1718 C0 07         [24] 5853 	push	ar7
   171A C0 06         [24] 5854 	push	ar6
   171C 12 01 7A      [24] 5855 	lcall	_Delay_x100us
   171F D0 06         [24] 5856 	pop	ar6
   1721 D0 07         [24] 5857 	pop	ar7
   1723 AD 85         [24] 5858 	mov	r5,_P5
   1725 74 EF         [12] 5859 	mov	a,#0xEF
   1727 5D            [12] 5860 	anl	a,r5
   1728 F5 85         [12] 5861 	mov	_P5,a
   172A                    5862 00105$:
                     1663  5863 	C$wsnms4.c$21$2$186 ==.
                           5864 ;	C:\Workspace\WSN_MS-4\wsnms4.c:21: if (B2_ON()) {
   172A E5 85         [12] 5865 	mov	a,_P5
   172C 20 E1 1C      [24] 5866 	jb	acc.1,00112$
                     1668  5867 	C$wsnms4.c$22$3$189 ==.
                           5868 ;	C:\Workspace\WSN_MS-4\wsnms4.c:22: deviceId = 0;
   172F 7E 00         [12] 5869 	mov	r6,#0x00
   1731 7F 00         [12] 5870 	mov	r7,#0x00
                     166C  5871 	C$wsnms4.c$23$4$190 ==.
                           5872 ;	C:\Workspace\WSN_MS-4\wsnms4.c:23: LED2_BLINK();
   1733 43 85 20      [24] 5873 	orl	_P5,#0x20
   1736 90 0B B8      [24] 5874 	mov	dptr,#0x0BB8
   1739 C0 07         [24] 5875 	push	ar7
   173B C0 06         [24] 5876 	push	ar6
   173D 12 01 7A      [24] 5877 	lcall	_Delay_x100us
   1740 D0 06         [24] 5878 	pop	ar6
   1742 D0 07         [24] 5879 	pop	ar7
   1744 AD 85         [24] 5880 	mov	r5,_P5
   1746 74 DF         [12] 5881 	mov	a,#0xDF
   1748 5D            [12] 5882 	anl	a,r5
   1749 F5 85         [12] 5883 	mov	_P5,a
   174B                    5884 00112$:
                     1684  5885 	C$wsnms4.c$25$1$185 ==.
                           5886 ;	C:\Workspace\WSN_MS-4\wsnms4.c:25: } while (deviceId == -1);
   174B BE FF 05      [24] 5887 	cjne	r6,#0xFF,00141$
   174E BF FF 02      [24] 5888 	cjne	r7,#0xFF,00141$
   1751 80 A8         [24] 5889 	sjmp	00111$
   1753                    5890 00141$:
                     168C  5891 	C$wsnms4.c$27$1$185 ==.
                           5892 ;	C:\Workspace\WSN_MS-4\wsnms4.c:27: if (deviceId > 0) {
   1753 C3            [12] 5893 	clr	c
   1754 E4            [12] 5894 	clr	a
   1755 9E            [12] 5895 	subb	a,r6
   1756 E4            [12] 5896 	clr	a
   1757 64 80         [12] 5897 	xrl	a,#0x80
   1759 8F F0         [24] 5898 	mov	b,r7
   175B 63 F0 80      [24] 5899 	xrl	b,#0x80
   175E 95 F0         [12] 5900 	subb	a,b
   1760 50 2A         [24] 5901 	jnc	00117$
                     169B  5902 	C$wsnms4.c$28$2$191 ==.
                           5903 ;	C:\Workspace\WSN_MS-4\wsnms4.c:28: printf("Wybrany wezel w trybie slave, adres: %x\n", deviceId);
   1762 C0 07         [24] 5904 	push	ar7
   1764 C0 06         [24] 5905 	push	ar6
   1766 C0 06         [24] 5906 	push	ar6
   1768 C0 07         [24] 5907 	push	ar7
   176A 74 43         [12] 5908 	mov	a,#__str_39
   176C C0 E0         [24] 5909 	push	acc
   176E 74 1B         [12] 5910 	mov	a,#(__str_39 >> 8)
   1770 C0 E0         [24] 5911 	push	acc
   1772 74 80         [12] 5912 	mov	a,#0x80
   1774 C0 E0         [24] 5913 	push	acc
   1776 12 03 EA      [24] 5914 	lcall	_printf
   1779 E5 81         [12] 5915 	mov	a,sp
   177B 24 FB         [12] 5916 	add	a,#0xfb
   177D F5 81         [12] 5917 	mov	sp,a
   177F D0 06         [24] 5918 	pop	ar6
   1781 D0 07         [24] 5919 	pop	ar7
                     16BC  5920 	C$wsnms4.c$29$2$191 ==.
                           5921 ;	C:\Workspace\WSN_MS-4\wsnms4.c:29: runSlaveMode(deviceId);
   1783 8E 82         [24] 5922 	mov	dpl,r6
   1785 8F 83         [24] 5923 	mov	dph,r7
   1787 12 12 92      [24] 5924 	lcall	_runSlaveMode
   178A 80 1C         [24] 5925 	sjmp	00119$
   178C                    5926 00117$:
                     16C5  5927 	C$wsnms4.c$30$1$185 ==.
                           5928 ;	C:\Workspace\WSN_MS-4\wsnms4.c:30: } else if (deviceId == 0) {
   178C EE            [12] 5929 	mov	a,r6
   178D 4F            [12] 5930 	orl	a,r7
   178E 70 18         [24] 5931 	jnz	00119$
                     16C9  5932 	C$wsnms4.c$31$2$192 ==.
                           5933 ;	C:\Workspace\WSN_MS-4\wsnms4.c:31: printf("Wybrany wezel w trybie master\n");
   1790 74 6C         [12] 5934 	mov	a,#__str_40
   1792 C0 E0         [24] 5935 	push	acc
   1794 74 1B         [12] 5936 	mov	a,#(__str_40 >> 8)
   1796 C0 E0         [24] 5937 	push	acc
   1798 74 80         [12] 5938 	mov	a,#0x80
   179A C0 E0         [24] 5939 	push	acc
   179C 12 03 EA      [24] 5940 	lcall	_printf
   179F 15 81         [12] 5941 	dec	sp
   17A1 15 81         [12] 5942 	dec	sp
   17A3 15 81         [12] 5943 	dec	sp
                     16DE  5944 	C$wsnms4.c$32$2$192 ==.
                           5945 ;	C:\Workspace\WSN_MS-4\wsnms4.c:32: runMasterMode();
   17A5 12 13 CC      [24] 5946 	lcall	_runMasterMode
   17A8                    5947 00119$:
                     16E1  5948 	C$wsnms4.c$34$1$185 ==.
                     16E1  5949 	XG$main$0$0 ==.
   17A8 22            [24] 5950 	ret
                           5951 	.area CSEG    (CODE)
                           5952 	.area CONST   (CODE)
                     0000  5953 Fwsnms4$_str_0$0$0 == .
   18AD                    5954 __str_0:
   18AD 0A                 5955 	.db 0x0A
   18AE 4B 61 6C 69 62 72  5956 	.ascii "Kalibracja..."
        61 63 6A 61 2E 2E
        2E
   18BB 00                 5957 	.db 0x00
                     000F  5958 Fwsnms4$_str_1$0$0 == .
   18BC                    5959 __str_1:
   18BC 42                 5960 	.ascii "B"
   18BD B3                 5961 	.db 0xB3
   18BE B9                 5962 	.db 0xB9
   18BF 64 20 6B 61 6C 69  5963 	.ascii "d kalibracji trybu RX"
        62 72 61 63 6A 69
        20 74 72 79 62 75
        20 52 58
   18D4 0A                 5964 	.db 0x0A
   18D5 00                 5965 	.db 0x00
                     0029  5966 Fwsnms4$_str_2$0$0 == .
   18D6                    5967 __str_2:
   18D6 50 6F 70 72 61 77  5968 	.ascii "Poprawna kalibracja trybu RX"
        6E 61 20 6B 61 6C
        69 62 72 61 63 6A
        61 20 74 72 79 62
        75 20 52 58
   18F2 0A                 5969 	.db 0x0A
   18F3 00                 5970 	.db 0x00
                     0047  5971 Fwsnms4$_str_3$0$0 == .
   18F4                    5972 __str_3:
   18F4 42                 5973 	.ascii "B"
   18F5 B3                 5974 	.db 0xB3
   18F6 B9                 5975 	.db 0xB9
   18F7 64 20 6B 61 6C 69  5976 	.ascii "d kalibracji trybu TX"
        62 72 61 63 6A 69
        20 74 72 79 62 75
        20 54 58
   190C 0A                 5977 	.db 0x0A
   190D 00                 5978 	.db 0x00
                     0061  5979 Fwsnms4$_str_4$0$0 == .
   190E                    5980 __str_4:
   190E 50 6F 70 72 61 77  5981 	.ascii "Poprawna kalibracja trybu TX"
        6E 61 20 6B 61 6C
        69 62 72 61 63 6A
        61 20 74 72 79 62
        75 20 54 58
   192A 0A                 5982 	.db 0x0A
   192B 00                 5983 	.db 0x00
                     007F  5984 Fwsnms4$_str_5$0$0 == .
   192C                    5985 __str_5:
   192C 4D 41 49 4E 3A 20  5986 	.ascii "MAIN: 0x%x "
        30 78 25 78 20
   1937 0A                 5987 	.db 0x0A
   1938 00                 5988 	.db 0x00
                     008C  5989 Fwsnms4$_str_6$0$0 == .
   1939                    5990 __str_6:
   1939 46 52 45 51 5F 32  5991 	.ascii "FREQ_2A: 0x%x "
        41 3A 20 30 78 25
        78 20
   1947 0A                 5992 	.db 0x0A
   1948 00                 5993 	.db 0x00
                     009C  5994 Fwsnms4$_str_7$0$0 == .
   1949                    5995 __str_7:
   1949 46 52 45 51 5F 31  5996 	.ascii "FREQ_1A: 0x%x "
        41 3A 20 30 78 25
        78 20
   1957 0A                 5997 	.db 0x0A
   1958 00                 5998 	.db 0x00
                     00AC  5999 Fwsnms4$_str_8$0$0 == .
   1959                    6000 __str_8:
   1959 46 52 45 51 5F 30  6001 	.ascii "FREQ_0A: 0x%x "
        41 3A 20 30 78 25
        78 20
   1967 0A                 6002 	.db 0x0A
   1968 00                 6003 	.db 0x00
                     00BC  6004 Fwsnms4$_str_9$0$0 == .
   1969                    6005 __str_9:
   1969 46 52 45 51 5F 32  6006 	.ascii "FREQ_2B: 0x%x "
        42 3A 20 30 78 25
        78 20
   1977 0A                 6007 	.db 0x0A
   1978 00                 6008 	.db 0x00
                     00CC  6009 Fwsnms4$_str_10$0$0 == .
   1979                    6010 __str_10:
   1979 46 52 45 51 5F 31  6011 	.ascii "FREQ_1B: 0x%x "
        42 3A 20 30 78 25
        78 20
   1987 0A                 6012 	.db 0x0A
   1988 00                 6013 	.db 0x00
                     00DC  6014 Fwsnms4$_str_11$0$0 == .
   1989                    6015 __str_11:
   1989 46 52 45 51 5F 30  6016 	.ascii "FREQ_0B: 0x%x "
        42 3A 20 30 78 25
        78 20
   1997 0A                 6017 	.db 0x0A
   1998 00                 6018 	.db 0x00
                     00EC  6019 Fwsnms4$_str_12$0$0 == .
   1999                    6020 __str_12:
   1999 46 53 45 50 31 3A  6021 	.ascii "FSEP1: 0x%x "
        20 30 78 25 78 20
   19A5 0A                 6022 	.db 0x0A
   19A6 00                 6023 	.db 0x00
                     00FA  6024 Fwsnms4$_str_13$0$0 == .
   19A7                    6025 __str_13:
   19A7 46 53 45 50 30 3A  6026 	.ascii "FSEP0: 0x%x "
        20 30 78 25 78 20
   19B3 0A                 6027 	.db 0x0A
   19B4 00                 6028 	.db 0x00
                     0108  6029 Fwsnms4$_str_14$0$0 == .
   19B5                    6030 __str_14:
   19B5 43 55 52 52 45 4E  6031 	.ascii "CURRENT: 0x%x "
        54 3A 20 30 78 25
        78 20
   19C3 0A                 6032 	.db 0x0A
   19C4 00                 6033 	.db 0x00
                     0118  6034 Fwsnms4$_str_15$0$0 == .
   19C5                    6035 __str_15:
   19C5 46 52 4F 4E 54 5F  6036 	.ascii "FRONT_END: 0x%x "
        45 4E 44 3A 20 30
        78 25 78 20
   19D5 0A                 6037 	.db 0x0A
   19D6 00                 6038 	.db 0x00
                     012A  6039 Fwsnms4$_str_16$0$0 == .
   19D7                    6040 __str_16:
   19D7 50 41 5F 50 4F 57  6041 	.ascii "PA_POW: 0x%x "
        3A 20 30 78 25 78
        20
   19E4 0A                 6042 	.db 0x0A
   19E5 00                 6043 	.db 0x00
                     0139  6044 Fwsnms4$_str_17$0$0 == .
   19E6                    6045 __str_17:
   19E6 50 4C 4C 3A 20 30  6046 	.ascii "PLL: 0x%x "
        78 25 78 20
   19F0 0A                 6047 	.db 0x0A
   19F1 00                 6048 	.db 0x00
                     0145  6049 Fwsnms4$_str_18$0$0 == .
   19F2                    6050 __str_18:
   19F2 4C 4F 43 4B 3A 20  6051 	.ascii "LOCK: 0x%x "
        30 78 25 78 20
   19FD 0A                 6052 	.db 0x0A
   19FE 00                 6053 	.db 0x00
                     0152  6054 Fwsnms4$_str_19$0$0 == .
   19FF                    6055 __str_19:
   19FF 43 41 4C 3A 20 30  6056 	.ascii "CAL: 0x%x "
        78 25 78 20
   1A09 0A                 6057 	.db 0x0A
   1A0A 00                 6058 	.db 0x00
                     015E  6059 Fwsnms4$_str_20$0$0 == .
   1A0B                    6060 __str_20:
   1A0B 4D 4F 44 45 4D 32  6061 	.ascii "MODEM2: 0x%x "
        3A 20 30 78 25 78
        20
   1A18 0A                 6062 	.db 0x0A
   1A19 00                 6063 	.db 0x00
                     016D  6064 Fwsnms4$_str_21$0$0 == .
   1A1A                    6065 __str_21:
   1A1A 4D 4F 44 45 4D 31  6066 	.ascii "MODEM1: 0x%x "
        3A 20 30 78 25 78
        20
   1A27 0A                 6067 	.db 0x0A
   1A28 00                 6068 	.db 0x00
                     017C  6069 Fwsnms4$_str_22$0$0 == .
   1A29                    6070 __str_22:
   1A29 4D 4F 44 45 4D 30  6071 	.ascii "MODEM0: 0x%x "
        3A 20 30 78 25 78
        20
   1A36 0A                 6072 	.db 0x0A
   1A37 00                 6073 	.db 0x00
                     018B  6074 Fwsnms4$_str_23$0$0 == .
   1A38                    6075 __str_23:
   1A38 4D 41 54 43 48 3A  6076 	.ascii "MATCH: 0x%x "
        20 30 78 25 78 20
   1A44 0A                 6077 	.db 0x0A
   1A45 00                 6078 	.db 0x00
                     0199  6079 Fwsnms4$_str_24$0$0 == .
   1A46                    6080 __str_24:
   1A46 46 53 43 54 52 4C  6081 	.ascii "FSCTRL: 0x%x "
        3A 20 30 78 25 78
        20
   1A53 0A                 6082 	.db 0x0A
   1A54 00                 6083 	.db 0x00
                     01A8  6084 Fwsnms4$_str_25$0$0 == .
   1A55                    6085 __str_25:
   1A55 50 52 45 53 43 41  6086 	.ascii "PRESCALER: 0x%x "
        4C 45 52 3A 20 30
        78 25 78 20
   1A65 0A                 6087 	.db 0x0A
   1A66 00                 6088 	.db 0x00
                     01BA  6089 Fwsnms4$_str_26$0$0 == .
   1A67                    6090 __str_26:
   1A67 54 45 53 54 36 3A  6091 	.ascii "TEST6: 0x%x "
        20 30 78 25 78 20
   1A73 0A                 6092 	.db 0x0A
   1A74 00                 6093 	.db 0x00
                     01C8  6094 Fwsnms4$_str_27$0$0 == .
   1A75                    6095 __str_27:
   1A75 54 45 53 54 35 3A  6096 	.ascii "TEST5: 0x%x "
        20 30 78 25 78 20
   1A81 0A                 6097 	.db 0x0A
   1A82 00                 6098 	.db 0x00
                     01D6  6099 Fwsnms4$_str_28$0$0 == .
   1A83                    6100 __str_28:
   1A83 54 45 53 54 34 3A  6101 	.ascii "TEST4: 0x%x "
        20 30 78 25 78 20
   1A8F 0A                 6102 	.db 0x0A
   1A90 00                 6103 	.db 0x00
                     01E4  6104 Fwsnms4$_str_29$0$0 == .
   1A91                    6105 __str_29:
   1A91 54 45 53 54 33 3A  6106 	.ascii "TEST3: 0x%x "
        20 30 78 25 78 20
   1A9D 0A                 6107 	.db 0x0A
   1A9E 00                 6108 	.db 0x00
                     01F2  6109 Fwsnms4$_str_30$0$0 == .
   1A9F                    6110 __str_30:
   1A9F 54 45 53 54 32 3A  6111 	.ascii "TEST2: 0x%x "
        20 30 78 25 78 20
   1AAB 0A                 6112 	.db 0x0A
   1AAC 00                 6113 	.db 0x00
                     0200  6114 Fwsnms4$_str_31$0$0 == .
   1AAD                    6115 __str_31:
   1AAD 54 45 53 54 31 3A  6116 	.ascii "TEST1: 0x%x "
        20 30 78 25 78 20
   1AB9 0A                 6117 	.db 0x0A
   1ABA 00                 6118 	.db 0x00
                     020E  6119 Fwsnms4$_str_32$0$0 == .
   1ABB                    6120 __str_32:
   1ABB 54 45 53 54 30 3A  6121 	.ascii "TEST0: 0x%x "
        20 30 78 25 78 20
   1AC7 0A                 6122 	.db 0x0A
   1AC8 00                 6123 	.db 0x00
                     021C  6124 Fwsnms4$_str_33$0$0 == .
   1AC9                    6125 __str_33:
   1AC9 0A                 6126 	.db 0x0A
   1ACA 4E 3D 25 78 20 46  6127 	.ascii "N=%x F=%x SI=%x "
        3D 25 78 20 53 49
        3D 25 78 20
   1ADA 00                 6128 	.db 0x00
                     022E  6129 Fwsnms4$_str_34$0$0 == .
   1ADB                    6130 __str_34:
   1ADB 25 78              6131 	.ascii "%x"
   1ADD 00                 6132 	.db 0x00
                     0231  6133 Fwsnms4$_str_35$0$0 == .
   1ADE                    6134 __str_35:
   1ADE 0A                 6135 	.db 0x0A
   1ADF 53 3D 25 78 20 46  6136 	.ascii "S=%x F=%x SI=%x SN=xx "
        3D 25 78 20 53 49
        3D 25 78 20 53 4E
        3D 78 78 20
   1AF5 00                 6137 	.db 0x00
                     0249  6138 Fwsnms4$_str_36$0$0 == .
   1AF6                    6139 __str_36:
   1AF6 57 79 62 72 61 6E  6140 	.ascii "Wybrany tryb %d"
        79 20 74 72 79 62
        20 25 64
   1B05 00                 6141 	.db 0x00
                     0259  6142 Fwsnms4$_str_37$0$0 == .
   1B06                    6143 __str_37:
   1B06 57 79 62 72 61 6E  6144 	.ascii "Wybrano tryb z potwierdzeniem"
        6F 20 74 72 79 62
        20 7A 20 70 6F 74
        77 69 65 72 64 7A
        65 6E 69 65 6D
   1B23 00                 6145 	.db 0x00
                     0277  6146 Fwsnms4$_str_38$0$0 == .
   1B24                    6147 __str_38:
   1B24 57 79 62 72 61 6E  6148 	.ascii "Wybrano tryb bez potwierdzenia"
        6F 20 74 72 79 62
        20 62 65 7A 20 70
        6F 74 77 69 65 72
        64 7A 65 6E 69 61
   1B42 00                 6149 	.db 0x00
                     0296  6150 Fwsnms4$_str_39$0$0 == .
   1B43                    6151 __str_39:
   1B43 57 79 62 72 61 6E  6152 	.ascii "Wybrany wezel w trybie slave, adres: %x"
        79 20 77 65 7A 65
        6C 20 77 20 74 72
        79 62 69 65 20 73
        6C 61 76 65 2C 20
        61 64 72 65 73 3A
        20 25 78
   1B6A 0A                 6153 	.db 0x0A
   1B6B 00                 6154 	.db 0x00
                     02BF  6155 Fwsnms4$_str_40$0$0 == .
   1B6C                    6156 __str_40:
   1B6C 57 79 62 72 61 6E  6157 	.ascii "Wybrany wezel w trybie master"
        79 20 77 65 7A 65
        6C 20 77 20 74 72
        79 62 69 65 20 6D
        61 73 74 65 72
   1B89 0A                 6158 	.db 0x0A
   1B8A 00                 6159 	.db 0x00
                           6160 	.area XINIT   (CODE)
                           6161 	.area CABS    (ABS,CODE)
