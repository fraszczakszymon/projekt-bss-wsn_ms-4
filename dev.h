#ifndef __DEV_H
#define __DEV_H


#include "compiler_defs.h"
#include "C8051F020_defs.h"

#define SYSCLK	22118400

// Diody modu�u Toolstick UNI DC
#define LED1_TOGGLE()  (P5 ^= 0x10)
#define LED2_TOGGLE()  (P5 ^= 0x20)
#define LED3_TOGGLE()  (P5 ^= 0x40)
#define LED4_TOGGLE()  (P5 ^= 0x80)

#define LED1_ON()  (P5 |= 0x10)
#define LED2_ON()  (P5 |= 0x20)
#define LED3_ON()  (P5 |= 0x40)
#define LED4_ON()  (P5 |= 0x80)

#define LED1_OFF()	(P5 &= ~0x10)
#define LED2_OFF()	(P5 &= ~0x20)
#define LED3_OFF()	(P5 &= ~0x40)
#define LED4_OFF()	(P5 &= ~0x80)

#define LEDs_ON()	(P5 |= 0xF0)
#define LEDs_OFF()	(P5 &= ~0xF0)



// Przyciski modu�u Toolstick UNI DC
#define B1_ON()   ((P5 & 0x01) == 0) 
#define B2_ON()   ((P5 & 0x02) == 0) 
#define B3_ON()   ((P5 & 0x04) == 0) 
#define B4_ON()   ((P5 & 0x08) == 0) 

//****************************************************************

// Peripheral specific initialization functions,
// Called from the Init_Device() function
void Reset_Sources_Init()
{
    WDTCN     = 0xDE;
    WDTCN     = 0xAD;
    RSTSRC    = 0x04;
}

void Timer_Init()
{
    CKCON     = 0x38;   // Timer0, Timer1 i Timer2 u�ywaj� SYSCLK
    TCON      = 0x50;   // Zezwolenie (start) dla Timer0 (TR0) i Timer1 (TR1)
    TMOD      = 0x21;   // Timer0 - tryb 16-bitowy, Timer1 - tryb 8-bitowy z autoprze�adowaniem
    TH1       = 0xFA;   // Warto�� prze�adowania Timera1 dla pr�dkosci 115200 bit/s
    T2CON     = 0x04;   // Start Timera2
	RCAP2L = (U8) (65536-(SYSCLK/10000)); 		// Dla przerwa� 100us z T2L
	RCAP2H = (U8) ((65536-(SYSCLK/10000)) >>8);
	TL2 = RCAP2L;   // Warto�� prze�adowania Timera2
	TH2 = RCAP2H;   //   "           "          " 
}

void UART_Init()
{
    SCON0     = 0x50;	// Tryb 8-bitowy; zezwolenie na odbi�r
	TI0		  = 1;		// Wa�ny; Transmit Interrupt Flag;  	
    SCON1     = 0x50;	// Tryb 8-bitowy; zezwolenie na odbi�r
//	TI1		  = 1;		// Wa�ny; Transmit Interrupt Flag;  	
}

void Port_IO_Init()
{
    P0MDOUT   = 0xA1;	// Port P0.0(TX0), P0.5(PCLK), P0.7(PALE) - wyj. push-pull
    P74OUT    = 0x08;   // Port P5.4-P5.7 (LED1-4) - wyj. push-pull  
    XBR0      = 0x04;   // Pod��czenie do port�w UART0
    XBR2      = 0x40;   // Zezwolenie Crossbar 
}

void Oscillator_Init()
{
    int i = 0;
    OSCXCN    = 0x67;
    for (i = 0; i < 3000; i++);  // Wait 1ms for initialization
    while ((OSCXCN & 0x80) == 0);
    OSCICN    = 0x08;
}

void Interrupts_Init()
{
    IE        = 0xA0;   // Zezwolenia na przerwania globalne i od Timera2 
}

// Initialization function for device,
// Call Init_Device() from your main program
void Init_Device(void)
{
    Reset_Sources_Init();
    Timer_Init();
    UART_Init();
    Port_IO_Init();
    Oscillator_Init();
    Interrupts_Init();
}

//********************************************************************

//--------------------------------------------------------------------
// INTERUPT - TIMER0  (time)
//--------------------------------------------------------------------
volatile U8   us100  = 0;		
volatile U16  hus    = 0;	// us x 100
volatile U16  T_1ms  = 0; 	// TIME_MS   = 0;
volatile U16  T_1s   = 0;	// TIME      = 0;

// U�ycie: np. 15s: T_1s=0;  while (T_1s >= 15);
// Procedura obs�ugi przerwania od przepe�nienia Timer2
void Tus100_ISR (void) __interrupt 5    // Przerwania 100us
{
	TF2 = 0;
	++us100;
	++hus;
	if (us100 >= 10) 	
	{	us100 = 0;
		++T_1ms; 
	}
	if (T_1ms >= 1000)          //  Modulo 1000
	{	T_1ms = 0;
		++T_1s; 
	}
}

void Delay_x100us(U16 n100us)	// W 100 x us
{
    hus = 0;
	while (hus <= n100us);
}

#define LED1_BLINK() do{ LED1_ON(); Delay_x100us(3000); LED1_OFF(); }while(0)  
#define LED2_BLINK() do{ LED2_ON(); Delay_x100us(3000); LED2_OFF(); }while(0)  
#define LED3_BLINK() do{ LED3_ON(); Delay_x100us(3000); LED3_OFF(); }while(0)  
#define LED4_BLINK() do{ LED4_ON(); Delay_x100us(3000); LED4_OFF(); }while(0)  


// Oczekiwanie na zwolnienie przycisku B1,B2,B3 lub B4 
void Wait_for_button_release (void) 
{
   while (B1_ON() || B2_ON() || B3_ON() || B4_ON());  
    Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w                                         
   while (B1_ON() || B2_ON() || B3_ON() || B4_ON());  
}

// Oczekiwanie na naci�ni�cie jednego z  przycisku B1,B2,B3 lub B4 
void Wait_for_button_pressed (void) 
{
   while (!(B1_ON() || B2_ON() || B3_ON() || B4_ON()));  
    Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w                                         
   while (!(B1_ON() || B2_ON() || B3_ON() || B4_ON()));  
}


U8 B1_PRESSED(void)
{
    if(B1_ON())
    {
        Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w 
        if(B1_ON())     return 1;   // Naci�ni�ty
    }
    return 0;
}                                         
        
U8 B2_PRESSED(void)
{
    if(B2_ON())
    {
        Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w 
        if(B2_ON())     return 1;   // Naci�ni�ty
    }
    return 0;
}                                         
        
U8 B3_PRESSED(void)
{
    if(B3_ON())
    {
        Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w 
        if(B3_ON())     return 1;   // Naci�ni�ty
    }
    return 0;
}                                         
        
U8 B4_PRESSED(void)
{
    if(B4_ON())
    {
        Delay_x100us(100);    // Czekanie 10ms dla usuni�cia drga� styk�w 
        if(B4_ON())     return 1;   // Naci�ni�ty
    }
    return 0;
}                                         


#endif
