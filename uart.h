#ifndef __UART_H
#define __UART_H

#include "compiler_defs.h"
#include "C8051F020_defs.h"

#include "stdarg.h"

#define UART_BAUDRATE_2400		0x01
#define UART_BAUDRATE_9600		0x02
#define UART_BAUDRATE_56000		0x03
#define UART_BAUDRATE_115200	0x04

//******************************************************************
// Konfiguracja pr�dkosci transmisji
void UART_SetBaudRate(U8 baudRate)
{
	switch(baudRate)
	{
		case UART_BAUDRATE_2400:	//?
			CKCON &= 0xF4;
			TH1    = 0x2B;
			break;

		case UART_BAUDRATE_9600:
			CKCON &= 0xF4;
			CKCON |= 0x10;
			TH1    = 0x96;
			break;

		case UART_BAUDRATE_56000:
			CKCON |= 0x08;
			TH1    = 0x25;
			break;

		case UART_BAUDRATE_115200:
			CKCON |= 0x08;
			TH1    = 0x96;
			break;
	}
}

/**
 *	Puts char into UART transmit buffor. Used by printf function.
 */
// TI0 - flaga ustawiana przez UART, gdy bajt zosta�
// wys�any. Przy w��czonych przerwaniach, flaga
// uruchamia procedur� obs�ugi przerwania. Flaga
// wymaga zerowania "r�cznego"

void putchar(S8 c)
{
    while (!TI0);
    TI0   = 0;
    SBUF0 = c;
}

/**
 *	Gets char from UART receive buffor. Used by printf function.
 *
 *	@return	char
 */
S8 getchar(void)
{
    while (!RI0);
    RI0 = 0;
    return SBUF0;
}

/**
 *	Converts 4-bits LSB value to ASCI code and sends using UART.
 *
 *	@param	unsigned char	value
 */
void printc(U8 val)
{
     
    val += (val>0x09) ? 0x37 : 0x30;
    putchar(val);    
}

/**
 *	Gets two chars from UART receive buffor and returns as one byte.
 *
 * Tzn.dwa znaki ASCII np.: e5 lub E5 zamienia na bajt E5H = 229D	
 */
// 
U8 gethex(void)
{
	S8 result = 0;
	S8 tmp;

    while (!RI0);
    RI0 = 0;
	tmp = SBUF0;

	if (tmp != 0x5C) {

		if (tmp > 0x60 && tmp < 0x67)
			tmp -= 0x20;
	
		result = (tmp<0x40) ? (tmp-0x30)<<4 : (tmp-55)<<4;

		while (!RI0);
	    RI0 = 0;
		tmp = SBUF0;

		if (tmp > 0x60 && tmp < 0x67)
			tmp -= 0x20;

		result |= (tmp<0x40) ? (tmp-0x30) : (tmp-55);
	} else {

		while (!RI0);
	    RI0 = 0;

		if (SBUF0 == 'n')
			return '\n';
	}
	return result;
}




/**
 *	Prints value in BCD format
 *
 *  @param	int		value
 *	@param	unsigned char	precission
 */
void printBCD(S16 value, U8 precission)
{
	char i,tmp[5];
	__bit  s = 0;

	if (value < 0)
		putchar('-');

	for (i = 4; i >= 0; --i) {
		tmp[i] = (value%10);
	    value /= 10;
	}

	for (i = 0; i < 5; ++i) {
	
		if (tmp[i] != 0)
			s = 1;
		
		if ( (5-i) == precission) {
			if (!s)
				putchar('0');
			putchar(',');
		}

		if (s || i == 4)
			printc(tmp[i]);
	}
}


/**
 *	Sends string by UART.
 *
 *	@param	const word8*		string pointer
 */
void printf(const S8* string, ...)
{
    va_list ap ;
    U16  ival;
	U8 precission = 0;
    
    va_start(ap,string);

    for (; *string; ++string) {
        if (*string == '%') {
            ++string;
            switch (*string) {
                case 'x':
                    
                    ival = va_arg(ap, U16);
                    
                    printc(ival>>4);
                    printc(ival&0x000F);

                    break;

                case 'X':
                    
                    ival = va_arg(ap, U16);
                    
                    printc(ival>>12);
                    printc((ival>>8)&0x000F);
                    printc((ival>>4)&0x000F);
                    printc(ival&0x000F);

                    break;

				case '.':

                    ++string;
					precission = (U16) *string-'0';
					++string;

				case 'd':

					ival = va_arg(ap, S16);

					printBCD(ival, precission);

					break;

                default:
                    ival = va_arg(ap, U16);
                    
                    printc(ival>>4);
                    printc(ival&0x000F);
            }
        } else {
			if (*string == '\n')
				putchar(0x0D);

            putchar(*string);
        }
    }
}


#endif
